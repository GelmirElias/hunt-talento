<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ClientFormationController;
use App\Http\Controllers\ClientHiringHistoryController;
use App\Http\Controllers\ClientProfileController;
use App\Http\Controllers\ClientSkillController;
use App\Http\Controllers\ClientSkillInterestController;
use App\Http\Controllers\ClientVacancyController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\CompanyFormationController;
use App\Http\Controllers\CompanyProfileController;
use App\Http\Controllers\CompanyToDoController;
use App\Http\Controllers\CompanyTutorController;
use App\Http\Controllers\CompanyVacancyController;
use App\Http\Controllers\FormationController;
use App\Http\Controllers\HiringHistoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SkillController;
use App\Http\Controllers\TutorCompanyController;
use App\Http\Controllers\TutorController;
use App\Http\Controllers\TutorFormationController;
use App\Http\Controllers\TutorProfileController;
use App\Http\Controllers\TutorToDoController;
use App\Http\Controllers\TutorVacancyController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VacancyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// PUBLIC **************************************************************************************************************************************

//home
Route::get('/', [HomeController::class, 'index']);
Route::post('/search', [HomeController::class, 'search']);

Route::get('/show/formation={formation_id}', [HomeController::class, 'showFormation']);
Route::get('/formation/store/formation={formation_id}', [HomeController::class, 'storeClient'])->middleware(['auth', 'auth.client']);

Route::get('/show/vacancy={vacancy_id}', [HomeController::class, 'showVacancy']);
Route::get('/candidate/store/vacancy={vacancy_id}', [HomeController::class, 'storeCandidate'])->middleware(['auth', 'auth.client']);

//auth
Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/storeCPF', [LoginController::class, 'storeCPF']);
Route::post('/storeCNPJ', [LoginController::class, 'storeCNPJ']);

Route::post('/signIn', [LoginController::class, 'signIn']);
Route::get('/signOut', [LoginController::class, 'signOut'])->middleware('auth');

Route::post('/forgotPassword', [LoginController::class, 'forgotPassword']);
Route::get('/userChangePassword/forgotPassword={forgot_password}', [LoginController::class, 'userChangePassword']);
Route::post('/changePassword', [LoginController::class, 'changePassword']);

Route::get('/userEmailChecker/emailVerified={email_verified}', [LoginController::class, 'userEmailChecker']);

// ADMIN **************************************************************************************************************************************

//admin profile
Route::get('/profile', [ProfileController::class, 'index'])->middleware(['auth', 'auth.admin']);
Route::post('/profile/update/photo', [ProfileController::class, 'updatePhoto'])->middleware(['auth', 'auth.admin']);
Route::post('/profile/update/password', [ProfileController::class, 'updatePassword'])->middleware(['auth', 'auth.admin']);
Route::post('/profile/update/active', [ProfileController::class, 'updateActive'])->middleware(['auth', 'auth.admin']);

//admin
Route::get('/admin', [AdminController::class, 'showAll'])->middleware(['auth', 'auth.admin'])->name('admin/admin');
Route::post('/admin', [AdminController::class, 'store'])->middleware(['auth', 'auth.admin']);
Route::get('/admin/edit={user_id}', [AdminController::class, 'edit'])->middleware(['auth', 'auth.admin']);
Route::post('/admin/update={user_id}', [AdminController::class, 'update'])->middleware(['auth', 'auth.admin']);
Route::get('/admin/active={user_id}', [AdminController::class, 'active'])->middleware(['auth', 'auth.admin']);

//validateRegistration
Route::get('/admin/validateRegistration', [LoginController::class, 'validateRegistration'])->middleware(['auth', 'auth.admin']);
Route::post('/admin/show/validateRegistrationUser={user_id}', [LoginController::class, 'validateRegistrationUser'])->middleware(['auth', 'auth.admin']);
Route::post('/admin/validateRegistrationUpdate={user_id}/status={status}', [LoginController::class, 'validateRegistrationUpdate'])->middleware(['auth', 'auth.admin']);

//client
Route::get('/client', [ClientController::class, 'showAll'])->middleware(['auth', 'auth.admin'])->name('admin/client');
Route::post('/client', [ClientController::class, 'store'])->middleware(['auth', 'auth.admin']);
Route::get('/client/edit={client_id}', [ClientController::class, 'edit'])->middleware(['auth', 'auth.admin']);
Route::post('/client/update={client_id}/user={user_id}', [ClientController::class, 'update'])->middleware(['auth', 'auth.admin']);
Route::get('/client/active={client_id}', [ClientController::class, 'active'])->middleware(['auth', 'auth.admin']);

//company
Route::get('/company', [CompanyController::class, 'showAll'])->middleware(['auth', 'auth.admin'])->name('admin/company');
Route::post('/company', [CompanyController::class, 'store'])->middleware(['auth', 'auth.admin']);
Route::get('/company/edit={company_id}', [CompanyController::class, 'edit'])->middleware(['auth', 'auth.admin']);
Route::post('/company/update={company_id}/user={user_id}', [CompanyController::class, 'update'])->middleware(['auth', 'auth.admin']);
Route::get('/company/active={company_id}', [CompanyController::class, 'active'])->middleware(['auth', 'auth.admin']);

//tutor
Route::get('/tutor', [TutorController::class, 'showAll'])->middleware(['auth', 'auth.admin'])->name('admin/tutor');
Route::post('/tutor', [TutorController::class, 'store'])->middleware(['auth', 'auth.admin']);
Route::get('/tutor/edit={tutor_id}', [TutorController::class, 'edit'])->middleware(['auth', 'auth.admin']);
Route::post('/tutor/update={tutor_id}/user={user_id}', [TutorController::class, 'update'])->middleware(['auth', 'auth.admin']);
Route::get('/tutor/active={tutor_id}', [TutorController::class, 'active'])->middleware(['auth', 'auth.admin']);

//vacancy -> All vacancies
Route::get('/vacancy', [VacancyController::class, 'showAll'])->middleware(['auth', 'auth.admin']);
Route::get('/vacancy/go={vacancy_id}', [VacancyController::class, 'goStatus'])->middleware(['auth', 'auth.admin']);
Route::get('/vacancy/back={vacancy_id}', [VacancyController::class, 'backStatus'])->middleware(['auth', 'auth.admin']);

//vacancy -> All vacancies -> CRUD
Route::post('/vacancy/store', [VacancyController::class, 'storeVacancy'])->middleware(['auth', 'auth.admin']);
Route::get('/vacancy/edit={vacancy_id}', [VacancyController::class, 'editVacancy'])->middleware(['auth', 'auth.admin']);
Route::post('/vacancy/update={vacancy_id}', [VacancyController::class, 'updateVacancy'])->middleware(['auth', 'auth.admin']);
Route::get('/vacancy/destroy={vacancy_id}', [VacancyController::class, 'destroyVacancy'])->middleware(['auth', 'auth.admin']);

//vacancy -> Details vacancies
Route::get('/vacancy/candidates/show={vacancy_id}', [VacancyController::class, 'show'])->middleware(['auth', 'auth.admin']);
Route::get('/vacancy/candidate/show={candidate_id}', [VacancyController::class, 'showCandidate'])->middleware(['auth', 'auth.admin']);

//vacancy -> Details vacancies -> CRUD
Route::post('/vacancy/interview/store={candidate_id}', [VacancyController::class, 'storeInterview'])->middleware(['auth', 'auth.admin']);
Route::post('/vacancy/interview/update={candidate_id}', [VacancyController::class, 'updateInterview'])->middleware(['auth', 'auth.admin']);
Route::post('/vacancy/hiringHistory/create={candidate_id}', [VacancyController::class, 'createHiringHistory'])->middleware(['auth', 'auth.admin']);

//vacancy -> Comments -> CRUD
Route::post('/vacancy/comment/create={candidate_id}', [VacancyController::class, 'createComment'])->middleware(['auth', 'auth.admin']);
Route::get('/vacancy/comment/destroy={candidate_comment_id}', [VacancyController::class, 'destroyComment'])->middleware(['auth', 'auth.admin']);
Route::get('/vacancy/comment/edit={candidate_comment_id}', [VacancyController::class, 'editComment'])->middleware(['auth', 'auth.admin']);
Route::post('/vacancy/comment/update={candidate_comment_id}', [VacancyController::class, 'updateComment'])->middleware(['auth', 'auth.admin']);

//vacancy -> Document -> CRUD
Route::post('/vacancy/document/create={candidate_id}', [VacancyController::class, 'createDocument'])->middleware(['auth', 'auth.admin']);
Route::get('/vacancy/document/destroy={candidate_document_id}', [VacancyController::class, 'destroyDocument'])->middleware(['auth', 'auth.admin']);
Route::get('/vacancy/document/show={candidate_document_id}', [VacancyController::class, 'showDocument'])->middleware(['auth', 'auth.admin']);

//formation
Route::get('/formation', [FormationController::class, 'showAll'])->middleware(['auth', 'auth.admin'])->name('admin/formation');
Route::post('/formation', [FormationController::class, 'store'])->middleware(['auth', 'auth.admin']);
Route::get('/formation/edit={formation_id}', [FormationController::class, 'edit'])->middleware(['auth', 'auth.admin']);
Route::post('/formation/update={formation_id}', [FormationController::class, 'update'])->middleware(['auth', 'auth.admin']);
Route::post('/formation/active={formation_id}', [FormationController::class, 'active'])->middleware(['auth', 'auth.admin']);
Route::get('/formation/destroy={formation_id}', [FormationController::class, 'destroy'])->middleware(['auth', 'auth.admin']);
Route::get('/formation/show={formation_id}', [FormationController::class, 'show'])->middleware(['auth', 'auth.admin']);
Route::post('/formation/showUpdate={formation_id}', [FormationController::class, 'showUpdate'])->middleware(['auth', 'auth.admin']);

//skill
Route::get('/skill', [SkillController::class, 'showAll'])->middleware(['auth', 'auth.admin'])->name('admin/skill');
Route::post('/skill', [SkillController::class, 'store'])->middleware(['auth', 'auth.admin']);
Route::get('/skill/edit={skill_id}', [SkillController::class, 'edit'])->middleware(['auth', 'auth.admin']);
Route::post('/skill/update={skill_id}', [SkillController::class, 'update'])->middleware(['auth', 'auth.admin']);
Route::get('/skill/active={skill_id}', [SkillController::class, 'active'])->middleware(['auth', 'auth.admin']);

//hiringHistory
Route::get('/hiringHistory', [HiringHistoryController::class, 'showAll'])->middleware(['auth', 'auth.admin']);
Route::post('/hiringHistory', [HiringHistoryController::class, 'store'])->middleware(['auth', 'auth.admin']);


// CLIENT **************************************************************************************************************************************

//client formation
Route::get('/client/formation', [ClientFormationController::class, 'showAll'])->middleware(['auth', 'auth.client']);
Route::get('/client/formation/show={formation_id}', [ClientFormationController::class, 'show'])->middleware(['auth', 'auth.client']);
Route::get('/client/formation/finish={formation_id}', [ClientFormationController::class, 'finish'])->middleware(['auth', 'auth.client']);
Route::get('/client/formation/history', [ClientFormationController::class, 'history'])->middleware(['auth', 'auth.client']);
Route::post('/client/formation/history/show={client_formation_id}', [ClientFormationController::class, 'showHistory'])->middleware(['auth', 'auth.client']);

//client skill
Route::get('/client/skill', [ClientSkillController::class, 'index'])->middleware(['auth', 'auth.client']);
Route::post('/client/skill', [ClientSkillController::class, 'store'])->middleware(['auth', 'auth.client']);
Route::get('/client/skill/edit={client_skill_id}', [ClientSkillController::class, 'edit'])->middleware(['auth', 'auth.client']);
Route::get('/client/skill/update={client_skill_id}', [ClientSkillController::class, 'update'])->middleware(['auth', 'auth.client']);
Route::get('/client/skill/destroy={client_skill_id}', [ClientSkillController::class, 'destroy'])->middleware(['auth', 'auth.client']);

//client skill interest
Route::get('/client/skill/interest', [ClientSkillInterestController::class, 'index'])->middleware(['auth', 'auth.client']);
Route::get('/client/skill/interest/destroy={client_skill_id}', [ClientSkillInterestController::class, 'destroy'])->middleware(['auth', 'auth.client']);

//client hiring history
Route::get('/client/history', [ClientHiringHistoryController::class, 'index'])->middleware(['auth', 'auth.client']);

//client profile
Route::get('/client/profile', [ClientProfileController::class, 'index'])->middleware(['auth', 'auth.client']);
Route::post('/client/profile/update/photo', [ClientProfileController::class, 'updatePhoto'])->middleware(['auth', 'auth.client']);
Route::post('/client/profile/update/password', [ClientProfileController::class, 'updatePassword'])->middleware(['auth', 'auth.client']);
Route::post('/client/profile/update/active', [ClientProfileController::class, 'updateActive'])->middleware(['auth', 'auth.client']);
Route::post('/client/profile/update/resume', [ClientProfileController::class, 'updateResume'])->middleware(['auth', 'auth.client']);

//clientVacancy -> All vacancies
Route::get('/client/vacancy', [ClientVacancyController::class, 'showAll'])->middleware(['auth', 'auth.client']);

//clientVacancy -> Details vacancies
Route::get('/client/vacancy/candidates/show={vacancy_id}', [ClientVacancyController::class, 'show'])->middleware(['auth', 'auth.client']);
Route::get('/client/vacancy/candidate/show={candidate_id}', [ClientVacancyController::class, 'showCandidate'])->middleware(['auth', 'auth.client']);

// TUTOR **************************************************************************************************************************************

//tutor profile
Route::get('/tutor/profile', [TutorProfileController::class, 'index'])->middleware(['auth', 'auth.tutor']);
Route::post('/tutor/profile/update/photo', [TutorProfileController::class, 'updatePhoto'])->middleware(['auth', 'auth.tutor']);
Route::post('/tutor/profile/update/password', [TutorProfileController::class, 'updatePassword'])->middleware(['auth', 'auth.tutor']);
Route::post('/tutor/profile/update/active', [TutorProfileController::class, 'updateActive'])->middleware(['auth', 'auth.tutor']);

//tutor company
Route::get('/tutor/company', [TutorCompanyController::class, 'index'])->middleware(['auth', 'auth.tutor']);

//tutor formation
Route::get('/tutor/formation', [TutorFormationController::class, 'showAll'])->middleware(['auth', 'auth.tutor'])->name('tutor/formation');
Route::post('/tutor/formation', [TutorFormationController::class, 'store'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/formation/edit={formation_id}', [TutorFormationController::class, 'edit'])->middleware(['auth', 'auth.tutor']);
Route::post('/tutor/formation/update={formation_id}', [TutorFormationController::class, 'update'])->middleware(['auth', 'auth.tutor']);
Route::post('/tutor/formation/active={formation_id}', [TutorFormationController::class, 'active'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/formation/destroy={formation_id}', [TutorFormationController::class, 'destroy'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/formation/show={formation_id}', [TutorFormationController::class, 'show'])->middleware(['auth', 'auth.tutor']);
Route::post('/tutor/formation/showUpdate={formation_id}', [TutorFormationController::class, 'showUpdate'])->middleware(['auth', 'auth.tutor']);

//tutor -> ToDo
Route::get('/tutor/toDo', [TutorToDoController::class, 'showAll'])->middleware(['auth', 'auth.tutor'])->name('tutor/toDo');
Route::post('/tutor/toDo', [TutorToDoController::class, 'store'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/toDo/edit={to_do_id}', [TutorToDoController::class, 'edit'])->middleware(['auth', 'auth.tutor']);
Route::post('/tutor/toDo/update={to_do_id}', [TutorToDoController::class, 'update'])->middleware(['auth', 'auth.tutor']);
Route::post('/tutor/toDo/active={to_do_id}', [TutorToDoController::class, 'active'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/toDo/destroy={to_do_id}', [TutorToDoController::class, 'destroy'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/toDo/show={to_do_id}', [TutorToDoController::class, 'show'])->middleware(['auth', 'auth.tutor']);

//tutor -> ToDo -> Comment
Route::get('/tutor/toDo={to_do_id}/comment/destroy={comment_id}', [TutorToDoController::class, 'commentDestroy'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/toDo={to_do_id}/comment/edit={comment_id}', [TutorToDoController::class, 'commentEdit'])->middleware(['auth', 'auth.tutor']);
Route::post('/tutor/toDo={to_do_id}/comment/update={comment_id}', [TutorToDoController::class, 'commentUpdate'])->middleware(['auth', 'auth.tutor']);
Route::post('/tutor/toDo={to_do_id}/comment/create', [TutorToDoController::class, 'commentCreate'])->middleware(['auth', 'auth.tutor']);

//tutor -> ToDo -> trello
Route::get('/tutor/toDo/go={to_do_id}', [TutorToDoController::class, 'goStatus'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/toDo/back={to_do_id}', [TutorToDoController::class, 'backStatus'])->middleware(['auth', 'auth.tutor']);

//tutorVacancy -> All vacancies
Route::get('/tutor/vacancy', [TutorVacancyController::class, 'showAll'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/vacancy/go={vacancy_id}', [TutorVacancyController::class, 'goStatus'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/vacancy/back={vacancy_id}', [TutorVacancyController::class, 'backStatus'])->middleware(['auth', 'auth.tutor']);

//tutorVacancy -> All vacancies -> CRUD
Route::post('/tutor/vacancy/store', [TutorVacancyController::class, 'storeVacancy'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/vacancy/edit={vacancy_id}', [TutorVacancyController::class, 'editVacancy'])->middleware(['auth', 'auth.tutor']);
Route::post('/tutor/vacancy/update={vacancy_id}', [TutorVacancyController::class, 'updateVacancy'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/vacancy/destroy={vacancy_id}', [TutorVacancyController::class, 'destroyVacancy'])->middleware(['auth', 'auth.tutor']);

//tutorVacancy -> Details vacancies
Route::get('/tutor/vacancy/candidates/show={vacancy_id}', [TutorVacancyController::class, 'show'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/vacancy/candidate/show={candidate_id}', [TutorVacancyController::class, 'showCandidate'])->middleware(['auth', 'auth.tutor']);

//tutorVacancy -> Details vacancies -> CRUD
Route::post('/tutor/vacancy/interview/store={candidate_id}', [TutorVacancyController::class, 'storeInterview'])->middleware(['auth', 'auth.tutor']);
Route::post('/tutor/vacancy/interview/update={candidate_id}', [TutorVacancyController::class, 'updateInterview'])->middleware(['auth', 'auth.tutor']);
Route::post('/tutor/vacancy/hiringHistory/create={candidate_id}', [TutorVacancyController::class, 'createHiringHistory'])->middleware(['auth', 'auth.tutor']);

//tutorVacancy -> Comments -> CRUD
Route::post('/tutor/vacancy/comment/create={candidate_id}', [TutorVacancyController::class, 'createComment'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/vacancy/comment/destroy={candidate_comment_id}', [TutorVacancyController::class, 'destroyComment'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/vacancy/comment/edit={candidate_comment_id}', [TutorVacancyController::class, 'editComment'])->middleware(['auth', 'auth.tutor']);
Route::post('/tutor/vacancy/comment/update={candidate_comment_id}', [TutorVacancyController::class, 'updateComment'])->middleware(['auth', 'auth.tutor']);

//tutorVacancy -> Document -> CRUD
Route::post('/tutor/vacancy/document/create={candidate_id}', [TutorVacancyController::class, 'createDocument'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/vacancy/document/destroy={candidate_document_id}', [TutorVacancyController::class, 'destroyDocument'])->middleware(['auth', 'auth.tutor']);
Route::get('/tutor/vacancy/document/show={candidate_document_id}', [TutorVacancyController::class, 'showDocument'])->middleware(['auth', 'auth.tutor']);

// COMPANY **************************************************************************************************************************************

//company profile
Route::get('/company/profile', [CompanyProfileController::class, 'index'])->middleware(['auth', 'auth.company']);
Route::post('/company/profile/update/photo', [CompanyProfileController::class, 'updatePhoto'])->middleware(['auth', 'auth.company']);
Route::post('/company/profile/update/password', [CompanyProfileController::class, 'updatePassword'])->middleware(['auth', 'auth.company']);
Route::post('/company/profile/update/active', [CompanyProfileController::class, 'updateActive'])->middleware(['auth', 'auth.company']);

//companyFormation
Route::get('/company/formation', [CompanyFormationController::class, 'showAll'])->middleware(['auth', 'auth.company'])->name('company/formation');
Route::post('/company/formation', [CompanyFormationController::class, 'store'])->middleware(['auth', 'auth.company']);
Route::get('/company/formation/edit={formation_id}', [CompanyFormationController::class, 'edit'])->middleware(['auth', 'auth.company']);
Route::post('/company/formation/update={formation_id}', [CompanyFormationController::class, 'update'])->middleware(['auth', 'auth.company']);
Route::post('/company/formation/active={formation_id}', [CompanyFormationController::class, 'active'])->middleware(['auth', 'auth.company']);
Route::get('/company/formation/destroy={formation_id}', [CompanyFormationController::class, 'destroy'])->middleware(['auth', 'auth.company']);
Route::get('/company/formation/show={formation_id}', [CompanyFormationController::class, 'show'])->middleware(['auth', 'auth.company']);
Route::post('/company/formation/showUpdate={formation_id}', [CompanyFormationController::class, 'showUpdate'])->middleware(['auth', 'auth.company']);

//companyTutor
Route::get('/company/tutor', [CompanyTutorController::class, 'showAll'])->middleware(['auth', 'auth.company'])->name('company/tutor');
Route::post('/company/tutor', [CompanyTutorController::class, 'store'])->middleware(['auth', 'auth.company']);
Route::get('/company/tutor/edit={tutor_id}', [CompanyTutorController::class, 'edit'])->middleware(['auth', 'auth.company']);
Route::post('/company/tutor/update={tutor_id}/user={user_id}', [CompanyTutorController::class, 'update'])->middleware(['auth', 'auth.company']);
Route::get('/company/tutor/active={tutor_id}', [CompanyTutorController::class, 'active'])->middleware(['auth', 'auth.company']);

//companyVacancy -> All vacancies
Route::get('/company/vacancy', [CompanyVacancyController::class, 'showAll'])->middleware(['auth', 'auth.company']);
Route::get('/company/vacancy/go={vacancy_id}', [CompanyVacancyController::class, 'goStatus'])->middleware(['auth', 'auth.company']);
Route::get('/company/vacancy/back={vacancy_id}', [CompanyVacancyController::class, 'backStatus'])->middleware(['auth', 'auth.company']);

//companyVacancy -> All vacancies -> CRUD
Route::post('/company/vacancy/store', [CompanyVacancyController::class, 'storeVacancy'])->middleware(['auth', 'auth.company']);
Route::get('/company/vacancy/edit={vacancy_id}', [CompanyVacancyController::class, 'editVacancy'])->middleware(['auth', 'auth.company']);
Route::post('/company/vacancy/update={vacancy_id}', [CompanyVacancyController::class, 'updateVacancy'])->middleware(['auth', 'auth.company']);
Route::get('/company/vacancy/destroy={vacancy_id}', [CompanyVacancyController::class, 'destroyVacancy'])->middleware(['auth', 'auth.company']);

//companyVacancy -> Details vacancies
Route::get('/company/vacancy/candidates/show={vacancy_id}', [CompanyVacancyController::class, 'show'])->middleware(['auth', 'auth.company']);
Route::get('/company/vacancy/candidate/show={candidate_id}', [CompanyVacancyController::class, 'showCandidate'])->middleware(['auth', 'auth.company']);

//companyVacancy -> Details vacancies -> CRUD
Route::post('/company/vacancy/interview/store={candidate_id}', [CompanyVacancyController::class, 'storeInterview'])->middleware(['auth', 'auth.company']);
Route::post('/company/vacancy/interview/update={candidate_id}', [CompanyVacancyController::class, 'updateInterview'])->middleware(['auth', 'auth.company']);
Route::post('/company/vacancy/hiringHistory/create={candidate_id}', [CompanyVacancyController::class, 'createHiringHistory'])->middleware(['auth', 'auth.company']);

//companyVacancy -> Comments -> CRUD
Route::post('/company/vacancy/comment/create={candidate_id}', [CompanyVacancyController::class, 'createComment'])->middleware(['auth', 'auth.company']);
Route::get('/company/vacancy/comment/destroy={candidate_comment_id}', [CompanyVacancyController::class, 'destroyComment'])->middleware(['auth', 'auth.company']);
Route::get('/company/vacancy/comment/edit={candidate_comment_id}', [CompanyVacancyController::class, 'editComment'])->middleware(['auth', 'auth.company']);
Route::post('/company/vacancy/comment/update={candidate_comment_id}', [CompanyVacancyController::class, 'updateComment'])->middleware(['auth', 'auth.company']);

//companyVacancy -> Document -> CRUD
Route::post('/company/vacancy/document/create={candidate_id}', [CompanyVacancyController::class, 'createDocument'])->middleware(['auth', 'auth.company']);
Route::get('/company/vacancy/document/destroy={candidate_document_id}', [CompanyVacancyController::class, 'destroyDocument'])->middleware(['auth', 'auth.company']);
Route::get('/company/vacancy/document/show={candidate_document_id}', [CompanyVacancyController::class, 'showDocument'])->middleware(['auth', 'auth.company']);

//company -> ToDo
Route::get('/company/toDo', [CompanyToDoController::class, 'showAll'])->middleware(['auth', 'auth.company'])->name('company/toDo');
Route::post('/company/toDo', [CompanyToDoController::class, 'store'])->middleware(['auth', 'auth.company']);
Route::get('/company/toDo/edit={to_do_id}', [CompanyToDoController::class, 'edit'])->middleware(['auth', 'auth.company']);
Route::post('/company/toDo/update={to_do_id}', [CompanyToDoController::class, 'update'])->middleware(['auth', 'auth.company']);
Route::post('/company/toDo/active={to_do_id}', [CompanyToDoController::class, 'active'])->middleware(['auth', 'auth.company']);
Route::get('/company/toDo/destroy={to_do_id}', [CompanyToDoController::class, 'destroy'])->middleware(['auth', 'auth.company']);
Route::get('/company/toDo/show={to_do_id}', [CompanyToDoController::class, 'show'])->middleware(['auth', 'auth.company']);

//company -> ToDo -> Comment
Route::get('/company/toDo={to_do_id}/comment/destroy={comment_id}', [CompanyToDoController::class, 'commentDestroy'])->middleware(['auth', 'auth.company']);
Route::get('/company/toDo={to_do_id}/comment/edit={comment_id}', [CompanyToDoController::class, 'commentEdit'])->middleware(['auth', 'auth.company']);
Route::post('/company/toDo={to_do_id}/comment/update={comment_id}', [CompanyToDoController::class, 'commentUpdate'])->middleware(['auth', 'auth.company']);
Route::post('/company/toDo={to_do_id}/comment/create', [CompanyToDoController::class, 'commentCreate'])->middleware(['auth', 'auth.company']);

//company -> ToDo -> trello
Route::get('/company/toDo/go={to_do_id}', [CompanyToDoController::class, 'goStatus'])->middleware(['auth', 'auth.company']);
Route::get('/company/toDo/back={to_do_id}', [CompanyToDoController::class, 'backStatus'])->middleware(['auth', 'auth.company']);

// **************************************************************************************************************************************
