<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies', function (Blueprint $table) {
            $table->id();
            $table->foreignId('status_vacancy_id')->constrained();
            $table->foreignId('company_id')->constrained();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->integer('quantity')->default(0);
            $table->float('start_salary')->default(0);
            $table->float('end_salary')->default(0);
            $table->string('photo')->nullable();
            $table->boolean('active')->default(1);
            $table->boolean('new_candidate')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies');
    }
}
