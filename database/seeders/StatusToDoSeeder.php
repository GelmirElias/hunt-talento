<?php

namespace Database\Seeders;

use App\Models\StatusToDo;
use Illuminate\Database\Seeder;

class StatusToDoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statusToDo = new StatusToDo();
        $statusToDo->name = 'No prazo';
        $statusToDo->description = 'Atividade no prazo';
        $statusToDo->active = '1';
        $statusToDo->save();

        $statusToDo = new StatusToDo();
        $statusToDo->name = 'Atrasada';
        $statusToDo->description = 'Atividade atrasada';
        $statusToDo->active = '1';
        $statusToDo->save();

        $statusToDo = new StatusToDo();
        $statusToDo->name = 'Concluida';
        $statusToDo->description = 'Atividade concluida';
        $statusToDo->active = '1';
        $statusToDo->save();

        $statusToDo = new StatusToDo();
        $statusToDo->name = 'Excluida';
        $statusToDo->description = 'Atividade excluida';
        $statusToDo->active = '1';
        $statusToDo->save();

        $statusToDo = new StatusToDo();
        $statusToDo->name = 'Alerta';
        $statusToDo->description = 'Atividade em alerta';
        $statusToDo->active = '1';
        $statusToDo->save();
    }
}
