<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionSeeder::class);

        $user = new User();
        $user->name = 'Leonardo Martin Theo Cardoso';
        $user->description = 'Descrição Leonardo';
        $user->email = 'leonardomartin98@admin.com.br';
        $user->password = bcrypt('12345');
        $user->phone = '(86)99877-4164';
        $user->active = '1';
        $user->permission_id = '1';
        $user->email_verified = 1;
        $user->save();

        $this->call(StatusCandidateSeeder::class);
        $this->call(StatusVacancySeeder::class);
        $this->call(StatusToDoSeeder::class);
        $this->call(FormationTypeSeeder::class);

        $this->call(ClientSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(TutorSeeder::class);
        $this->call(PlanSeeder::class);
        $this->call(SkillSeeder::class);
        $this->call(FormationSeeder::class);
        $this->call(LevelSeeder::class);
        $this->call(VacancySeeder::class);

        $this->call(CompanyTutorSeeder::class);
        $this->call(FormationTutorSeeder::class);
        $this->call(CompanyFormationSeeder::class);
        $this->call(ClientFormationSeeder::class);
        $this->call(CompanyToDoSeeder::class);
        $this->call(CandidateSeeder::class);
        $this->call(InterviewSeeder::class);

        $this->call(ClientSkillSeeder::class);
        $this->call(CompanyCommentSeeder::class);
        $this->call(CandidateCommentSeeder::class);
        $this->call(CandidateHistorySeeder::class);
        $this->call(VacancyFormationSeeder::class);
    }
}
