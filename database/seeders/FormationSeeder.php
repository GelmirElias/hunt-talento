<?php

namespace Database\Seeders;

use App\Models\CompanyFormation;
use App\Models\Formation;
use App\Models\FormationSkill;
use Illuminate\Database\Seeder;

class FormationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $formation = new Formation();
        $formation->name = 'Laravel';
        $formation->description = 'Nesso curso de 5 aulas de laravel será apresentado:\n\n1. Como criar uma lista encadeada.\n2. Como criar um ponteiro analogico.\n3. Como rezar o pai nosso.';
        $formation->content = '# 1- Como criar uma lista encadeada.\n\n&nbsp;&nbsp;&nbsp;&nbsp;Como monark disse uma vez:\n* Nunca sabemos o dia de amanha?\n* Sabemos o que sabemos apenas quando sabemos de algo?\n* Sim isso foi uma pergunta afirmativa.\n* [Monark link](https://www.youtube.com/channel/UCil9VmYQwiYGE9B04OEL1Lw)\n	\n&nbsp;&nbsp;&nbsp;&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam congue nulla mi, sit amet ultrices lacus tempus vitae. Sed efficitur nibh dapibus mattis pharetra. Aliquam ullamcorper venenatis malesuada. Integer mollis dui vel blandit consectetur. Aenean sed turpis ut turpis volutpat placerat sed eu velit. Etiam viverra, justo non rutrum faucibus, nibh ligula dignissim risus, et congue elit neque ut urna. Aenean cursus ipsum sem, ut sollicitudin erat tempus non. Sed gravida consectetur augue, in consequat elit ultricies vitae. Donec eleifend vel nunc tempus accumsan. Aliquam vitae elementum magna, ac sodales orci. Curabitur gravida mi et semper efficitur. Sed in justo aliquam, auctor eros eget, scelerisque sapien. Nullam hendrerit pharetra enim, vitae congue massa vestibulum non. Cras auctor dui vitae sem egestas, vitae congue lorem pharetra.\n\n![Imagem da net](https://user-images.githubusercontent.com/11467074/36202809-94933220-1185-11e8-8cb4-964e8fe6e5f5.png)\n\n&nbsp;&nbsp;&nbsp;&nbsp;Phasellus luctus congue lectus ac bibendum. Suspendisse pulvinar sem sed ultricies convallis. Sed tempus sem orci, eu tincidunt risus pulvinar eu. Pellentesque neque libero, aliquam et velit vel, condimentum rutrum tortor. Aenean sed dolor quis sem efficitur eleifend. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus auctor feugiat metus sit amet aliquam. Fusce vitae magna volutpat, scelerisque augue ac, condimentum tellus. Maecenas a ornare ante, non volutpat erat. Quisque eu varius ante.\n\n# 2- Como criar ponteiro analogico.\n\n&nbsp;&nbsp;&nbsp;&nbsp;Cras ut pellentesque purus, eu egestas urna. Integer efficitur laoreet nisl, a tincidunt enim maximus quis. Nunc at lacus sit amet dui malesuada vestibulum et a nibh. Nunc convallis imperdiet quam sit amet ullamcorper. Aenean congue neque libero. Aenean risus metus, volutpat ut bibendum eget, convallis quis turpis. Vestibulum ut mi ut tellus fringilla commodo. Aliquam augue tellus, egestas rhoncus porttitor nec, suscipit non ipsum. Integer ac erat eu quam venenatis congue. Donec in odio feugiat, porttitor velit ac, tempor eros.\n\n![Imagem da net](https://laravelnews.s3.amazonaws.com/images/laravel-tutorial-database-migrate-02.png)\n\n&nbsp;&nbsp;&nbsp;&nbsp;Nam in ligula sed mi hendrerit viverra. Nullam lacinia sollicitudin arcu a maximus. Proin blandit nisl nisi, sit amet tincidunt urna convallis id. Cras auctor iaculis vulputate. Morbi at vehicula tortor. Cras bibendum euismod lacinia. Integer porta cursus eros sed ultrices. Integer suscipit diam ex, eu consequat turpis tincidunt eu. Praesent magna sapien, blandit id dictum ac, rutrum ac ligula. Donec sed erat tellus. Maecenas a tortor tellus. Vestibulum gravida vel eros vitae tristique.\n\n# 3- Como rezar o pai nosso.\n\n&nbsp;&nbsp;&nbsp;&nbsp;Phasellus quis pharetra sapien, quis maximus velit. Nullam quis tortor purus. Praesent nec varius purus, eget elementum felis. Curabitur libero dui, congue non porttitor et, hendrerit quis magna. Nam cursus, quam sed fringilla vulputate, quam justo placerat leo, a pulvinar sem odio ac massa. Nam ut enim suscipit, faucibus purus at, cursus metus. In magna ante, posuere eu mollis ut, volutpat eget metus. Pellentesque et condimentum felis. Pellentesque suscipit rhoncus nulla eget hendrerit. Etiam pellentesque semper leo ac elementum. Praesent at erat id lectus interdum finibus. Aliquam at sem libero. Duis ornare est a lectus condimentum, venenatis accumsan ante laoreet. Nam sed nisi elementum, cursus leo in, dignissim augue. Suspendisse rutrum augue ac est interdum, et feugiat libero consectetur. Suspendisse bibendum magna sed urna sollicitudin ultrices.';
        $formation->start_date = '2022-10-04 16:00:00';
        $formation->end_date = '2022-12-04 16:00:00';
        $formation->active = '1';
        $formation->plan_id = '1';
        $formation->photo = 'laravel.png';
        $formation->formation_types_id = '1';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 5;
        $formationSkill->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 1;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'Java iniciante';
        $formation->description = 'Descrição Java iniciante';
        $formation->start_date = '2022-01-01 18:00:00';
        $formation->end_date = '2022-02-01 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '3';
        $formation->photo = 'java.png';
        $formation->formation_types_id = '2';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 2;
        $formationSkill->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 29;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'PHP introdução';
        $formation->description = 'Descrição PHP introdução';
        $formation->start_date = '2022-01-01 18:00:00';
        $formation->end_date = '2022-02-01 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '3';
        $formation->photo = 'php.png';
        $formation->formation_types_id = '2';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 5;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'Java intermediário';
        $formation->description = 'Descrição Java intermediário';
        $formation->start_date = '2022-01-01 18:00:00';
        $formation->end_date = '2022-02-01 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '3';
        $formation->photo = 'java2.png';
        $formation->formation_types_id = '2';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 2;
        $formationSkill->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 29;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'Dispositivos móveis';
        $formation->description = 'Descrição Dispositivos móveis';
        $formation->start_date = '2022-05-01 10:00:00';
        $formation->end_date = '2022-05-07 10:00:00';
        $formation->active = '1';
        $formation->plan_id = '1';
        $formation->photo = 'flutter.png';
        $formation->formation_types_id = '3';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 41;
        $formationSkill->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 18;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'Python iniciante';
        $formation->description = 'Descrição Python';
        $formation->start_date = '2022-06-05 08:00:00';
        $formation->end_date = '2022-07-10 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '1';
        $formation->photo = 'python.png';
        $formation->formation_types_id = '3';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 3;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'PHP CRUD';
        $formation->description = 'Descrição PHP CRUD';
        $formation->start_date = '2022-01-01 18:00:00';
        $formation->end_date = '2022-02-01 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '3';
        $formation->photo = 'php2.png';
        $formation->formation_types_id = '2';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 5;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'Swift';
        $formation->description = 'Descrição Swift';
        $formation->start_date = '2022-06-05 08:00:00';
        $formation->end_date = '2022-07-10 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '1';
        $formation->photo = 'swift.png';
        $formation->formation_types_id = '3';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 18;
        $formationSkill->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 2;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'Python intermediário';
        $formation->description = 'Descrição intermediário';
        $formation->start_date = '2022-06-05 08:00:00';
        $formation->end_date = '2022-07-10 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '1';
        $formation->photo = 'python2.png';
        $formation->formation_types_id = '3';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 3;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'Arduino';
        $formation->description = 'Descrição Arduino';
        $formation->start_date = '2022-06-05 08:00:00';
        $formation->end_date = '2022-07-10 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '1';
        $formation->photo = 'arduino.png';
        $formation->formation_types_id = '3';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 25;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'JavaScript introdução';
        $formation->description = 'Descrição JavaScript introdução';
        $formation->start_date = '2022-06-05 08:00:00';
        $formation->end_date = '2022-07-10 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '1';
        $formation->photo = 'javascript.png';
        $formation->formation_types_id = '3';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 1;
        $formationSkill->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 16;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'C++ estrutura de um programa';
        $formation->description = 'Descrição C++ estrutura de um programa';
        $formation->start_date = '2022-06-05 08:00:00';
        $formation->end_date = '2022-07-10 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '1';
        $formation->photo = 'c.png';
        $formation->formation_types_id = '3';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 7;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'JavaScript módulos';
        $formation->description = 'Descrição JavaScript módulos';
        $formation->start_date = '2022-06-05 08:00:00';
        $formation->end_date = '2022-07-10 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '1';
        $formation->photo = 'javascript2.png';
        $formation->formation_types_id = '3';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 1;
        $formationSkill->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 16;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'C++ introdução';
        $formation->description = 'Descrição C++ introdução';
        $formation->start_date = '2022-06-05 08:00:00';
        $formation->end_date = '2022-07-10 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '1';
        $formation->photo = 'c2.png';
        $formation->formation_types_id = '3';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 7;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'Dart';
        $formation->description = 'Descrição Dart';
        $formation->start_date = '2022-06-05 08:00:00';
        $formation->end_date = '2022-07-10 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '1';
        $formation->photo = 'dart.png';
        $formation->formation_types_id = '3';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 41;
        $formationSkill->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 2;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'PowerShell';
        $formation->description = 'Descrição PowerShell';
        $formation->start_date = '2022-06-05 08:00:00';
        $formation->end_date = '2022-07-10 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '1';
        $formation->photo = 'powershell.png';
        $formation->formation_types_id = '3';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 29;
        $formationSkill->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 9;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'Rust';
        $formation->description = 'Descrição Rust';
        $formation->start_date = '2022-06-05 08:00:00';
        $formation->end_date = '2022-07-10 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '1';
        $formation->photo = 'rust.png';
        $formation->formation_types_id = '3';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 28;
        $formationSkill->save();

        // --------------------------------------------------------------------

        $formation = new Formation();
        $formation->name = 'Assembly';
        $formation->description = 'Descrição Assembly';
        $formation->start_date = '2022-06-05 08:00:00';
        $formation->end_date = '2022-07-10 18:00:00';
        $formation->active = '1';
        $formation->plan_id = '1';
        $formation->photo = 'assembly.png';
        $formation->formation_types_id = '3';
        $formation->save();

        $formationSkill = new FormationSkill();
        $formationSkill->formation_id = $formation->id;
        $formationSkill->skill_id = 33;
        $formationSkill->save();
    }
}
