<?php

namespace Database\Seeders;

use App\Models\FormationType;
use Illuminate\Database\Seeder;

class FormationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $formationType = new FormationType();
        $formationType->name = 'Capacitação';
        $formationType->description = 'Descrição Capacitação';
        $formationType->active = '1';
        $formationType->save();

        $formationType = new FormationType();
        $formationType->name = 'Formação';
        $formationType->description = 'Descrição Formação';
        $formationType->active = '1';
        $formationType->save();

        $formationType = new FormationType();
        $formationType->name = 'Curso';
        $formationType->description = 'Descrição Curso';
        $formationType->active = '1';
        $formationType->save();
    }
}
