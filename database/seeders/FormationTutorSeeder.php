<?php

namespace Database\Seeders;

use App\Models\FormationTutor;
use Illuminate\Database\Seeder;

class FormationTutorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $formationTutor = new FormationTutor();
        $formationTutor->formation_id = 1;
        $formationTutor->tutor_id = 1;
        $formationTutor->save();

        $formationTutor = new FormationTutor();
        $formationTutor->formation_id = 2;
        $formationTutor->tutor_id = 1;
        $formationTutor->save();

        $formationTutor = new FormationTutor();
        $formationTutor->formation_id = 3;
        $formationTutor->tutor_id = 1;
        $formationTutor->save();
    }
}
