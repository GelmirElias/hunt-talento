<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Juan e Eduardo Informática Ltda';
        $user->description = 'Descrição Informática';
        $user->email = 'treinamento@juaneeduardoinformaticaltda.com.br';
        $user->password = bcrypt('12345');
        $user->phone = '(63)98249-3792';
        $user->permission_id = '4';
        $user->email_verified = 1;

        $user->save();
        $company = new Company();
        $company->user_id = $user->id;
        $company->cnpj = '20.812.992/0001-69';
        $company->save();

        $user = new User();
        $user->name = 'Igor e Márcia Filmagens ME';
        $user->description = 'Descrição Filmagens';
        $user->email = 'igoremarciafilmagensme@empresa.com.br';
        $user->password = bcrypt('12345');
        $user->phone = '(11)98641-3270';
        $user->active = '1';
        $user->permission_id = '4';
        $user->email_verified = 1;
        $user->save();
        $company = new Company();
        $company->user_id = $user->id;
        $company->cnpj = '57.178.568/0001-14';
        $company->save();

        $user = new User();
        $user->name = 'Francisco e Eduarda Telecomunicações Ltda';
        $user->description = 'Descrição Telecomunicações';
        $user->email = 'pesquisa@franciscoeeduarda.com.br';
        $user->password = bcrypt('12345');
        $user->phone = '(14)99988-1759';
        $user->active = '1';
        $user->permission_id = '4';
        $user->email_verified = 1;
        $user->save();
        $company = new Company();
        $company->user_id = $user->id;
        $company->cnpj = '39.918.785/0001-11';
        $company->save();

        $user = new User();
        $user->name = 'Giovanna e Carlos Eduardo Telas Ltda';
        $user->description = 'Descrição Telas';
        $user->email = 'marketing@giovannaecarloseduardotelasltda.com.br';
        $user->password = bcrypt('12345');
        $user->phone = '(11)99178-8995';
        $user->active = '0';
        $user->permission_id = '4';
        $user->email_verified = 1;
        $user->save();
        $company = new Company();
        $company->user_id = $user->id;
        $company->cnpj = '77.272.065/0001-10';
        $company->save();

        $user = new User();
        $user->name = 'Josefa e Renata Fotografias ME';
        $user->description = 'descrição Fathima Knott';
        $user->email = 'rh@josefaerenatafotografiasme.com.br';
        $user->password = bcrypt('12345');
        $user->phone = '(11)99429-0741';
        $user->active = '1';
        $user->permission_id = '4';
        $user->email_verified = 1;
        $user->save();
        $company = new Company();
        $company->user_id = $user->id;
        $company->cnpj = '19.165.515/0001-70';
        $company->save();
    }
}
