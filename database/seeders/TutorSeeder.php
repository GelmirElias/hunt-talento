<?php

namespace Database\Seeders;

use App\Models\Tutor;
use App\Models\User;
use Illuminate\Database\Seeder;

class TutorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Liz Sophia Larissa Ferreira';
        $user->description = 'Descrição Liz';
        $user->email = 'lizsophialarissaferreira@tutor.com.br';
        $user->password = bcrypt('12345');
        $user->phone = '(27)99714-4600';
        $user->active = '1';
        $user->permission_id = '5';
        $user->email_verified = 1;

        $user->save();
        $tutor = new Tutor();
        $tutor->user_id = $user->id;
        $tutor->cpf = '560.967.256-27';
        $tutor->save();

        $user = new User();
        $user->name = 'Antonio Diego da Paz';
        $user->description = 'Descrição Antonio';
        $user->email = 'antoniodiegodapaz@picolotoengenharia.com.br';
        $user->password = bcrypt('12345');
        $user->phone = '(61)98846-7572';
        $user->active = '1';
        $user->permission_id = '5';
        $user->email_verified = 1;
        $user->save();
        $tutor = new Tutor();
        $tutor->user_id = $user->id;
        $tutor->cpf = '540.943.327-07';
        $tutor->save();

        $user = new User();
        $user->name = 'Marcela Olivia da Rosa';
        $user->description = 'Descrição Marcela';
        $user->email = 'marcelaoliviadarosa-76@riscao.com.br';
        $user->password = bcrypt('12345');
        $user->phone = '(62)99762-7385';
        $user->active = '0';
        $user->permission_id = '5';
        $user->email_verified = 1;
        $user->save();
        $tutor = new Tutor();
        $tutor->user_id = $user->id;
        $tutor->cpf = '117.327.847-87';
        $tutor->save();

        $user = new User();
        $user->name = 'Alícia Aline Caroline dos Santos';
        $user->description = 'Descrição Alícia';
        $user->email = 'aliciaalinecarolinedossantos@cognis.com';
        $user->password = bcrypt('12345');
        $user->phone = '(48)98811-5895';
        $user->active = '1';
        $user->permission_id = '5';
        $user->email_verified = 1;
        $user->save();
        $tutor = new Tutor();
        $tutor->user_id = $user->id;
        $tutor->cpf = '565.144.827-67';
        $tutor->save();
    }
}
