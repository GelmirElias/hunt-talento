<?php

namespace Database\Seeders;

use App\Models\CandidateHistory;
use Illuminate\Database\Seeder;

class CandidateHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $candidateHistory = new CandidateHistory();
        $candidateHistory->candidate_id = 1;
        $candidateHistory->description = 'Ingressou na vaga';
        $candidateHistory->save();

        $candidateHistory = new CandidateHistory();
        $candidateHistory->candidate_id = 2;
        $candidateHistory->description = 'Ingressou na vaga';
        $candidateHistory->save();
    }
}
