<?php

namespace Database\Seeders;

use App\Models\Plan;
use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plan = new Plan();
        $plan->name = 'Bronze';
        $plan->description = 'Descrição do plano Bronze';
        $plan->value = '60';
        $plan->validity = '3';
        $plan->active = '1';
        $plan->save();

        $plan = new Plan();
        $plan->name = 'Prata';
        $plan->description = 'Descrição do plano Prata';
        $plan->value = '90';
        $plan->validity = '5';
        $plan->active = '1';
        $plan->save();

        $plan = new Plan();
        $plan->name = 'Ouro';
        $plan->description = 'Descrição do plano Ouro';
        $plan->value = '120';
        $plan->validity = '12';
        $plan->active = '1';
        $plan->save();

        $plan = new Plan();
        $plan->name = 'Platina';
        $plan->description = 'Descrição do plano Platina';
        $plan->value = '200';
        $plan->validity = '24';
        $plan->active = '0';
        $plan->save();
    }
}
