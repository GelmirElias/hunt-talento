<?php

namespace Database\Seeders;

use App\Models\StatusVacancy;
use Illuminate\Database\Seeder;

class StatusVacancySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statusVacancy = new StatusVacancy();
        $statusVacancy->name = 'Aberta';
        $statusVacancy->description = 'Descrição Aberta';
        $statusVacancy->active = '1';
        $statusVacancy->save();

        $statusVacancy = new StatusVacancy();
        $statusVacancy->name = 'Inscrição';
        $statusVacancy->description = 'Descrição Inscrição';
        $statusVacancy->active = '1';
        $statusVacancy->save();

        $statusVacancy = new StatusVacancy();
        $statusVacancy->name = 'Entrevista';
        $statusVacancy->description = 'Descrição Entrevista';
        $statusVacancy->active = '1';
        $statusVacancy->save();

        $statusVacancy = new StatusVacancy();
        $statusVacancy->name = 'Encerrada';
        $statusVacancy->description = 'Descrição Encerrada';
        $statusVacancy->active = '1';
        $statusVacancy->save();

        $statusVacancy = new StatusVacancy();
        $statusVacancy->name = 'Suspensa';
        $statusVacancy->description = 'Descrição Suspensa';
        $statusVacancy->active = '1';
        $statusVacancy->save();
    }
}
