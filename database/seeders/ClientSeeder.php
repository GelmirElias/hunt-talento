<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Mateus Marcos Lucas Castro';
        $user->description = 'Descrição Mateus';
        $user->email = 'mmateusmarcoslucascastro@delfrateinfo.com.br';
        $user->email_verified = 1;
        $user->phone = '(19)99732-4052';
        $user->password = bcrypt('12345');
        $user->permission_id = '2';
        $user->save();
        $client = new Client();
        $client->user_id = $user->id;
        $client->cpf = '738.389.191-06';
        $client->save();

        $user = new User();
        $user->name = 'Simone Liz Esther Nunes';
        $user->description = 'Descrição Simone';
        $user->email = 'simonelizesthernunes@cliente.com.br';
        $user->email_verified = 1;
        $user->phone = '(48)98652-9594';
        $user->password = bcrypt('12345');
        $user->active = '1';
        $user->permission_id = '2';
        $user->save();
        $client = new Client();
        $client->user_id = $user->id;
        $client->cpf = '893.994.702-96';
        $client->save();

        $user = new User();
        $user->name = 'Levi Leonardo Santos';
        $user->description = 'Descrição Levi';
        $user->email = 'levileonardosantos_@umbernardo.com.br';
        $user->email_verified = 1;
        $user->phone = '(21)98752-9356';
        $user->password = bcrypt('12345');
        $user->active = '1';
        $user->permission_id = '2';
        $user->save();
        $client = new Client();
        $client->user_id = $user->id;
        $client->cpf = '112.541.436-70';
        $client->save();

        $user = new User();
        $user->name = 'Yuri Lucca José Silva';
        $user->description = 'Descrição Yuri';
        $user->email = 'yuriluccajosesilva_@fojsc.unesp.br';
        $user->email_verified = 1;
        $user->phone = '(44)99475-6545';
        $user->password = bcrypt('12345');
        $user->active = '0';
        $user->permission_id = '2';
        $user->save();
        $client = new Client();
        $client->user_id = $user->id;
        $client->cpf = '928.129.133-98';
        $client->save();

        $user = new User();
        $user->name = 'Augusto Leandro Moura';
        $user->description = 'Descrição Augusto';
        $user->email = 'augustoleandromoura-97@velc.com.br';
        $user->email_verified = 1;
        $user->phone = '(43)99151-6161';
        $user->password = bcrypt('12345');
        $user->active = '1';
        $user->permission_id = '3';
        $user->save();
        $client = new Client();
        $client->user_id = $user->id;
        $client->cpf = '287.990.833-70';
        $client->save();
    }
}
