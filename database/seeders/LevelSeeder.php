<?php

namespace Database\Seeders;

use App\Models\Level;
use Illuminate\Database\Seeder;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = new Level();
        $level->name = 'Iniciante';
        $level->description = 'Descrição Iniciante';
        $level->necessary_points = '0';
        $level->active = '1';
        $level->save();

        $level = new Level();
        $level->name = 'Dedicado';
        $level->description = 'Descrição Dedicado';
        $level->necessary_points = '10';
        $level->active = '1';
        $level->save();

        $level = new Level();
        $level->name = 'Esforçado';
        $level->description = 'Descrição Esforçado';
        $level->necessary_points = '20';
        $level->active = '0';
        $level->save();

        $level = new Level();
        $level->name = 'Lendario';
        $level->description = 'Descrição Lendario';
        $level->necessary_points = '999';
        $level->active = '1';
        $level->save();
    }
}
