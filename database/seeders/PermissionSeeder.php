<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = new Permission();
        $permission->name = 'Administrador';
        $permission->description = 'permissão do tipo administrador';
        $permission->active = '1';
        $permission->save();

        $permission = new Permission();
        $permission->name = 'Cliente';
        $permission->description = 'permissão do tipo cliente';
        $permission->active = '1';
        $permission->save();

        $permission = new Permission();
        $permission->name = 'Cliente Premium';
        $permission->description = 'permissão do tipo cliente premium';
        $permission->active = '1';
        $permission->save();

        $permission = new Permission();
        $permission->name = 'Empresa';
        $permission->description = 'permissão do tipo empresa';
        $permission->active = '1';
        $permission->save();

        $permission = new Permission();
        $permission->name = 'Tutor';
        $permission->description = 'permissão do tipo tutor';
        $permission->active = '1';
        $permission->save();
    }
}
