<?php

namespace Database\Seeders;

use App\Models\CompanyFormation;
use Illuminate\Database\Seeder;

class CompanyFormationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $CompanyFormation = new CompanyFormation();
        $CompanyFormation->company_id = 2;
        $CompanyFormation->formation_id = 1;
        $CompanyFormation->save();

        $CompanyFormation = new CompanyFormation();
        $CompanyFormation->company_id = 1;
        $CompanyFormation->formation_id = 1;
        $CompanyFormation->save();
    }
}
