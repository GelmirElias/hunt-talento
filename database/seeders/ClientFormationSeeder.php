<?php

namespace Database\Seeders;

use App\Models\ClientFormation;
use Illuminate\Database\Seeder;

class ClientFormationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clientFormation = new ClientFormation();
        $clientFormation->client_id = 2;
        $clientFormation->formation_id = 2;
        $clientFormation->save();

        $clientFormation = new ClientFormation();
        $clientFormation->client_id = 4;
        $clientFormation->formation_id = 1;
        $clientFormation->save();

        $clientFormation = new ClientFormation();
        $clientFormation->client_id = 4;
        $clientFormation->formation_id = 2;
        $clientFormation->save();
    }
}
