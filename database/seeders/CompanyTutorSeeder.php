<?php

namespace Database\Seeders;

use App\Models\CompanyTutor;
use Illuminate\Database\Seeder;

class CompanyTutorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companyTutor = new CompanyTutor();
        $companyTutor->company_id = 2;
        $companyTutor->tutor_id = 1;
        $companyTutor->save();

        $companyTutor = new CompanyTutor();
        $companyTutor->company_id = 2;
        $companyTutor->tutor_id = 2;
        $companyTutor->save();

        $companyTutor = new CompanyTutor();
        $companyTutor->company_id = 4;
        $companyTutor->tutor_id = 1;
        $companyTutor->save();

        $companyTutor = new CompanyTutor();
        $companyTutor->company_id = 2;
        $companyTutor->tutor_id = 3;
        $companyTutor->save();

        $companyTutor = new CompanyTutor();
        $companyTutor->company_id = 2;
        $companyTutor->tutor_id = 4;
        $companyTutor->save();

        $companyTutor = new CompanyTutor();
        $companyTutor->company_id = 3;
        $companyTutor->tutor_id = 1;
        $companyTutor->save();
    }
}
