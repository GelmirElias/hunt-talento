<?php

namespace Database\Seeders;

use App\Http\Controllers\UtilController;
use App\Models\Skill;
use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        $skill = new Skill();
        $skill->name = 'Java';
        $skill->description = 'Descrição Java';
        $skill->active = '1';
        $skill->save();

        $skill = new Skill();
        $skill->name = 'PHP';
        $skill->description = 'Descrição PHP';
        $skill->active = '1';
        $skill->save();

        $skill = new Skill();
        $skill->name = 'Laravel';
        $skill->description = 'Descrição Laravel';
        $skill->active = '0';
        $skill->save();

        $skill = new Skill();
        $skill->name = 'HTML';
        $skill->description = 'Descrição HTML';
        $skill->active = '1';
        $skill->save();

        $skill = new Skill();
        $skill->name = 'CSS';
        $skill->description = 'Descrição CSS';
        $skill->active = '1';
        $skill->save();
        */

        $skillNames = (new UtilController)->languages();

        foreach($skillNames as $skillName){
            $skill = new Skill();
            $skill->name = $skillName;
            $skill->description = 'Linguagem de programação '.$skillName;
            $skill->save();
        }
    }
}
