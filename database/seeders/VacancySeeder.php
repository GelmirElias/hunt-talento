<?php

namespace Database\Seeders;

use App\Models\Vacancy;
use App\Models\VacancySkill;
use Illuminate\Database\Seeder;

class VacancySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vacancy = new Vacancy();
        $vacancy->name = 'Desenvolvedor PHP junio';
        $vacancy->description = 'Descrição desenvolvedor PHP junio';
        $vacancy->start_salary = '500';
        $vacancy->end_salary = '900';
        $vacancy->quantity = '5';
        $vacancy->active = '1';
        $vacancy->status_vacancy_id = '2';
        $vacancy->company_id = '2';
        $vacancy->photo = 'php.png';
        $vacancy->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 5;
        $vacancySkill->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 1;
        $vacancySkill->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 4;
        $vacancySkill->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 41;
        $vacancySkill->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 18;
        $vacancySkill->save();


        $vacancy = new Vacancy();
        $vacancy->name = 'Desenvolvedor Java junio';
        $vacancy->description = 'Descrição desenvolvedor Java junio';
        $vacancy->start_salary = '1100';
        $vacancy->end_salary = '1200';
        $vacancy->quantity = '1';
        $vacancy->active = '1';
        $vacancy->status_vacancy_id = '2';
        $vacancy->company_id = '2';
        $vacancy->photo = 'java.png';
        $vacancy->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 2;
        $vacancySkill->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 29;
        $vacancySkill->save();

        $vacancy = new Vacancy();
        $vacancy->name = 'Desenvolvedor Laravel pleno';
        $vacancy->description = 'Descrição desenvolvedor Laravel pleno';
        $vacancy->start_salary = '4500';
        $vacancy->end_salary = '5000';
        $vacancy->quantity = '1';
        $vacancy->active = '1';
        $vacancy->status_vacancy_id = '4';
        $vacancy->company_id = '3';
        $vacancy->photo = 'laravel.png';
        $vacancy->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 5;
        $vacancySkill->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 1;
        $vacancySkill->save();

        $vacancy = new Vacancy();
        $vacancy->name = 'Backend Mysql';
        $vacancy->description = 'Descrição backend Mysql';
        $vacancy->start_salary = '1500';
        $vacancy->end_salary = '2000';
        $vacancy->quantity = '1';
        $vacancy->active = '1';
        $vacancy->status_vacancy_id = '1';
        $vacancy->company_id = '4';
        $vacancy->photo = 'mysql.png';
        $vacancy->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 5;
        $vacancySkill->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 1;
        $vacancySkill->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 4;
        $vacancySkill->save();

        $vacancy = new Vacancy();
        $vacancy->name = 'Mobile dart';
        $vacancy->description = 'Descrição mobile dart';
        $vacancy->start_salary = '6500';
        $vacancy->end_salary = '8000';
        $vacancy->quantity = '1';
        $vacancy->active = '1';
        $vacancy->status_vacancy_id = '5';
        $vacancy->company_id = '4';
        $vacancy->photo = 'flutter.png';
        $vacancy->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 41;
        $vacancySkill->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 18;
        $vacancySkill->save();

        // ------------------------------------------------------------

        $vacancy = new Vacancy();
        $vacancy->name = 'Senior front-end';
        $vacancy->description = 'Descrição senior front-end';
        $vacancy->start_salary = '8500';
        $vacancy->end_salary = '9000';
        $vacancy->quantity = '3';
        $vacancy->active = '1';
        $vacancy->status_vacancy_id = '2';
        $vacancy->company_id = '4';
        $vacancy->photo = 'senior_front_end.png';
        $vacancy->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 4;
        $vacancySkill->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 1;
        $vacancySkill->save();

        // ------------------------------------------------------------

        $vacancy = new Vacancy();
        $vacancy->name = 'Dev PHP';
        $vacancy->description = 'Dev PHP';
        $vacancy->start_salary = '3500';
        $vacancy->end_salary = '4000';
        $vacancy->quantity = '3';
        $vacancy->active = '1';
        $vacancy->status_vacancy_id = '2';
        $vacancy->company_id = '4';
        $vacancy->photo = 'php2.png';
        $vacancy->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 5;
        $vacancySkill->save();

        // ------------------------------------------------------------

        $vacancy = new Vacancy();
        $vacancy->name = 'Dev para aplicações Windowns';
        $vacancy->description = 'Descrição dev para aplicações Windowns';
        $vacancy->start_salary = '2500';
        $vacancy->end_salary = '3000';
        $vacancy->quantity = '2';
        $vacancy->active = '1';
        $vacancy->status_vacancy_id = '2';
        $vacancy->company_id = '3';
        $vacancy->photo = 'dev_windowns.png';
        $vacancy->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 7;
        $vacancySkill->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 2;
        $vacancySkill->save();

        // ------------------------------------------------------------

        $vacancy = new Vacancy();
        $vacancy->name = 'Automação de dispositivos';
        $vacancy->description = 'Descrição automação de dispositivos';
        $vacancy->start_salary = '1500';
        $vacancy->end_salary = '2000';
        $vacancy->quantity = '1';
        $vacancy->active = '1';
        $vacancy->status_vacancy_id = '2';
        $vacancy->company_id = '2';
        $vacancy->photo = 'arduino.png';
        $vacancy->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 25;
        $vacancySkill->save();

        // ------------------------------------------------------------

        $vacancy = new Vacancy();
        $vacancy->name = 'Dev para C';
        $vacancy->description = 'Descrição dev para C';
        $vacancy->start_salary = '5500';
        $vacancy->end_salary = '6000';
        $vacancy->quantity = '1';
        $vacancy->active = '1';
        $vacancy->status_vacancy_id = '2';
        $vacancy->company_id = '1';
        $vacancy->photo = 'c.png';
        $vacancy->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 7;
        $vacancySkill->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 8;
        $vacancySkill->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 10;
        $vacancySkill->save();

        // ------------------------------------------------------------

        $vacancy = new Vacancy();
        $vacancy->name = 'Dev para Assembly';
        $vacancy->description = 'Descrição dev para Assembly';
        $vacancy->start_salary = '2500';
        $vacancy->end_salary = '3000';
        $vacancy->quantity = '1';
        $vacancy->active = '1';
        $vacancy->status_vacancy_id = '2';
        $vacancy->company_id = '1';
        $vacancy->photo = 'assembly.png';
        $vacancy->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 33;
        $vacancySkill->save();

        // ------------------------------------------------------------

        $vacancy = new Vacancy();
        $vacancy->name = 'Dev para Racket';
        $vacancy->description = 'Descrição dev para Racket';
        $vacancy->start_salary = '2500';
        $vacancy->end_salary = '3000';
        $vacancy->quantity = '1';
        $vacancy->active = '1';
        $vacancy->status_vacancy_id = '2';
        $vacancy->company_id = '1';
        $vacancy->photo = 'racket.png';
        $vacancy->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 48;
        $vacancySkill->save();

        // ------------------------------------------------------------

        $vacancy = new Vacancy();
        $vacancy->name = 'Dev para ASP';
        $vacancy->description = 'Descrição dev para ASP';
        $vacancy->start_salary = '2500';
        $vacancy->end_salary = '3000';
        $vacancy->quantity = '1';
        $vacancy->active = '1';
        $vacancy->status_vacancy_id = '2';
        $vacancy->company_id = '1';
        $vacancy->photo = 'asp.png';
        $vacancy->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 37;
        $vacancySkill->save();

        $vacancySkill = new VacancySkill();
        $vacancySkill->vacancy_id = $vacancy->id;
        $vacancySkill->skill_id = 7;
        $vacancySkill->save();
    }
}
