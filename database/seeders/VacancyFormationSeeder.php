<?php

namespace Database\Seeders;

use App\Models\VacancyFormation;
use Illuminate\Database\Seeder;

class VacancyFormationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vacancyFormation = new VacancyFormation();
        $vacancyFormation->vacancy_id = 1;
        $vacancyFormation->formation_id = 1;
        $vacancyFormation->save();

        $vacancyFormation = new VacancyFormation();
        $vacancyFormation->vacancy_id = 1;
        $vacancyFormation->formation_id = 3;
        $vacancyFormation->required = true;
        $vacancyFormation->save();

        $vacancyFormation = new VacancyFormation();
        $vacancyFormation->vacancy_id = 1;
        $vacancyFormation->formation_id = 2;
        $vacancyFormation->save();
    }
}
