<?php

namespace Database\Seeders;

use App\Models\StatusCandidate;
use Illuminate\Database\Seeder;

class StatusCandidateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statusCandidate = new StatusCandidate();
        $statusCandidate->name = 'Sem entrevista';
        $statusCandidate->description = 'O candidato não possui entrevista marcada.';
        $statusCandidate->save();

        $statusCandidate = new StatusCandidate();
        $statusCandidate->name = 'Entrevista agendada';
        $statusCandidate->description = 'O candidato possui entrevista marcada.';
        $statusCandidate->save();

        $statusCandidate = new StatusCandidate();
        $statusCandidate->name = 'Entrevista adiada';
        $statusCandidate->description = 'Por algum motivo a entrevista do candidato foi adiada.';
        $statusCandidate->save();

        $statusCandidate = new StatusCandidate();
        $statusCandidate->name = 'Entrevista finalizada';
        $statusCandidate->description = 'A entrevista do candidato foi finalizada.';
        $statusCandidate->save();

        $statusCandidate = new StatusCandidate();
        $statusCandidate->name = 'Aprovado';
        $statusCandidate->description = 'O candidato foi aprovado no entrevista.';
        $statusCandidate->save();

        $statusCandidate = new StatusCandidate();
        $statusCandidate->name = 'Rejeitado';
        $statusCandidate->description = 'O candidato foi reprovado na entrevista.';
        $statusCandidate->save();

        $statusCandidate = new StatusCandidate();
        $statusCandidate->name = 'Contratado';
        $statusCandidate->description = 'O candidato foi contratado para vaga.';
        $statusCandidate->save();

    }
}
