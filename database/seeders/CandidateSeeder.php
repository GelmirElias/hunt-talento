<?php

namespace Database\Seeders;

use App\Models\Candidate;
use Illuminate\Database\Seeder;

class CandidateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*$candidate = new Candidate();
        $candidate->client_id = 2;
        $candidate->vacancy_id = 1;
        $candidate->active = true;
        $candidate->save();*/

        $candidate = new Candidate();
        $candidate->client_id = 3;
        $candidate->vacancy_id = 1;
        $candidate->active = true;
        $candidate->save();

        $candidate = new Candidate();
        $candidate->client_id = 4;
        $candidate->vacancy_id = 1;
        $candidate->active = true;
        $candidate->save();
    }
}
