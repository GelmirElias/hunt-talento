<?php

namespace Database\Seeders;

use App\Models\CompanyToDo;
use App\Models\TutorCompanyToDo;
use Illuminate\Database\Seeder;

class CompanyToDoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companyToDo = new CompanyToDo();
        $companyToDo->company_id = 2;
        $companyToDo->user_id = 8;
        $companyToDo->status_to_do_id = 1;
        $companyToDo->name = "Criar vaga php junior";
        $companyToDo->description = "Lembrar de criar uma nova vaga de php junior, o estagiario foi demitido";
        $companyToDo->start_date = "2022-09-16 06:00:00";
        $companyToDo->end_date = "2022-09-17 18:00:00";
        $companyToDo->active = 1;
        $companyToDo->save();

        $tutorComapanyToDo = new TutorCompanyToDo();
        $tutorComapanyToDo->company_to_do_id = $companyToDo->id;
        $tutorComapanyToDo->tutor_id = 1;
        $tutorComapanyToDo->save();

        $companyToDo = new CompanyToDo();
        $companyToDo->company_id = 2;
        $companyToDo->user_id = 8;
        $companyToDo->status_to_do_id = 1;
        $companyToDo->name = "Buscar por candidato";
        $companyToDo->description = "Lembrar buscar candidato";
        $companyToDo->start_date = "2022-09-20 06:00:00";
        $companyToDo->end_date = "2022-09-22 18:00:00";
        $companyToDo->active = 1;
        $companyToDo->save();

        $tutorComapanyToDo = new TutorCompanyToDo();
        $tutorComapanyToDo->company_to_do_id = $companyToDo->id;
        $tutorComapanyToDo->tutor_id = 1;
        $tutorComapanyToDo->save();
    }
}
