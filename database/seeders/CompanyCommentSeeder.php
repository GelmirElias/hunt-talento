<?php

namespace Database\Seeders;

use App\Models\CompanyComment;
use Illuminate\Database\Seeder;

class CompanyCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $comment = new CompanyComment();
        $comment->user_id = 12;
        $comment->company_to_do_id = 1;
        $comment->description = "Que atividade mais chata, não quero fazer não!!!";
        $comment->active = 1;
        $comment->created_at = '2022-09-20 00:40:00';
        $comment->save();

        $comment = new CompanyComment();
        $comment->user_id = 13;
        $comment->company_to_do_id = 1;
        $comment->description = "Deixa pra mim que eu faço!!!";
        $comment->active = 1;
        $comment->created_at = '2022-09-20 00:45:00';
        $comment->save();
    }
}
