<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'necessary_points',
        'photo',
        'active',
    ];

    public function clientLevels(){
        return $this->hasMany(ClientLevel::class);
    }
}
