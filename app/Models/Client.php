<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
        'cpf',
        'user_id',
    ];

    public function user(){
        return $this->morphOne(User::class, 'user');
    }

    public function clientLevel(){
        return $this->hasOne(ClientLevel::class);
    }

    public function clientSkills(){
        return $this->hasMany(ClientSkill::class);
    }

    public function clientPlan(){
        return $this->hasOne(ClientPlan::class);
    }

    public function hiringHistorys(){
        return $this->hasMany(HiringHistory::class);
    }

    public function interview(){
        return $this->hasMany(Interview::class);
    }

    public function clientSelection(){
        return $this->hasMany(ClientSelection::class);
    }

    public function clientFormations(){
        return $this->hasMany(ClientFormation::class);
    }
}
