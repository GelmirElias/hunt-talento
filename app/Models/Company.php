<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'cnpj',
    ];

    public function user(){
        return $this->morphOne(User::class, 'user');
    }

    public function hiringHistorys(){
        return $this->hasMany(HiringHistory::class);
    }

    public function vacancies(){
        return $this->hasMany(Vacancy::class);
    }

    public function companyTutors(){
        return $this->hasMany(CompanyTutor::class);
    }
}
