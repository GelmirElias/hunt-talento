<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'description',
        'email',
        'email_verified',
        'password',
        'forgot_password',
        'photo',
        'resume',
        'phone',
        'active'
    ];

    public function userble(){
        return $this->morphTo();
    }

    public function permission(){
        return $this->hasOne(Permission::class);
    }
}
