<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientLevel extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id',
        'level_id',
        'points',
    ];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function level(){
        return $this->belongsTo(Level::class);
    }
}
