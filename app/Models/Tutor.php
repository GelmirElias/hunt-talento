<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tutor extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'cpf',
    ];

    public function user(){
        return $this->morphOne(User::class, 'user');
    }

    public function formationTutors(){
        return $this->hasMany(FormationTutor::class);
    }

    public function companyTutors(){
        return $this->hasMany(CompanyTutor::class);
    }
}
