<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormationTutor extends Model
{
    use HasFactory;

    protected $fillable = [
        'formation_id',
        'tutor_id',
    ];

    public function tutor(){
        return $this->belongsTo(Tutor::class);
    }

    public function formation(){
        return $this->belongsTo(Formation::class);
    }
}
