<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VacancyFormation extends Model
{
    use HasFactory;

    protected $fillable = [
        'vacancy_id',
        'formation_id',
        'required',
        'active',
    ];
}
