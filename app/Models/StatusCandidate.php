<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusCandidate extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'active',
    ];
}
