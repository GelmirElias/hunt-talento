<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    use HasFactory;

    protected $fillable = [
        'candidate_id',
        'start_date_work',
        'end_date_work',
        'approved',
        'active'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function interviewSelections()
    {
        return $this->hasMany(InterviewSelection::class);
    }
}
