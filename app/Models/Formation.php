<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formation extends Model
{
    use HasFactory;

    protected $fillable = [
        'formation_types_id',
        'plan_id',
        'name',
        'description',
        'start_date',
        'end_date',
        'photo',
        'active',
        'content'
    ];

    public function clientFormations(){
        return $this->hasMany(ClientFormation::class);
    }

    public function formationType(){
        return $this->belongsTo(FormationType::class);
    }

    protected $dates = ['start_date', 'end_date'];
}
