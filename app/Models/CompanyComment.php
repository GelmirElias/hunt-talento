<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyComment extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'company_to_do_id',
        'description',
        'active',
    ];
}
