<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    use HasFactory;

    protected $fillable = [
        'status_vacancy_id',
        'company_id',
        'name',
        'description',
        'quantity',
        'start_salary',
        'end_salary',
        'photo',
        'active',
    ];

    public function selections()
    {
        return $this->hasMany(Selection::class);
    }

    public function statusVacancy()
    {
        return $this->belongsTo(StatusVacancy::class);
    }

    public function companies()
    {
        return $this->belongsTo(Company::class);
    }
}
