<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HiringHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'client_id',
        'start_date',
        'end_date',
        'role',
        'description',
        'active',
    ];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function company(){
        return $this->belongsTo(Company::class);
    }
}
