<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyTutor extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'tutor_id',
    ];

    public function tutor(){
        return $this->belongsTo(Tutor::class);
    }

    public function company(){
        return $this->belongsTo(Company::class);
    }
}
