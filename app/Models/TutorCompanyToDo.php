<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TutorCompanyToDo extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_to_do_id',
        'tutor_id',
    ];
}
