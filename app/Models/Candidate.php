<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id',
        'vacancy_id',
        'start_date',
        'end_date',
        'description',
        'approved',
        'active',
    ];
}
