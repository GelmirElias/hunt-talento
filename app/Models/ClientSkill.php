<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientSkill extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id',
        'skill_id',
        'rate',
    ];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function skill(){
        return $this->belongsTo(Skill::class);
    }
}
