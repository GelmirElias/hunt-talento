<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyToDo extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'company_id',
        'status_to_do_id',
        'name',
        'description',
        'active',
    ];

    protected $dates = ['start_date', 'end_date'];
}
