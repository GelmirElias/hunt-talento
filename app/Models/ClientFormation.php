<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientFormation extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id',
        'formation_id',
        'finish',
        'finish_at'
    ];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function formation(){
        return $this->belongsTo(Formation::class);
    }

    protected $dates = ['finish_at'];
}
