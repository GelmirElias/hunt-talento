<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientPlan extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id',
        'plan_id',
        'start_date',
        'end_date',
        'reason_cancellation',
    ];

    public function plan(){
        return $this->belongsTo(Plan::class);
    }

    public function client(){
        return $this->belongsTo(Client::class);
    }
}
