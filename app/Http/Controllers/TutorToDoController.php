<?php

namespace App\Http\Controllers;

use App\Models\CompanyComment;
use App\Models\CompanyToDo;
use App\Models\StatusToDo;
use App\Models\Tutor;
use App\Models\TutorCompanyToDo;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TutorToDoController extends Controller
{
    private function toDos()
    {
        $companies = new CompanyController;
        $companies = $companies->getAllCompaniesOfTutorAuth();

        $toDos = array();
        foreach ($companies as $company) {
            $companyToDos = CompanyToDo::where('company_id', $company->id)->get();
            foreach ($companyToDos as $companyToDo) {
                $toDos[] = $companyToDo;
            }
        }

        return $toDos;
    }

    private function tutorsToDo($to_do_id)
    {
        //encontrando todos os tutores que o toDo editado possui
        $tutorCompanyToDos = TutorCompanyToDo::where('company_to_do_id', $to_do_id)->get();
        $tutorsIdEdit = array();
        foreach ($tutorCompanyToDos as $tutorCompanyToDo) {
            $tutor = Tutor::where('id', $tutorCompanyToDo->tutor_id)->first();
            $tutorsIdEdit[] = $tutor->id;
        }
        return $tutorsIdEdit;
    }

    public function showAll($alertMessage = null)
    {
        return view(
            'tutor.toDo.showAll',
            [
                'alertMessage' => $alertMessage,
                'statusToDos' => StatusToDo::where('active', true)->get(),
                'tutors' => (new TutorController)->getAllTutorsOfTutorAuth(),
                'companies' => (new CompanyController)->getAllCompaniesOfTutorAuth()
            ]
        );
    }

    public function goStatus(int $to_do_id)
    {
        $toDo = CompanyToDo::where('id', $to_do_id)->first();
        $statusToDoMax = StatusToDo::where('active', true)->max('id');

        if ($toDo->status_to_do_id == $statusToDoMax) {
            return $this->showAll('statusEditFail');
        }

        $toDo->status_to_do_id = $toDo->status_to_do_id + 1;
        $toDo->save();

        return redirect()->back()->with('success', 'Status editado com sucesso');
    }

    public function backStatus(int $to_do_id)
    {
        $toDo = CompanyToDo::where('id', $to_do_id)->first();
        $statusToDoMin = StatusToDo::where('active', true)->min('id');

        if ($toDo->status_to_do_id == $statusToDoMin) {
            return $this->showAll('statusEditFail');
        }

        $toDo->status_to_do_id = $toDo->status_to_do_id - 1;
        $toDo->save();

        return redirect()->back()->with('success', 'Status editado com sucesso');
    }

    public function edit(int $to_do_id)
    {
        return view(
            'tutor.toDo.showAll',
            [
                'toDoEdit' => CompanyToDo::where('id', $to_do_id)->first(),
                'tutorsIdEdit' => $this->tutorsToDo($to_do_id),
                'statusToDos' => StatusToDo::where('active', true)->get(),
                'tutors' => (new TutorController)->getAllTutorsOfTutorAuth(),
                'companies' => (new CompanyController)->getAllCompaniesOfTutorAuth()
            ]
        );
    }

    public function update(Request $request, int $to_do_id)
    {
        $toDoEdit = CompanyToDo::where('id', $to_do_id)->first();
        $toDoEdit->name = $request->nameEdit;
        $toDoEdit->description = $request->descriptionEdit;
        $toDoEdit->status_to_do_id = $request->statusIdEdit;
        $toDoEdit->start_date = $request->startDateEdit;
        $toDoEdit->end_date = $request->endDateEdit;
        $toDoEdit->active = $request->activeEdit;
        $toDoEdit->save();

        //deletando todos os relacionamentos antigos de tutores da formação
        DB::table('tutor_company_to_dos')->where('company_to_do_id', $to_do_id)->delete();

        //salvando os novos relacionamento de tutores da formação
        $tutors = $request->tutorIdEdit;
        if (!empty($tutors)) {
            foreach ($tutors as $tutor) {
                $userCompanyToDo = new TutorCompanyToDo();
                $userCompanyToDo->company_to_do_id = $to_do_id;
                $userCompanyToDo->tutor_id = $tutor;
                $userCompanyToDo->save();
            }
        }

        return redirect()->route('tutor/toDo')->with('success', 'Editado com sucesso');
    }

    public function store(Request $request)
    {
        $toDo = new CompanyToDo();
        $toDo->user_id = Auth::user()->id;
        $toDo->company_id = $request->companyId;
        $toDo->status_to_do_id = $request->statusId;
        $toDo->name = $request->name;
        $toDo->description = $request->description;
        $toDo->start_date = $request->startDate;
        $toDo->end_date = $request->endDate;
        $toDo->end_date = $request->endDate;
        $toDo->active = $request->active;
        $toDo->save();

        //salvando os novos relacionamento de tutores da formação
        $tutors = $request->tutorId;
        if (!empty($tutors)) {
            foreach ($tutors as $tutor) {
                $userCompanyToDo = new TutorCompanyToDo();
                $userCompanyToDo->company_to_do_id = $toDo->id;
                $userCompanyToDo->tutor_id = $tutor;
                $userCompanyToDo->save();
            }
        }

        return redirect()->route('tutor/toDo')->with('success', 'Cadastrado com sucesso');
    }

    public function destroy(int $to_do_id)
    {
        TutorCompanyToDo::where('company_to_do_id', $to_do_id)->delete();
        CompanyComment::where('company_to_do_id', $to_do_id)->delete();
        CompanyToDo::where('id', $to_do_id)->delete();

        return redirect()->route('tutor/toDo')->with('success', 'Removido com sucesso');
    }

    //-----------------------------------------------------------------------------------------------

    public function show(int $to_do_id, $alertMessage = null)
    {
        return view(
            'tutor.toDo.showAll',
            [
                'alertMessage' => $alertMessage,
                'toDoDetail' => CompanyToDo::where('id', $to_do_id)->first(),
                'statusDetail' => StatusToDo::where('id', CompanyToDo::where('id', $to_do_id)->first()->status_to_do_id)->first(),
                'tutorsIdEdit' => $this->tutorsToDo($to_do_id),
                'comments' => CompanyComment::where('company_to_do_id', $to_do_id)->get(),
                'statusToDos' => StatusToDo::where('active', true)->get(),
                'tutors' => (new TutorController)->getAllTutorsOfTutorAuth(),
                'companies' => (new CompanyController)->getAllCompaniesOfTutorAuth()
            ]
        );
    }

    //Comentarios
    public function commentDestroy(int $to_do_id, int $comment_id)
    {
        CompanyComment::where('id', $comment_id)->delete();

        return $this->show($to_do_id, 'removeSucess');
    }

    public function commentCreate(Request $request, int $to_do_id)
    {
        $companyComment = new CompanyComment;
        $companyComment->user_id = Auth::user()->id;
        $companyComment->company_to_do_id = $to_do_id;
        $companyComment->description = $request->description;
        $companyComment->active = 1;
        $companyComment->save();

        return redirect()->back()->with('success', 'Cadastrado com sucesso');
    }

    public function commentEdit(int $to_do_id, int $comment_id)
    {
        return view(
            'tutor.toDo.showAll',
            [
                'commentEdit' => CompanyComment::where('id', $comment_id)->first(),
                'toDoCommentEdit' => CompanyToDo::where('id', $to_do_id)->first(),
                'statusDetail' => StatusToDo::where('id', CompanyToDo::where('id', $to_do_id)->first()->status_to_do_id)->first(),
                'tutorsIdEdit' => $this->tutorsToDo($to_do_id),
                'comments' => CompanyComment::where('company_to_do_id', $to_do_id)->get(),
                'statusToDos' => StatusToDo::where('active', true)->get(),
                'tutors' => (new TutorController)->getAllTutorsOfTutorAuth(),
                'companies' => (new CompanyController)->getAllCompaniesOfTutorAuth()
            ]
        );
    }

    public function commentUpdate(Request $request, int $to_do_id, int $comment_id)
    {
        $commentEdit = CompanyComment::where('id', $comment_id)->first();
        $commentEdit->description = $request->descriptionEdit;
        $commentEdit->save();

        return $this->show($to_do_id, 'editSucess');
    }
}
