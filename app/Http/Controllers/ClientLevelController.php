<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\ClientLevel;
use App\Models\Level;
use App\Models\User;
use Illuminate\Http\Request;

class ClientLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clientLevel.index' , []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::all();
        $users = User::all();
        $levels = Level::all();
        return view('clientLevel.create', ['levels' => $levels, 'clients' => $clients, 'users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clientLevel = new ClientLevel();
        $clientLevel->client_id = $request->client_id;
        $clientLevel->level_id = $request->level_id;
        $clientLevel->points = $request->points;
        $clientLevel->save();
        return redirect("clientLevel");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ClientLevel  $clientLevel
     * @return \Illuminate\Http\Response
     */
    public function show(ClientLevel $clientLevel)
    {
        //
    }

    public function showAll()
    {
        $clientLevels = ClientLevel::all();
        return view('clientLevel.showAll' , ["clientLevels" => $clientLevels]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ClientLevel  $clientLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientLevel $clientLevel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ClientLevel  $clientLevel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientLevel $clientLevel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ClientLevel  $clientLevel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientLevel $clientLevel)
    {
        //
    }
}
