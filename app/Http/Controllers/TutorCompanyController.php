<?php

namespace App\Http\Controllers;

use App\Models\Tutor;
use Illuminate\Http\Request;

class TutorCompanyController extends Controller
{
    public function index()
    {
        $company = new CompanyController;
        return view('tutor.company.index', ["companies" => $company->getAllCompaniesOfTutorAuth()]);
    }
}
