<?php

namespace App\Http\Controllers;

use App\Models\ClientSkill;
use App\Models\Skill;
use Illuminate\Http\Request;

class ClientSkillInterestController extends Controller
{
    public function index()
    {
        $skillsCreate = Skill::where('active', true)->get();
        $clientSkills = ClientSkill::where('client_id', session('client')->id)->where('client_assigned', false)->get();

        return view('client.skillInterest.index', ['clientSkills' => $clientSkills, 'skillsCreate' => $skillsCreate]);
    }

    public function destroy(int $client_skill_id)
    {
        ClientSkill::where('id', $client_skill_id)->delete();

        return redirect()->back()->with('success', 'Interesse removida com sucesso');
    }
}
