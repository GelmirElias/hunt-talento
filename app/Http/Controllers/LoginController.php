<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Company;
use App\Models\Tutor;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function signIn(Request $request)
    {
        $home = new HomeController();

        $user = User::where('email', $request->emailSignIn)->first();
        if ($user != null) {
            if (Hash::check($request->passwordSignIn, $user->password)) {
                if ($user->active == 1) {
                    if ($user->email_verified == 1) {
                        $client = Client::where('user_id', $user->id)->first();
                        if ($client != null) {
                            Auth::login($user);
                            session(['client' => $client]);
                            return $home->index('clientLogado');
                            /*
                            $teste = session('client');
                            echo $teste->cpf;

                            $teste2 = Auth::user();
                            echo $teste2->name;
                            */
                        } else {
                            $tutor = Tutor::where('user_id', $user->id)->first();
                            if ($tutor != null) {
                                Auth::login($user);
                                session(['tutor' => $tutor]);
                                return $home->index('tutorLogado');
                                //echo 'é um tutor ';
                            } else {
                                $company = Company::where('user_id', $user->id)->first();
                                if ($company != null) {
                                    Auth::login($user);
                                    session(['company' => $company]);
                                    return $home->index('companyLogado');
                                    //echo 'é uma empresa ';
                                } else {
                                    Auth::login($user);
                                    session(['admin' => 'admin']);
                                    return $home->index('adminLogado');
                                    //echo 'é um admin';
                                }
                            }
                        }
                    } else {
                        if ($user->email_verified != 1) {
                            return view('auth.login', ['alertMessage' => 'emailInvalidated']);
                            //echo 'email não verificado';
                        } else {
                            return view('auth.login', ['alertMessage' => 'awaitApproved']);
                            //echo 'aguardando validação da conta';
                        }
                    }
                } else {
                    if ($user->email_verified != 1) {
                        return view('auth.login', ['alertMessage' => 'emailInvalidated']);
                        //echo 'email não verificado';
                    } else {
                        return view('auth.login', ['alertMessage' => 'awaitApproved']);
                        //echo 'aguardando validação da conta';
                    }
                }
            } else {
                return view('auth.login', ['alertMessage' => 'passwordError']);
                //echo 'senha incorreta';
            }
        } else {
            return view('auth.login', ['alertMessage' => 'emailNotFound']);
            //echo 'email não encontrado';
        }
    }

    public function signOut()
    {
        Auth::logout();
        session()->forget(['admin', 'company', 'tutor', 'client']);


        return (new HomeController())->index('signOut');
    }

    public function validateRegistration()
    {
        return view('admin.admin.validateRegistration', ['users' => User::where('active', null)->get()]);
    }

    public function validateRegistrationUser(int $user_id)
    {
        $client = Client::where('user_id', $user_id)->first();
        if (!empty($client))
            return view('admin.admin.validateRegistration', ['users' => User::where('active', null)->get(), 'userEdit' => User::where('id', $user_id)->first(), 'clientEdit' => $client]);
        else
            return view('admin.admin.validateRegistration', ['users' => User::where('active', null)->get(), 'userEdit' => User::where('id', $user_id)->first(), 'companyEdit' => Company::where('user_id', $user_id)->first()]);
    }

    public function validateRegistrationUpdate(int $user_id, int $active)
    {
        $user = User::where('id', $user_id)->first();
        if ($active == 1) {
            $user->active = true;
            $user->save();
            return view('admin.admin.validateRegistration', ['users' => User::where('active', null)->get(), 'alertMessage' => 'validedSucess']);
        } else {
            $user->active = false;
            $user->save();
            return view('admin.admin.validateRegistration', ['users' => User::where('active', null)->get(), 'alertMessage' => 'validedFail']);
        }
    }


    public function storeCPF(Request $request)
    {
        //validando telefone
        if (strlen($request->phone) != 14)
            return view('auth.login', ['alertMessage' => 'failPhone']);

        //valida se email informado já existe
        $user = User::where('email', $request->email)->first();
        if ($user != null) {
            return view('auth.login', ['alertMessage' => 'emailNotUnique']);
        }

        //verifica se vem cpf
        if (strlen($request->cpf) == 14) {
            //validando se o cpf já existe
            $cpf = Client::where('cpf', $request->cpfcnpj)->first();
            if ($cpf != null) {
                return view('auth.login', ['alertMessage' => 'cpfNotUnique']);
            } else {
                $cpf = Tutor::where('cpf', $request->cpf)->first();
                if ($cpf != null) {
                    return view('auth.login', ['alertMessage' => 'cpfNotUnique']);
                }
            }
        } else {
            return view('auth.login', ['alertMessage' => 'failCPF']);
        }

        $user = new User();
        $user->name = $request->name;
        $user->description = 'Descrição ' . $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->permission_id = 2;
        //$user->email_verified = Str::random(8);
        $user->email_verified = 1;
        $user->active = 1;
        $user->save();

        $client = new Client();
        $client->user_id = $user->id;
        $client->cpf = $request->cpf;
        $client->save();

        //enviar email de recuperação de senha passando o usuario como parametro
        /*Mail::send('emailChecker.emailCheckerEmail', ['user' => $user], function ($m) use ($user) {
            $m->from('gelmirbaumgratz@gmail.com');
            $m->to($user->email);
            $m->subject('Confirmação de email');
        });*/

        return view('auth.login', ['alertMessage' => /*'checkEmail'*/ 'createSucess']);
    }

    public function storeCNPJ(Request $request)
    {

        //validando telefone
        if (strlen($request->phone) != 14)
            return view('auth.login', ['alertMessage' => 'failPhone']);

        //valida se email informado já existe
        $user = User::where('email', $request->email)->first();
        if ($user != null) {
            return view('auth.login', ['alertMessage' => 'emailNotUnique']);
        }

        //verifica se vem cnpj
        if (strlen($request->cnpj) == 18) {
            //validando se o cnpj já existe
            $cnpj = Company::where('cnpj', $request->cnpj)->first();
            if ($cnpj != null) {
                return view('auth.login', ['alertMessage' => 'cnpjNotUnique']);
            }
        } else {
            return view('auth.login', ['alertMessage' => 'failCNPJ']);
        }

        $user = new User();
        $user->name = $request->name;
        $user->description = 'Descrição ' . $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->permission_id = 4;
        $user->email_verified = Str::random(8);
        $user->save();

        $company = new Company();
        $company->user_id = $user->id;
        $company->cnpj = $request->cnpj;
        $company->save();

        //enviar email de recuperação de senha passando o usuario como parametro
        Mail::send('emailChecker.emailCheckerEmail', ['user' => $user], function ($m) use ($user) {
            $m->from('gelmirbaumgratz@gmail.com');
            $m->to($user->email);
            $m->subject('Confirmação de email');
        });

        return view('auth.login', ['alertMessage' => 'checkEmail']);
    }

    public function userEmailChecker($emailVerified)
    {
        //valida se foi passado alguma informação no campo
        if ($emailVerified == null) {
            return view('auth.login', ['alertMessage' => 'accessDenied']);
        }

        //valida se foi passado algum codigo valido
        if (strlen($emailVerified) != 8) {
            return view('auth.login', ['alertMessage' => 'accessDenied']);
        }

        //valida se existe algum usuario com esse codigo de recuperação
        $user = User::where('email_verified', $emailVerified)->first();
        if ($user == null) {
            return view('auth.login', ['alertMessage' => 'accessDenied']);
        }

        $user->email_verified = 1;
        $user->save();

        return view('auth.login', ['alertMessage' => 'emailVerified']);
    }

    public function forgotPassword(Request $request)
    {
        //valida se email foi informado
        if ($request->email == null) {
            return view('auth.login', ['alertMessage' => 'failEmail']);
        }

        //valida se email informado existe
        $user = User::where('email', $request->email)->first();
        if ($user == null) {
            return view('auth.login', ['alertMessage' => 'failEmail']);
        }

        //valida se usuario já tentou recuperar a senha
        if ($user->forgot_password != null) {
            return view('auth.login', ['alertMessage' => 'emailAlreadySent']);
        }

        //gerando e salvando hash para recuperar senha no usuario
        //$user->forgot_password = Hash::make(Str::random(8));
        $user->forgot_password = Str::random(8);
        $user->save();

        //enviar email de recuperação de senha passando o usuario como parametro
        Mail::send('forgotPassword.forgotPasswordEmail', ['user' => $user], function ($m) use ($user) {
            $m->from('gelmirbaumgratz@gmail.com');
            $m->to($user->email);
            $m->subject('Recuperação de senha');
        });

        return view('auth.login', ['alertMessage' => 'forgotPasswordSentSuccess']);
    }

    public function userChangePassword($forgotPassword)
    {
        //valida se foi passado alguma informação no campo
        if ($forgotPassword == null) {
            return view('auth.login', ['alertMessage' => 'accessDenied']);
        }

        //valida se existe algum usuario com esse codigo de recuperação
        $userEdit = User::where('forgot_password', $forgotPassword)->first();
        if ($userEdit == null) {
            return view('auth.login', ['alertMessage' => 'accessDenied']);
        }

        return view('forgotPassword.changePassword', ['userEdit' => $userEdit]);
    }

    public function changePassword(Request $request)
    {
        //valida se foi passado algum email
        if ($request->email == null) {
            return view('auth.login', ['alertMessage' => 'accessDenied']);
        }

        //valida se existe algum usuario com o email
        $user = User::where('email', $request->email)->first();
        if ($user == null) {
            return view('auth.login', ['alertMessage' => 'accessDenied']);
        }

        $user->password = bcrypt($request->passwordConfirm);
        $user->forgot_password = null;
        $user->save();

        return view('auth.login', ['alertMessage' => 'changePasswordSuccess']);
    }
}
