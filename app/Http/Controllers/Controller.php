<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function convertStringToDateTimeFormateDateTime(string $datetime)
    {
        $datetime = date_create($datetime);
        $datetime = date_format($datetime, 'd/m/Y - H:i:s');
        return $datetime;
    }

    public function convertStringToDateTimeFormateDate(string $datetime)
    {
        $datetime = date_create($datetime);
        $datetime = date_format($datetime, 'd/m/Y');
        return $datetime;
    }

    public function convertStringToDateTimeFormateTime(string $datetime)
    {
        $datetime = date_create($datetime);
        $datetime = date_format($datetime, 'H:i:s');
        return $datetime;
    }
}
