<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\CompanyTutor;
use App\Models\Permission;
use App\Models\Tutor;
use App\Models\User;
use Illuminate\Http\Request;

class CompanyController extends Controller
{

    public function showAll()
    {
        return view(
            'admin.company.showAll',
            [
                'companies' => Company::all()
            ]
        );
    }

    public function store(Request $request)
    {
        //validando cnpj
        if (strlen($request->cnpj) != 18)
            return redirect()->route('admin/company')->with('fail', 'CNPJ inválido');

        //validando telefone
        if (strlen($request->phone) != 14)
            return redirect()->route('admin/company')->with('fail', 'Telefone inválido');

        //validando se email já existe
        $user = User::where('email', $request->email)->first();
        if ($user != null) {
            return redirect()->route('admin/company')->with('fail', 'Email já cadastrado');
        }

        //validando se cnpj já existe
        $company = Company::where('cnpj', $request->cnpj)->first();
        if ($company != null) {
            return redirect()->route('admin/company')->with('fail', 'CNPJ já cadastrado');
        }

        $user = new User();
        $user->name = $request->name;
        if ($request->description != null)
            $user->description = $request->description;
        else
            $user->description = 'Descrição ' . $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->phone = $request->phone;
        $user->active = $request->active;
        $user->permission_id = 4;
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $requestPhoto = $request->photo;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/companies'), $imagemName);
            $user->photo = $imagemName;
        }
        $user->save();

        $company = new Company();
        $company->user_id = $user->id;
        $company->cnpj = $request->cnpj;
        $company->save();

        return redirect()->route('admin/company')->with('success', 'Usuário cadastrado com sucesso');
    }

    public function edit(int $user_id)
    {
        return view(
            'admin.company.showAll',
            [
                'companies' => Company::all(),
                'userEdit' => (User::where('id', $user_id)->first()),
                'companyEdit' => (Company::where('user_id', $user_id)->first())
            ]
        );
    }

    public function update(Request $request, int $id_company, int $id_user)
    {
        //validando cnpj
        if (strlen($request->cnpjEdit) != 18)
            return redirect()->back()->with('fail', 'CNPJ inválido');

        //validando telefone
        if (strlen($request->phoneEdit) != 14)
            return redirect()->back()->with('fail', 'Telefone inválido');

        //validando se email já existe
        $user = User::where('email', $request->emailEdit)->first();
        if ($user != null) {
            if ($user->id != $id_user) {
                return redirect()->back()->with('fail', 'Email já cadastrado');
            }
        }

        //validando se cnpj já existe
        $company = Company::where('cnpj', $request->cnpjEdit)->first();
        if ($company != null) {
            if ($company->id != $id_company) {
                return redirect()->back()->with('fail', 'CNPJ já cadastrado');
            }
        }

        $user = User::where('id', $id_user)->first();
        $user->name = $request->nameEdit;
        $user->description = $request->descriptionEdit;
        $user->email = $request->emailEdit;
        if ($request->passwordEdit != $user->password)
            $user->password = bcrypt($request->passwordEdit);
        $user->active = $request->activeEdit;
        $user->phone = $request->phoneEdit;
        $user->permission_id = 4;
        if ($request->hasFile('photoEdit') && $request->file('photoEdit')->isValid()) {
            // verifica se ele tem uma foto para deletar
            if (!is_null($user->photo))
                unlink('img/companies/' . $user->photo);
            $requestPhoto = $request->photoEdit;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/companies'), $imagemName);
            $user->photo = $imagemName;
        }
        $user->save();

        $company = Company::where('id', $id_company)->first();
        $company->cnpj = $request->cnpjEdit;
        $company->save();

        return redirect()->route('admin/company')->with('success', 'Usuário editado com sucesso');
    }

    public function active(int $id)
    {
        $user = User::where('id', $id)->first();
        if ($user->active == 0)
            $user->active = 1;
        else
            $user->active = 0;
        $user->save();

        return redirect()->route('admin/company')->with('success', 'Status do usuário editado com sucesso');
    }

    // ------------------------------------------------------------------------------------------------------------------

    public function getCompaniesStatus(bool $status)
    {
        $companies = Company::all();
        $companiesActive = array();
        foreach ($companies as $company) {
            $user = User::where('id', $company->user_id)->first();
            if ($user->active == $status) {
                $companiesActive[] = $company;
            }
        }
        return $companiesActive;
    }

    public function getAllCompaniesOfTutorAuth()
    {
        $companyTutors = CompanyTutor::where('tutor_id', session('tutor')->id)->get();
        $companies = array();
        foreach ($companyTutors as $companyTutor) {
            $companies[] =
                Company::where('companies.id', $companyTutor->company_id)
                ->select('companies.*')
                ->join('users', 'users.id', '=', 'companies.user_id')
                ->where('users.active', true)
                ->first();
        }
        return array_filter($companies);
    }
}
