<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Tutor;
use App\Models\User;
use Illuminate\Http\Request;

class ClientController extends Controller
{

    public function showAll()
    {
        return view(
            'admin.client.showAll',
            [
                'clients' => Client::all(),
                'permissions' => (new PermissionController)->getPermissionsClient()
            ]
        );
    }

    public function store(Request $request)
    {
        //validando cpf
        if (strlen($request->cpf) != 14)
            return redirect()->route('admin/client')->with('fail', 'CPF inválido');

        //validando telefone
        if (strlen($request->phone) != 14)
            return redirect()->route('admin/client')->with('fail', 'Telefone inválido');

        //validando se email já existe
        $user = User::where('email', $request->email)->first();
        if ($user != null) {
            return redirect()->route('admin/client')->with('fail', 'Email já cadastrado');
        }

        //validando se cpf já existe
        $client = Client::where('cpf', $request->cpf)->first();
        if ($client != null) {
            return redirect()->route('admin/client')->with('fail', 'CPF já cadastrado');
        } else {
            $tutor = Tutor::where('cpf', $request->cpf)->first();
            if ($tutor != null) {
                return redirect()->route('admin/client')->with('fail', 'CPF já cadastrado');
            }
        }

        $user = new User();
        $user->name = $request->name;
        if ($request->description == '' || $request->description == null)
            $user->description = 'Descrição ' . $request->name;
        else
            $user->description = $request->description;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->active = $request->active;
        $user->permission_id = $request->permission_id;
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $requestPhoto = $request->photo;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/clients'), $imagemName);
            $user->photo = $imagemName;
        }
        $user->save();

        $client = new Client();
        $client->user_id = $user->id;
        $client->cpf = $request->cpf;
        $client->save();

        return redirect()->route('admin/client')->with('success', 'Usuário cadastrado com sucesso');
    }

    public function edit(int $user_id)
    {
        return view(
            'admin.client.showAll',
            [
                'clients' => Client::all(),
                'permissions' => (new PermissionController)->getPermissionsClient(),
                'userEdit' => (User::where('id', $user_id)->first()),
                'clientEdit' => (Client::where('user_id', $user_id)->first())
            ]
        );
    }

    public function update(Request $request, int $id_client, int $id_user)
    {
        //validando cpf
        if (strlen($request->cpfEdit) != 14)
            return redirect()->back()->with('fail', 'CPF inválido');

        //validando telefone
        if (strlen($request->phoneEdit) != 14)
            return redirect()->back()->with('fail', 'Telefone inválido');

        //validando se email já existe
        $user = User::where('email', $request->emailEdit)->first();
        if ($user != null) {
            if ($user->id != $id_user) {
                return redirect()->back()->with('fail', 'Email já cadastrado');
            }
        }

        //validando se cpf já existe
        $client = Client::where('cpf', $request->cpfEdit)->first();
        if ($client != null) {
            if ($client->id != $id_client) {
                return redirect()->back()->with('fail', 'CPF já cadastrado');
            }
        } else {
            $tutor = Tutor::where('cpf', $request->cpfEdit)->first();
            if ($tutor != null) {
                return redirect()->back()->with('fail', 'CPF já cadastrado');
            }
        }

        $user = User::where('id', $id_user)->first();
        $user->name = $request->nameEdit;
        if ($request->descriptionEdit != null)
            $user->description = $request->descriptionEdit;
        else
            $user->description = 'Descrição ' . $request->nameEdit;
        $user->email = $request->emailEdit;
        $user->phone = $request->phoneEdit;
        if ($request->passwordEdit != $user->password)
            $user->password = bcrypt($request->passwordEdit);
        $user->active = $request->activeEdit;
        $user->permission_id = $request->permission_idEdit;
        if ($request->hasFile('photoEdit') && $request->file('photoEdit')->isValid()) {
            // verifica se ele tem uma foto para deletar
            if (!is_null($user->photo))
                unlink('img/clients/' . $user->photo);
            $requestPhoto = $request->photoEdit;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/clients'), $imagemName);
            $user->photo = $imagemName;
        }
        $user->save();

        $client = Client::where('id', $id_client)->first();
        $client->cpf = $request->cpfEdit;
        $client->save();

        return redirect()->route('admin/client')->with('success', 'Usuário editado com sucesso');
    }

    public function active(int $id)
    {
        $user = User::where('id', $id)->first();
        if ($user->active == 0)
            $user->active = 1;
        else
            $user->active = 0;
        $user->save();

        return redirect()->route('admin/client')->with('success', 'Status do usuário editado com sucesso');
    }
}
