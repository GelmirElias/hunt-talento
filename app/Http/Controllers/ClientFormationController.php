<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use App\Models\Client;
use App\Models\ClientFormation;
use App\Models\ClientSkill;
use App\Models\Company;
use App\Models\CompanyFormation;
use App\Models\Formation;
use App\Models\FormationSkill;
use App\Models\FormationTutor;
use App\Models\FormationType;
use App\Models\Plan;
use App\Models\Tutor;
use App\Models\User;
use App\Models\Vacancy;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientFormationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAll()
    {
        $formation = new FormationController;
        return view('client.formation.showAll', ['formations' => $formation->getAllFormationForClientAuth()]);
    }

    public function show(int $id_formation)
    {
        $formationDetail = Formation::where('id', $id_formation)->first();

        //encontrando todos os tutores que a formação editada possui
        $formationTutors = FormationTutor::where('formation_id', $id_formation)->get();
        $tutorsIdDetail = array();
        foreach ($formationTutors as $formationTutor) {
            $tutor = Tutor::where('id', $formationTutor->tutor_id)->first();
            $tutorsIdDetail[] = $tutor->id;
        }

        return view('client.formation.show', [
            'formationDetail' => $formationDetail,
            'tutorsIdDetail' => $tutorsIdDetail,
            'formationSkillsDetail' => FormationSkill::where('formation_id', $formationDetail->id)->where('active', true)->get()
        ]);
    }

    public function finish(int $id_formation)
    {
        $clientFormation = ClientFormation::where('client_id', session('client')->id)->where('formation_id', $id_formation)->first();
        $clientFormation->finish = true;
        $clientFormation->finish_at = Carbon::now();
        $clientFormation->save();

        // adiciona os talentos da formação no candidato
        $formationSkills = FormationSkill::where('formation_id', $id_formation)->where('active', true)->get();

        // verifica se a formação tem skills
        if (!empty($formationSkills)) {

            // para cada talento vinculado a formação
            foreach ($formationSkills as $formationSkill) {

                // verifica se o cliente já possui o talento
                $clientSkill = ClientSkill::where('skill_id', $formationSkill->skill_id)->where('client_id', session('client')->id)->where('client_assigned', false)->first();

                // se o candidato ja tiver o talento
                if (!is_null($clientSkill)) {

                    // verificar se o rate é menor que 5
                    if ($clientSkill->rate < 5) {

                        // se for incrementa o rate dele
                        $clientSkill->rate = $clientSkill->rate + 1;
                        $clientSkill->save();
                    }
                }

                // se não cria um novo relacionamento
                else {
                    $clientSkill = new ClientSkill();
                    $clientSkill->client_id = session('client')->id;
                    $clientSkill->skill_id = $formationSkill->skill_id;
                    $clientSkill->rate = 1;
                    $clientSkill->save();
                }
            }

            // buscar todos as vagas que o cliente é candidato
            $candidates = Candidate::where('client_id', session('client')->id)->where('active', true)->get();

            // se o cliente for candidato
            if (!empty($candidates)) {

                // para cada vaga que ele é candidato
                foreach ($candidates as $candidate) {

                    // busca a vaga correspondente
                    $vacancy = Vacancy::where('id', $candidate->vacancy_id)->where('active', true)->first();

                    // se for uma vaga valida
                    if (!empty($vacancy)) {

                        // atualizar a flag de new_candidate
                        $vacancy->new_candidate = true;
                        $vacancy->save();
                    }
                }
            }
        }

        $formation = new FormationController;
        return view('client.formation.showAll', ['alertMessage' => 'formationFinish', 'formations' => $formation->getAllFormationForClientAuth()]);
    }

    public function history()
    {
        $clientFormations = ClientFormation::where('client_id', session('client')->id)->where('finish', true)->get();
        return view('client.formation.history', ['clientFormations' => $clientFormations]);
    }

    public function showHistory(int $client_formation_id)
    {
        //encontra o relacionamento clientFormation
        $clientFormationDetail = ClientFormation::where('id', $client_formation_id)->first();

        //encontrando formação para exibir
        $formationDetail = Formation::where('id', $clientFormationDetail->formation_id)->first();

        //encontrando todos os relacionamentos companyFormations que a formação editada possui
        $companyFormations = CompanyFormation::where('formation_id', $clientFormationDetail->formation_id)->get();
        $companiesIdDetail = array();
        foreach ($companyFormations as $companyFormation) {
            $company = Company::where('id', $companyFormation->company_id)->first();
            $companiesIdDetail[] = $company->id;
        }

        //encontrando todos os tutores que a formação editada possui
        $formationTutors = FormationTutor::where('formation_id', $clientFormationDetail->formation_id)->get();
        $tutorsIdDetail = array();
        foreach ($formationTutors as $formationTutor) {
            $tutor = Tutor::where('id', $formationTutor->tutor_id)->first();
            $tutorsIdDetail[] = $tutor->id;
        }

        $clientFormations = ClientFormation::where('client_id', session('client')->id)->where('finish', true)->get();

        return view('client.formation.history', [
            'clientFormations' => $clientFormations,
            'clientFormationDetail' => $clientFormationDetail,
            'formationDetail' => $formationDetail,
            'companiesIdDetail' => $companiesIdDetail,
            'tutorsIdDetail' => $tutorsIdDetail,
            'plans' => Plan::where('active', true)->get(),
            'formationTypes' => FormationType::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::all();
        $users = User::all();
        $formations = Formation::all();
        return view('clientFormation.create', ['formations' => $formations, 'clients' => $clients, 'users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clientFormation = new ClientFormation();
        $clientFormation->client_id = $request->client_id;
        $clientFormation->formation_id = $request->formation_id;
        $clientFormation->save();
        return redirect('clientFormation');
    }
}
