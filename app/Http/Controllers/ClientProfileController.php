<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use App\Models\ClientSkill;
use App\Models\Permission;
use App\Models\Skill;
use App\Models\User;
use App\Models\Vacancy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ClientProfileController extends Controller
{
    public function index($alertMessage = null)
    {
        return view(
            'client.profile.index',
            [
                'alertMessage' => $alertMessage,
                'client' => session('client'),
                'user' => User::where('id', Auth::user()->id)->first(),
                'permissions' => Permission::where('active', true)->get()
            ]
        );
    }

    public function updatePhoto(Request $request)
    {
        // verifica se ta chegando o arquivo de foto
        if ($request->hasFile('photoEdit') && $request->file('photoEdit')->isValid()) {

            // busca o objeto do usuario da sessão
            $user = User::where('id', Auth::user()->id)->first();

            // verifica se ele tem uma foto para deletar
            if (!is_null($user->photo))
                unlink('img/clients/' . $user->photo);

            // trata a imagem para ser armazenada
            $requestPhoto = $request->photoEdit;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/clients'), $imagemName);

            // salva o nome da imagem no usuario da sessão
            $user->photo = $imagemName;
            $user->save();

            // retorna sucesso ao salvar a imagem
            return redirect()->back()->with('success', 'Imagem salva com sucesso');
        } else {
            // retorna falha ao não ter o arquivo de foto
            return $this->index('fail');
        }
    }

    public function updatePassword(Request $request)
    {
        // verifica se as duas senhas são iguais
        if ($request->newPassword != $request->newPasswordConfirm) {
            return redirect()->back()->with('fail', 'Novas senhas não correspondem');
        }

        // busca o objeto do usuario da sessão
        $user = User::where('id', Auth::user()->id)->first();

        // verifica se a senha é a mesma da antiga
        if (!Hash::check($request->oldPassword, $user->password)) {
            return redirect()->back()->with('fail', 'Senha antiga não corresponde');
        }

        // salva a nova senha criptografada
        $user->password = bcrypt($request->newPassword);
        $user->save();

        // retorna sucesso ao salvar a senha
        return redirect()->back()->with('success', 'Senha alterada com sucesso');
    }

    public function updateActive(Request $request)
    {
        // busca o objeto do usuario da sessão
        $user = User::where('id', Auth::user()->id)->first();

        // verifica se a senha é a mesma da antiga
        if (!Hash::check($request->password, $user->password)) {
            return redirect()->back()->with('fail', 'Senha não corresponde');
        }

        // salva a nova senha criptografada
        $user->active = !$user->active;
        $user->save();

        // retorna sucesso ao salvar a senha
        if ($user->active == false) {
            return (new LoginController)->signOut();
        }
    }

    public function updateResume(Request $request)
    {
        // verifica se ta chegando o arquivo de foto
        if ($request->hasFile('resumeEdit') && $request->file('resumeEdit')->isValid()) {

            // busca o objeto do usuario da sessão
            $user = User::where('id', Auth::user()->id)->first();

            // verifica se ele tem uma foto para deletar
            if (!is_null($user->resume))
                unlink('doc/resume/' . $user->resume);

            // trata a imagem para ser armazenada
            $requestResume = $request->resumeEdit;
            $extension = $requestResume->extension();
            $resumeName = md5($requestResume->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestResume->move(public_path('/doc/resume/'), $resumeName);

            // salva o nome da imagem no usuario da sessão
            $user->resume = $resumeName;
            $user->save();

            // extrai texto do pdf
            $util = new UtilController;
            $resume = $util->extractTextFromPdf(public_path('/doc/resume/' . $resumeName));

            // busca todas os talentos
            $skills = Skill::where('active', true)->get();

            // para cada talento
            foreach ($skills as $skill) {

                // buscar se possui no texto extraido
                if (preg_match("/(?i)\b{$skill->name}\b/", $resume)) {

                    // se tiver busca os talentos do candido
                    $clientSkill = ClientSkill::where('skill_id', $skill->id)->where('client_id', session('client')->id)->where('client_assigned', false)->first();

                    // se o candidato ja tiver o talento
                    if (!is_null($clientSkill)) {

                        // verificar se o rate é menor que 5
                        if ($clientSkill->rate < 5) {

                            // se for incrementa o rate dele
                            $clientSkill->rate = $clientSkill->rate + 1;
                            $clientSkill->save();
                        }
                    }

                    // se não cria um novo relacionamento
                    else {
                        $clientSkill = new ClientSkill();
                        $clientSkill->client_id = session('client')->id;
                        $clientSkill->skill_id = $skill->id;
                        $clientSkill->rate = 1;
                        $clientSkill->save();
                    }

                    // buscar todos as vagas que o cliente é candidato
                    $candidates = Candidate::where('client_id', session('client')->id)->where('active', true)->get();

                    // se o cliente for candidato
                    if (!empty($candidates)) {

                        // para cada vaga que ele é candidato
                        foreach ($candidates as $candidate) {

                            // busca a vaga correspondente
                            $vacancy = Vacancy::where('id', $candidate->vacancy_id)->where('active', true)->first();

                            // se for uma vaga valida
                            if (!empty($vacancy)) {

                                // atualizar a flag de new_candidate
                                $vacancy->new_candidate = true;
                                $vacancy->save();
                            }
                        }
                    }
                }
            }

            // retorna sucesso ao salvar a imagem
            return redirect()->back()->with('success', 'Currículo salvo com sucesso');
        } else {
            // retorna falha ao não ter o arquivo de foto
            return $this->index('fail');
        }
    }
}
