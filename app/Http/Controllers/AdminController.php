<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function showAll()
    {
        return view(
            'admin.admin.showAll',
            [
                'users' => (User::where('permission_id', 1)->get())
            ]
        );
    }

    public function store(Request $request)
    {
        //validando telefone
        if (strlen($request->phone) != 14)
            return redirect()->route('admin/admin')->with('fail', 'Telefone inválido');

        //valida se email informado já existe
        $user = User::where('email', $request->email)->first();
        if ($user != null) {
            return redirect()->route('admin/admin')->with('fail', 'Email já cadastrado');
        }

        $user = new User();
        $user->name = $request->name;
        $user->active = $request->active;
        if ($request->description == null)
            $user->description = 'Descrição ' . $request->name;
        else
            $user->description = $request->description;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->permission_id = 1;
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $requestPhoto = $request->photo;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/admins'), $imagemName);
            $user->photo = $imagemName;
        }
        $user->save();

        return redirect()->route('admin/admin')->with('success', 'Usuário cadastrado com sucesso');
    }

    public function edit(int $user_id)
    {
        return view('admin.admin.showAll', [
            'users' => User::where('permission_id', 1)->get(),
            'userEdit' => (User::where('id', $user_id)->first())
        ]);
    }

    public function update(Request $request, int $id_user)
    {
        //validando telefone
        if (strlen($request->phoneEdit) != 14)
            return redirect()->back()->with('fail', 'Telefone inválido');

        //validando se email já existe
        $user = User::where('email', $request->emailEdit)->first();
        if ($user != null) {
            if ($user->id != $id_user) {
                return redirect()->back()->with('fail', 'Email já cadastrado');
            }
        }

        $user = User::where('id', $id_user)->first();
        $user->name = $request->nameEdit;
        if ($request->descriptionEdit != null)
            $user->description = $request->descriptionEdit;
        else
            $user->description = 'Descrição ' . $request->nameEdit;
        $user->email = $request->emailEdit;
        $user->password = bcrypt($request->passwordEdit);
        $user->phone = $request->phoneEdit;
        $user->active = $request->activeEdit;
        if ($request->hasFile('photoEdit') && $request->file('photoEdit')->isValid()) {
            // verifica se ele tem uma foto para deletar
            if (!is_null($user->photo))
                unlink('img/admins/' . $user->photo);
            $requestPhoto = $request->photoEdit;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/admins'), $imagemName);
            $user->photo = $imagemName;
        }
        $user->save();

        return redirect()->route('admin/admin')->with('success', 'Usuário editado com sucesso');
    }

    public function active(int $id)
    {
        $user = User::where('id', $id)->first();
        if ($user->active == 0)
            $user->active = 1;
        else
            $user->active = 0;
        $user->save();

        return redirect()->route('admin/admin')->with('success', 'Status do usuário editado com sucesso');
    }
}
