<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Company;
use App\Models\CompanyTutor;
use App\Models\Tutor;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TutorController extends Controller
{

    public function showAll()
    {
        return view(
            'admin.tutor.showAll',
            [
                'tutors' => Tutor::all(),
                'companies' => (new CompanyController)->getCompaniesStatus(true)
            ]
        );
    }

    public function store(Request $request)
    {
        //validando cpf
        if (strlen($request->cpf) != 14)
            return redirect()->route('admin/tutor')->with('fail', 'CPF inválido');

        //validando telefone
        if (strlen($request->phone) != 14)
            return redirect()->route('admin/tutor')->with('fail', 'Telefone inválido');

        //validando se email já existe
        $user = User::where('email', $request->email)->first();
        if ($user != null) {
            return redirect()->route('admin/tutor')->with('fail', 'Email já cadastrado');
        }

        //validando se cpf já existe
        $tutor = Tutor::where('cpf', $request->cpf)->first();
        if ($tutor != null) {
            return redirect()->route('admin/tutor')->with('fail', 'CPF já cadastrado');
        } else {
            $client = Client::where('cpf', $request->cpf)->first();
            if ($client != null) {
                return redirect()->route('admin/tutor')->with('fail', 'CPF já cadastrado');
            }
        }

        $user = new User();
        $user->name = $request->name;
        if ($request->description != null)
            $user->description = $request->description;
        else
            $user->description = 'Descrição ' . $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->active = $request->active;
        $user->phone = $request->phone;
        $user->permission_id = 5;
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $requestPhoto = $request->photo;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/tutors'), $imagemName);
            $user->photo = $imagemName;
        }
        $user->save();

        $tutor = new Tutor();
        $tutor->user_id = $user->id;
        $tutor->cpf = $request->cpf;
        $tutor->save();

        $companyTutor = new CompanyTutor();
        $companyTutor->company_id = $request->companyId;
        $companyTutor->tutor_id = $tutor->id;
        $companyTutor->save();

        return redirect()->route('admin/tutor')->with('success', 'Usuário cadastrado com sucesso');
    }

    public function edit(int $user_id)
    {
        $userEdit = User::where('id', $user_id)->first();
        $tutorEdit = Tutor::where('user_id', $user_id)->first();

        return view(
            'admin.tutor.showAll',
            [
                'tutors' => Tutor::all(),
                'userEdit' => $userEdit,
                'tutorEdit' => $tutorEdit,
                'companies' => (new CompanyController)->getCompaniesStatus(true)
            ]
        );
    }

    public function update(Request $request, int $id_tutor, int $id_user)
    {
        //validando cpf
        if (strlen($request->cpfEdit) != 14)
            return redirect()->back()->with('fail', 'CPF inválido');

        //validando telefone
        if (strlen($request->phoneEdit) != 14)
            return redirect()->back()->with('fail', 'Telefone inválido');

        //validando se email já existe
        $user = User::where('email', $request->emailEdit)->first();
        if ($user != null) {
            if ($user->id != $id_user) {
                return redirect()->back()->with('fail', 'Email já cadastrado');
            }
        }

        //validando se cpf já existe
        $tutor = Tutor::where('cpf', $request->cpfEdit)->first();
        if ($tutor != null) {
            if ($tutor->id != $id_tutor)
                return redirect()->back()->with('fail', 'CPF já cadastrado');
        } else {
            $client = Client::where('cpf', $request->cpfEdit)->first();
            if ($client != null) {
                return redirect()->back()->with('fail', 'CPF já cadastrado');
            }
        }

        $user = User::where('id', $id_user)->first();
        $user->name = $request->nameEdit;
        if ($request->descriptionEdit != null)
            $user->description = $request->descriptionEdit;
        else
            $user->description = 'Descrição ' . $request->nameEdit;
        $user->email = $request->emailEdit;
        if ($request->passwordEdit != $user->password)
            $user->password = bcrypt($request->passwordEdit);
        $user->active = $request->activeEdit;
        $user->phone = $request->phoneEdit;
        $user->permission_id = 5;
        if ($request->hasFile('photoEdit') && $request->file('photoEdit')->isValid()) {
            // verifica se ele tem uma foto para deletar
            if (!is_null($user->photo))
                unlink('img/tutors/' . $user->photo);
            $requestPhoto = $request->photoEdit;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/tutors'), $imagemName);
            $user->photo = $imagemName;
        }
        $user->save();

        $tutor = Tutor::where('id', $id_tutor)->first();
        $tutor->cpf = $request->cpfEdit;
        $tutor->save();

        DB::table('company_tutors')->where('tutor_id', '=', $tutor->id)->update(['company_id' => $request->companyIdEdit]);

        return redirect()->route('admin/tutor')->with('success', 'Usuário editado com sucesso');
    }

    public function active(int $id)
    {
        $user = User::where('id', $id)->first();
        if ($user->active == 0)
            $user->active = 1;
        else
            $user->active = 0;
        $user->save();

        return redirect()->route('admin/tutor')->with('success', 'Status do usuário editado com sucesso');
    }

    // ----------------------------------------------------------------

    public function getAllTutorsOfCompanyAuth()
    {
        $companyTutors = CompanyTutor::where('company_id', session('company')->id)->get();
        $tutors = array();
        foreach ($companyTutors as $companyTutor) {
            $tutors[] = Tutor::where('id', $companyTutor->tutor_id)->first();
        }
        return $tutors;
    }

    public function getAllTutorsOfTutorAuth()
    {
        $companies = new CompanyController;
        $companies = $companies->getAllCompaniesOfTutorAuth();

        $companyTutors = array();
        foreach ($companies as $company) {
            $companyTutors[] = CompanyTutor::where('company_id', $company->id)->get();
        }

        $tutors = array();
        foreach ($companyTutors as $companyTutor) {
            foreach ($companyTutor as $tutor) {
                $tutors[] = Tutor::where('tutors.id', $tutor->tutor_id)
                    ->select('tutors.*')
                    ->join('users', 'users.id', '=', 'tutors.user_id')
                    ->where('users.active', true)
                    ->first();
            }
        }

        return array_filter(array_unique($tutors));
    }
}
