<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\ClientPlan;
use App\Models\Plan;
use App\Models\User;
use Illuminate\Http\Request;

class ClientPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clientPlan.index' , []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::all();
        $users = User::all();
        $plans = Plan::all();
        return view('clientPlan.create', ['plans' => $plans, 'clients' => $clients, 'users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clientPlan = new ClientPlan();
        $clientPlan->client_id = $request->client_id;
        $clientPlan->plan_id = $request->plan_id;
        $clientPlan->start_date = $request->start_date;
        $clientPlan->end_date = $request->end_date;
        $clientPlan->reason_cancellation = $request->reason_cancellation;
        $clientPlan->save();
        return redirect("clientPlan");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ClientPlan  $clientPlan
     * @return \Illuminate\Http\Response
     */
    public function show(ClientPlan $clientPlan)
    {
        //
    }

    public function showAll()
    {
        $clientPlans = ClientPlan::all();
        return view('clientPlan.showAll' , ["clientPlans" => $clientPlans]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ClientPlan  $clientPlan
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientPlan $clientPlan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ClientPlan  $clientPlan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientPlan $clientPlan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ClientPlan  $clientPlan
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientPlan $clientPlan)
    {
        //
    }
}
