<?php

namespace App\Http\Controllers;

use App\Models\ClientFormation;
use App\Models\CompanyFormation;
use App\Models\Formation;
use App\Models\FormationSkill;
use App\Models\FormationTutor;
use App\Models\FormationType;
use App\Models\Plan;
use App\Models\Skill;
use App\Models\Tutor;
use App\Models\VacancyFormation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TutorFormationController extends Controller
{
    public function showAll()
    {
        $tutor = new TutorController;
        $formation = new FormationController;
        return view('tutor.formation.showAll', [
            'formations' => $formation->getAllFormationForTutorAuth(),
            'formationTypes' => FormationType::all(),
            'tutors' => $tutor->getAllTutorsOfTutorAuth(),
            'skills' => Skill::where('active', true)->get(),
            'companies' => (new CompanyController)->getAllCompaniesOfTutorAuth(),
            'plans' => Plan::where('active', true)->get()
        ]);
    }

    public function store(Request $request)
    {
        $formation = new Formation();
        $formation->name = $request->name;

        //tratar markdown
        if (!empty($request->description)) {
            $formation->description = (new UtilController)->markdown($request->description);
        } else
            $formation->description = 'Descrição ' . $request->name;

        $formation->start_date = $request->startDate;
        $formation->end_date = $request->endDate;
        $formation->active = $request->active;
        $formation->formation_types_id = $request->formationTypeId;
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $requestPhoto = $request->photo;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/formations'), $imagemName);
            $formation->photo = $imagemName;
        }
        $formation->plan_id = $request->planId;
        $formation->save();

        //salvando as empresas da formação
        $companies = $request->companyId;
        if (!empty($companies)) {
            foreach ($companies as $company) {
                $companyFormation = new CompanyFormation();
                $companyFormation->formation_id = $formation->id;
                $companyFormation->company_id = $company;
                $companyFormation->save();
            }
        }

        //salvando todas os tutores da formação
        $tutors = $request->tutorId;
        if (!empty($tutors)) {
            foreach ($tutors as $tutor) {
                $formationTutor = new FormationTutor();
                $formationTutor->formation_id = $formation->id;
                $formationTutor->tutor_id = $tutor;
                $formationTutor->save();
            }
        }

        //salvando os talentos da formação
        $skills = $request->skillId;
        if (!empty($skills)) {
            foreach ($skills as $skill) {
                $formationSkill = new FormationSkill();
                $formationSkill->formation_id = $formation->id;
                $formationSkill->skill_id = $skill;
                $formationSkill->save();
            }
        }

        return redirect()->route('tutor/formation')->with('success', 'Cadastrado com sucesso');
    }

    public function edit(int $formation_id)
    {
        //encontrando todos os tutores que a formação editada possui
        $formationTutors = FormationTutor::where('formation_id', $formation_id)->get();
        $tutorsIdEdit = array();
        foreach ($formationTutors as $formationTutor) {
            $tutor = Tutor::where('id', $formationTutor->tutor_id)->first();
            $tutorsIdEdit[] = $tutor->id;
        }

        //encontrando formação para editar
        $formationEdit = Formation::where('id', $formation_id)->first();
        //dd($formationEdit->description);

        //inicializando controladores para usar os metodos deles
        $tutor = new TutorController;
        $formation = new FormationController;

        return view('tutor.formation.showAll', [
            'formations' => $formation->getAllFormationForTutorAuth(),
            'formationTypes' => FormationType::all(),
            'formationEdit' => $formationEdit,
            'tutors' => $tutor->getAllTutorsOfTutorAuth(),
            'tutorsIdEdit' => $tutorsIdEdit,
            'skills' => Skill::where('active', true)->get(),
            'companies' => (new CompanyController)->getAllCompaniesOfTutorAuth(),
            'formationSkillsEdit' => FormationSkill::where('formation_id', $formationEdit->id)->where('active', true)->get(),
            'formationCompaniesEdit' => CompanyFormation::where('formation_id', $formationEdit->id)->get(),
            'plans' => Plan::where('active', true)->get()
        ]);
    }

    public function update(Request $request, int $formation_id)
    {
        $formation = Formation::where('id', $formation_id)->first();
        $formation->name = $request->nameEdit;

        //tratar markdown
        if (!empty($request->descriptionEdit)) {
            $formation->description = (new UtilController)->markdown($request->descriptionEdit);
        } else
            $formation->description = 'Descrição ' . $request->nameEdit;

        $formation->start_date = $request->startDateEdit;
        $formation->end_date = $request->endDateEdit;
        $formation->active = $request->activeEdit;
        $formation->formation_types_id = $request->formationTypeIdEdit;
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $requestPhoto = $request->photo;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/formations'), $imagemName);
            $formation->photo = $imagemName;
        }
        $formation->plan_id = $request->planIdEdit;
        $formation->save();

        //deletando todos os relacionamentos antigos de tutores da formação
        DB::table('formation_tutors')->where('formation_id', $formation_id)->delete();

        //salvando os novos relacionamento de tutores da formação
        $tutors = $request->tutorIdEdit;
        if (!empty($tutors)) {
            foreach ($tutors as $tutor) {
                $formationTutor = new FormationTutor();
                $formationTutor->formation_id = $formation->id;
                $formationTutor->tutor_id = $tutor;
                $formationTutor->save();
            }
        }

        //deletando todos os relacionamentos antigos de empresas da formação
        DB::table('company_formations')->where('formation_id', $formation_id)->delete();

        //salvando os novos relacionamento de empresas da formação
        $companies = $request->formationCompaniesEdit;
        if (!empty($companies)) {
            foreach ($companies as $company) {
                $companyFormation = new CompanyFormation();
                $companyFormation->formation_id = $formation->id;
                $companyFormation->company_id = $company;
                $companyFormation->save();
            }
        }

        //deletando todos os relacionamentos antigos de talentos da formação
        DB::table('formation_skills')->where('formation_id', $formation_id)->delete();

        //salvando os novos relacionamento de talentos da formação
        $skills = $request->formationSkillsEdit;
        if (!empty($skills)) {
            foreach ($skills as $skill) {
                $formationSkill = new FormationSkill();
                $formationSkill->formation_id = $formation->id;
                $formationSkill->skill_id = $skill;
                $formationSkill->save();
            }
        }

        //controladores para usar seus metodos
        $tutor = new TutorController;
        $formation = new FormationController;

        return redirect()->route('tutor/formation')->with('success', 'Editado com sucesso');
    }

    public function destroy(int $formation_id)
    {
        /*
        FormationTutor::where('formation_id', $formation_id)->delete();
        CompanyFormation::where('formation_id', $formation_id)->delete();
        ClientFormation::where('formation_id', $formation_id)->delete();
        VacancyFormation::where('formation_id', $formation_id)->delete();
        FormationSkill::where('formation_id', $formation_id)->delete();
        Formation::where('id', $formation_id)->delete();

        $tutor = new TutorController;
        $formation = new FormationController;
        return view('tutor.formation.showAll', [
            'alertMessage' => 'removeSucess',
            'formations' => $formation->getAllFormationForTutorAuth(),
            'formationTypes' => FormationType::all(),
            'tutors' => $tutor->getAllTutorsOfTutorAuth(),
            'skills' => Skill::where('active', true)->get(),
            'companies' => (new CompanyController)->getAllCompaniesOfTutorAuth(),
            'plans' => Plan::where('active', true)->get()
        ]);
        */

        $formation = Formation::where('id', $formation_id)->first();
        if ($formation->active == 0)
            $formation->active = 1;
        else
            $formation->active = 0;
        $formation->save();

        return redirect()->route('tutor/formation')->with('success', 'Status editado com sucesso');
    }

    //--------------------------------------------------------------------------------------------

    public function show(int $formation_id)
    {
        $formationDetail = Formation::where('id', $formation_id)->first();

        //encontrando todos os tutores que a formação editada possui
        $formationTutors = FormationTutor::where('formation_id', $formation_id)->get();
        $tutorsIdDetail = array();
        foreach ($formationTutors as $formationTutor) {
            $tutor = Tutor::where('id', $formationTutor->tutor_id)->first();
            $tutorsIdDetail[] = $tutor->id;
        }

        return view('tutor.formation.show', [
            'formationDetail' => $formationDetail,
            'tutorsIdDetail' => $tutorsIdDetail,
            'formationSkillsDetail' => FormationSkill::where('formation_id', $formationDetail->id)->where('active', true)->get()
        ]);
    }

    public function showUpdate(Request $request, int $formation_id)
    {
        $formationEdit = Formation::where('id', $formation_id)->first();

        //tratar markdown
        if (!empty($request->descriptionEdit))
            $formationEdit->description = (new UtilController)->markdown($request->descriptionEdit);

        if (!empty($request->contentEdit))
            $formationEdit->content = (new UtilController)->markdown($request->contentEdit);


        $formationEdit->save();

        //encontrando todos os tutores que a formação editada possui
        $formationTutors = FormationTutor::where('formation_id', $formation_id)->get();
        $tutorsIdDetail = array();
        foreach ($formationTutors as $formationTutor) {
            $tutor = Tutor::where('id', $formationTutor->tutor_id)->first();
            $tutorsIdDetail[] = $tutor->id;
        }

        return redirect()->back()->with('success', 'Editado com sucesso');
    }
}
