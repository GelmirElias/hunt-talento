<?php

namespace App\Http\Controllers;

use App\Models\ClientFormation;
use App\Models\CompanyFormation;
use App\Models\Formation;
use App\Models\FormationSkill;
use App\Models\FormationTutor;
use App\Models\FormationType;
use App\Models\Plan;
use App\Models\Skill;
use App\Models\Tutor;
use App\Models\VacancyFormation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyFormationController extends Controller
{
    public function showAll()
    {
        $tutor = new TutorController;
        $formation = new FormationController;
        return view('company.formation.showAll', [
            'formations' => $formation->getAllFormationForCompanyAuth(),
            'formationTypes' => FormationType::all(),
            'skills' => Skill::where('active', true)->get(),
            'tutors' => $tutor->getAllTutorsOfCompanyAuth(),
            'plans' => Plan::where('active', true)->get()
        ]);
    }

    public function store(Request $request)
    {
        $formation = new Formation();
        $formation->name = $request->name;

        //tratar markdown
        if (!empty($request->description))
            $formation->description = (new UtilController)->markdown($request->description);
        else
            $formation->description = 'Descrição ' . $request->name;

        $formation->start_date = $request->startDate;
        $formation->end_date = $request->endDate;
        $formation->active = $request->active;
        $formation->formation_types_id = $request->formationTypeId;
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $requestPhoto = $request->photo;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/formations'), $imagemName);
            $formation->photo = $imagemName;
        }
        $formation->plan_id = $request->planId;
        $formation->save();

        //salvando a empresa logada na formação
        $companyFormation = new CompanyFormation();
        $companyFormation->formation_id = $formation->id;
        $companyFormation->company_id = session('company')->id;
        $companyFormation->save();

        //salvando todas os tutores da formação
        $tutors = $request->tutorId;
        if (!empty($tutors)) {
            foreach ($tutors as $tutor) {
                $formationTutor = new FormationTutor();
                $formationTutor->formation_id = $formation->id;
                $formationTutor->tutor_id = $tutor;
                $formationTutor->save();
            }
        }

        //salvando os talentos da formação
        $skills = $request->skillId;
        if (!empty($skills)) {
            foreach ($skills as $skill) {
                $formationSkill = new FormationSkill();
                $formationSkill->formation_id = $formation->id;
                $formationSkill->skill_id = $skill;
                $formationSkill->save();
            }
        }

        return redirect()->route('company/formation')->with('success', 'Cadastrado com sucesso');
    }

    public function edit(int $formation_id)
    {
        //encontrando todos os tutores que a formação editada possui
        $formationTutors = FormationTutor::where('formation_id', $formation_id)->get();
        $tutorsIdEdit = array();
        foreach ($formationTutors as $formationTutor) {
            $tutor = Tutor::where('id', $formationTutor->tutor_id)->first();
            $tutorsIdEdit[] = $tutor->id;
        }

        //encontrando formação para editar
        $formationEdit = Formation::where('id', $formation_id)->first();

        return view('company.formation.showAll', [
            'formations' => (new FormationController)->getAllFormationForCompanyAuth(),
            'formationTypes' => FormationType::all(),
            'skills' => Skill::where('active', true)->get(),
            'formationEdit' => $formationEdit,
            'tutors' => (new TutorController)->getAllTutorsOfCompanyAuth(),
            'tutorsIdEdit' => $tutorsIdEdit,
            'formationSkillsEdit' => FormationSkill::where('formation_id', $formationEdit->id)->where('active', true)->get(),
            'plans' => Plan::where('active', true)->get()
        ]);
    }

    public function update(Request $request, int $id_formation)
    {
        $formation = Formation::where('id', $id_formation)->first();
        $formation->name = $request->nameEdit;

        //tratar markdown
        if (!empty($request->descriptionEdit))
            $formation->description = (new UtilController)->markdown($request->descriptionEdit);
        else
            $formation->description = 'Descrição ' . $request->nameEdit;

        $formation->start_date = $request->startDateEdit;
        $formation->end_date = $request->endDateEdit;
        $formation->active = $request->activeEdit;
        $formation->formation_types_id = $request->formationTypeIdEdit;
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $requestPhoto = $request->photo;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/formations'), $imagemName);
            $formation->photo = $imagemName;
        }
        $formation->plan_id = $request->planIdEdit;
        $formation->save();

        //deletando todos os relacionamentos antigos de tutores da formação
        DB::table('formation_tutors')->where('formation_id', $id_formation)->delete();

        //salvando os novos relacionamento de tutores da formação
        $tutors = $request->tutorIdEdit;
        if (!empty($tutors)) {
            foreach ($tutors as $tutor) {
                $formationTutor = new FormationTutor();
                $formationTutor->formation_id = $formation->id;
                $formationTutor->tutor_id = $tutor;
                $formationTutor->save();
            }
        }

        //deletando todos os relacionamentos antigos de talentos da formação
        DB::table('formation_skills')->where('formation_id', $id_formation)->delete();

        //salvando os novos relacionamento de talentos da formação
        $skills = $request->formationSkillsEdit;
        if (!empty($skills)) {
            foreach ($skills as $skill) {
                $formationSkill = new FormationSkill();
                $formationSkill->formation_id = $formation->id;
                $formationSkill->skill_id = $skill;
                $formationSkill->save();
            }
        }

        return redirect()->route('company/formation')->with('success', 'Editado com sucesso');
    }

    public function destroy(int $id_formation)
    {
        /*
        FormationTutor::where('formation_id', $id_formation)->delete();
        CompanyFormation::where('formation_id', $id_formation)->delete();
        ClientFormation::where('formation_id', $id_formation)->delete();
        VacancyFormation::where('formation_id', $id_formation)->delete();
        FormationSkill::where('formation_id', $id_formation)->delete();
        Formation::where('id', $id_formation)->delete();

        $tutor = new TutorController;
        $formation = new FormationController;
        return view('company.formation.showAll', [
            'alertMessage' => 'removeSucess',
            'formations' => $formation->getAllFormationForCompanyAuth(),
            'formationTypes' => FormationType::all(),
            'skills' => Skill::where('active', true)->get(),
            'tutors' => $tutor->getAllTutorsOfCompanyAuth(),
            'plans' => Plan::where('active', true)->get()
        ]);
        */

        $formation = Formation::where('id', $id_formation)->first();
        if ($formation->active == 0)
            $formation->active = 1;
        else
            $formation->active = 0;
        $formation->save();

        return redirect()->route('company/formation')->with('success', 'Status editado com sucesso');
    }

    //--------------------------------------------------------------------

    // exibir formação
    public function show(int $id_formation)
    {
        $formationDetail = Formation::where('id', $id_formation)->first();

        //encontrando todos os tutores que a formação editada possui
        $formationTutors = FormationTutor::where('formation_id', $id_formation)->get();
        $tutorsIdDetail = array();
        foreach ($formationTutors as $formationTutor) {
            $tutor = Tutor::where('id', $formationTutor->tutor_id)->first();
            $tutorsIdDetail[] = $tutor->id;
        }

        return view('company.formation.show', [
            'formationDetail' => $formationDetail,
            'tutorsIdDetail' => $tutorsIdDetail,
            'formationSkillsDetail' => FormationSkill::where('formation_id', $formationDetail->id)->where('active', true)->get()
        ]);
    }

    public function showUpdate(Request $request, int $id_formation)
    {
        $formationEdit = Formation::where('id', $id_formation)->first();

        //tratar markdown
        if (!empty($request->descriptionEdit))
            $formationEdit->description = (new UtilController)->markdown($request->descriptionEdit);

        if (!empty($request->contentEdit))
            $formationEdit->content = (new UtilController)->markdown($request->contentEdit);

        $formationEdit->save();

        //encontrando todos os tutores que a formação editada possui
        $formationTutors = FormationTutor::where('formation_id', $id_formation)->get();
        $tutorsIdDetail = array();
        foreach ($formationTutors as $formationTutor) {
            $tutor = Tutor::where('id', $formationTutor->tutor_id)->first();
            $tutorsIdDetail[] = $tutor->id;
        }

        return redirect()->back()->with('success', 'Editado com sucesso');
    }
}
