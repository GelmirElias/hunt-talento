<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use App\Models\CandidateHistory;
use App\Models\ClientFormation;
use App\Models\ClientSkill;
use App\Models\Company;
use App\Models\CompanyFormation;
use App\Models\Formation;
use App\Models\FormationSkill;
use App\Models\FormationTutor;
use App\Models\Plan;
use App\Models\Skill;
use App\Models\StatusVacancy;
use App\Models\Tutor;
use App\Models\User;
use App\Models\Vacancy;
use App\Models\VacancyFormation;
use App\Models\VacancySkill;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index($alertMessage = null)
    {
        // variaveis para sugestao de vagas
        {
            // com base nas skills atribuidas de forma automatica
            $interestClientVacancies = null;
            $qtdinterestClientVacancies = null;
            $interestClientVacanciesIds = [];

            // com base nas skills atribuidas de forma manual
            $skillClientVacancies = null;
            $qtdskillClientVacancies = null;
            $skillClientVacanciesIds = [];
        }

        // variaveis para sugestao de formações
        {
            // com base nas skills atribuidas de forma automatica
            $interestClientFormations = null;
            $qtdinterestClientFormations = null;
            $interestClientFormationsIds = [];

            // com base nas skills atribuidas de forma manual
            $skillClientFormations = null;
            $qtdskillClientFormations = null;
            $skillClientFormationsIds = [];
        }

        // se for um cliente
        if (session('client')) {

            // sugestão de vagas
            {
                // busca as vagas que o candidato foi aprovado
                $clientApprovedVacancies = Candidate::where('client_id', session('client')->id)->where('approved', true)->get();

                // salva o id das formações finalizadas
                $clientApprovedVacanciesIds = [];
                foreach ($clientApprovedVacancies as $clientApprovedVacancie)
                    $clientApprovedVacanciesIds[] = $clientApprovedVacancie->vacancy_id;

                // com base nos interesses do client (skills atribuidas de forma automatica)
                {

                    // busco todas as skills do candidato com client_assigned false
                    $clientSkills = ClientSkill::where('client_assigned', false)->inRandomOrder()->get();

                    // se não for null entao possui skills automaticas
                    if ($clientSkills->isNotEmpty()) {

                        $interestClientVacanciesIds = [];
                        $interesetClientVacanciesMax = 3;
                        $qtdTries = 0;

                        // enquanto  não atingiu o limite
                        while (count($interestClientVacanciesIds) < $interesetClientVacanciesMax) {

                            // se atingir o maximo sair do while
                            if ($qtdTries == $interesetClientVacanciesMax) {
                                break;
                            }

                            // para cada posição selecionada aleatoriamente
                            foreach ($clientSkills as $clientSkill) {

                                // se atingir o maximo sair do foreach
                                if (count($interestClientVacanciesIds) == $interesetClientVacanciesMax) {
                                    break;
                                }

                                // busca uma vacancySkill que possua a skill sorteada do cliente,
                                // que não seja uma vaga já selecionada,
                                // e que o cliente já tenha sido aprovado.
                                $vacancySkill = VacancySkill::where('skill_id', $clientSkill->skill_id)
                                    ->where('active', true)
                                    ->whereNotIn('vacancy_id', $interestClientVacanciesIds)
                                    ->whereNotIn('vacancy_id', $clientApprovedVacanciesIds)
                                    ->inRandomOrder()
                                    ->first();

                                // se retornar um vacancySkill então
                                if (!is_null($vacancySkill)) {

                                    // salva o id da vaga para não ser selecionado na proxima repetição do laço
                                    $interestClientVacanciesIds[] = $vacancySkill->vacancy_id;
                                }
                            }

                            $qtdTries++;
                        }

                        // busca as vagas com os ids selecionados
                        $interestClientVacancies = Vacancy::whereIn('id', $interestClientVacanciesIds)
                            ->where('active', true)
                            ->inRandomOrder()
                            ->get();
                        $qtdinterestClientVacancies = count($interestClientVacancies);
                    }
                }

                // com base nas habilidades do client (skills atribuidas de forma manual)
                {

                    // busco todas as skills do candidato com client_assigned true
                    $clientSkills = ClientSkill::where('client_assigned', true)->inRandomOrder()->get();

                    // se não for null entao possui skills automaticas
                    if ($clientSkills->isNotEmpty()) {

                        $skillClientVacanciesIds = [];
                        $skillClientVacanciesMax = 3;
                        $qtdTries = 0;

                        // enquanto  não atingiu o limite
                        while (count($skillClientVacanciesIds) < $skillClientVacanciesMax) {

                            // se atingir o maximo sair do while
                            if ($qtdTries == $skillClientVacanciesMax) {
                                break;
                            }

                            // para cada posição selecionada aleatoriamente
                            foreach ($clientSkills as $clientSkill) {

                                // se a qtd de vagas igual ao max então sai
                                if (count($skillClientVacanciesIds) == $skillClientVacanciesMax) {
                                    continue;
                                }

                                // busca uma vacancySkill que possua a skill sorteada do cliente,
                                // e que não seja uma vaga já selecionada
                                $vacancySkill = VacancySkill::where('skill_id', $clientSkill->skill_id)
                                    ->where('active', true)
                                    ->whereNotIn('vacancy_id', $skillClientVacanciesIds)
                                    ->whereNotIn('vacancy_id', $interestClientVacanciesIds)
                                    ->whereNotIn('vacancy_id', $clientApprovedVacanciesIds)
                                    ->inRandomOrder()
                                    ->first();

                                // se retornar um vacancySkill então
                                if (!is_null($vacancySkill)) {

                                    // salva o id da vaga para não ser selecionado na proxima repetição do laço
                                    $skillClientVacanciesIds[] = $vacancySkill->vacancy_id;
                                }
                            }
                            $qtdTries++;
                        }

                        // busca as vagas com os ids selecionados
                        $skillClientVacancies = Vacancy::whereIn('id', $skillClientVacanciesIds)
                            ->where('active', true)
                            ->inRandomOrder()
                            ->get();
                        $qtdskillClientVacancies = count($skillClientVacancies);
                    }
                }
            }

            // sugestão formações
            {
                // busca as formações finalizadas pelo candidato
                $clientFinishFormations = ClientFormation::where('client_id', session('client')->id)->where('finish', true)->get();

                // salva o id das formações finalizadas
                $clientFinishFormationsIds = [];
                foreach ($clientFinishFormations as $clientFinishFormation)
                    $clientFinishFormationsIds[] = $clientFinishFormation->formation_id;

                // com base nos interesses do client (skills atribuidas de forma automatica)
                {

                    // busco todas as skills do candidato com client_assigned false
                    $clientSkills = ClientSkill::where('client_assigned', false)->inRandomOrder()->get();

                    // se não for null entao possui skills automaticas
                    if ($clientSkills->isNotEmpty()) {

                        $interestClientFormationsIds = [];
                        $interesetClientFormationsMax = 3;
                        $qtdTries = 0;

                        // enquanto  não atingiu o limite
                        while (count($interestClientFormationsIds) < $interesetClientFormationsMax) {

                            // se atingir o maximo sair do while
                            if ($qtdTries == $interesetClientFormationsMax) {
                                break;
                            }

                            // para cada posição selecionada aleatoriamente
                            foreach ($clientSkills as $clientSkill) {

                                // se a qtd de vagas igual ao max então sai
                                if (count($interestClientFormationsIds) == $interesetClientFormationsMax) {
                                    continue;
                                }

                                // busca uma formationSkill que possua a skill sorteada do cliente,
                                // que não seja uma vaga já selecionada, e que o cliente não finalizou
                                $formationSkill = FormationSkill::where('skill_id', $clientSkill->skill_id)
                                    ->where('active', true)
                                    ->whereNotIn('formation_id', $interestClientFormationsIds)
                                    ->whereNotIn('formation_id', $clientFinishFormationsIds)
                                    ->inRandomOrder()
                                    ->first();

                                // se retornar um formationSkill então
                                if (!is_null($formationSkill)) {

                                    // salva o id da formação para não ser selecionado na proxima repetição do laço
                                    $interestClientFormationsIds[] = $formationSkill->formation_id;
                                }
                            }
                            $qtdTries++;
                        }

                        // busca as vagas com os ids selecionados
                        $interestClientFormations = Formation::whereIn('id', $interestClientFormationsIds)
                            ->where('active', true)
                            ->inRandomOrder()
                            ->get();
                        $qtdinterestClientFormations = count($interestClientFormations);
                    }
                }

                // com base nos interesses do client (skills atribuidas de forma manual)
                {

                    // busco todas as skills do candidato com client_assigned true
                    $clientSkills = ClientSkill::where('client_assigned', true)->inRandomOrder()->get();

                    // se não for null entao possui skills automaticas
                    if ($clientSkills->isNotEmpty()) {

                        $skillClientFormationsIds = [];
                        $skillClientFormationsMax = 3;
                        $qtdTries = 0;

                        // enquanto  não atingiu o limite
                        while (count($skillClientFormationsIds) < $skillClientFormationsMax) {

                            // se atingir o maximo sair do while
                            if ($qtdTries == $skillClientFormationsMax) {
                                break;
                            }

                            // para cada posição selecionada aleatoriamente
                            foreach ($clientSkills as $clientSkill) {

                                // se a qtd de vagas igual ao max então sai
                                if (count($skillClientFormationsIds) == $skillClientFormationsMax) {
                                    continue;
                                }

                                // busca uma formationSkill que possua a skill sorteada do cliente, e que não seja uma vaga já selecionada
                                $formationSkill = FormationSkill::where('skill_id', $clientSkill->skill_id)
                                    ->where('active', true)
                                    ->whereNotIn('formation_id', $skillClientFormationsIds)
                                    ->whereNotIn('formation_id', $interestClientFormationsIds)
                                    ->whereNotIn('formation_id', $clientFinishFormationsIds)
                                    ->inRandomOrder()
                                    ->first();

                                // se retornar um vacancySkill então
                                if (!is_null($formationSkill)) {

                                    // salva o id da vaga para não ser selecionado na proxima repetição do laço
                                    $skillClientFormationsIds[] = $formationSkill->formation_id;
                                }
                            }
                            $qtdTries++;
                        }

                        // busca as vagas com os ids selecionados
                        $skillClientFormations = Formation::whereIn('id', $skillClientFormationsIds)
                            ->inRandomOrder()
                            ->where('active', true)
                            ->get();
                        $qtdskillClientFormations = count($skillClientFormations);
                    }
                }
            }
        }

        // todas as vagas ativas
        $vacancies = Vacancy::where('active', true)
            ->whereNotIn('id', $skillClientVacanciesIds)
            ->whereNotIn('id', $interestClientVacanciesIds)
            ->inRandomOrder()
            ->get();

        // todas as formações ativas
        $formations = Formation::where('active', true)
            ->whereNotIn('id', $skillClientFormationsIds)
            ->whereNotIn('id', $interestClientFormationsIds)
            ->inRandomOrder()
            ->get();

        return view(
            'home.index',
            [
                'alertMessage' => $alertMessage,

                // vagas sugeridas com base nas skills atribuidas de forma automatica
                'interestClientVacancies' => $interestClientVacancies,
                'qtdinterestClientVacancies' => $qtdinterestClientVacancies,
                // vagas sugeridas com base nas skills atribuidas de forma manual
                'skillClientVacancies' => $skillClientVacancies,
                'qtdskillClientVacancies' => $qtdskillClientVacancies,

                // formações sugeridas com base nas skills atribuidas de forma automatica
                'interestClientFormations' => $interestClientFormations,
                'qtdinterestClientFormations' => $qtdinterestClientFormations,
                // formações sugeridas com base nas skills atribuidas de forma manual
                'skillClientFormations' => $skillClientFormations,
                'qtdskillClientFormations' => $qtdskillClientFormations,

                'vacancies' => $vacancies,
                'qtdVacancies' => count($vacancies),

                'formations' => $formations,
                'qtdFormations' => count($formations)
            ]
        );
    }

    public function search(Request $request)
    {
        if ($request->search == '') {
            return $this->index();
        }

        // todas as vagas ativas
        $vacancies = Vacancy::where('active', true)->where('name', 'LIKE', '%' . $request->search . '%')->get();

        // todas as formações ativas
        $formations = Formation::where('active', true)->where('name', 'LIKE', '%' . $request->search . '%')->get();

        return view(
            'home.index',
            [
                // vagas sugeridas com base nas skills atribuidas de forma automatica
                'interestClientVacancies' => null,
                'qtdinterestClientVacancies' => null,
                // vagas sugeridas com base nas skills atribuidas de forma manual
                'skillClientVacancies' => null,
                'qtdskillClientVacancies' => null,

                // formações sugeridas com base nas skills atribuidas de forma automatica
                'interestClientFormations' => null,
                'qtdinterestClientFormations' => null,
                // formações sugeridas com base nas skills atribuidas de forma manual
                'skillClientFormations' => null,
                'qtdskillClientFormations' => null,

                'vacancies' => $vacancies,
                'qtdVacancies' => count($vacancies),

                'formations' => $formations,
                'qtdFormations' => count($formations)
            ]
        );
    }

    public function showFormation(int $formation_id)
    {
        $formation = Formation::where('id', $formation_id)->first();

        $plan = Plan::where('id', $formation->plan_id)->first();

        $companyFormations = CompanyFormation::where('formation_id', $formation->id)->get();
        $companies = '';
        foreach ($companyFormations as $companyFormation) {
            $company = Company::where('id', $companyFormation->company_id)->first();
            $user = User::where('id', $company->user_id)->first();
            $companies = $companies . ' </br> - ' . $user->name;
        }

        $formationTutors = FormationTutor::where('formation_id', $formation->id)->get();
        $tutors = '';
        foreach ($formationTutors as $formationTutor) {
            $tutor = Tutor::where('id', $formationTutor->tutor_id)->first();
            $user = User::where('id', $tutor->user_id)->first();
            $tutors = $tutors . ' </br> - ' . $user->name;
        }

        $formationSkills = FormationSkill::where('formation_id', $formation->id)->get();
        $skills = '';
        foreach ($formationSkills as $formationSkill) {
            $skill = Skill::where('id', $formationSkill->skill_id)->first();
            $skills = $skills . ' </br> - ' . $skill->name;
        }

        return view('home.showFormation', [
            'formation' => $formation,
            'plan' => $plan,
            'companies' => $companies,
            'tutors' => $tutors,
            'skills' => $skills
        ]);
    }

    public function showVacancy(int $vacancy_id, $alertMessage = null)
    {
        $vacancy = Vacancy::where('id', $vacancy_id)->first();
        $statusVacancy = StatusVacancy::where('id', $vacancy->status_vacancy_id)->first();

        $company = Company::where('id', $vacancy->company_id)->first();
        $user = User::where('id', $company->user_id)->first();

        return view('home.showVacancy', [
            'alertMessage' => $alertMessage,
            'vacancy' => $vacancy,
            'user' => $user,
            'statusVacancy' => $statusVacancy,
            'vacancyFormationsNotRequired' => VacancyFormation::where('active', true)->where('vacancy_id', $vacancy->id)->where('required', false)->get(),
            'vacancyFormationsRequired' => VacancyFormation::where('active', true)->where('vacancy_id', $vacancy->id)->where('required', true)->get(),
            'vacancySkills' => VacancySkill::where('active', true)->where('vacancy_id', $vacancy->id)->get()
        ]);
    }

    // salva o cliente como candidato para vaga
    public function storeCandidate(int $vacancy_id)
    {
        $vacancy = Vacancy::where('id', $vacancy_id)->first();

        if ($vacancy->status_vacancy_id > 2) {
            return $this->showVacancy($vacancy->id, 'subFailStatus');
        }

        // busca todas as formações obrigatórias para a vaga
        $vacancyFormations = VacancyFormation::where('vacancy_id', $vacancy->id)->where('required', true)->get();

        foreach ($vacancyFormations as $vacancyFormation) {
            // verifica se o cliente da sessão possui a formação finalizada
            $clientFormation = ClientFormation::where('formation_id', $vacancyFormation->formation_id)->where('client_id', session('client')->id)->where('finish', true)->first();

            // se não retorna erro
            if (is_null($clientFormation)) {
                return $this->showVacancy($vacancy->id, 'subFail');
            }
        }

        // salvando o cliente da sessão como candidato para a vaga
        $candidate = new Candidate();
        $candidate->client_id = session('client')->id;
        $candidate->vacancy_id = $vacancy_id;
        $candidate->active = true;
        $candidate->save();

        // criar historico de ingressão do candidato para a vaga
        $candidateHistory = new CandidateHistory();
        $candidateHistory->candidate_id = $candidate->id;
        $candidateHistory->description = "Ingressou na vaga";
        $candidateHistory->save();

        // salvando que existe novos candidatos na vaga
        $vacancy->new_candidate = true;
        $vacancy->save();

        // retorna sucesso
        return $this->showVacancy($vacancy->id, 'subSucess');
    }

    // inscreve o cliente para formação
    public function storeClient(int $formation_id)
    {
        $clientFormation = new ClientFormation();
        $clientFormation->client_id = session('client')->id;
        $clientFormation->formation_id = $formation_id;
        $clientFormation->save();

        $formation = Formation::where('id', $formation_id)->first();
        $plan = Plan::where('id', $formation->plan_id)->first();
        $companyFormations = CompanyFormation::where('formation_id', $formation->id)->get();
        $companies = '';

        foreach ($companyFormations as $companyFormation) {
            $company = Company::where('id', $companyFormation->company_id)->first();
            $user = User::where('id', $company->user_id)->first();
            $companies = $companies . ' </br> - ' . $user->name;
        }

        $formationTutors = FormationTutor::where('formation_id', $formation->id)->get();
        $tutors = '';
        foreach ($formationTutors as $formationTutor) {
            $tutor = Tutor::where('id', $formationTutor->tutor_id)->first();
            $user = User::where('id', $tutor->user_id)->first();
            $tutors = $tutors . ' </br> - ' . $user->name;
        }

        $formationSkills = FormationSkill::where('formation_id', $formation->id)->get();
        $skills = '';
        foreach ($formationSkills as $formationSkill) {
            $skill = Skill::where('id', $formationSkill->skill_id)->first();
            $skills = $skills . ' </br> - ' . $skill->name;
        }

        return view('home.showFormation', [
            'alertMessage' => 'subSucess',
            'formation' => $formation,
            'plan' => $plan,
            'companies' => $companies,
            'tutors' => $tutors,
            'skills' => $skills
        ]);
    }
}
