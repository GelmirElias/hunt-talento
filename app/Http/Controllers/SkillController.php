<?php

namespace App\Http\Controllers;

use App\Models\ClientSkill;
use App\Models\Skill;
use Illuminate\Http\Request;

class SkillController extends Controller
{

    public function showAll()
    {
        return view(
            'admin.skill.showAll',
            [
                'skills' => Skill::all()
            ]
        );
    }

    public function store(Request $request)
    {
        //validando se nome já existe
        $skill = Skill::where('name', $request->name)->first();
        if ($skill != null) {
            return redirect()->route('admin/skill')->with('fail', 'Nome já cadastrado');
        }

        $skill = new Skill();
        $skill->name = $request->name;
        if ($request->description != null)
            $skill->description = $request->description;
        else
            $skill->description = 'Descrição ' . $request->name;
        $skill->active = $request->active;
        $skill->save();

        return redirect()->route('admin/skill')->with('success', 'Cadastrado com sucesso');
    }

    public function edit(int $skill_id)
    {
        return view(
            'admin.skill.showAll',
            [
                'skills' => Skill::all(),
                'skillEdit' => (Skill::where('id', $skill_id)->first())
            ]
        );
    }

    public function update(Request $request, int $skill_id)
    {
        //validando se nome já existe
        $skill = Skill::where('name', $request->nameEdit)->first();
        if ($skill != null) {
            if ($skill->id != $skill_id)
                return redirect()->back()->with('fail', 'Nome já cadastrado');
        }

        $skill = Skill::where('id', $skill_id)->first();
        $skill->name = $request->nameEdit;
        if ($request->descriptionEdit != null)
            $skill->description = $request->descriptionEdit;
        else
            $skill->description = 'Descrição ' . $request->nameEdit;
        $skill->active = $request->activeEdit;
        $skill->save();

        return redirect()->route('admin/skill')->with('success', 'Editado com sucesso');
    }

    public function active(int $id)
    {
        $skill = Skill::where('id', $id)->first();
        if ($skill->active == 0)
            $skill->active = 1;
        else
            $skill->active = 0;
        $skill->save();

        return redirect()->route('admin/skill')->with('success', 'Status editado com sucesso');
    }
}
