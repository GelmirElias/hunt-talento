<?php

namespace App\Http\Controllers;

use App\Models\Level;
use Illuminate\Http\Request;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('level.index' , []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('level.create' , []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $level = new Level();
        $level->name = $request->name;
        $level->description = $request->description;
        $level->necessary_points = $request->necessary_points;
        $level->active = $request->active;
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $requestPhoto = $request->photo;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/levels'), $imagemName);
            $level->photo = $imagemName;
        }
        $level->save();
        return redirect("level");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function show(Level $level)
    {
        //
    }

    public function showAll()
    {
        $levels = Level::all();
        return view('level.showAll' , ["levels" => $levels]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function edit(Level $level)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Level $level)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function destroy(Level $level)
    {
        //
    }
}
