<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Company;
use App\Models\HiringHistory;
use App\Models\User;
use Illuminate\Http\Request;

class HiringHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('hiringHistory.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::all();
        $users = User::all();
        $companies = Company::all();
        return view('hiringHistory.create', ['companies' => $companies, 'clients' => $clients, 'users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hiringHistory = new HiringHistory();
        $hiringHistory->client_id = $request->client_id;
        $hiringHistory->company_id = $request->company_id;
        $hiringHistory->start_date = $request->start_date;
        $hiringHistory->end_date = $request->end_date;
        $hiringHistory->role = $request->role;
        $hiringHistory->description = $request->description;
        $hiringHistory->active = $request->active;
        $hiringHistory->save();
        return redirect("hiringHistory");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HiringHistory  $hiringHistory
     * @return \Illuminate\Http\Response
     */
    public function show(HiringHistory $hiringHistory)
    {
        //
    }

    public function showAll()
    {
        $hiringHistories = HiringHistory::all();
        return view('hiringHistory.showAll' , ["hiringHistories" => $hiringHistories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HiringHistory  $hiringHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(HiringHistory $hiringHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HiringHistory  $hiringHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HiringHistory $hiringHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HiringHistory  $hiringHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(HiringHistory $hiringHistory)
    {
        //
    }
}
