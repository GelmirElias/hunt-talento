<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\ClientSkill;
use App\Models\Skill;
use App\Models\User;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isEmpty;

class ClientSkillController extends Controller
{
    public function index()
    {
        $skillsCreate = Skill::where('active', true)->get();
        $clientSkills = ClientSkill::where('client_id', session('client')->id)->where('client_assigned', true)->get();

        return view('client.skill.index', ['clientSkills' => $clientSkills, 'skillsCreate' => $skillsCreate]);
    }

    public function store(Request $request)
    {
        // verifica se o cliente já possui o talento
        $clientSkill = ClientSkill::where('skill_id', $request->skillCreate)->where('client_id', session('client')->id)->where('client_assigned', true)->first();

        // se o candidato ja tiver o talento
        if (!is_null($clientSkill)) {

            // deletar ela
            $clientSkill->delete();
        }

        // criar um novo relacionamento
        $clientSkill = new ClientSkill();
        $clientSkill->client_id = session('client')->id;
        $clientSkill->skill_id = $request->skillCreate;
        $clientSkill->rate = $request->rateCreate;
        $clientSkill->client_assigned = true;
        $clientSkill->save();

        return redirect()->back()->with('success', 'Habilidade salva com sucesso');
    }

    public function edit(int $client_skill_id)
    {
        $clientSkillEdit = ClientSkill::where('id', $client_skill_id)->first();

        return redirect()->back()->with([
            'clientSkillEdit' => $clientSkillEdit
        ]);
    }

    public function update(Request $request, int $client_skill_id)
    {
        $clientSkillEdit = ClientSkill::where('id', $client_skill_id)->first();
        $clientSkillEdit->rate = $request->rateEdit;
        $clientSkillEdit->save();

        return redirect()->back()->with('success', 'Habilidade editada com sucesso');
    }

    public function destroy(int $client_skill_id)
    {
        ClientSkill::where('id', $client_skill_id)->delete();

        return redirect()->back()->with('success', 'Habilidade removida com sucesso');
    }
}
