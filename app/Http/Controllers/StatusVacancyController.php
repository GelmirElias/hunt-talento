<?php

namespace App\Http\Controllers;

use App\Models\StatusVacancy;
use Illuminate\Http\Request;

class StatusVacancyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('statusVacancy.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('statusVacancy.create' , []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $statusVacancy = new StatusVacancy();
        $statusVacancy->name = $request->name;
        $statusVacancy->description = $request->description;
        $statusVacancy->active = $request->active;
        $statusVacancy->save();
        return redirect("statusVacancy");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StatusVacancy  $statusVacancy
     * @return \Illuminate\Http\Response
     */
    public function show(StatusVacancy $statusVacancy)
    {
        //
    }

    public function showAll()
    {
        $statusVacancies = StatusVacancy::all();
        return view('statusVacancy.showAll' , ["statusVacancies" => $statusVacancies]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StatusVacancy  $statusVacancy
     * @return \Illuminate\Http\Response
     */
    public function edit(StatusVacancy $statusVacancy)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StatusVacancy  $statusVacancy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StatusVacancy $statusVacancy)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StatusVacancy  $statusVacancy
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatusVacancy $statusVacancy)
    {
        //
    }
}
