<?php

namespace App\Http\Controllers;

use App\Models\Formation;
use App\Models\FormationTutor;
use App\Models\Tutor;
use App\Models\User;
use Illuminate\Http\Request;

class FormationTutorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('formationTutor.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tutors = Tutor::all();
        $users = User::all();
        $formations = Formation::all();
        return view('formationTutor.create', ['formations' => $formations, 'tutors' => $tutors, 'users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formationTutor = new FormationTutor();
        $formationTutor->tutor_id = $request->tutor_id;
        $formationTutor->formation_id = $request->formation_id;
        $formationTutor->save();
        return redirect("formationTutor");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FormationTutor  $formationTutor
     * @return \Illuminate\Http\Response
     */
    public function show(FormationTutor $formationTutor)
    {
        //
    }

    public function showAll()
    {
        $formationTutors = FormationTutor::all();
        return view('formationTutor.showAll' , ["formationTutors" => $formationTutors]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FormationTutor  $formationTutor
     * @return \Illuminate\Http\Response
     */
    public function edit(FormationTutor $formationTutor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FormationTutor  $formationTutor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FormationTutor $formationTutor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FormationTutor  $formationTutor
     * @return \Illuminate\Http\Response
     */
    public function destroy(FormationTutor $formationTutor)
    {
        //
    }
}
