<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use App\Models\CandidateComment;
use App\Models\CandidateDocument;
use App\Models\CandidateHistory;
use App\Models\ClientSkill;
use App\Models\Formation;
use App\Models\FormationSkill;
use App\Models\HiringHistory;
use App\Models\Interview;
use App\Models\Skill;
use App\Models\StatusCandidate;
use App\Models\StatusVacancy;
use App\Models\Vacancy;
use App\Models\VacancyFormation;
use App\Models\VacancySkill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CompanyVacancyController extends Controller
{
    public function showAll()
    {
        return view(
            'company.vacancy.showAll',
            [
                'statusVacancies' => StatusVacancy::where('active', true)->get(),
                'formations' => Formation::where('active', true)->get(),
                'skills' => Skill::where('active', true)->get()
            ]
        );
    }

    public function storeVacancy(Request $request)
    {
        // criando a vaga
        $vacancy = new Vacancy();

        // definindo os atributos passados pelo formulário
        $vacancy->name = $request->name;
        $vacancy->description = $request->description;
        $vacancy->start_salary = $request->startSalary;
        $vacancy->end_salary = $request->endSalary;
        $vacancy->quantity = $request->quantity;
        $vacancy->active = $request->active;
        $vacancy->status_vacancy_id = 1;

        // salvando a vaga
        $vacancy->company_id = session('company')->id;
        $vacancy->save();

        // salvando a foto
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $requestPhoto = $request->photo;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/vacancies'), $imagemName);
            $vacancy->photo = $imagemName;
        }

        // salvar as formações recomendadas
        if (!is_null($request->vacancyFormationsNotRequired)) {

            // para cada formação id
            foreach ($request->vacancyFormationsNotRequired as $formationId) {

                // criando relacionamento da formação com a vaga
                $vacancyFormation = new VacancyFormation();
                $vacancyFormation->formation_id = $formationId;
                $vacancyFormation->vacancy_id = $vacancy->id;
                $vacancyFormation->required = false;
                $vacancyFormation->save();

                // buscando os talentos que a formação possui
                $formationSkills = FormationSkill::where('formation_id', $formationId)->where('active', true)->get();

                // para cada formation skill
                foreach ($formationSkills as $formationSkill) {

                    // salvando os novos talentos da formação na vaga
                    $vacancySkill = new VacancySkill();
                    $vacancySkill->skill_id = $formationSkill->skill_id;
                    $vacancySkill->vacancy_id = $vacancy->id;
                    $vacancySkill->save();
                }
            }
        }

        // salvar as formações obrigatorias
        if (!is_null($request->vacancyFormationsRequired)) {

            foreach ($request->vacancyFormationsRequired as $formationId) {

                // criando relacionamento da formação com a vaga
                $vacancyFormation = new VacancyFormation();
                $vacancyFormation->formation_id = $formationId;
                $vacancyFormation->vacancy_id = $vacancy->id;
                $vacancyFormation->required = true;
                $vacancyFormation->save();

                // buscando os talentos que a formação possui
                $formationSkills = FormationSkill::where('formation_id', $formationId)->where('active', true)->get();

                // para cada formation skill
                foreach ($formationSkills as $formationSkill) {

                    // salvando os novos talentos da formação na vaga
                    $vacancySkill = new VacancySkill();
                    $vacancySkill->skill_id = $formationSkill->skill_id;
                    $vacancySkill->vacancy_id = $vacancy->id;
                    $vacancySkill->save();
                }
            }
        }

        // salvar os talentos
        if (!is_null($request->vacancySkills)) {
            // criando relacionamentos de formação
            foreach ($request->vacancySkills as $skillId) {
                $vacancySkill = new VacancySkill();
                $vacancySkill->skill_id = $skillId;
                $vacancySkill->vacancy_id = $vacancy->id;
                $vacancySkill->save();
            }
        }

        // remove os talentos repetidos
        DB::delete('delete `t1` from `vacancy_skills` as `t1`
            inner join `vacancy_skills` as `t2` on `t1`.`id` < `t2`.`id`
            where `t1`.`vacancy_id` = `t2`.vacancy_id and `t1`.`skill_id` = `t2`.skill_id;');

        // retorna sucesso ao salvar a vaga
        return redirect()->back()->with('success', 'Vaga salva com sucesso');
    }

    public function editVacancy(int $vacancy_id)
    {
        $vacancyEdit = Vacancy::where('id', $vacancy_id)->where('active', true)->first();

        // retorna vaga para edição
        return redirect()->back()->with('vacancyEdit', $vacancyEdit);
    }

    public function updateVacancy(Request $request, int $id_vacancy)
    {
        $vacancy = Vacancy::where('id', $id_vacancy)->first();

        // editando a vaga
        $vacancy->name = $request->nameEdit;
        $vacancy->description = $request->descriptionEdit;
        $vacancy->start_salary = $request->startSalaryEdit;
        $vacancy->end_salary = $request->endSalaryEdit;
        $vacancy->quantity = $request->quantityEdit;
        $vacancy->active = $request->activeEdit;

        // salvar a foto
        if ($request->hasFile('photoEdit') && $request->file('photoEdit')->isValid()) {
            // verifica se ele tem uma foto para deletar
            if (!is_null($vacancy->photo))
                unlink('img/vacancies/' . $vacancy->photo);
            $requestPhoto = $request->photoEdit;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/vacancies'), $imagemName);
            $vacancy->photo = $imagemName;
        }

        // salvando a vaga
        $vacancy->save();

        // deletando relacionamento antigos de talentos
        VacancySkill::where('vacancy_id', $vacancy->id)->delete();

        // deletando relacionamento antigos de formações
        VacancyFormation::where('vacancy_id', $vacancy->id)->delete();

        // salvar as formações recomendadas
        if (!is_null($request->vacancyFormationsNotRequiredEdit)) {

            foreach ($request->vacancyFormationsNotRequiredEdit as $formationId) {

                // criando novos relacionamentos de formação
                $vacancyFormation = new VacancyFormation();
                $vacancyFormation->formation_id = $formationId;
                $vacancyFormation->vacancy_id = $vacancy->id;
                $vacancyFormation->required = false;
                $vacancyFormation->save();

                // buscando os talentos que a formação possui
                $formationSkills = FormationSkill::where('formation_id', $formationId)->where('active', true)->get();

                // para cada formation skill
                foreach ($formationSkills as $formationSkill) {

                    // salvando os novos talentos da formação na vaga
                    $vacancySkill = new VacancySkill();
                    $vacancySkill->skill_id = $formationSkill->skill_id;
                    $vacancySkill->vacancy_id = $vacancy->id;
                    $vacancySkill->save();
                }
            }
        }

        // salvar as formações obrigatorias
        if (!is_null($request->vacancyFormationsRequiredEdit)) {

            foreach ($request->vacancyFormationsRequiredEdit as $formationId) {

                // criando novos relacionamentos de formação
                $vacancyFormation = new VacancyFormation();
                $vacancyFormation->formation_id = $formationId;
                $vacancyFormation->vacancy_id = $vacancy->id;
                $vacancyFormation->required = true;
                $vacancyFormation->save();

                // buscando os talentos que a formação possui
                $formationSkills = FormationSkill::where('formation_id', $formationId)->where('active', true)->get();

                // para cada formation skill
                foreach ($formationSkills as $formationSkill) {

                    // salvando os novos talentos da formação na vaga
                    $vacancySkill = new VacancySkill();
                    $vacancySkill->skill_id = $formationSkill->skill_id;
                    $vacancySkill->vacancy_id = $vacancy->id;
                    $vacancySkill->save();
                }
            }
        }

        // salvar os talentos
        if (!is_null($request->vacancySkillsEdit)) {

            // criando novos relacionamentos de formação
            foreach ($request->vacancySkillsEdit as $skillId) {
                $vacancySkill = new VacancySkill();
                $vacancySkill->skill_id = $skillId;
                $vacancySkill->vacancy_id = $vacancy->id;
                $vacancySkill->save();
            }
        }

        // remove os talentos repetidos
        DB::delete('delete `t1` from `vacancy_skills` as `t1`
            inner join `vacancy_skills` as `t2` on `t1`.`id` < `t2`.`id`
            where `t1`.`vacancy_id` = `t2`.vacancy_id and `t1`.`skill_id` = `t2`.skill_id;');

        // retorna sucesso ao editar a vaga
        return redirect()->back()->with('success', 'Vaga editada com sucesso');
    }

    public function destroyVacancy(int $id_vacancy)
    {
        // buscando a vaga
        $vacancy = Vacancy::where('id', $id_vacancy)->first();

        // deletando as formações da vaga
        VacancyFormation::where('vacancy_id', $vacancy->id)->delete();

        // deletando os talentos da vaga
        VacancySkill::where('vacancy_id', $vacancy->id)->delete();

        // buscando candidatos da vaga
        $candidates = Candidate::where('vacancy_id', $id_vacancy)->get();

        // deletando os comentários dos candidatos
        foreach ($candidates as $candidate)
            CandidateComment::where('candidate_id', $candidate->id)->delete();

        // deletando os documentos dos candidatos
        foreach ($candidates as $candidate) {

            // busca os candidateDocuments
            $candidateDocuments = CandidateDocument::where('candidate_id', $candidate->id)->get();

            // deletando os documentos do candidato
            CandidateDocument::where('candidate_id', $candidate->id)->delete();

            // deleta o documento na pasta
            foreach ($candidateDocuments as $candidateDocument)
                unlink('doc/candidate/' . $candidateDocument->document);
        }

        // deletando os historicos dos candidatos
        foreach ($candidates as $candidate)
            CandidateHistory::where('candidate_id', $candidate->id)->delete();

        // deletando os candidatos
        foreach ($candidates as $candidate)
            Candidate::where('vacancy_id', $candidate->vacancy_id)->delete();

        // deletando a vaga
        Vacancy::where('id', $vacancy->id)->delete();

        // retorna sucesso ao remover a vaga
        return redirect()->back()->with('success', 'Vaga removida com sucesso');
    }

    public function backStatus(int $vacancy_id)
    {
        $vacancy = Vacancy::where('id', $vacancy_id)->first();
        $statusVacancyMin = StatusVacancy::where('active', true)->min('id');

        if ($vacancy->status_vacancy_id == $statusVacancyMin) {
            // retorna falha ao editar a vaga
            return redirect()->back()->with('fail', 'Status incorreto');
        }

        $vacancy->status_vacancy_id = $vacancy->status_vacancy_id - 1;
        $vacancy->save();

        // retorna sucesso ao editar a vaga
        return redirect()->back()->with('success', 'Status da vaga editada com sucesso');
    }

    public function goStatus(int $vacancy_id)
    {
        $vacancy = Vacancy::where('id', $vacancy_id)->first();
        $statusVacancyMax = StatusVacancy::where('active', true)->max('id');

        if ($vacancy->status_vacancy_id == $statusVacancyMax) {
            // retorna falha ao editar a vaga
            return redirect()->back()->with('fail', 'Status incorreto');
        }

        $vacancy->status_vacancy_id = $vacancy->status_vacancy_id + 1;
        $vacancy->save();

        // retorna sucesso ao editar a vaga
        return redirect()->back()->with('success', 'Status da vaga editada com sucesso');
    }

    public function show(int $vacancy_id)
    {
        $vacancy = Vacancy::where('id', $vacancy_id)->first();

        //(new UtilController)->classificationCandidate(3);

        // classificar os candidatos somente quando tiver novos
        if ($vacancy->new_candidate == 1) {

            // busca todos os candidatos da vaga
            $candidates = Candidate::where('vacancy_id', $vacancy->id)->get();

            // classifica cada um
            foreach ($candidates as $candidate) (new UtilController)->classificationCandidate($candidate->id);

            // salva que não existe novos candidatos
            $vacancy->new_candidate = false;
            $vacancy->save();
        }

        return view(
            'company.vacancy.show',
            [
                'vacancy' => $vacancy,
                'statusCandidates' => StatusCandidate::where('active', true)->get()
            ]
        );
    }

    public function showCandidate(int $candidate_id)
    {
        $candidate = Candidate::where('id', $candidate_id)->first();

        $candidateCreateInterview = null;
        $candidateEditInterview = null;
        $candidateApproved = null;
        $candidateUnapproved = null;
        $historicInterview = null;

        if ($candidate->status_candidates_id === 1) {
            $candidateCreateInterview = $candidate;
        } else if ($candidate->status_candidates_id === 2 || $candidate->status_candidates_id === 3 || $candidate->status_candidates_id === 4) {
            $candidateEditInterview = $candidate;
        } else if ($candidate->status_candidates_id === 5) {
            $candidateApproved = $candidate;
        } else if ($candidate->status_candidates_id === 6) {
            $candidateUnapproved = $candidate;
        } else if ($candidate->status_candidates_id === 7) {
            $historicInterview = $candidate;
        }

        // retorna sucesso ao salvar a entrevista
        return redirect()->back()->with([
            'candidateShowDetail' => $candidate,
            'candidateCreateInterview' => $candidateCreateInterview,
            'candidateEditInterview' => $candidateEditInterview,
            'candidateApproved' => $candidateApproved,
            'candidateUnapproved' => $candidateUnapproved,
            'historicInterview' => $historicInterview,
            'candidateComments' => CandidateComment::where('candidate_id', $candidate->id)->get(),
            'candidateHistories' => CandidateHistory::where('candidate_id', $candidate->id)->get(),
            'candidateDocuments' => CandidateDocument::where('candidate_id', $candidate->id)->get()
        ]);
    }

    public function storeInterview(Request $request, int $candidate_id)
    {
        // atualizando status do candidato para entrevista marcada
        $candidate = Candidate::where('id', $candidate_id)->first();
        $candidate->status_candidates_id = 2;
        $candidate->save();

        // salvando a entrevista
        $interview = new Interview();
        $interview->candidate_id = $candidate->id;
        $interview->start_date = $request->startDateCreate;
        $interview->end_date = $request->endDateCreate;
        $interview->description = $request->descriptionCreate;
        $interview->active = $request->activeCreate;
        $interview->save();

        // criar historico
        $statusCandidate = StatusCandidate::where('id', $candidate->status_candidates_id)->first();
        $candidateHistory = new CandidateHistory();
        $candidateHistory->candidate_id = $candidate->id;
        $candidateHistory->description = 'Status do candidato atualizado: ' . $statusCandidate->name . '. Razão: ' . $statusCandidate->description;
        $candidateHistory->save();

        // retorna sucesso ao salvar a entrevista
        return redirect()->back()->with('success', 'Entrevista salva com sucesso');
    }

    public function updateInterview(Request $request, int $candidate_id)
    {
        $candidate = Candidate::where('id', $candidate_id)->first();
        $interview = Interview::where('candidate_id', $candidate->id)->first();

        // verifica se o status do candidato foi alterado para criar historico
        if ($candidate->status_candidates_id !== $request->statusCandidateEdit) {

            // busca o status
            $statusCandidate = StatusCandidate::where('id', $request->statusCandidateEdit)->first();

            // atualiza o status do candidato
            $candidate->status_candidates_id = $statusCandidate->id;
            $candidate->save();

            // salva o historico de atualização do status do candidato
            $candidateHistory = new CandidateHistory();
            $candidateHistory->candidate_id = $candidate->id;
            $candidateHistory->description = 'Status do candidato atualizado: ' . $statusCandidate->name . '. Razão: ' . $statusCandidate->description;
            $candidateHistory->save();
        }

        // salvando a entrevista
        $interview->start_date = $request->startDateEdit;
        $interview->end_date = $request->endDateEdit;
        $interview->description = $request->descriptionEdit;
        $interview->active = $request->activeEdit;
        $interview->save();

        // retorna sucesso ao editar a entrevista
        return redirect()->back()->with('success', 'Entrevista editada com sucesso');
    }

    public function createHiringHistory(Request $request, int $candidate_id)
    {
        $candidate = Candidate::where('id', $candidate_id)->first();
        $vacancy = Vacancy::where('id', $candidate->vacancy_id)->first();

        // salva o historico de contratação do candidato
        $hiring = new HiringHistory();
        $hiring->company_id = $vacancy->company_id;
        $hiring->client_id = $candidate->client_id;
        $hiring->vacancy_id = $vacancy->id;
        $hiring->start_date = $request->startDateHiring;
        $hiring->role = $request->roleHiring;
        $hiring->description = $request->descriptionHiring;
        $hiring->salary = $request->salaryHiring;
        $hiring->active = true;
        $hiring->save();

        // busca o status contratado
        $statusCandidate = StatusCandidate::where('id', 7)->first();

        // atualizando status do candidato para contratado
        $candidate->status_candidates_id = $statusCandidate->id;
        $candidate->active = false;
        $candidate->save();

        // salva o historico de atualização do status do candidato
        $candidateHistory = new CandidateHistory();
        $candidateHistory->candidate_id = $candidate->id;
        $candidateHistory->description = 'Status do candidato atualizado: ' . $statusCandidate->name . '. Razão: ' . $statusCandidate->description;
        $candidateHistory->save();

        // adiciona os talentos da vaga no candidato
        $vacancySkills = VacancySkill::where('vacancy_id', $vacancy->id)->where('active', true)->get();

        // verifica se a formação tem skills
        if (!empty($vacancySkills)) {

            // para cada talento vinculado a vaga
            foreach ($vacancySkills as $vacancySkill) {

                // verifica se o cliente já possui o talento
                $clientSkill = ClientSkill::where('skill_id', $vacancySkill->skill_id)->where('client_id', $candidate->client_id)->where('client_assigned', false)->first();

                // se o candidato ja tiver o talento
                if (!is_null($clientSkill)) {

                    // verificar se o rate é menor que 5
                    if ($clientSkill->rate < 5) {

                        // se for incrementa o rate dele
                        $clientSkill->rate = $clientSkill->rate + 1;
                        $clientSkill->save();
                    }
                }

                // se não cria um novo relacionamento
                else {
                    $clientSkill = new ClientSkill();
                    $clientSkill->client_id = $candidate->client_id;
                    $clientSkill->skill_id = $vacancySkill->skill_id;
                    $clientSkill->rate = 1;
                    $clientSkill->save();
                }
            }

            // buscar todos as vagas que o cliente é candidato
            $candidates = Candidate::where('client_id', $candidate->client_id)->where('active', true)->get();

            // se o cliente for candidato
            if (!empty($candidates)) {

                // para cada vaga que ele é candidato
                foreach ($candidates as $candidate) {

                    // busca a vaga correspondente
                    $vacancy = Vacancy::where('id', $candidate->vacancy_id)->where('active', true)->first();

                    // se for uma vaga valida
                    if (!empty($vacancy)) {

                        // atualizar a flag de new_candidate
                        $vacancy->new_candidate = true;
                        $vacancy->save();
                    }
                }
            }
        }

        // retorna sucesso ao contratar o candidato
        return redirect()->back()->with('success', 'Candidato aprovado com sucesso');
    }

    public function createComment(Request $request, int $candidate_id)
    {
        $candidate = Candidate::where('id', $candidate_id)->first();

        $candidateComment = new CandidateComment();
        $candidateComment->candidate_id = $candidate->id;
        $candidateComment->user_id = Auth::user()->id;
        $candidateComment->description = $request->description;
        $candidateComment->save();

        // retorna sucesso ao salvar o comentario
        session(['success' => 'Comentário cadastrado com sucesso']);
        return $this->showCandidate($candidate->id);
    }

    public function destroyComment(int $candidate_comment_id)
    {
        $candidateComment = CandidateComment::where('id', $candidate_comment_id)->first();
        $candidateComment->delete();

        // retorna sucesso ao excluir comentário
        session(['success' => 'Comentário removido com sucesso']);
        return $this->showCandidate($candidateComment->candidate_id);
    }

    public function editComment(int $candidate_comment_id)
    {
        $candidateComment = CandidateComment::where('id', $candidate_comment_id)->first();

        // retorna comentário para editar
        return redirect()->back()->with('commentEdit', $candidateComment);
    }

    public function updateComment(Request $request, int $candidate_comment_id)
    {
        $candidateComment = CandidateComment::where('id', $candidate_comment_id)->first();
        $candidateComment->description = $request->descriptionEdit;
        $candidateComment->save();

        // retorna sucesso ao editar comentário
        session(['success' => 'Comentário editado com sucesso']);
        return $this->showCandidate($candidateComment->candidate_id);
    }

    public function createDocument(Request $request, int $candidate_id)
    {
        // verifica se ta chegando o arquivo de foto
        if ($request->hasFile('document') && $request->file('document')->isValid()) {

            // busca o candidato do documento
            $candidate = Candidate::where('id', $candidate_id)->first();

            // trata a imagem para ser armazenada
            $requestDocument = $request->document;
            $extension = $requestDocument->extension();
            $documentName = md5($requestDocument->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestDocument->move(public_path('/doc/candidate/'), $documentName);

            // salva o nome do documento no candidato documento
            $candidateDocument = new CandidateDocument();
            $candidateDocument->document = $documentName;
            $candidateDocument->candidate_id = $candidate->id;
            if (is_null($request->name))
                $candidateDocument->name = $requestDocument->getClientOriginalName();
            else
                $candidateDocument->name = $request->name;
            $candidateDocument->description = $request->description;
            $candidateDocument->save();

            // retorna sucesso ao salvar o documento
            session(['success' => 'Documento cadastrado com sucesso']);
            return $this->showCandidate($candidate->id);
        } else {
            // retorna falha ao não ter o arquivo
            session(['fail' => 'Documento inválido']);
            return $this->showCandidate($candidate_id);
        }
    }

    public function destroyDocument(int $candidate_document_id)
    {
        // seleciona o documento do candidato
        $candidateDocument = CandidateDocument::where('id', $candidate_document_id)->first();

        // deleta o documento da pasta
        unlink('doc/candidate/' . $candidateDocument->document);

        // deleta o relacionamento no banco
        $candidateDocument->delete();

        // retorna sucesso ao excluir documento
        session(['success' => 'Documento deletado com sucesso']);
        return $this->showCandidate($candidateDocument->candidate_id);
    }

    public function showDocument(int $candidate_document_id)
    {
        $candidateDocument = CandidateDocument::where('id', $candidate_document_id)->first();

        // retorna comentário para editar
        return redirect()->back()->with('showDocument', $candidateDocument);
    }
}
