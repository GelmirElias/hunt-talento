<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use App\Models\ClientSkill;
use App\Models\VacancySkill;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;

class UtilController extends Controller
{
    public function roundToNearestMinuteInterval($minuteInterval = 60)
    {
        $dateTime = new DateTime();
        return $dateTime->setTime($dateTime->format('H'), round($dateTime->format('i') / $minuteInterval) * $minuteInterval, 0)->format("Y-m-d H:i");
    }

    public function roundUpToMinuteInterval($minuteInterval = 60)
    {
        $dateTime = new DateTime();
        return $dateTime->setTime($dateTime->format('H'), ceil($dateTime->format('i') / $minuteInterval) * $minuteInterval, 0)->format("Y-m-d H:i");
    }

    public function roundDownToMinuteInterval($minuteInterval = 60)
    {
        $dateTime = new DateTime();
        return $dateTime->setTime($dateTime->format('H'), floor($dateTime->format('i') / $minuteInterval) * $minuteInterval, 0)->format("Y-m-d H:i");
    }

    public function extractTextFromPdf($pdfUrl)
    {
        $parser = new \Smalot\PdfParser\Parser();
        $pdf = $parser->parseFile($pdfUrl);
        $output = $pdf->getText();
        return $output;
    }

    public function removeSpaceText($output)
    {
        $output = preg_replace('/\s+/', '', $output);
        $output = strtolower($output);
        return $output;
    }

    public function languages()
    {
        $languages = [
            'JavaScript',
            'Java',
            'Python',
            'CSS',
            'PHP',
            'Ruby',
            'C++',
            'C',
            'Shell',
            'C#',
            'Objective-C',
            'R',
            'VimL',
            'Go',
            'Perl',
            'CoffeeScript',
            'TeX',
            'Swift',
            'Scala',
            'Emacs Lisp',
            'Haskell',
            'Lua',
            'Clojure',
            'Matlab',
            'Arduino',
            'Groovy',
            'Puppet',
            'Rust',
            'PowerShell',
            'Erlang',
            'Visual Basic',
            'Processing',
            'Assembly',
            'TypeScript',
            'XSLT',
            'ActionScript',
            'ASP',
            'OCaml',
            'D',
            'Scheme',
            'Dart',
            'Common Lisp',
            'Julia',
            'F#',
            'Elixir',
            'FORTRAN',
            'Haxe',
            'Racket',
            'Logos'
        ];
        return $languages;
    }

    public function markdown($text)
    {
        return str_ireplace(array("\r\n", '\r\n'), '\n', $text);
    }

    public function classificationCandidate($candidate_id)
    {
        // --------------- Verificando se o candidato é válido para classificação ---------------

        // busca o candidato
        $candidate = Candidate::where('id', $candidate_id)->first();
        if (empty($candidate) || is_null($candidate))
            return;

        // busca as skills da vaga
        $vacancySkills = VacancySkill::where('vacancy_id', $candidate->vacancy_id)->get();
        // se vaga não tiver skill, classifica ele como null
        if (empty($vacancySkills[0]) || is_null($vacancySkills[0])) {
            $candidate->classification = null;
            $candidate->save();
            return;
        }

        // busca as skills do client
        $clientSkills = ClientSkill::where('client_id', $candidate->client_id)->get();
        // se candidato não tiver skill, classifica ele como 0
        if (empty($clientSkills[0]) || is_null($clientSkills[0])) {
            $candidate->classification = 0;
            $candidate->save();
            return;
        }

        // --------------- Organizando as variaveis para classificação ---------------

        // organizar as skills do cliente com as skills da vaga (ex: "1 3 3 4 5")
        $clientSkillCommand = '';
        foreach ($vacancySkills as $vacancySkill) {
            $clientSkill = ClientSkill::where('client_id', $candidate->client_id)->where('skill_id', $vacancySkill->skill_id)->first();

            // caso o cliente não possua a skill, então seta 0
            if (empty($clientSkill) || is_null($clientSkill))
                $clientSkillCommand = $clientSkillCommand . ' 0';

            // se ele possui, então seta o rate do clientSkill
            else
                $clientSkillCommand = $clientSkillCommand . ' ' . $clientSkill->rate;
        }
        // dd($clientSkillCommand);

        // executar com python,
        // o arquivo do path public,
        // com nome de candidate_classification.py,
        // passando como parametro o rate de cada clientSkill com vinculo da vaga (ex: [1, 1, 1, 1, 1]),
        // passando como parametro a quantidade de niveis de classificação (ex: [0, 1, 2, 3, 4, 5]).
        $command = "python " . public_path('candidate_classification.py') . " " . escapeshellarg($clientSkillCommand) . " " . 5;

        // converte o path formato windowns para linux
        $command = str_replace('\\', '/', $command);

        // retorna o resultado da classificação
        $output = shell_exec($command);

        // trata o retorno para inteiro
        $output = intval(str_replace(['[', ']'], ' ', $output));
        //dd($output);

        $candidate->classification = $output;
        $candidate->save();
        return;

        //return $output;
    }
}
