<?php

namespace App\Http\Controllers;

use App\Models\HiringHistory;
use Illuminate\Http\Request;

class ClientHiringHistoryController extends Controller
{
    public function index()
    {
        return view('client.hiringHistory.index', [
            'hiringHistories' => HiringHistory::where('client_id', session('client')->id)->get()
        ]);
    }
}
