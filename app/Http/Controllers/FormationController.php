<?php

namespace App\Http\Controllers;

use App\Models\ClientFormation;
use App\Models\Company;
use App\Models\CompanyFormation;
use App\Models\CompanyTutor;
use App\Models\Formation;
use App\Models\FormationSkill;
use App\Models\FormationTutor;
use App\Models\FormationType;
use App\Models\Plan;
use App\Models\Skill;
use App\Models\Tutor;
use App\Models\User;
use App\Models\VacancyFormation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormationController extends Controller
{
    public function showAll()
    {
        return view('admin.formation.showAll', [
            'formations' => Formation::all(),
            'formationTypes' => FormationType::all(),
            'tutors' => Tutor::all(),
            'companies' => Company::all(),
            'skills' => Skill::where('active', true)->get(),
            'plans' => Plan::where('active', true)->get()
        ]);
    }

    public function store(Request $request)
    {
        //salvando a formação
        $formation = new Formation();
        $formation->name = $request->name;

        //tratar markdown
        if (!empty($request->description))
            $formation->description = (new UtilController)->markdown($request->description);
        else
            $formation->description = 'Descrição ' . $request->name;

        $formation->start_date = $request->startDate;
        $formation->end_date = $request->endDate;
        $formation->active = $request->active;
        $formation->formation_types_id = $request->formationTypeId;
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $requestPhoto = $request->photo;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/formations'), $imagemName);
            $formation->photo = $imagemName;
        }
        $formation->plan_id = $request->planId;
        $formation->save();

        //salvando todas as empresas da formação
        $companies = $request->companyId;
        if (!empty($companies)) {
            foreach ($companies as $company) {
                $companyFormation = new CompanyFormation();
                $companyFormation->formation_id = $formation->id;
                $companyFormation->company_id = $company;
                $companyFormation->save();
            }
        }

        //salvando todas os tutores da formação
        $tutors = $request->tutorId;
        if (!empty($tutors)) {
            foreach ($tutors as $tutor) {
                $formationTutor = new FormationTutor();
                $formationTutor->formation_id = $formation->id;
                $formationTutor->tutor_id = $tutor;
                $formationTutor->save();
            }
        }

        //salvando os talentos da formação
        $skills = $request->skillId;
        if (!empty($skills)) {
            foreach ($skills as $skill) {
                $formationSkill = new FormationSkill();
                $formationSkill->formation_id = $formation->id;
                $formationSkill->skill_id = $skill;
                $formationSkill->save();
            }
        }

        return redirect()->route('admin/formation')->with('success', 'Cadastrado com sucesso');
    }

    public function edit(int $formation_id)
    {
        //encontrando formação para editar
        $formationEdit = Formation::where('id', $formation_id)->first();

        //encontrando todos os relacionamentos companyFormations que a formação editada possui
        $companyFormations = CompanyFormation::where('formation_id', $formation_id)->get();
        $companiesIdEdit = array();
        foreach ($companyFormations as $companyFormation) {
            $company = Company::where('id', $companyFormation->company_id)->first();
            $companiesIdEdit[] = $company->id;
        }

        //encontrando todos os tutores que a formação editada possui
        $formationTutors = FormationTutor::where('formation_id', $formation_id)->get();
        $tutorsIdEdit = array();
        foreach ($formationTutors as $formationTutor) {
            $tutor = Tutor::where('id', $formationTutor->tutor_id)->first();
            $tutorsIdEdit[] = $tutor->id;
        }

        return view('admin.formation.showAll', [
            'formations' => Formation::all(),
            'formationEdit' => $formationEdit,
            'formationTypes' => FormationType::all(),
            'companies' => Company::all(),
            'companiesIdEdit' => $companiesIdEdit,
            'tutors' => Tutor::all(),
            'tutorsIdEdit' => $tutorsIdEdit,
            'skills' => Skill::where('active', true)->get(),
            'formationSkillsEdit' => FormationSkill::where('formation_id', $formationEdit->id)->where('active', true)->get(),
            'formationCompaniesEdit' => CompanyFormation::where('formation_id', $formationEdit->id)->get(),
            'plans' => Plan::where('active', true)->get()
        ]);
    }

    public function update(Request $request, int $id_formation)
    {
        //salvando a formação
        $formation = Formation::where('id', $id_formation)->first();
        $formation->name = $request->nameEdit;

        //tratar markdown
        if (!empty($request->descriptionEdit != null))
            $formation->description = (new UtilController)->markdown($request->descriptionEdit);
        else
            $formation->description = 'Descrição ' . $request->nameEdit;

        $formation->start_date = $request->startDateEdit;
        $formation->end_date = $request->endDateEdit;
        $formation->active = $request->activeEdit;
        $formation->formation_types_id = $request->formationTypeIdEdit;
        if ($request->hasFile('photoEdit') && $request->file('photoEdit')->isValid()) {
            // verifica se ele tem uma foto para deletar
            if (!is_null($formation->photo))
                unlink('img/formations/' . $formation->photo);
            $requestPhoto = $request->photoEdit;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/formations'), $imagemName);
            $formation->photo = $imagemName;
        }
        $formation->plan_id = $request->planIdEdit;
        $formation->save();

        //deletando todos os relacionamentos antigos de empresas da formação
        DB::table('company_formations')->where('formation_id', $formation->id)->delete();

        //salvando os novos relacionamento de empresas da formação
        $companies = $request->formationCompaniesEdit;
        if (!empty($companies)) {
            foreach ($companies as $company) {
                $companyFormation = new CompanyFormation();
                $companyFormation->formation_id = $formation->id;
                $companyFormation->company_id = $company;
                $companyFormation->save();
            }
        }

        //deletando todos os relacionamentos antigos de tutores da formação
        DB::table('formation_tutors')->where('formation_id', $id_formation)->delete();

        //salvando os novos relacionamento de tutores da formação
        $tutors = $request->tutorIdEdit;
        if (!empty($tutors)) {
            foreach ($tutors as $tutor) {
                $formationTutor = new FormationTutor();
                $formationTutor->formation_id = $formation->id;
                $formationTutor->tutor_id = $tutor;
                $formationTutor->save();
            }
        }

        //deletando todos os relacionamentos antigos de talentos da formação
        DB::table('formation_skills')->where('formation_id', $id_formation)->delete();

        //salvando os novos relacionamento de talentos da formação
        $skills = $request->formationSkillsEdit;
        if (!empty($skills)) {
            foreach ($skills as $skill) {
                $formationSkill = new FormationSkill();
                $formationSkill->formation_id = $formation->id;
                $formationSkill->skill_id = $skill;
                $formationSkill->save();
            }
        }

        return redirect()->route('admin/formation')->with('success', 'Editado com sucesso');
    }

    public function destroy(int $formation_id)
    {
        /*
        FormationTutor::where('formation_id', $formation_id)->delete();
        CompanyFormation::where('formation_id', $formation_id)->delete();
        ClientFormation::where('formation_id', $formation_id)->delete();
        VacancyFormation::where('formation_id', $formation_id)->delete();
        FormationSkill::where('formation_id', $formation_id)->delete();
        Formation::where('id', $formation_id)->delete();

        return redirect()->route('admin/formation')->with('success', 'Removido com sucesso');
        */

        $formation = Formation::where('id', $formation_id)->first();
        if ($formation->active == 0)
            $formation->active = 1;
        else
            $formation->active = 0;
        $formation->save();

        return redirect()->route('admin/formation')->with('success', 'Status editado com sucesso');
    }

    // ------------------------------------------------------------------------

    public function show(int $formation_id)
    {
        $formationDetail = Formation::where('id', $formation_id)->first();

        //encontrando todos os tutores que a formação editada possui
        $formationTutors = FormationTutor::where('formation_id', $formation_id)->get();
        $tutorsIdDetail = array();
        foreach ($formationTutors as $formationTutor) {
            $tutor = Tutor::where('id', $formationTutor->tutor_id)->first();
            $tutorsIdDetail[] = $tutor->id;
        }

        return view('admin.formation.show', [
            'formationDetail' => $formationDetail,
            'tutorsIdDetail' => $tutorsIdDetail,
            'formationSkillsDetail' => FormationSkill::where('formation_id', $formationDetail->id)->where('active', true)->get()
        ]);
    }

    public function showUpdate(Request $request, int $formation_id)
    {
        $formationEdit = Formation::where('id', $formation_id)->first();

        //tratar markdown
        if (!empty($request->descriptionEdit))
            $formationEdit->description = (new UtilController)->markdown($request->descriptionEdit);

        if (!empty($request->contentEdit))
            $formationEdit->content = (new UtilController)->markdown($request->contentEdit);


        $formationEdit->save();

        //encontrando todos os tutores que a formação editada possui
        $formationTutors = FormationTutor::where('formation_id', $formation_id)->get();
        $tutorsIdDetail = array();
        foreach ($formationTutors as $formationTutor) {
            $tutor = Tutor::where('id', $formationTutor->tutor_id)->first();
            $tutorsIdDetail[] = $tutor->id;
        }

        return redirect()->back()->with('success', 'Formação editada com sucesso');
    }

    // ---------------------------------------------------------------------------------------------

    public function getAllFormationForCompanyAuth()
    {
        $companyFormations = CompanyFormation::where('company_id', session('company')->id)->get();

        $formations = array();
        foreach ($companyFormations as $companyFormation) {
            $formations[] = Formation::where('id', $companyFormation->formation_id)->first();
        }
        return $formations;
    }

    public function getAllFormationForTutorAuth()
    {
        $companyFormations = FormationTutor::where('tutor_id', session('tutor')->id)->get();

        $formations = array();
        foreach ($companyFormations as $companyFormation) {
            $formations[] = Formation::where('id', $companyFormation->formation_id)->first();
        }
        return $formations;
    }

    public function getAllFormationForClientAuth()
    {
        $clientFormations = ClientFormation::where('client_id', session('client')->id)->get();

        $formations = array();
        foreach ($clientFormations as $clientFormation) {
            $formations[] = Formation::where('id', $clientFormation->formation_id)->first();
        }
        return $formations;
    }

    public function getAllFormationForClientId($client_id)
    {
        $clientFormations = ClientFormation::where('client_id', $client_id)->get();

        $formations = array();
        foreach ($clientFormations as $clientFormation) {
            $formations[] = Formation::where('id', $clientFormation->formation_id)->first();
        }
        return $formations;
    }
}
