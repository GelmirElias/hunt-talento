<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index($alertMessage = null)
    {
        return view(
            'admin.profile.index',
            [
                'alertMessage' => $alertMessage,
                'user' => User::where('id', Auth::user()->id)->first(),
                'permissions' => Permission::where('active', true)->get()
            ]
        );
    }

    public function updatePhoto(Request $request)
    {
        // verifica se ta chegando o arquivo de foto
        if ($request->hasFile('photoEdit') && $request->file('photoEdit')->isValid()) {

            // busca o objeto do usuario da sessão
            $user = User::where('id', Auth::user()->id)->first();

            // verifica se ele tem uma foto para deletar
            if (!is_null($user->photo))
                unlink('img/admins/' . $user->photo);

            // trata a imagem para ser armazenada
            $requestPhoto = $request->photoEdit;
            $extension = $requestPhoto->extension();
            $imagemName = md5($requestPhoto->getClientOriginalName() . strtotime('now') . '.' . $extension);
            $requestPhoto->move(public_path('img/admins'), $imagemName);

            // salva o nome da imagem no usuario da sessão
            $user->photo = $imagemName;
            $user->save();

            // retorna sucesso ao salvar a imagem
            return redirect()->back()->with('success', 'Imagem salva com sucesso');
        } else {
            // retorna falha ao não ter o arquivo de foto
            return $this->index('fail');
        }
    }

    public function updatePassword(Request $request)
    {
        // verifica se as duas senhas são iguais
        if ($request->newPassword != $request->newPasswordConfirm) {
            return redirect()->back()->with('fail', 'Novas senhas não correspondem');
        }

        // busca o objeto do usuario da sessão
        $user = User::where('id', Auth::user()->id)->first();

        // verifica se a senha é a mesma da antiga
        if (!Hash::check($request->oldPassword, $user->password)) {
            return redirect()->back()->with('fail', 'Senha antiga não corresponde');
        }

        // salva a nova senha criptografada
        $user->password = bcrypt($request->newPassword);
        $user->save();

        // retorna sucesso ao salvar a senha
        return redirect()->back()->with('success', 'Senha alterada com sucesso');
    }

    public function updateActive(Request $request)
    {
        // busca o objeto do usuario da sessão
        $user = User::where('id', Auth::user()->id)->first();

        // verifica se a senha é a mesma da antiga
        if (!Hash::check($request->password, $user->password)) {
            return redirect()->back()->with('fail', 'Senha não corresponde');
        }

        // salva a nova senha criptografada
        $user->active = !$user->active;
        $user->save();

        // retorna sucesso ao salvar a senha
        if ($user->active == false) {
            return (new LoginController)->signOut();
        }
    }
}
