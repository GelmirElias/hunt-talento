//cpfcnpj mask
var options = {
    onKeyPress: function (cpf, ev, el, op) {
        var masks = ['000.000.000-000', '00.000.000/0000-00'];
        $('#cpfcnpj').mask((cpf.length > 14) ? masks[1] : masks[0], op);
    }
}
$('#cpfcnpj').length > 11 ? $('#cpfcnpj').mask('00.000.000/0000-00', options) : $('#cpfcnpj').mask('000.000.000-00#', options);

var options = {
    onKeyPress: function (cpf, ev, el, op) {
        $('#cpf').mask('000.000.000-00', op);
        $('#cpfEdit').mask('000.000.000-00', op);
        $('#cnpj').mask('00.000.000/0000-00', op);
        $('#cnpjEdit').mask('00.000.000/0000-00', op);
    }
}
$('#cpf').mask('000.000.000-00#', options);
$('#cpfEdit').mask('000.000.000-00#', options);
$('#cnpj').mask('00.000.000/0000-00#', options);
$('#cnpjEdit').mask('00.000.000/0000-00#', options);

//Telefone mask
function mask(o, f) {
    setTimeout(function () {
        var v = mphone(o.value);
        if (v != o.value) {
            o.value = v;
        }
    }, 1);
}

function mphone(v) {
    var r = v.replace(/\D/g, "");
    r = r.replace(/^0/, "");
    if (r.length > 10) {
        r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1)$2-$3");
    } else if (r.length > 5) {
        r = r.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1)$2-$3");
    } else if (r.length > 2) {
        r = r.replace(/^(\d\d)(\d{0,5})/, "($1)$2");
    } else {
        r = r.replace(/^(\d*)/, "($1");
    }
    return r;
}

// Config datatable
$(document).ready(function () {
    $('#datatable').DataTable({
        "language": {
            "lengthMenu": "Exibindo _MENU_ registros por página",
            "zeroRecords": "Sem registro encontrado",
            "info": "Exibindo página _PAGE_ de _PAGES_",
            "infoEmpty": "Sem registro disponível",
            "loadingRecords": "Carregando...",
            "processing": "Processando...",
            "search": "Buscar:",
            "paginate": {
                "first": "Primeiro",
                "last": "Último",
                "next": "Próximo",
                "previous": "Anterior"
            },
            "infoFiltered": "(Exibindo os itens filtrados do total de _MAX_ registros)"
        }
    });
});
