import sys
from sklearn import tree

# busca parametro passado pelo PHP
client_skills = list(map(int, sys.argv[1].split()))
qtd_classifications = int(sys.argv[2])

# quantidade de skills
qtd_skills = len(client_skills)

# criando aprendizado supervisionado, com amostras e suas classificações
qtd = 0
sample = []
classification = []
while qtd <= qtd_classifications:
    sample.append([qtd] * qtd_skills)
    classification.append(qtd)
    qtd += 1

# cria uma nova classificação por árvore de decisão
clf = tree.DecisionTreeClassifier()

# passa as amostras e as classificações criadas
clf = clf.fit(sample, classification)
#print(client_skills)
# classifica as skills do candidato
print(clf.predict([client_skills]))

# Examplo codigo bruto
# sample = [[1, 1, 1, 1], [2, 2, 2, 2], [3, 3, 3, 3]]
# classification = [1, 2, 3]
# clf = tree.DecisionTreeClassifier()
# clf = clf.fit(X, Y)
# print(clf.predict([[0, 0, 0, 0]]))
