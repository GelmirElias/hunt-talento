<?php

use App\Http\Controllers\FormationController;
use App\Http\Controllers\SkillController;
use App\Http\Controllers\UtilController;
use App\Models\Candidate;
use App\Models\CandidateComment;
use App\Models\CandidateHistory;
use App\Models\Client;
use App\Models\ClientFormation;
use App\Models\ClientSkill;
use App\Models\Company;
use App\Models\Formation;
use App\Models\FormationType;
use App\Models\HiringHistory;
use App\Models\Interview;
use App\Models\Permission;
use App\Models\Skill;
use App\Models\StatusVacancy;
use App\Models\User;
use App\Models\Vacancy;
use App\Models\VacancySkill;
use Illuminate\Support\Facades\Session;

?>

<!DOCTYPE html>
@extends('layout.app')

@section('title', "$vacancy->name")

@section('body')

<div class="col">
    <div class="row">
        <div class="col">
            <a type="submit" class="float-left btn btn-primary" href="/tutor/vacancy"><i class="fa-solid fa-backward"></i></a>
        </div>
    </div>
    <br>
    <div class="row">

        <!-- Todos os candidatos -->
        <section class="lists-container">

            <!-- Status Candidate -->
            @foreach($statusCandidates as $statusCandidate)
            <div class="list">

                <?php
                $candidates = Candidate::where('status_candidates_id', $statusCandidate->id)->where('vacancy_id', $vacancy->id)->get();
                ?>

                <h4 data-toggle="tooltip" title="{{$statusCandidate->description}}" class="list-title">{{$statusCandidate->name}}</h4>

                <ul class="list-items">
                    @foreach ($candidates as $candidate)
                    <?php $client = Client::where('id', $candidate->client_id)->first(); ?>
                    <?php $user = User::where('id', $client->user_id)->first(); ?>

                    <a href="/tutor/vacancy/candidate/show={{$candidate->id}}">
                        <li>
                            <div class="row" type="submit">
                                <div class="col">
                                    <div data-toggle="tooltip" title="{{$user->description}}" class="text-div-center">{{$user->name}}</div>
                                    <div class="divider"></div>
                                </div>
                            </div>
                            <td>
                                @if($candidate->classification == 1)
                                <i class="fa-solid fa-star" style="color: #f37101;"></i>
                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                @elseif($candidate->classification == 2)
                                <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                @elseif($candidate->classification == 3)
                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                @elseif($candidate->classification == 4)
                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                <i class="fa-regular fa-star" style="color: #8fbc30;"></i>
                                @elseif($candidate->classification == 5)
                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                @else
                                <i class="fa-regular fa-star"></i>
                                <i class="fa-regular fa-star"></i>
                                <i class="fa-regular fa-star"></i>
                                <i class="fa-regular fa-star"></i>
                                <i class="fa-regular fa-star"></i>
                                @endif
                            </td>
                        </li>
                    </a>
                    <div class="divider"></div>
                    @endforeach
                </ul>

            </div>
            @endforeach

        </section>
        <!-- End of lists container -->
    </div>
</div>

<!-- MODAL SHOW DETALHES -->
@if(Session::has('candidateShowDetail'))
<?php

// recupera dados da sessão
$candidateShowDetail = Session::get('candidateShowDetail');
$candidateCreateInterview = Session::get('candidateCreateInterview');
$candidateEditInterview = Session::get('candidateEditInterview');
$candidateApproved = Session::get('candidateApproved');
$candidateUnapproved = Session::get('candidateUnapproved');
$historicInterview = Session::get('historicInterview');
$candidateComments = Session::get('candidateComments');
$candidateHistories = Session::get('candidateHistories');
$candidateDocuments = Session::get('candidateDocuments');

//informações do candidato
$clientShow = Client::where('id', $candidateShowDetail->client_id)->first();
$userShow = User::where('id', $clientShow->user_id)->first();

//tratar o zap
$search  = array('(', ')', '-');
$replace = array('', '', '');
$userZap = str_replace($search, $replace, $userShow->phone);

$permissionShow = Permission::where('id', $userShow->permission_id)->first();

//formações do candidato
$clientFormations = ClientFormation::where('client_id', $clientShow->id)->get();
$clientFormations = $clientFormations->count() == 0 ? false : $clientFormations;

//talentos do candidato
$manualClientSkills = ClientSkill::where('client_id', $clientShow->id)->where('client_assigned', true)->get();
$manualClientSkills = $manualClientSkills->count() == 0 ? false : $manualClientSkills;

$automaticClientSkills = ClientSkill::where('client_id', $clientShow->id)->where('client_assigned', false)->get();
$automaticClientSkills = $automaticClientSkills->count() == 0 ? false : $automaticClientSkills;
?>

<script>
    $(function() {
        $('#nameShow').val('<?php echo $userShow->name ?>');
        $('#phoneShow').val('<?php echo $userShow->phone ?>');
        $('#cpfShow').val('<?php echo $clientShow->cpf ?>');
        $('#descriptionShow').val('<?php echo $userShow->description ?>');
        $('#emailShow').val('<?php echo $userShow->email ?>');
        $('#permissionShow').val('<?php echo $permissionShow->name ?>');
        $('#activeShow').val('<?php echo $userShow->active ?>');
        $('#myModal').modal('show');
    });
</script>

<!-- myModal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#personaInfo" role="tab" aria-controls="personaInfo">Candidato</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#interview" role="tab" aria-controls="interview">Entrevista</a>
                    </li>
                </ul>

                <form action="/tutor/vacancy/candidates/show={{$vacancy->id}}">
                    <button class="close" type="submit" aria-hidden="true">&times;</button>
                </form>
            </div>
            <div class="modal-body">
                <div class="tab-content">
                    <!-- Candidato -->
                    <div class="tab-pane active" id="personaInfo" role="tabpanel">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#persona" role="tab" aria-controls="persona">Dados Pessoais</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#resume" role="tab" aria-controls="resume">Currículo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#formations" role="tab" aria-controls="formations">Cursos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#clientAssignedSkills" role="tab" aria-controls="skills">Interesses</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#systemAssignedSkills" role="tab" aria-controls="skills">Habilidades</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <!-- DADOS PESSOAIS -->
                            <div class="tab-pane active" id="persona" role="tabpanel">
                                <div style="margin: 3%">
                                    <div class="row">
                                        <div class="form-group col">
                                            <label for="name">Nome:</label>
                                            <input disabled type="text" class="form-control" name="nameShow" id="nameShow" placeholder="Nome">
                                        </div>
                                        <div class="form-group col">
                                            <label for="cpf">CPF:</label>
                                            <input disabled type="text" class="form-control" name="cpfShow" id="cpfShow" placeholder="CPF">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col">
                                            <label for="phoneShow">Telefone:</label>
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <input disabled type="text" class="form-control" name="phoneShow" id="phoneShow" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" placeholder="Telefone">
                                                </div>
                                                <div class="col-md-2">
                                                    <a aria-label="Chat on WhatsApp" href="https://wa.me/{{$userZap}}" target="_blank" rel="noopener noreferrer"> <img alt="Chat on WhatsApp" src="/img/whatsapp.png" height="40" /> </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col">
                                            <label for="email">Email:</label>
                                            <input disabled type="text" class="form-control" name="emailShow" id="emailShow" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="row">
                                        @if(!empty($userShow->photo))
                                        <div class="form-group col">
                                            <label for="photoShow">Foto:</label>
                                            <img id="photoShow" src="/img/clients/{{$userShow->photo}}" alt="photo user" width=240 height=240>
                                        </div>
                                        @else
                                        <div class="form-group col">
                                            <label for="photoShow">Foto:</label>
                                            <img id="photoShow" src="/img/semimagem.png" alt="photo user" width=240 height=240>
                                        </div>
                                        @endif
                                        <div class="form-group col">
                                            <label for="description">Descrição:</label>
                                            <textarea rows="9" cols="50" disabled type="text" class="form-control" name="descriptionShow" id="descriptionShow" placeholder="Descrição"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col">
                                            <label for="permissionShow">Permissão:</label>
                                            <input disabled type="text" class="form-control" name="permissionShow" id="permissionShow" placeholder="Permissão">
                                        </div>
                                        <div class="form-group col">
                                            <label for="activeShow">Status:</label>
                                            <select disabled name="activeShow" id="activeShow" class="form-control">
                                                <option value="1">Ativo</option>
                                                <option value="0">Inativo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- CURRICULO -->
                            <div class="tab-pane" id="resume" role="tabpanel">
                                <div style="margin: 3%" class="text-div-center">
                                    @if(!empty($userShow->resume))
                                    <embed src="/doc/resume/{{$user->resume}}" height="800" width="1000" type="application/pdf">
                                    @else
                                    O candidato selecionado não possui currículo na plataforma!
                                    @endif
                                </div>
                            </div>

                            <!-- CURSOS -->
                            <div class="tab-pane" id="formations" role="tabpanel">
                                <div style="margin: 3%" class="text-div-center">
                                    @if(!empty($clientFormations))
                                    <table class="table table-bordered table-hover display">
                                        <thead>
                                            <tr>
                                                <td>Name</td>
                                                <td>Data Inicio</td>
                                                <td>Data Fim</td>
                                                <td>Tipo</td>
                                                <td>Terminou</td>
                                                <td>Visualizar</td>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($clientFormations as $clientFormation)
                                            <?php
                                            $formation = Formation::where('id', $clientFormation->formation_id)->first();
                                            $formationType = FormationType::where('id', $formation->formation_types_id)->first();
                                            ?>
                                            @if($formation->active > -1)
                                            <tr>
                                                <td>{{$formation->name}}</td>
                                                <td>{{$formation->start_date->format('d/m/Y H:i')}}</td>
                                                <td>{{$formation->end_date->format('d/m/Y H:i')}}</td>
                                                <td>{{$formationType->name}}</td>
                                                <td>{{$clientFormation->finish == 1 ? "Finalizado" : "Cursando"}}</td>
                                                <td>
                                                    <div class="cellContainer">
                                                        <div style="margin: 5px">
                                                            <a href="/tutor/formation/show={{$formation->id}}" target="_blank" rel="noopener noreferrer">
                                                                <button class="btn btn-primary" type="submit"><i class="fa-solid fa-eye"></i></button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                    O candidato selecionado não possui cursos de especialização feitos na plataforma!
                                    @endif
                                </div>
                            </div>

                            <!-- INTERESSES (CLIENTE ADICIONOU MANUALMENTE A SKILL E SEU RATE) -->
                            <div class="tab-pane" id="clientAssignedSkills" role="tabpanel">
                                <div style="margin: 3%" class="text-div-center">
                                    @if(!empty($manualClientSkills))
                                    <?php
                                    $qtdSkills = count($manualClientSkills);
                                    $countRate = 0;
                                    foreach ($manualClientSkills as $manualClientSkill)
                                        $countRate = $countRate + $manualClientSkill->rate;
                                    $mean = ($countRate/$qtdSkills);
                                    ?>
                                    <div class="col">
                                        <div class="row">
                                            Média de interesse:
                                            <div class="col">
                                                @if($mean == 1)
                                                <i class="fa-solid fa-star" style="color: #f37101;"></i>
                                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                @elseif($mean == 2)
                                                <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                                <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                @elseif($mean == 3)
                                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                                <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                                @elseif($mean == 4)
                                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                <i class="fa-regular fa-star" style="color: #8fbc30;"></i>
                                                @elseif($mean == 5)
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                @else
                                                <i class="fa-regular fa-star"></i>
                                                <i class="fa-regular fa-star"></i>
                                                <i class="fa-regular fa-star"></i>
                                                <i class="fa-regular fa-star"></i>
                                                <i class="fa-regular fa-star"></i>
                                                @endif
                                            </div>
                                        </div>
                                        </br>
                                        <div class="row">
                                            Interesses do candidato:
                                            <table class="table table-bordered table-hover display">
                                                <thead>
                                                    <tr>
                                                        <td>Name</td>
                                                        <td>Descrição</td>
                                                        <td>Associado em</td>
                                                        <td>Associado a vaga?</td>
                                                        <td>Avaliação</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($manualClientSkills as $clientSkill)
                                                    <?php
                                                    $skill = Skill::where('id', $clientSkill->skill_id)->first();
                                                    $vacancySkill = VacancySkill::where('vacancy_id', $vacancy->id)->where('skill_id', $skill->id)->where('active', true)->first(); ?>
                                                    <tr>
                                                        <td>{{$skill->name}}</td>
                                                        <td>{{$skill->description}}</td>
                                                        <td>{{$clientSkill->created_at->format('d/m/Y H:i')}}</td>
                                                        @if(!empty($vacancySkill))
                                                        <td style="background-color: #308945; color: white;">Sim</td>
                                                        @else
                                                        <td style="background-color: #f37101; color: white;">Não</td>
                                                        @endif
                                                        <td>
                                                            @if($clientSkill->rate == 1)
                                                            <i class="fa-solid fa-star" style="color: #f37101;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                            @elseif($clientSkill->rate == 2)
                                                            <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                                            <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                            @elseif($clientSkill->rate == 3)
                                                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                                            @elseif($clientSkill->rate == 4)
                                                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                            <i class="fa-regular fa-star" style="color: #8fbc30;"></i>
                                                            @elseif($clientSkill->rate == 5)
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            @else
                                                            <i class="fa-regular fa-star"></i>
                                                            <i class="fa-regular fa-star"></i>
                                                            <i class="fa-regular fa-star"></i>
                                                            <i class="fa-regular fa-star"></i>
                                                            <i class="fa-regular fa-star"></i>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    @else
                                    O candidato selecionado não possui interesses identificados na plataforma!
                                    @endif
                                </div>
                            </div>

                            <!-- HABILIDADES (SISTEMA ADICIONOU AUTOMATICAMENTE A SKILL E SEU RATE) -->
                            <div class="tab-pane" id="systemAssignedSkills" role="tabpanel">
                                <div style="margin: 3%" class="text-div-center">
                                    @if(!empty($automaticClientSkills))
                                    <div class="col">
                                        <div class="row">
                                            Classificação de pertencimento:
                                            <div class="col">
                                                @if($candidate->classification == 1)
                                                <i class="fa-solid fa-star" style="color: #f37101;"></i>
                                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                @elseif($candidate->classification == 2)
                                                <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                                <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                @elseif($candidate->classification == 3)
                                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                                <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                                @elseif($candidate->classification == 4)
                                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                <i class="fa-regular fa-star" style="color: #8fbc30;"></i>
                                                @elseif($candidate->classification == 5)
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                @else
                                                <i class="fa-regular fa-star"></i>
                                                <i class="fa-regular fa-star"></i>
                                                <i class="fa-regular fa-star"></i>
                                                <i class="fa-regular fa-star"></i>
                                                <i class="fa-regular fa-star"></i>
                                                @endif
                                            </div>
                                        </div>
                                        </br>
                                        <div class="row">
                                            Habilidades do candidato:
                                            <table class="table table-bordered table-hover display">
                                                <thead>
                                                    <tr>
                                                        <td>Name</td>
                                                        <td>Descrição</td>
                                                        <td>Associado em</td>
                                                        <td>Associado a vaga?</td>
                                                        <td>Avaliação</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($automaticClientSkills as $clientSkill)
                                                    <?php
                                                    $skill = Skill::where('id', $clientSkill->skill_id)->first();
                                                    $vacancySkill = VacancySkill::where('vacancy_id', $vacancy->id)->where('skill_id', $skill->id)->where('active', true)->first(); ?>
                                                    <tr>
                                                        <td>{{$skill->name}}</td>
                                                        <td>{{$skill->description}}</td>
                                                        <td>{{$clientSkill->created_at->format('d/m/Y H:i')}}</td>
                                                        @if(!empty($vacancySkill))
                                                        <td style="background-color: #308945; color: white;">Sim</td>
                                                        @else
                                                        <td style="background-color: #f37101; color: white;">Não</td>
                                                        @endif
                                                        <td>
                                                            @if($clientSkill->rate == 1)
                                                            <i class="fa-solid fa-star" style="color: #f37101;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                            @elseif($clientSkill->rate == 2)
                                                            <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                                            <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                            @elseif($clientSkill->rate == 3)
                                                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                                            @elseif($clientSkill->rate == 4)
                                                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                            <i class="fa-regular fa-star" style="color: #8fbc30;"></i>
                                                            @elseif($clientSkill->rate == 5)
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            @else
                                                            <i class="fa-regular fa-star"></i>
                                                            <i class="fa-regular fa-star"></i>
                                                            <i class="fa-regular fa-star"></i>
                                                            <i class="fa-regular fa-star"></i>
                                                            <i class="fa-regular fa-star"></i>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    @else
                                    O candidato selecionado não possui habilidades identificadas na plataforma!
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Entrevista -->
                    <div class="tab-pane" id="interview" role="tabpanel">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            @if(!empty($candidateCreateInterview))
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#createInterview" role="tab" aria-controls="createInterview">Agenda entrevista</a>
                            </li>
                            @endif

                            @if(!empty($candidateEditInterview))
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#editInterview" role="tab" aria-controls="editInterview">Editar entrevista</a>
                            </li>
                            @endif

                            @if(!empty($candidateApproved))
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#createHiringHistory" role="tab" aria-controls="createHiringHistory">Contratar candidato</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#historicInterview" role="tab" aria-controls="historicInterview">Histórico entrevista</a>
                            </li>
                            @endif

                            @if(!empty($historicInterview))
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#historicHiring" role="tab" aria-controls="historicHiring">Histórico contratação</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#historicInterview" role="tab" aria-controls="historicInterview">Histórico entrevista</a>
                            </li>
                            @endif

                            @if(!empty($candidateUnapproved))
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#historicInterview" role="tab" aria-controls="historicInterview">Histórico entrevista</a>
                            </li>
                            @endif

                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#historicCandidate" role="tab" aria-controls="historicCandidate">Histórico candidato</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#candidateComments" role="tab" aria-controls="candidateComments">Comentários candidato</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#candidateDocuments" role="tab" aria-controls="candidateDocuments">Documentos candidato</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            @if(!empty($candidateCreateInterview))
                            <!-- Agenda entrevista -->
                            <div class="tab-pane active" id="createInterview" role="tabpanel">
                                <div style="margin: 3%">
                                    <script>
                                        $(function() {
                                            $('#clientNameCreate').val('<?php echo $userShow->name ?>');
                                            $('#clientCpfCreate').val('<?php echo $clientShow->cpf ?>');
                                            $('#clientEmailCreate').val('<?php echo $userShow->email ?>');
                                            $('#clientePhoneCreate').val('<?php echo $userShow->phone ?>');
                                            $('#startDateCreate').val('<?php echo (new UtilController)->roundDownToMinuteInterval(); ?>');
                                            $('#endDateCreate').val('<?php echo (new UtilController)->roundUpToMinuteInterval(); ?>');
                                            $('#create').modal('show');
                                        });
                                    </script>
                                    <form action="/tutor/vacancy/interview/store={{$candidateCreateInterview->id}}" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="name">Nome do Cliente:</label>
                                                    <input disabled type="text" class="form-control" name="clientNameCreate" id="clientNameCreate" placeholder="Nome">
                                                </div>
                                                <div class="form-group">
                                                    <label for="name">CPF do Cliente:</label>
                                                    <input disabled type="text" class="form-control" name="clientCpfCreate" id="clientCpfCreate" placeholder="Cpf">
                                                </div>
                                                <div class="form-group">
                                                    <label for="name">Email do Cliente:</label>
                                                    <input disabled type="text" class="form-control" name="clientEmailCreate" id="clientEmailCreate" placeholder="Email">
                                                </div>
                                                <div class="form-group">
                                                    <label for="clientePhoneCreate">Telefone do Candidato:</label>
                                                    <input disabled type="text" class="form-control" name="clientePhoneCreate" id="clientePhoneCreate" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" placeholder="Telefone">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="description">Informações da entrevista:</label>
                                                    <textarea rows="4" cols="50" type="text" class="form-control" name="descriptionCreate" id="descriptionCreate" placeholder="Detalhes de como acontecerá a entrevista"></textarea>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col">
                                                        <label for="startDate">Data e Hora Inicio:</label>
                                                        <input require type="datetime-local" class="form-control" name="startDateCreate" id="startDateCreate">
                                                    </div>
                                                    <div class="form-group col">
                                                        <label for="endDate">Data e Hora Fim:</label>
                                                        <input require type="datetime-local" class="form-control" name="endDateCreate" id="endDateCreate">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label for="active">Status:</label>
                                                        <select required name="activeCreate" class="selectpicker" title="Selecione o status" data-width="100%">
                                                            <option value="1" selected>Ativo</option>
                                                            <option value="0">Inativo</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            @endif

                            @if(!empty($candidateEditInterview))
                            <!-- Editar entrevista -->
                            <div class="tab-pane active" id="editInterview" role="tabpanel">
                                <div style="margin: 3%">
                                    <?php
                                    $interviewEdit = Interview::where('candidate_id', $candidateEditInterview->id)->first();
                                    ?>
                                    <script>
                                        $(function() {
                                            $('#nameClientEdit').val('<?php echo $userShow->name ?>');
                                            $('#phoneClienteCreate').val('<?php echo $userShow->phone ?>');
                                            $('#cpfClientEdit').val('<?php echo $clientShow->cpf ?>');
                                            $('#emailClientEdit').val('<?php echo $userShow->email ?>');
                                            $('#descriptionEdit').val('<?php echo $interviewEdit->description ?>');
                                            $('#startDateEdit').val('<?php echo $interviewEdit->start_date ?>');
                                            $('#endDateEdit').val('<?php echo $interviewEdit->end_date ?>');
                                            $('.activeEdit').selectpicker('val', '<?php echo $interviewEdit->active ?>');
                                            $('.statusCandidateEdit').selectpicker('val', '<?php echo $candidateEditInterview->status_candidates_id ?>');
                                            $('#edit').modal('show');
                                        });
                                    </script>

                                    <form action="/tutor/vacancy/interview/update={{$candidateEditInterview->id}}" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="nameClientEdit">Nome do Candidato:</label>
                                                    <input disabled type="text" class="form-control" name="nameClientEdit" id="nameClientEdit" placeholder="Nome">
                                                </div>
                                                <div class="form-group">
                                                    <label for="cpfClientEdit">CPF do Candidato:</label>
                                                    <input disabled type="text" class="form-control" name="cpfClientEdit" id="cpfClientEdit" placeholder="CPF">
                                                </div>
                                                <div class="form-group">
                                                    <label for="emailClientEdit">Email do Candidato:</label>
                                                    <input disabled type="text" class="form-control" name="emailClientEdit" id="emailClientEdit" placeholder="Email">
                                                </div>
                                                <div class="form-group">
                                                    <label for="phoneClienteCreate">Telefone do Candidato:</label>
                                                    <input disabled type="text" class="form-control" name="phoneClienteCreate" id="phoneClienteCreate" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" placeholder="Telefone">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="descriptionEdit">Informações da entrevista:</label>
                                                    <textarea rows="4" cols="50" type="text" class="form-control" name="descriptionEdit" id="descriptionEdit" placeholder="Detalhes de como acontecerá a entrevista"></textarea>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col">
                                                        <label for="startDateEdit">Data e Hora Inicio:</label>
                                                        <input require type="datetime-local" class="form-control" name="startDateEdit" id="startDateEdit">
                                                    </div>
                                                    <div class="form-group col">
                                                        <label for="endDateEdit">Data e Hora Fim:</label>
                                                        <input require type="datetime-local" class="form-control" name="endDateEdit" id="endDateEdit">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col">
                                                        <label for="statusCandidateEdit">Situação entrevista:</label>
                                                        <select required name="statusCandidateEdit" class="selectpicker statusCandidateEdit" title="Selecione o status" data-width="100%">
                                                            @foreach($statusCandidates as $statusCandidate)
                                                            @if($statusCandidate->id != 7)
                                                            <option value="{{$statusCandidate->id}}">{{$statusCandidate->name}}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group col">
                                                        <label for="activeEdit">Status:</label>
                                                        <select required name="activeEdit" class="selectpicker activeEdit" title="Selecione o status" data-width="100%">
                                                            <option value="1">Ativo</option>
                                                            <option value="0">Inativo</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            @endif

                            @if(!empty($candidateApproved))
                            <!-- Contratar candidato -->
                            <div class="tab-pane active" id="createHiringHistory" role="tabpanel">
                                <div style="margin: 3%">
                                    <script>
                                        $(function() {
                                            $('#nameClientShow').val('<?php echo $userShow->name ?>');
                                            $('#phoneClienteShow').val('<?php echo $userShow->phone ?>');
                                            $('#cpfClientShow').val('<?php echo $clientShow->cpf ?>');
                                            $('#emailClientShow').val('<?php echo $userShow->email ?>');
                                            $('#startDateHiring').val('<?php echo date('Y-m-d\TH:i') ?>');
                                            $('#show').modal('show');
                                        });
                                    </script>
                                    <form action="/tutor/vacancy/hiringHistory/create={{$candidateApproved->id}}" method="POST">
                                        <div class="row">
                                            <div class="col">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="nameClientShow">Nome do Candidato:</label>
                                                    <input disabled type="text" class="form-control" name="nameClientShow" id="nameClientShow" placeholder="Nome">
                                                </div>
                                                <div class="form-group">
                                                    <label for="cpfClientShow">CPF do Candidato:</label>
                                                    <input disabled type="text" class="form-control" name="cpfClientShow" id="cpfClientShow" placeholder="CPF">
                                                </div>
                                                <div class="form-group">
                                                    <label for="emailClientShow">Email do Candidato:</label>
                                                    <input disabled type="text" class="form-control" name="emailClientShow" id="emailClientShow" placeholder="Email">
                                                </div>
                                                <div class="form-group">
                                                    <label for="phoneClienteShow">Telefone do Candidato:</label>
                                                    <input disabled type="text" class="form-control" name="phoneClienteShow" id="phoneClienteShow" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" placeholder="Telefone">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="row">
                                                    <div class="form-group col">
                                                        <label for="startDateHiring">Contratação em:</label>
                                                        <input required type="datetime-local" class="form-control" name="startDateHiring" id="startDateHiring">
                                                    </div>
                                                    <div class="form-group col">
                                                        <label for="salaryHiring">Salário:</label>
                                                        <input required type="number" step=any class="form-control" name="salaryHiring" id="salaryHiring" placeholder="Salário do candidato">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="roleHiring">Cargo Ocupado:</label>
                                                    <input required type="text" class="form-control" name="roleHiring" id="roleHiring" placeholder="Nome do cargo ocupado">
                                                </div>
                                                <div class="form-group">
                                                    <label for="descriptionHiring">Descrição da Vaga:</label>
                                                    <textarea rows="4" cols="50" type="text" class="form-control" name="descriptionHiring" id="descriptionHiring" placeholder="Descrição detalhada do cargo ocupado"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- Historico entrevista -->
                            <div class="tab-pane" id="historicInterview" role="tabpanel">
                                <div style="margin: 3%">
                                    <?php
                                    $interviewEdit = Interview::where('candidate_id', $candidateApproved->id)->first();
                                    ?>
                                    <script>
                                        $(function() {
                                            $('#nameClientEdit').val('<?php echo $userShow->name ?>');
                                            $('#phoneClienteCreate').val('<?php echo $userShow->phone ?>');
                                            $('#cpfClientEdit').val('<?php echo $clientShow->cpf ?>');
                                            $('#emailClientEdit').val('<?php echo $userShow->email ?>');
                                            $('#descriptionEdit').val('<?php echo $interviewEdit->description ?>');
                                            $('#startDateEdit').val('<?php echo $interviewEdit->start_date ?>');
                                            $('#endDateEdit').val('<?php echo $interviewEdit->end_date ?>');
                                            $('.approvedEdit').selectpicker('val', '<?php echo $interviewEdit->approved == null ? "-1" : $interviewEdit->approved ?>');
                                            $('.activeEdit').selectpicker('val', '<?php echo $interviewEdit->active ?>');
                                            $('#edit').modal('show');
                                        });
                                    </script>

                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="nameClientEdit">Nome do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="nameClientEdit" id="nameClientEdit" placeholder="Nome">
                                            </div>
                                            <div class="form-group">
                                                <label for="cpfClientEdit">CPF do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="cpfClientEdit" id="cpfClientEdit" placeholder="CPF">
                                            </div>
                                            <div class="form-group">
                                                <label for="emailClientEdit">Email do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="emailClientEdit" id="emailClientEdit" placeholder="Email">
                                            </div>
                                            <div class="form-group">
                                                <label for="phoneClienteCreate">Telefone do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="phoneClienteCreate" id="phoneClienteCreate" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" placeholder="Telefone">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="descriptionEdit">Informações da entrevista:</label>
                                                <textarea disabled rows="4" cols="50" type="text" class="form-control" name="descriptionEdit" id="descriptionEdit" placeholder="Detalhes de como acontecerá a entrevista"></textarea>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col">
                                                    <label for="startDateEdit">Data e Hora Inicio:</label>
                                                    <input disabled require type="datetime-local" class="form-control" name="startDateEdit" id="startDateEdit">
                                                </div>
                                                <div class="form-group col">
                                                    <label for="endDateEdit">Data e Hora Fim:</label>
                                                    <input disabled require type="datetime-local" class="form-control" name="endDateEdit" id="endDateEdit">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col">
                                                    <label for="approvedEdit">Situação entrevista:</label>
                                                    <select disabled required name="approvedEdit" class="selectpicker approvedEdit" title="Selecione o status" data-width="100%">
                                                        <option value="-1">Agendada</option>
                                                        <option value="3">Adiada</option>
                                                        <option value="2">Finalizada</option>
                                                        <option value="1">Aprovado</option>
                                                        <option value="4">Contratado</option>
                                                        <option value="0">Reprovado</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col">
                                                    <label for="activeEdit">Status:</label>
                                                    <select disabled required name="activeEdit" class="selectpicker activeEdit" title="Selecione o status" data-width="100%">
                                                        <option value="1">Ativo</option>
                                                        <option value="0">Inativo</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif

                            @if(!empty($historicInterview))
                            <!-- Historico contratação -->
                            <div class="tab-pane active" id="historicHiring" role="tabpanel">
                                <div style="margin: 3%">
                                    <?php $hiringHistory = HiringHistory::where('vacancy_id', $vacancy->id)->where('client_id', $clientShow->id)->first(); ?>
                                    <script>
                                        $(function() {
                                            $('#nameClientShow').val('<?php echo $userShow->name ?>');
                                            $('#phoneClienteShow').val('<?php echo $userShow->phone ?>');
                                            $('#cpfClientShow').val('<?php echo $clientShow->cpf ?>');
                                            $('#emailClientShow').val('<?php echo $userShow->email ?>');
                                            $('#startDateHiring').val('<?php echo $hiringHistory->start_date ?>');
                                            $('#salaryHiring').val('<?php echo $hiringHistory->salary ?>');
                                            $('#roleHiring').val('<?php echo $hiringHistory->role ?>');
                                            $('#descriptionHiring').val('<?php echo $hiringHistory->description ?>');
                                            $('#show').modal('show');
                                        });
                                    </script>
                                    <div class="row">
                                        <div class="col">
                                            @csrf
                                            <div class="form-group">
                                                <label for="nameClientShow">Nome do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="nameClientShow" id="nameClientShow" placeholder="Nome">
                                            </div>
                                            <div class="form-group">
                                                <label for="cpfClientShow">CPF do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="cpfClientShow" id="cpfClientShow" placeholder="CPF">
                                            </div>
                                            <div class="form-group">
                                                <label for="emailClientShow">Email do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="emailClientShow" id="emailClientShow" placeholder="Email">
                                            </div>
                                            <div class="form-group">
                                                <label for="phoneClienteShow">Telefone do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="phoneClienteShow" id="phoneClienteShow" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" placeholder="Telefone">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="row">
                                                <div class="form-group col">
                                                    <label for="startDateHiring">Contratação em:</label>
                                                    <input disabled required type="datetime-local" class="form-control" name="startDateHiring" id="startDateHiring">
                                                </div>
                                                <div class="form-group col">
                                                    <label for="salaryHiring">Salário:</label>
                                                    <input disabled required type="number" step=any class="form-control" name="salaryHiring" id="salaryHiring" placeholder="Salário do candidato">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="roleHiring">Cargo Ocupado:</label>
                                                <input disabled required type="text" class="form-control" name="roleHiring" id="roleHiring" placeholder="Nome do cargo ocupado">
                                            </div>
                                            <div class="form-group">
                                                <label for="descriptionHiring">Descrição da Vaga:</label>
                                                <textarea disabled rows="4" cols="50" type="text" class="form-control" name="descriptionHiring" id="descriptionHiring" placeholder="Descrição detalhada do cargo ocupado"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Historico entrevista -->
                            <div class="tab-pane" id="historicInterview" role="tabpanel">
                                <div style="margin: 3%">
                                    <?php
                                    $interviewEdit = Interview::where('candidate_id', $historicInterview->id)->first();
                                    ?>
                                    <script>
                                        $(function() {
                                            $('#nameClientEdit').val('<?php echo $userShow->name ?>');
                                            $('#phoneClienteCreate').val('<?php echo $userShow->phone ?>');
                                            $('#cpfClientEdit').val('<?php echo $clientShow->cpf ?>');
                                            $('#emailClientEdit').val('<?php echo $userShow->email ?>');
                                            $('#descriptionEdit').val('<?php echo $interviewEdit->description ?>');
                                            $('#startDateEdit').val('<?php echo $interviewEdit->start_date ?>');
                                            $('#endDateEdit').val('<?php echo $interviewEdit->end_date ?>');
                                            $('.approvedEdit').selectpicker('val', '<?php echo $interviewEdit->approved == null ? "-1" : $interviewEdit->approved ?>');
                                            $('.activeEdit').selectpicker('val', '<?php echo $interviewEdit->active ?>');
                                            $('#edit').modal('show');
                                        });
                                    </script>

                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="nameClientEdit">Nome do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="nameClientEdit" id="nameClientEdit" placeholder="Nome">
                                            </div>
                                            <div class="form-group">
                                                <label for="cpfClientEdit">CPF do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="cpfClientEdit" id="cpfClientEdit" placeholder="CPF">
                                            </div>
                                            <div class="form-group">
                                                <label for="emailClientEdit">Email do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="emailClientEdit" id="emailClientEdit" placeholder="Email">
                                            </div>
                                            <div class="form-group">
                                                <label for="phoneClienteCreate">Telefone do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="phoneClienteCreate" id="phoneClienteCreate" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" placeholder="Telefone">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="descriptionEdit">Informações da entrevista:</label>
                                                <textarea disabled rows="4" cols="50" type="text" class="form-control" name="descriptionEdit" id="descriptionEdit" placeholder="Detalhes de como acontecerá a entrevista"></textarea>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col">
                                                    <label for="startDateEdit">Data e Hora Inicio:</label>
                                                    <input disabled require type="datetime-local" class="form-control" name="startDateEdit" id="startDateEdit">
                                                </div>
                                                <div class="form-group col">
                                                    <label for="endDateEdit">Data e Hora Fim:</label>
                                                    <input disabled require type="datetime-local" class="form-control" name="endDateEdit" id="endDateEdit">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col">
                                                    <label for="approvedEdit">Situação entrevista:</label>
                                                    <select disabled required name="approvedEdit" class="selectpicker approvedEdit" title="Selecione o status" data-width="100%">
                                                        <option value="-1">Agendada</option>
                                                        <option value="3">Adiada</option>
                                                        <option value="2">Finalizada</option>
                                                        <option value="1">Aprovado</option>
                                                        <option value="4">Contratado</option>
                                                        <option value="0">Reprovado</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col">
                                                    <label for="activeEdit">Status:</label>
                                                    <select disabled required name="activeEdit" class="selectpicker activeEdit" title="Selecione o status" data-width="100%">
                                                        <option value="1">Ativo</option>
                                                        <option value="0">Inativo</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif

                            @if(!empty($candidateUnapproved))
                            <!-- Historico entrevista -->
                            <div class="tab-pane active" id="historicInterview" role="tabpanel">
                                <div style="margin: 3%">
                                    <?php
                                    $interviewEdit = Interview::where('candidate_id', $candidateUnapproved->id)->first();
                                    ?>
                                    <script>
                                        $(function() {
                                            $('#nameClientEdit').val('<?php echo $userShow->name ?>');
                                            $('#phoneClienteCreate').val('<?php echo $userShow->phone ?>');
                                            $('#cpfClientEdit').val('<?php echo $clientShow->cpf ?>');
                                            $('#emailClientEdit').val('<?php echo $userShow->email ?>');
                                            $('#descriptionEdit').val('<?php echo $interviewEdit->description ?>');
                                            $('#startDateEdit').val('<?php echo $interviewEdit->start_date ?>');
                                            $('#endDateEdit').val('<?php echo $interviewEdit->end_date ?>');
                                            $('.approvedEdit').selectpicker('val', '<?php echo $interviewEdit->approved == null ? "-1" : $interviewEdit->approved ?>');
                                            $('.activeEdit').selectpicker('val', '<?php echo $interviewEdit->active ?>');
                                            $('#edit').modal('show');
                                        });
                                    </script>

                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="nameClientEdit">Nome do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="nameClientEdit" id="nameClientEdit" placeholder="Nome">
                                            </div>
                                            <div class="form-group">
                                                <label for="cpfClientEdit">CPF do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="cpfClientEdit" id="cpfClientEdit" placeholder="CPF">
                                            </div>
                                            <div class="form-group">
                                                <label for="emailClientEdit">Email do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="emailClientEdit" id="emailClientEdit" placeholder="Email">
                                            </div>
                                            <div class="form-group">
                                                <label for="phoneClienteCreate">Telefone do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="phoneClienteCreate" id="phoneClienteCreate" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" placeholder="Telefone">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="descriptionEdit">Informações da entrevista:</label>
                                                <textarea disabled rows="4" cols="50" type="text" class="form-control" name="descriptionEdit" id="descriptionEdit" placeholder="Detalhes de como acontecerá a entrevista"></textarea>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col">
                                                    <label for="startDateEdit">Data e Hora Inicio:</label>
                                                    <input disabled require type="datetime-local" class="form-control" name="startDateEdit" id="startDateEdit">
                                                </div>
                                                <div class="form-group col">
                                                    <label for="endDateEdit">Data e Hora Fim:</label>
                                                    <input disabled require type="datetime-local" class="form-control" name="endDateEdit" id="endDateEdit">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col">
                                                    <label for="approvedEdit">Situação entrevista:</label>
                                                    <select disabled required name="approvedEdit" class="selectpicker approvedEdit" title="Selecione o status" data-width="100%">
                                                        <option value="-1">Agendada</option>
                                                        <option value="3">Adiada</option>
                                                        <option value="2">Finalizada</option>
                                                        <option value="1">Aprovado</option>
                                                        <option value="4">Contratado</option>
                                                        <option value="0">Reprovado</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col">
                                                    <label for="activeEdit">Status:</label>
                                                    <select disabled required name="activeEdit" class="selectpicker activeEdit" title="Selecione o status" data-width="100%">
                                                        <option value="1">Ativo</option>
                                                        <option value="0">Inativo</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif

                            <!-- Historico candidato -->
                            <div class="tab-pane" id="historicCandidate" role="tabpanel">
                                <div style="margin: 3%">
                                    @if(!empty($candidateHistories[0]))
                                    <table class="table table-bordered table-hover display">
                                        <thead>
                                            <tr>
                                                <td>Ordem</td>
                                                <td>Nome</td>
                                                <td>Ação</td>
                                                <td>Realizada</td>
                                            </tr>
                                        </thead>

                                        <?php $count = 0; ?>
                                        <tbody>
                                            @foreach ($candidateHistories as $candidateHistory)
                                            <?php
                                            $candidate = Candidate::where('id', $candidateHistory->candidate_id)->first();
                                            $client = Client::where('id', $candidate->client_id)->first();
                                            $user = User::where('id', $client->user_id)->first();
                                            $permission = Permission::where('id', $user->permission_id)->first();
                                            $count++;
                                            ?>
                                            <tr>
                                                <td>{{$count}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$candidateHistory->description}}</td>
                                                <td>{{$candidateHistory->created_at->format('d/m/Y H:i')}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                    O candidato selecionado não possui histórico!
                                    @endif
                                </div>
                            </div>

                            <!-- Comentario candidato -->
                            <div class="tab-pane" id="candidateComments" role="tabpanel">
                                <div class="col">
                                    <div style="margin: 3%">
                                        <div class="row">
                                            <h5>Todos os comentários</h5>
                                            <div class="col">
                                                @if(!empty($candidateComments[0]))
                                                <table class="table table-bordered table-hover display">
                                                    <thead>
                                                        <tr>
                                                            <td>Autor</td>
                                                            <td>Comentário</td>
                                                            <td>Data</td>
                                                            <td>Editar</td>
                                                            <td>Excluir</td>
                                                        </tr>
                                                    </thead>

                                                    <?php $count = 0; ?>
                                                    <tbody>
                                                        @foreach ($candidateComments as $candidateComment)
                                                        <?php
                                                        $user = User::where('id', $candidateComment->user_id)->first();
                                                        ?>
                                                        <tr>
                                                            <td>{{$user->name}}</td>
                                                            <td>{{$candidateComment->description}}</td>
                                                            <td>{{$candidateComment->updated_at->format('d/m/Y H:i')}}</td>
                                                            <td><a href="/tutor/vacancy/comment/edit={{$candidateComment->id}}"><button class="btn btn-warning"><i class="fa-solid fa-pen-to-square"></i></button></a></td>
                                                            <td><a href="/tutor/vacancy/comment/destroy={{$candidateComment->id}}"><button type="submit" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button></a></td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                @else
                                                O candidato selecionado não possui nenhum comentário!
                                                @endif
                                            </div>
                                        </div>
                                        </br>
                                        <hr>
                                        </br>
                                        <div class="row">
                                            <h5>Adicionar comentário</h5>
                                            <div class="col-11">
                                                <form action="/tutor/vacancy/comment/create={{$candidate->id}}" method="POST">
                                                    @csrf
                                                    <textarea rows="3" type="text" class="form-control" name="description" id="description" placeholder="Digite aqui um novo comentário..."></textarea>
                                            </div>
                                            <div class="col">
                                                <button type="submit" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Documento candidato -->
                            <div class="tab-pane" id="candidateDocuments" role="tabpanel">
                                <div class="col">
                                    <div style="margin: 3%">
                                        <div class="row">
                                            <h5>Todos os documentos</h5>
                                            <div class="col">
                                                @if(!empty($candidateDocuments[0]))
                                                <table class="table table-bordered table-hover display">
                                                    <thead>
                                                        <tr>
                                                            <td>Nome</td>
                                                            <td>Descrição</td>
                                                            <td>Atualizado</td>
                                                            <td>Visualizar</td>
                                                            <td>Excluir</td>
                                                        </tr>
                                                    </thead>

                                                    <?php $count = 0; ?>
                                                    <tbody>
                                                        @foreach ($candidateDocuments as $candidateDocument)
                                                        <tr>
                                                            <td>{{$candidateDocument->name}}</td>
                                                            <td>{{!is_null($candidateDocument->description) ? $candidateDocument->description : 'Sem descrição'}}</td>
                                                            <td>{{$candidateDocument->updated_at->format('d/m/Y H:i')}}</td>
                                                            <td><a href="/tutor/vacancy/document/show={{$candidateDocument->id}}"><button class="btn btn-primary" type="submit"><i class="fa-solid fa-eye"></i></button></a></td>
                                                            <td><a href="/tutor/vacancy/document/destroy={{$candidateDocument->id}}"><button type="submit" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button></a></td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                @else
                                                O candidato selecionado não possui nenhum documento!
                                                @endif
                                            </div>
                                        </div>
                                        </br>
                                        <hr>
                                        </br>
                                        <div class="row">
                                            <h5>Adicionar documento</h5>
                                            <form action="/tutor/vacancy/document/create={{$candidate->id}}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-11">
                                                        <div class="form-group">
                                                            <label for="document">Carregar novo documento:</label>
                                                            <input required type="file" accept="application/pdf" class="form-control-file" name="document" id="document">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="name">Nome:</label>
                                                            <input rows="10" type="text" class="form-control" name="name" id="name" placeholder="Nome do documento"></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="description">Descrição:</label>
                                                            <textarea rows="3" type="text" class="form-control" name="description" id="description" placeholder="Descrição do documento"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-1">
                                                        <button type="submit" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<!-- END MODAL SHOW DETALHES -->

<!-- MODAL EDITAR COMENTARIO -->
@if(Session::has('commentEdit'))

<?php
$commentEdit = Session::get('commentEdit');
?>

<script>
    $(function() {
        $('#descriptionEdit').val('<?php echo $commentEdit->description ?>');
        $('.activeEdit').selectpicker('val', '<?php echo $commentEdit->active ?>');
        $('#edit').modal('show');
    });
</script>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
                <form action="/tutor/vacancy/candidate/show={{$commentEdit->candidate_id}}">
                    <button class="close" type="submit" aria-hidden="true">&times;</button>
                </form>
            </div>
            <div class="modal-body">
                <div style="margin: 1%">
                    <form id="updateCandidateComment" action="/tutor/vacancy/comment/update={{$commentEdit->id}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="description">Descrição:</label>
                            <textarea rows="10" cols="50" type="text" class="form-control" name="descriptionEdit" id="descriptionEdit" placeholder="Descrição da vaga"></textarea>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" form="updateCandidateComment" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
            </div>
        </div>
    </div>
</div>
@endif

<!-- MODAL VISUALIZAR DOCUMENTO -->
@if(Session::has('showDocument'))

<?php
$showDocument = Session::get('showDocument');
?>

<script>
    $(function() {
        $('#edit').modal('show');
    });
</script>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Documento</h5>
                <form action="/tutor/vacancy/candidate/show={{$showDocument->candidate_id}}">
                    <button class="close" type="submit" aria-hidden="true">&times;</button>
                </form>
            </div>
            <div class="modal-body">
                <div style="margin: 1%">
                    <div class="row">
                        <embed src="/doc/candidate/{{$showDocument->document}}" height="800" type="application/pdf">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@endsection
