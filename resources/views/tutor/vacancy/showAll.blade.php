<?php

use App\Models\Company;
use App\Models\Formation;
use App\Models\StatusVacancy;
use App\Models\User;
use App\Models\Vacancy;
use App\Models\VacancyFormation;
use App\Models\VacancySkill;
use Illuminate\Support\Facades\Session;

?>

<!DOCTYPE html>
@extends('layout.app')

@section('title', "Gerenciar Vagas")

@section('body')
<button type="submit" class="float-right btn btn-success" data-toggle="modal" data-target="#create"><i class="fa-solid fa-plus"></i></button>
<br><br>

<!-- Lists container -->
<section class="lists-container">
    @foreach ($statusVacancies as $statusVacancy)
    <div class="list">
        <h4 data-toggle="tooltip" title="{{$statusVacancy->description}}" class="list-title">{{$statusVacancy->name}}</h4>
        <?php
        $companiesId = [];
        foreach ($companies as $company)
            $companiesId[] = $company->id;
        $vacancies = Vacancy::where('status_vacancy_id', $statusVacancy->id)->whereIn('company_id', $companiesId)->get(); ?>
        <ul class="list-items">
            @foreach ($vacancies as $vacancy)
            <?php
            $company = Company::where('id', $vacancy->company_id)->first();
            $user = User::where('id', $company->user_id)->first();
            ?>
            <li>
                <a href="/tutor/vacancy/candidates/show={{$vacancy->id}}">
                    <div class="row" type="submit">
                        <div class="col">
                            <div data-toggle="tooltip" title="{{$vacancy->description}}" class="text-div-center">{{$vacancy->name}}</div>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col">
                            <div class="text-div-center">
                                <a href="/tutor/vacancy/back={{$vacancy->id}}">
                                    <button class="btn btn-primary btn-sm">
                                        <i class="fa-solid fa-caret-left"></i>
                                    </button>
                                </a>
                                <div class="divider"></div>
                                <a href="/tutor/vacancy/go={{$vacancy->id}}">
                                    <button class="btn btn-primary btn-sm">
                                        <i class="fa-solid fa-caret-right"></i>
                                    </button>
                                </a>
                                <div class="divider"></div>
                                <a href="/tutor/vacancy/edit={{$vacancy->id}}">
                                    <button class="btn btn-warning btn-sm">
                                        <i class="fa-solid fa-pen-to-square"></i>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    </br>
                </a>
                <div data-toggle="tooltip" title="{{$user->description}}" class="text-div-center">{{strtok($user->name, " ")}}</div>
            </li>
            @endforeach
        </ul>
    </div>
    @endforeach
</section>

<!-- End of lists container -->

<!-- MODAL EDITAR -->
@if(Session::has('vacancyEdit'))

<?php
$vacancyEdit = Session::get('vacancyEdit');
$vacancyFormationsNotRequiredEdit = VacancyFormation::where('vacancy_id', $vacancyEdit->id)->where('active', true)->where('required', false)->get();
$vacancyFormationsRequiredEdit = VacancyFormation::where('vacancy_id', $vacancyEdit->id)->where('active', true)->where('required', true)->get();
$vacancySkillsEdit = VacancySkill::where('vacancy_id', $vacancyEdit->id)->where('active', true)->get();
?>

<script>
    $(function() {
        $('.companyIdEdit').selectpicker('val', '<?php echo $vacancyEdit->company_id ?>');
        $('#nameEdit').val('<?php echo $vacancyEdit->name ?>');
        $('#descriptionEdit').val('<?php echo $vacancyEdit->description ?>');
        $('#startSalaryEdit').val('<?php echo $vacancyEdit->start_salary ?>');
        $('#endSalaryEdit').val('<?php echo $vacancyEdit->end_salary ?>');
        $('#quantityEdit').val('<?php echo $vacancyEdit->quantity ?>');
        $('#statusVacancyIdEdit').val('<?php echo $vacancyEdit->status_vacancy_id ?>');
        $('.activeEdit').selectpicker('val', '<?php echo $vacancyEdit->active ?>');

        //preenchendo o multi select com as formações recomendadas
        var vacancyFormationsNotRequiredEdit = [];
        <?php foreach ($vacancyFormationsNotRequiredEdit as $vacancyFormationEdit) { ?>
            vacancyFormationsNotRequiredEdit.push(<?php echo $vacancyFormationEdit->formation_id; ?>);
        <?php } ?>
        $('.vacancyFormationsNotRequiredEdit').selectpicker('val', vacancyFormationsNotRequiredEdit);

        //preenchendo o multi select com as formações obrigatórias
        var vacancyFormationsRequiredEdit = [];
        <?php foreach ($vacancyFormationsRequiredEdit as $vacancyFormationEdit) { ?>
            vacancyFormationsRequiredEdit.push(<?php echo $vacancyFormationEdit->formation_id; ?>);
        <?php } ?>
        $('.vacancyFormationsRequiredEdit').selectpicker('val', vacancyFormationsRequiredEdit);

        //preenchendo o multi select com os talentos
        var vacancySkillsEdit = [];
        <?php foreach ($vacancySkillsEdit as $vacancySkillEdit) { ?>
            vacancySkillsEdit.push(<?php echo $vacancySkillEdit->skill_id; ?>);
        <?php } ?>
        $('.vacancySkillsEdit').selectpicker('val', vacancySkillsEdit);

        $('#edit').modal('show');
    });
</script>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
                <form action="vacancy">
                    <button class="close" type="submit" aria-hidden="true">&times;</button>
                </form>
            </div>
            <div class="modal-body">
                <div style="margin: 1%">
                    <form id="updateVacancy" action="/tutor/vacancy/update={{$vacancyEdit->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col">
                                @if(!empty($vacancyEdit->photo))
                                <div class="form-group">
                                    <label for="photoShow">Foto:</label>
                                    <img id="photoShow" src="/img/vacancies/{{$vacancyEdit->photo}}" alt="photo formation" width=240 height=330>
                                </div>
                                @else
                                <div class="form-group">
                                    <label for="photoShow">Foto:</label>
                                    <img id="photoShow" src="/img/semimagem.png" alt="photo formation" width=240 height=330>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label for="photoEdit">Foto:</label>
                                    <input type="file" class="form-control-file" name="photoEdit" id="photoEdit">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="companyIdEdit">Empresa:</label>
                                    <select required name="companyIdEdit" class="selectpicker companyIdEdit" title="Selecione a empresa" data-width="100%" data-live-search="true">
                                        @foreach($companies as $company)
                                        <?php $user = User::where('id', $company->user_id)->first(); ?>
                                        <option value="{{$company->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="name">Nome:</label>
                                    <input required type="text" class="form-control" name="nameEdit" id="nameEdit" placeholder="Nome da vaga">
                                </div>
                                <div class="form-group">
                                    <label for="description">Descrição:</label>
                                    <textarea rows="4" cols="50" type="text" class="form-control" name="descriptionEdit" id="descriptionEdit" placeholder="Descrição da vaga"></textarea>
                                </div>
                                <div class="row">
                                    <div class="form-group col">
                                        <label for="startSalaryEdit">Salário inicio:</label>
                                        <input required type="number" step=any class="form-control" name="startSalaryEdit" id="startSalaryEdit" placeholder="Range inicial">
                                    </div>
                                    <div class="form-group col">
                                        <label for="endSalaryEdit">Salário fim:</label>
                                        <input required type="number" step=any class="form-control" name="endSalaryEdit" id="endSalaryEdit" placeholder="Range final">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col">
                                        <label for="quantityEdit">Quantidade de Vagas:</label>
                                        <input required type="number" step="1" class="form-control" name="quantityEdit" id="quantityEdit" placeholder="Vagas disponíveis">
                                    </div>
                                    <div class="form-group col">
                                        <label for="activeEdit">Visibilidade:</label>
                                        <select required name="activeEdit" class="selectpicker activeEdit" title="Selecione a visibilidade" data-width="100%">
                                            <option value="1">Ativo</option>
                                            <option value="0">Inativo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="vacancyFormationsNotRequiredEdit">Formações recomendadas:</label>
                                    <select multiple name="vacancyFormationsNotRequiredEdit[]" class="selectpicker vacancyFormationsNotRequiredEdit" title="Selecione a formação" data-width="100%" data-live-search="true">
                                        @foreach($formations as $formation)
                                        <option value="{{$formation->id}}">{{$formation->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="vacancyFormationsRequiredEdit">Formações obrigatórias:</label>
                                    <select multiple name="vacancyFormationsRequiredEdit[]" class="selectpicker vacancyFormationsRequiredEdit" title="Selecione a formação" data-width="100%" data-live-search="true">
                                        @foreach($formations as $formation)
                                        <option value="{{$formation->id}}">{{$formation->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="vacancySkillsEdit">Habilidades desejadas:</label>
                                    <select multiple name="vacancySkillsEdit[]" class="selectpicker vacancySkillsEdit" title="Selecione a habilidade" data-width="100%" data-live-search="true">
                                        @foreach($skills as $skill)
                                        <option value="{{$skill->id}}">{{$skill->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" form="updateVacancy" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
                <form action="/tutor/vacancy/destroy={{$vacancyEdit->id}}">
                    <button type="submit" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
@endif

<!-- MODAL CRIAR -->
<div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Criar vaga</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="storeVacancy" action="/tutor/vacancy/store" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="companyId">Empresa:</label>
                                <select required name="companyId" class="selectpicker" title="Selecione a empresa" data-width="100%" data-live-search="true">
                                    @foreach($companies as $company)
                                    <?php $user = User::where('id', $company->user_id)->first(); ?>
                                    <option value="{{$company->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Nome:</label>
                                <input required type="text" class="form-control" name="name" id="name" placeholder="Nome da vaga">
                            </div>
                            <div class="form-group">
                                <label for="description">Descrição:</label>
                                <textarea rows="4" cols="50" type="text" class="form-control" name="description" id="description" placeholder="Descrição da vaga"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="photo">Foto:</label>
                                <input type="file" class="form-control-file" name="photo" id="photo">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="startSalary">Salário inicial (R$):</label>
                                <input required type="number" step=any class="form-control" name="startSalary" id="startSalary" placeholder="Range inicial (R$)">
                            </div>
                            <div class="form-group">
                                <label for="endSalary">Salário final (R$):</label>
                                <input required type="number" step=any class="form-control" name="endSalary" id="endSalary" placeholder="Range final (R$)">
                            </div>
                            <div class="form-group">
                                <label for="quantity">Quantidade de Vagas:</label>
                                <input required type="number" step="1" class="form-control" name="quantity" id="quantity" placeholder="Vagas disponíveis">
                            </div>
                            <div class="form-group">
                                <label for="active">Visibilidade:</label>
                                <select required name="active" class="selectpicker" title="Selecione a visibilidade" data-width="100%">
                                    <option selected value="1">Ativo</option>
                                    <option value="0">Inativo</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="vacancyFormationsNotRequired">Formações recomendadas:</label>
                                <select multiple name="vacancyFormationsNotRequired[]" class="selectpicker vacancyFormationsNotRequired" data-live-search="true" title="Selecione a formação" data-width="100%">
                                    @foreach($formations as $formation)
                                    <option value="{{$formation->id}}">{{$formation->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="vacancyFormationsRequired">Formações obrigatórias:</label>
                                <select multiple name="vacancyFormationsRequired[]" class="selectpicker vacancyFormationsRequired" data-live-search="true" title="Selecione a formação" data-width="100%">
                                    @foreach($formations as $formation)
                                    <option value="{{$formation->id}}">{{$formation->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="vacancySkills">Habilidades desejadas:</label>
                                <select multiple name="vacancySkills[]" class="selectpicker vacancySkills" data-live-search="true" title="Selecione a habilidade" data-width="100%">
                                    @foreach($skills as $skill)
                                    <option value="{{$skill->id}}">{{$skill->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="storeVacancy" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
            </div>
        </div>
    </div>
</div>

@endsection
