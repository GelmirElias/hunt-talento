<!DOCTYPE html>

<?php

use App\Http\Controllers\UtilController;
use App\Models\Company;
use App\Models\CompanyToDo;
use App\Models\StatusToDo;
use App\Models\Tutor;
use App\Models\TutorCompanyToDo;
use App\Models\User;
?>

@extends('layout.app')

@section('title', "Gerenciar Tarefas")

@section('body')
<button type="submit" class="float-right btn btn-success" data-toggle="modal" data-target="#create"><i class="fa-solid fa-plus"></i></button>
</br></br>

<div class="col">
    <div class="row">
        <section class="lists-container">
            <!-- Status ToDo -->
            @foreach($statusToDos as $statusToDo)
            <div class="list">
                <?php
                $tutorToDosIds = TutorCompanyToDo::select('company_to_do_id')->where('tutor_id', session('tutor')->id)->get();
                $tutorToDos = CompanyToDo::whereIn('id', $tutorToDosIds)->where('status_to_do_id', $statusToDo->id)->get();
                ?>
                <h3 data-toggle="tooltip" title="{{$statusToDo->description}}" class="list-title">{{$statusToDo->name}}</h3>
                <ul class="list-items">
                    @foreach ($tutorToDos as $toDo)
                    <?php
                    $company = Company::where('id', $toDo->company_id)->first();
                    $user = User::where('id', $company->user_id)->first();
                    ?>
                    <li>
                        <a href="/tutor/toDo/show={{$toDo->id}}">
                            <div class="row" type="submit">
                                <div class="col">
                                    <div data-toggle="tooltip" title="{{$toDo->description}}" class="text-div-center">{{$toDo->name}}</div>
                                </div>
                            </div>
                            </br>
                            <div class="row">
                                <div class="col">
                                    <div class="text-div-center">
                                        <a href="/tutor/toDo/back={{$toDo->id}}">
                                            <button class="btn btn-primary btn-sm">
                                                <i class="fa-solid fa-caret-left"></i>
                                            </button>
                                        </a>
                                        <div class="divider"></div>
                                        <a href="/tutor/toDo/go={{$toDo->id}}">
                                            <button class="btn btn-primary btn-sm">
                                                <i class="fa-solid fa-caret-right"></i>
                                            </button>
                                        </a>
                                        <div class="divider"></div>
                                        <a href="/tutor/toDo/edit={{$toDo->id}}">
                                            <button class="btn btn-warning btn-sm">
                                                <i class="fa-solid fa-pen-to-square"></i>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            </br>
                        </a>
                        <div data-toggle="tooltip" title="{{$user->description}}" class="text-div-center">{{strtok($user->name, " ")}}</div>
                    </li>
                    @endforeach
                </ul>
            </div>
            @endforeach
        </section>
        <!-- End of lists container -->
    </div>
</div>

<!-- MODAL CRIAR -->
<script>
    $(function() {
        $('#startDate').val('<?php echo (new UtilController)->roundDownToMinuteInterval(); ?>');
        $('#endDate').val('<?php echo (new UtilController)->roundUpToMinuteInterval(); ?>');

        $('.active').selectpicker('val', '1');
        $('.statusId').selectpicker('val', '1');
        $('.tutorId').selectpicker('val', '<?php echo session('tutor')->id; ?>');
    });
</script>

<div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Criar tarefa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/tutor/toDo" id="formCreateToDo" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Titulo*:</label>
                        <input required type="text" class="form-control" name="name" id="name" placeholder="Nome">
                    </div>
                    <div class="form-group">
                        <label for="description">Descrição:</label>
                        <textarea type="text" rows="4" cols="50" class="form-control" name="description" id="description" placeholder="Descrição"></textarea>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="startDate">Data inicio:</label>
                            <input type="datetime-local" class="form-control" name="startDate" id="startDate">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="endDate">Data fim:</label>
                            <input type="datetime-local" class="form-control" name="endDate" id="endDate">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="statusId">Status*:</label>
                            <select required class="selectpicker statusId" name="statusId" title="Selecione o status" data-width="100%" data-live-search="true">
                                @foreach($statusToDos as $statusToDo)
                                <option value="{{$statusToDo->id}}" data-subtext="{{$statusToDo->description}}">{{$statusToDo->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="active">Visibilidade*:</label>
                            <select required name="active" class="selectpicker active" title="Selecione a visibilidade" data-width="100%">
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Responsáveis:</label>
                            <select class="selectpicker tutorId" name="tutorId[]" multiple title="Selecione o(s) tutor(s)" data-width="100%" data-live-search="true">
                                @foreach($tutors as $tutor)
                                <?php $userTutor = User::where('id', $tutor->user_id)->first(); ?>
                                <option value="{{$tutor->id}}">{{$userTutor->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name">Empresa*:</label>
                            <select required class="selectpicker companyId" name="companyId" multiple title="Selecione a empresa" data-width="100%" data-live-search="true">
                                @foreach($companies as $company)
                                <?php $userCompany = User::where('id', $company->user_id)->first(); ?>
                                <option value="{{$company->id}}">{{$userCompany->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="formCreateToDo" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL EDITAR -->
@if(!empty($toDoEdit))
<script>
    $(function() {
        $('#nameEdit').val('<?php echo $toDoEdit->name ?>');
        $('#descriptionEdit').val('<?php echo $toDoEdit->description ?>');
        $('#startDateEdit').val('<?php echo $toDoEdit->start_date ?>');
        $('#endDateEdit').val('<?php echo $toDoEdit->end_date ?>');

        $('.activeEdit').selectpicker('val', '<?php echo  $toDoEdit->active ?>');
        $('.statusIdEdit').selectpicker('val', '<?php echo $toDoEdit->status_to_do_id ?>');

        //preenchendo o multi select com as empresas da formação
        var tutorsIdEdit = [];
        <?php foreach ($tutorsIdEdit as $tutorIdEdit) { ?>
            tutorsIdEdit.push(<?php echo $tutorIdEdit ?>);
        <?php } ?>
        $('.tutorIdEdit').selectpicker('val', tutorsIdEdit);

        $('#edit').modal('show');
    });
</script>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
                <form action="/tutor/toDo">
                    <button class="close" type="submit" aria-hidden="true">&times;</button>
                </form>
            </div>
            <div class="modal-body">
                <form action="/tutor/toDo/update={{$toDoEdit->id}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Titulo:</label>
                        <input required type="text" class="form-control" name="nameEdit" id="nameEdit" placeholder="Nome">
                    </div>
                    <div class="form-group">
                        <label for="description">Descrição:</label>
                        <textarea type="text" rows="4" cols="50" class="form-control" name="descriptionEdit" id="descriptionEdit" placeholder="Descrição"></textarea>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="startDateEdit">Data inicio:</label>
                            <input type="datetime-local" class="form-control" name="startDateEdit" id="startDateEdit">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="endDateEdit">Data fim:</label>
                            <input type="datetime-local" class="form-control" name="endDateEdit" id="endDateEdit">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="statusIdEdit">Status:</label>
                            <select required class="selectpicker statusIdEdit" name="statusIdEdit" title="Selecione o status" data-width="100%" data-live-search="true">
                                @foreach($statusToDos as $statusToDo)
                                <option value="{{$statusToDo->id}}" data-subtext="{{$statusToDo->description}}">{{$statusToDo->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="activeEdit">Visibilidade:</label>
                            <select required name="activeEdit" class="selectpicker activeEdit" title="Selecione a visibilidade" data-width="100%">
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Responsáveis:</label>
                        <select class="selectpicker tutorIdEdit" name="tutorIdEdit[]" multiple title="Selecione o(s) tutor(s)" data-width="100%" data-live-search="true">
                            @foreach($tutors as $tutor)
                            <?php $userTutor = User::where('id', $tutor->user_id)->first(); ?>
                            <option value="{{$tutor->id}}">{{$userTutor->name}}</option>
                            @endforeach
                        </select>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
                </form>
                <form action="/tutor/toDo/destroy={{$toDoEdit->id}}">
                    <button type="submit" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
@endif

<!-- MODAL COMENTARIO -->
@if(!empty($toDoDetail))
<script>
    $(function() {
        $('#comment').modal('show');
    });
</script>

<div class="modal fade" id="comment" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{$toDoDetail->name}}</h5>
                <form action="/tutor/toDo">
                    <button class="close" type="submit" aria-hidden="true">&times;</button>
                </form>
            </div>
            <div class="modal-body">
                <h6>Detalhes</h6>
                <div class="form-group">
                    <label><i class="fa-regular fa-file-lines"></i> Descrição: {{$toDoDetail->description == null ? "Indefinido" : $toDoDetail->description}}</label>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><i class="fa-solid fa-hourglass-start"></i> Começa: {{$toDoDetail->start_date == null ? "Indefinido" : $toDoDetail->start_date->format('d/m/Y H:i')}}</label>
                    </div>
                    <div class="form-group col-md-6">
                        <label><i class="fa-solid fa-hourglass-end"></i> Termina: {{$toDoDetail->end_date == null ? "Indefinido" : $toDoDetail->end_date->format('d/m/Y H:i')}}</label>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label name="statusIdEdit" data-width="100%"><i class="fa-solid fa-calendar-days"></i> Status: {{$statusDetail->name}}</label>
                    </div>
                    <div class="form-group col-md-6">
                        <label name="activeEdit" data-width="100%"><i class="fa-solid fa-toggle-off"></i> Visibilidade: {{$toDoDetail->active == 1 ? 'Visivel' : 'Oculto'}}</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <h6>Seguidores</h6>
                    @foreach($tutorsIdEdit as $tutorId)
                    <?php $tutor = Tutor::where('id', $tutorId)->first(); ?>
                    <?php $user = User::where('id', $tutor->user_id)->first(); ?>
                    <label><i class="fa-solid fa-user"></i> Tutor: {{$user->name}}</label>
                    @endforeach
                </div>
                <hr>
                <div class="form-group">
                    <h6>Comentários</h6>
                    @foreach($comments as $comment)
                    <?php $userTutor = User::where('id', $comment->user_id)->first(); ?>
                    <div class="form-row">
                        <div class="form-group col-md-10">
                            <label><i class="fa-solid fa-user"></i> Tutor: {{$userTutor->name}}</label>
                            <label><i class="fa-solid fa-clock"></i> Data: {{$comment->updated_at->format('d/m/Y H:i')}}</label>
                            <label><i class="fa-regular fa-file-lines"></i> Comentário: {{$comment->description}}</label>
                        </div>
                        <div class="form-group col-md-2">
                            @if($userTutor == Auth::user())
                            <form action="/tutor/toDo={{$toDoDetail->id}}/comment/edit={{$comment->id}}">
                                @csrf
                                <button class="btn btn-warning edit" type="submit"><i class="fa-solid fa-pen-to-square"></i></button>
                            </form>
                            <br>
                            <form action="/tutor/toDo={{$toDoDetail->id}}/comment/destroy={{$comment->id}}">
                                <button type="submit" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                            </form>
                            @endif
                        </div>
                    </div>
                    <hr>
                    @endforeach
                </div>

                <form action="/tutor/toDo={{$toDoDetail->id}}/comment/create" method="POST">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="description">Comentário:</label>
                        </div>
                        <div class="form-group col-md-7">
                            <textarea type="text" rows="2" cols="50" class="form-control" name="description" id="description" placeholder="Descrição"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <button type="submit" class="btn btn-success"><i class="fa-solid fa-comment"></i></button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endif

<!-- MODAL EDITAR COMENTARIO -->
@if(!empty($commentEdit) && !empty($toDoCommentEdit))
<script>
    $(function() {
        $('#commentEdit').modal('show');
    });
</script>

<div class="modal fade" id="commentEdit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar mensagem</h5>
                <form action="/tutor/toDo/show={{$toDoCommentEdit->id}}">
                    <button class="close" type="submit" aria-hidden="true">&times;</button>
                </form>
            </div>
            <div class="modal-body">
                <form action="/tutor/toDo={{$toDoCommentEdit->id}}/comment/update={{$commentEdit->id}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <textarea type="text" rows="2" cols="50" class="form-control" name="descriptionEdit" id="descriptionEdit" placeholder="Editar mensagem">{{$commentEdit->description}}</textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
                </form>
                <form action="/tutor/toDo={{$toDoCommentEdit->id}}/comment/destroy={{$commentEdit->id}}">
                    <button type="submit" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
@endif

@endsection
