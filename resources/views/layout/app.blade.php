<!DOCTYPE html>

<?php

use Illuminate\Support\Facades\Session;
?>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <meta name="viewport" content="width=1024">

    <title>@yield('title')</title>

    <!-- CSS imports -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/trello.css') }}">

    <script src="https://kit.fontawesome.com/107ad067d9.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- MDB -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.10.0/mdb.min.js"></script>

    <!-- Bootstrap imports -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>

    <!-- Mask !-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>

    <!-- Open modal -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>

    <!-- Datatables imports -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.3/datatables.min.css" />
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.3/datatables.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.10.0/mdb.min.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap" rel="stylesheet" />

    <!-- Toastr imports -->
    <script src="{{ url('toastr/toastr.js') }}"></script>
    <link href="{{ url('toastr/toastr.css') }}" rel="stylesheet" />

    <!-- CSS imports -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/cookiePermission.css') }}">

    <!-- JS imports -->
    <script src="{{ url('js/app.js') }}"></script>
    <script src="{{ url('js/cookiePermission.js') }}"></script>

    <!-- SimpleMDE -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
    <script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/marked/marked.min.js"></script>

    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</head>

<body>
    <!-- NavBar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="/">Hunting de Talentos</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">Principal</a>
                </li>
                @auth
                    @if (session()->has('admin'))
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Operacional</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/client">Clientes</a>
                                <a class="dropdown-item" href="/tutor">Tutores</a>
                                <a class="dropdown-item" href="/company">Empresas</a>
                                <a class="dropdown-item" href="/admin">Administradores</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/admin/validateRegistration">Validar Cadastros</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tático</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/vacancy">Vagas</a>
                                <a class="dropdown-item" href="/formation">Formações</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/skill">Habilidades</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/profile">Perfil</a>
                        </li>
                    @elseif(session()->has('company'))
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Operacional</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/company/tutor">Meus Tutores</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tático</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/company/vacancy">Minhas Vagas</a>
                                <a class="dropdown-item" href="/company/formation">Minhas Formações</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/company/toDo">Minhas Tarefas</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/company/profile">Perfil</a>
                        </li>
                    @elseif(session()->has('tutor'))
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Operacional</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/tutor/company">Minhas Empresas</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tático</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/tutor/vacancy">Minhas Vagas</a>
                                <a class="dropdown-item" href="/tutor/formation">Minhas Formações</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/tutor/toDo">Minhas Tarefas</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/tutor/profile">Perfil</a>
                        </li>
                    @elseif(session()->has('client'))
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Acompanhar</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/client/vacancy">Minhas Vagas</a>
                                <a class="dropdown-item" href="/client/formation">Minhas Formações</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/client/skill">Minhas Habilidades</a>
                                <a class="dropdown-item" href="/client/skill/interest">Meus Interesses</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Gerenciar</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/client/history">Meu Histórico De Contratação</a>
                                <a class="dropdown-item" href="/client/formation/history">Meu Histórico De Formação</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/client/profile">Perfil</a>
                        </li>
                    @endif
                @endauth
            </ul>
            @guest
                <a class="btn btn-success" href="/login"><i class="fa-solid fa-right-from-bracket"></i></a>
            @endguest
            @auth
                <a class="btn btn-danger" href="/signOut"><i class="fa-solid fa-right-from-bracket"></i></a>
            @endauth
        </div>
    </nav>

    @if (Session::has('success'))
        <script>
            toastr.success('<?php echo Session::get('success'); ?>', 'Sucesso');
        </script>
    @endif

    @if (Session::has('fail'))
        <script>
            toastr.error('<?php echo Session::get('fail'); ?>', 'Falha');
        </script>
    @endif

    <?php session()->forget(['success', 'fail']); ?>

    @if (!empty($alertMessage))
        @if ($alertMessage == 'accessDenied')
            <script>
                toastr.error('Você não tem permissão para essa função', 'Falha');
            </script>
        @elseif($alertMessage == 'adminLogado')
            <script>
                toastr.success('Administrador logado com sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'companyLogado')
            <script>
                toastr.success('Empresa logada com sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'tutorLogado')
            <script>
                toastr.success('Tutor logado com sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'clientLogado')
            <script>
                toastr.success('Cliente logado com sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'signOut')
            <script>
                toastr.success('Saiu com sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'passwordError')
            <script>
                toastr.error('Senha errada', 'Falha');
            </script>
        @elseif($alertMessage == 'awaitApproved')
            <script>
                toastr.warning('Estamos analisando seu perfil, aguarde aprovação para realizar login', 'Atenção');
            </script>
        @elseif($alertMessage == 'checkEmail')
            <script>
                toastr.warning('Enviamos um email no endereço informado para confirmação da criação de conta', 'Atenção');
            </script>
        @elseif($alertMessage == 'emailInvalidated')
            <script>
                toastr.error('Email não validado, vefique no spam', 'Falha');
            </script>
        @elseif($alertMessage == 'emailVerified')
            <script>
                toastr.success('Email validado com sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'emailNotFound')
            <script>
                toastr.error('Email não encontrado', 'Falha');
            </script>
        @elseif($alertMessage == 'emailNotUnique')
            <script>
                toastr.error('Email já cadastrado', 'Falha');
            </script>
        @elseif($alertMessage == 'cpfNotUnique')
            <script>
                toastr.error('CPF já cadastrado', 'Falha');
            </script>
        @elseif($alertMessage == 'cnpjNotUnique')
            <script>
                toastr.error('CNPJ já cadastrado', 'Falha');
            </script>
        @elseif($alertMessage == 'subSucess')
            <script>
                toastr.success('Inscrito com sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'subFail')
            <script>
                toastr.error('Você não concluiu todos os cursos obrigatórios', 'Inscrito não realizada');
            </script>
        @elseif($alertMessage == 'fail')
            <script>
                toastr.error('Tente novamente mais tarde', 'Falha ao salvar');
            </script>
        @elseif($alertMessage == 'subFailStatus')
            <script>
                toastr.error('A vaga não aceita novos candidatos', 'Inscrito não realizada');
            </script>
        @elseif($alertMessage == 'createSucess')
            <script>
                toastr.success('Cadastrado com sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'removeSucess')
            <script>
                toastr.success('Removido com sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'editSucess')
            <script>
                toastr.success('Editado com sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'statusEdit')
            <script>
                toastr.success('Status editado com sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'statusEditFail')
            <script>
                toastr.error('Status não pode ser editado', 'Falha');
            </script>
        @elseif($alertMessage == 'failCPF')
            <script>
                toastr.error('CPF inválido', 'Falha');
            </script>
        @elseif($alertMessage == 'failCNPJ')
            <script>
                toastr.error('CNPJ inválido', 'Falha');
            </script>
        @elseif($alertMessage == 'failEmail')
            <script>
                toastr.error('Email não encontrado', 'Falha');
            </script>
        @elseif($alertMessage == 'failPhone')
            <script>
                toastr.error('Telefone inválido', 'Falha');
            </script>
        @elseif($alertMessage == 'validedSucess')
            <script>
                toastr.success('Usuário Validado Com Sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'forgotPasswordSentSuccess')
            <script>
                toastr.success('Email enviado com sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'changePasswordSuccess')
            <script>
                toastr.success('Senha alterada com sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'emailAlreadySent')
            <script>
                toastr.warning('Email já enviado, verifique no spam', 'Sucesso');
            </script>
        @elseif($alertMessage == 'formationFinish')
            <script>
                toastr.success('Formação concluida com sucesso', 'Sucesso');
            </script>
        @elseif($alertMessage == 'validedFail')
            <script>
                toastr.success('Usuário Negado Com Sucesso', 'Sucesso');
            </script>
        @endif
        <?php $alertMessage = null; ?>
    @endif

    <!-- Conteúdo da página -->
    <div class="container">
        <div style="margin: 2%" class="text-div-center">
            <h1>@yield('title')</h1>
        </div>
        @yield('body')
    </div>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

    <!-- SimpleMDE -->
    <script src="{{ url('js/markdown.js') }}"></script>
</body>

</html>
