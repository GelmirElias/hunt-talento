<!DOCTYPE html>
@extends('layout.app')

@section('title', 'Gerenciar Habilidades')

@section('body')
    <button type="submit" class="float-right btn btn-success" data-toggle="modal" data-target="#create"><i
            class="fa-solid fa-plus"></i></button>
    </br></br>
    <table id="datatable" class="table table-bordered table-hover display" border="1px">
        <thead>
            <tr>
                <td>Name</td>
                <td>Descrição</td>
                <td>Visibilidade</td>
                <td>Editar</td>
                <td>Visibilidade</td>
            </tr>
        </thead>

        <tbody>
            @foreach ($skills as $skill)
                @if ($skill->active > -1)
                    <tr>
                        <td>{{ $skill->name }}</td>
                        <td>{{ $skill->description }}</td>
                        <td>{{ $skill->active == 1 ? 'Ativo' : 'Inativo' }}</td>
                        <td>
                            <a href="/skill/edit={{ $skill->id }}" class="btn btn-warning edit"><i
                                    class="fa-solid fa-pen-to-square"></i></a>
                        </td>
                        <td><a href="/skill/active={{ $skill->id }}" class="btn btn-danger edit"><i
                                    class="fa-solid fa-arrows-rotate"></i></a>
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>

    <!-- MODAL EDITAR -->
    @if (!empty($skillEdit))
        <script>
            $(function() {
                $('#nameEdit').val('<?php echo $skillEdit->name; ?>');
                $('#descriptionEdit').val('<?php echo $skillEdit->description; ?>');
                $('.activeEdit').selectpicker('val', '<?php echo $skillEdit->active; ?>');
                $('#edit').modal('show');
            });
        </script>

        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
                        <form action="/skill">
                            <button class="close" type="submit" aria-hidden="true">&times;</button>
                        </form>
                    </div>
                    <div class="modal-body">
                        <form id="formEdit" action="{{ '/skill/update=' . $skillEdit->id }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="name">Nome:</label>
                                <input required type="text" class="form-control" name="nameEdit" id="nameEdit"
                                    placeholder="Nome">
                            </div>
                            <div class="form-group">
                                <label for="description">Descrição:</label>
                                <textarea rows="4" cols="50" type="text" class="form-control" name="descriptionEdit" id="descriptionEdit"
                                    placeholder="Descrição"></textarea>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="activeEdit">Visibilidade:</label>
                                    <select required name="activeEdit" class="selectpicker activeEdit"
                                        title="Selecione a visibilidade" data-width="100%">
                                        <option value="1">Ativo</option>
                                        <option value="0">Inativo</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <input form="formEdit" type="submit" class="btn btn-success" value="Salvar">
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- MODAL CRIAR -->
    <div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Criar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formCreate" action="/skill" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nome:</label>
                            <input required type="text" class="form-control" name="name" id="name"
                                placeholder="Nome">
                        </div>
                        <div class="form-group">
                            <label for="description">Descrição:</label>
                            <textarea rows="4" cols="50" type="text" class="form-control" name="description" id="description"
                                placeholder="Descrição"></textarea>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="active">Visibilidade:</label>
                                <select required name="active" class="selectpicker" title="Selecione a visibilidade"
                                    data-width="100%">
                                    <option selected value="1">Ativo</option>
                                    <option value="0">Inativo</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button form="formCreate" type="submit" class="btn btn-success"><i
                            class="fa-solid fa-floppy-disk"></i></button>
                </div>
            </div>
        </div>
    </div>
@endsection
