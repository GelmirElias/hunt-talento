<?php

use App\Models\Company;
use App\Models\CompanyTutor;
use App\Models\Permission;
use App\Models\User;

?>

<!DOCTYPE html>
@extends('layout.app')

@section('title', 'Gerenciar Tutores')

@section('body')
    <button type="submit" class="float-right btn btn-success" data-toggle="modal" data-target="#create"><i
            class="fa-solid fa-plus"></i></button>
    </br></br>
    <table id="datatable" class="table table-bordered table-hover display" border="1px">
        <thead>
            <tr>
                <td>Name</td>
                <td>CPF</td>
                <td>Email</td>
                <td>Permissão</td>
                <td>Visibilidade</td>
                <td>Editar</td>
                <td>Visibilidade</td>
            </tr>
        </thead>

        <tbody>
            @foreach ($tutors as $tutor)
                <?php
                $user = User::where('id', $tutor->user_id)->first();
                $permission = Permission::where('id', $user->permission_id)->first();
                ?>
                @if ($user->active > -1)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $tutor->cpf }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $permission->name }}</td>
                        <td>{{ $user->active == 1 ? 'Ativo' : 'Inativo' }}</td>
                        <td>
                            <a href="/tutor/edit={{ $user->id }}" class="btn btn-warning edit"><i
                                    class="fa-solid fa-pen-to-square"></i></a>
                        </td>
                        <td><a href="/tutor/active={{ $user->id }}" class="btn btn-danger edit"><i
                                    class="fa-solid fa-arrows-rotate"></i></a>
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>

    <!-- MODAL EDITAR -->
    @if (!empty($userEdit) && !empty($tutorEdit))
        <script>
            $(function() {
                <?php $companyTutor = CompanyTutor::where('tutor_id', $tutorEdit->id)->first(); ?>
                $('.companyIdEdit').selectpicker('val', '<?php echo $companyTutor->company_id; ?>');
                $('#nameEdit').val('<?php echo $userEdit->name; ?>');
                $('#cpfEdit').val('<?php echo $tutorEdit->cpf; ?>');
                $('#phoneEdit').val('<?php echo $userEdit->phone; ?>');
                $('#descriptionEdit').val('<?php echo $userEdit->description; ?>');
                $('#emailEdit').val('<?php echo $userEdit->email; ?>');
                $('#passwordEdit').val('<?php echo $userEdit->password; ?>');
                $('#permission_idEdit').val('<?php echo $userEdit->permission_id; ?>');
                $('.activeEdit').selectpicker('val', '<?php echo $userEdit->active; ?>');
                $('#edit').modal('show');
            });
        </script>

        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
                        <form action="/tutor">
                            <button class="close" type="submit" aria-hidden="true">&times;</button>
                        </form>
                    </div>
                    <div class="modal-body">
                        <form id="formEdit" action="{{ '/tutor/update=' . $tutorEdit->id . '/user=' . $userEdit->id }}"
                            method="POST" enctype="multipart/form-data">
                            @csrf
                            @if (!empty($userEdit->photo))
                                <div class="form-group">
                                    <label for="photoShow">Foto:</label>
                                    <img id="photoShow" src="/img/tutors/{{ $userEdit->photo }}" alt="photo user" width=240
                                        height=240>
                                </div>
                            @endif
                            <div class="row">
                                <div class="form-group">
                                    <label for="companyIdEdit">Empresa:</label>
                                    <select required name="companyIdEdit" class="selectpicker companyIdEdit"
                                        title="Selecione a empresa" data-width="100%" data-live-search="true">
                                        <option value="">Sem empresa</option>
                                        @foreach ($companies as $company)
                                            <?php $user = User::where('id', $company->user_id)->first(); ?>
                                            <option value="{{ $company->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name">Nome:</label>
                                <input required type="text" class="form-control" name="nameEdit" id="nameEdit"
                                    placeholder="Nome">
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="cpf">CPF:</label>
                                        <input required type="text" class="form-control" name="cpfEdit" id="cpfEdit"
                                            placeholder="CPF">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="phoneEdit">Telefone:</label>
                                        <input required type="text" class="form-control" name="phoneEdit" id="phoneEdit"
                                            onkeypress="mask(this, mphone);" onblur="mask(this, mphone);"
                                            placeholder="Telefone">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input required type="email" class="form-control" name="emailEdit" id="emailEdit"
                                    placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="password">Senha:</label>
                                <input required type="password" class="form-control" name="passwordEdit" id="passwordEdit"
                                    placeholder="Senha">
                            </div>
                            <div class="form-group">
                                <label for="description">Descrição:</label>
                                <textarea rows="4" cols="50" type="text" class="form-control" name="descriptionEdit" id="descriptionEdit"
                                    placeholder="Descrição"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="photo">Foto:</label>
                                <input type="file" class="form-control-file" name="photoEdit" id="photoEdit">
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="activeEdit">Visibilidade:</label>
                                    <select required name="activeEdit" class="selectpicker activeEdit"
                                        title="Selecione a visibilidade" data-width="100%">
                                        <option value="1">Ativo</option>
                                        <option value="0">Inativo</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button form="formEdit" type="submit" class="btn btn-success"><i
                                class="fa-solid fa-floppy-disk"></i></button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- MODAL CRIAR -->
    <div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Criar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formCreate" action="/tutor" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="form-group">
                                <label for="companyId">Empresa:</label>
                                <select required name="companyId" class="selectpicker" title="Selecione a empresa"
                                    data-width="100%" data-live-search="true">
                                    <option selected value="">Sem empresa</option>
                                    @foreach ($companies as $company)
                                        <?php $user = User::where('id', $company->user_id)->first(); ?>
                                        <option value="{{ $company->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name">Nome:</label>
                            <input required type="text" class="form-control" name="name" id="name"
                                placeholder="Nome">
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="cpf">CPF:</label>
                                    <input required type="text" class="form-control" name="cpf" id="cpf"
                                        placeholder="CPF">
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label for="phone">Telefone:</label>
                                    <input required type="text" class="form-control" name="phone" id="phone"
                                        onkeypress="mask(this, mphone);" onblur="mask(this, mphone);"
                                        placeholder="Telefone">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input required type="email" class="form-control" name="email" id="email"
                                placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="password">Senha:</label>
                            <input required type="password" class="form-control" name="password" id="password"
                                placeholder="Senha">
                        </div>
                        <div class="form-group">
                            <label for="description">Descrição:</label>
                            <textarea rows="4" cols="50" type="text" class="form-control" name="description" id="description"
                                placeholder="Descrição"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="photo">Foto:</label>
                            <input type="file" class="form-control-file" name="photo" id="photo">
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="active">Visibilidade:</label>
                                <select required name="active" class="selectpicker" title="Selecione o status"
                                    data-width="100%">
                                    <option selected value="1">Ativo</option>
                                    <option value="0">Inativo</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button form="formCreate" type="submit" class="btn btn-success"><i
                            class="fa-solid fa-floppy-disk"></i></button>
                </div>
            </div>
        </div>
    </div>
@endsection
