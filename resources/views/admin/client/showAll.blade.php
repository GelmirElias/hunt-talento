<?php

use App\Models\Permission;
use App\Models\User;

?>

<!DOCTYPE html>
@extends('layout.app')

@section('title', 'Gerenciar Clientes')

@section('body')
    <button type="submit" class="float-right btn btn-success" data-toggle="modal" data-target="#create"><i
            class="fa-solid fa-plus"></i></button>
    </br></br>
    <table id="datatable" class="table table-bordered table-hover display" border="1px">
        <thead>
            <tr>
                <td>Name</td>
                <td>CPF</td>
                <td>Email</td>
                <td>Permissão</td>
                <td>Visibilidade</td>
                <td>Editar</td>
                <td>Visibilidade</td>
            </tr>
        </thead>

        <tbody>
            @foreach ($clients as $client)
                <?php
                $user = User::where('id', $client->user_id)->first();
                $permission = Permission::where('id', $user->permission_id)->first();
                ?>
                @if ($user->active > -1)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $client->cpf }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $permission->name }}</td>
                        <td>{{ $user->active == 1 ? 'Ativo' : 'Inativo' }}</td>
                        <td>
                            <a href="/client/edit={{ $user->id }}" class="btn btn-warning edit"><i
                                    class="fa-solid fa-pen-to-square"></i></a>
                        </td>
                        <td><a href="/client/active={{ $user->id }}" class="btn btn-danger edit"><i
                                    class="fa-solid fa-arrows-rotate"></i></a>
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>

    <!-- MODAL EDITAR -->
    @if (!empty($userEdit) && !empty($clientEdit))
        <script>
            $(function() {
                $('#nameEdit').val('<?php echo $userEdit->name; ?>');
                $('#cpfEdit').val('<?php echo $clientEdit->cpf; ?>');
                $('#phoneEdit').val('<?php echo $userEdit->phone; ?>');
                $('#descriptionEdit').val('<?php echo $userEdit->description; ?>');
                $('#emailEdit').val('<?php echo $userEdit->email; ?>');
                $('#passwordEdit').val('<?php echo $userEdit->password; ?>');
                $('.permission_idEdit').selectpicker('val', '<?php echo $userEdit->permission_id; ?>');
                $('.activeEdit').selectpicker('val', '<?php echo $userEdit->active; ?>');
                $('#edit').modal('show');
            });
        </script>

        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
                        <form action="/client">
                            <button class="close" type="submit" aria-hidden="true">&times;</button>
                        </form>
                    </div>
                    <div class="modal-body">
                        <form id="formEdit" action="{{ '/client/update=' . $clientEdit->id . '/user=' . $userEdit->id }}"
                            method="POST" enctype="multipart/form-data">
                            @csrf
                            @if (!empty($userEdit->photo))
                                <div class="form-group">
                                    <label for="photoShow">Foto:</label>
                                    <img id="photoShow" src="/img/clients/{{ $userEdit->photo }}" alt="photo user" width=240
                                        height=240>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="name">Nome:</label>
                                <input required type="text" class="form-control" name="nameEdit" id="nameEdit"
                                    placeholder="Nome">
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="cpf">CPF:</label>
                                        <input required type="text" class="form-control" name="cpfEdit" id="cpfEdit"
                                            placeholder="CPF">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="phoneEdit">Telefone:</label>
                                        <input required type="text" class="form-control" name="phoneEdit" id="phoneEdit"
                                            onkeypress="mask(this, mphone);" onblur="mask(this, mphone);"
                                            placeholder="Telefone">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input required type="email" class="form-control" name="emailEdit" id="emailEdit"
                                    placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="password">Senha:</label>
                                <input required type="password" class="form-control" name="passwordEdit" id="passwordEdit"
                                    placeholder="Senha">
                            </div>
                            <div class="form-group">
                                <label for="description">Descrição:</label>
                                <textarea rows="4" cols="50" type="text" class="form-control" name="descriptionEdit" id="descriptionEdit"
                                    placeholder="Descrição"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="photo">Foto:</label>
                                <input type="file" class="form-control-file" name="photoEdit" id="photoEdit">
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="permissionEdit">Permissão:</label>
                                        <select required name="permission_idEdit" class="selectpicker permission_idEdit"
                                            title="Selecione a permissão" data-width="100%" data-live-search="true">
                                            @foreach ($permissions as $permission)
                                                <option value="{{ $permission->id }}">{{ $permission->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="activeEdit">Visibilidade:</label>
                                        <select required name="activeEdit" class="selectpicker activeEdit"
                                            title="Selecione a visibilidade" data-width="100%">
                                            <option value="1">Ativo</option>
                                            <option value="0">Inativo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button form="formEdit" type="submit" class="btn btn-success"><i
                                class="fa-solid fa-floppy-disk"></i></button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- MODAL CRIAR -->
    <div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Criar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formCreate" action="/client" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nome:</label>
                            <input required type="text" class="form-control" name="name" id="name"
                                placeholder="Nome">
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="cpf">CPF:</label>
                                    <input required type="text" class="form-control" name="cpf" id="cpf"
                                        placeholder="CPF">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="phone">Telefone:</label>
                                    <input required type="text" class="form-control" name="phone" id="phone"
                                        onkeypress="mask(this, mphone);" onblur="mask(this, mphone);"
                                        placeholder="Telefone">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input required type="email" class="form-control" name="email" id="email"
                                placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="password">Senha:</label>
                            <input required type="password" class="form-control" name="password" id="password"
                                placeholder="Senha">
                        </div>
                        <div class="form-group">
                            <label for="description">Descrição:</label>
                            <textarea rows="4" cols="50" type="text" class="form-control" name="description" id="description"
                                placeholder="Descrição"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="photo">Foto:</label>
                            <input type="file" class="form-control-file" name="photo" id="photo">
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission">Permissão:</label>
                                    <select required class="selectpicker" name="permission_id"
                                        title="Selecione a permissão" data-width="100%" data-live-search="true">
                                        @foreach ($permissions as $permission)
                                            <option value="{{ $permission->id }}">{{ $permission->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="active">Visibilidade:</label>
                                    <select required name="active" class="selectpicker" title="Selecione a visibilidade"
                                        data-width="100%">
                                        <option selected value="1">Ativo</option>
                                        <option value="0">Inativo</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button form="formCreate" type="submit" class="btn btn-success"><i
                            class="fa-solid fa-floppy-disk"></i></button>
                </div>
            </div>
        </div>
    </div>
@endsection
