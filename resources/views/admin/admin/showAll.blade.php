<?php

use App\Models\Permission;
use App\Models\User;

?>

<!DOCTYPE html>
@extends('layout.app')

@section('title', 'Gerenciar Administradores')

@section('body')
    <button type="submit" class="float-right btn btn-success" data-toggle="modal" data-target="#create"><i
            class="fa-solid fa-plus"></i></button>
    </br></br>
    <table id="datatable" class="table table-bordered table-hover display" border="1px">
        <thead>
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>Permissão</td>
                <td>Visibilidade</td>
                <td>Editar</td>
                <td>Visibilidade</td>
            </tr>
        </thead>

        <tbody>
            @foreach ($users as $user)
                <?php
                $permission = Permission::where('id', $user->permission_id)->first();
                ?>

                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $permission->name }}</td>
                    <td>{{ $user->active == 1 ? 'Ativo' : 'Inativo' }}</td>
                    <td>
                        <a href="/admin/edit={{ $user->id }}" class="btn btn-warning edit"><i
                                class="fa-solid fa-pen-to-square"></i></a>
                    </td>
                    <td><a href="/admin/active={{ $user->id }}" class="btn btn-danger edit"><i
                                class="fa-solid fa-arrows-rotate"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <!-- MODAL EDITAR -->
    @if (!empty($userEdit))
        <script>
            $(function() {
                $('#nameEdit').val('<?php echo $userEdit->name; ?>');
                $('#descriptionEdit').val('<?php echo $userEdit->description; ?>');
                $('#phoneEdit').val('<?php echo $userEdit->phone; ?>');
                $('#emailEdit').val('<?php echo $userEdit->email; ?>');
                $('#passwordEdit').val('<?php echo $userEdit->password; ?>');
                $('.activeEdit').selectpicker('val', '<?php echo $userEdit->active; ?>');
                $('#edit').modal('show');
            });
        </script>

        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
                        <form action="/admin">
                            <button class="close" type="submit" aria-hidden="true">&times;</button>
                        </form>
                    </div>
                    <div class="modal-body">
                        <form id="formEdit" action="{{ '/admin/update=' . $userEdit->id }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @if (!empty($userEdit->photo))
                                <div class="form-group">
                                    <label for="photoShow">Foto:</label>
                                    <img id="photoShow" src="/img/admins/{{ $userEdit->photo }}" alt="photo user" width=240
                                        height=240>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="name">Nome:</label>
                                <input required type="text" class="form-control" name="nameEdit" id="nameEdit"
                                    placeholder="Nome">
                            </div>
                            <div class="form-group">
                                <label for="phoneEdit">Telefone:</label>
                                <input required type="text" class="form-control" name="phoneEdit" id="phoneEdit"
                                    onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" placeholder="Telefone">
                            </div>
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input required type="email" class="form-control" name="emailEdit" id="emailEdit"
                                    placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="password">Senha:</label>
                                <input required type="password" class="form-control" name="passwordEdit" id="passwordEdit"
                                    placeholder="Senha">
                            </div>
                            <div class="form-group">
                                <label for="description">Descrição:</label>
                                <textarea rows="4" cols="50" type="text" class="form-control" name="descriptionEdit" id="descriptionEdit"
                                    placeholder="Descrição"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="photo">Foto:</label>
                                <input type="file" class="form-control-file" name="photoEdit" id="photoEdit">
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="activeEdit">Visibilidade:</label>
                                    <select required name="activeEdit" class="selectpicker activeEdit"
                                        title="Selecione a visibilidade" data-width="100%">
                                        <option value="1">Ativo</option>
                                        <option value="0">Inativo</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button form="formEdit" type="submit" class="btn btn-success"><i
                                class="fa-solid fa-floppy-disk"></i></button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- MODAL CRIAR -->
    <div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Criar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formCreate" action="/admin" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nome:</label>
                            <input required type="text" class="form-control" name="name" id="name"
                                placeholder="Nome">
                        </div>
                        <div class="form-group">
                            <label for="phone">Telefone:</label>
                            <input required type="text" class="form-control" name="phone" id="phone"
                                onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" placeholder="Telefone">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input required type="email" class="form-control" name="email" id="email"
                                placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="password">Senha:</label>
                            <input required type="password" class="form-control" name="password" id="password"
                                placeholder="Senha">
                        </div>
                        <div class="form-group">
                            <label for="description">Descrição:</label>
                            <textarea rows="4" cols="50" type="text" class="form-control" name="description" id="description"
                                placeholder="Descrição"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="photo">Foto:</label>
                            <input type="file" class="form-control-file" name="photo" id="photo">
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="active">Visibilidade:</label>
                                <select required name="active" class="selectpicker" title="Selecione a visibilidade"
                                    data-width="100%">
                                    <option selected value="1">Ativo</option>
                                    <option value="0">Inativo</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button form="formCreate" type="submit" class="btn btn-success"><i
                            class="fa-solid fa-floppy-disk"></i></button>
                </div>
            </div>
        </div>
    </div>
@endsection
