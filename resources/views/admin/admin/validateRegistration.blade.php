<?php

use App\Models\Permission;
use App\Models\User;

?>

<!DOCTYPE html>
@extends('layout.app')

@section('title', "Validar Cadastros")

@section('body')
<table id="datatable" class="table table-bordered table-hover display" border="1px">
    <thead>
        <tr>
            <td>Name</td>
            <td>Email</td>
            <td>Permissão</td>
            <td>Status</td>
            <td>Ações</td>
        </tr>
    </thead>

    <tbody>
        @foreach ($users as $user)
        <?php
        $permission = Permission::where('id', $user->permission_id)->first();
        ?>

        <tr>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$permission->name}}</td>
            <td>Aguardando Aprovação</td>
            <td>
                <form action="{{'/admin/show/validateRegistrationUser=' . $user->id}}" method="POST">
                    @csrf
                    <button class="btn btn-primary edit" type="submit">Validar Usuário</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<!-- MODAL VALIDADOR -->
@if(!empty($userEdit))
<script>
    $(function() {
        $('#nameEdit').val('<?php echo $userEdit->name ?>');
        $('#emailEdit').val('<?php echo $userEdit->email ?>');
        $('#phoneEdit').val('<?php echo $userEdit->phone ?>');
        <?php if (!empty($clientEdit)) { ?>
            $('#cpfcnpj').val('<?php echo $clientEdit->cpf ?>');
        <?php } else { ?>
            $('#cpfcnpj').val('<?php echo $companyEdit->cnpj ?>');
        <?php } ?>
        $('#edit').modal('show');
    });
</script>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Usuário Selecionado</h5>
                <form action="/admin/validateRegistration">
                    <button class="close" type="submit" aria-hidden="true">&times;</button>
                </form>
            </div>
            <div class="modal-body">
                <form action="{{'/admin/validateRegistrationUpdate=' . $userEdit->id . '/status=' . 1}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nome:</label>
                        <input disabled type="text" class="form-control" name="nameEdit" id="nameEdit" placeholder="Nome">
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input disabled type="text" class="form-control" name="emailEdit" id="emailEdit" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="cpfcnpj">CPF ou CNPJ:</label>
                        <input disabled class="form-control" name="cpfcnpj" id="cpfcnpj" type="text" placeholder="Pessoa física CPF, Pessoa juridica CNPJ">
                    </div>
                    <div class="form-group">
                        <label for="phoneEdit">Telefone:</label>
                        <input disabled class="form-control" name="phoneEdit" id="phoneEdit" type="text" placeholder="Telefone">
                    </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-success" value="Validar Acesso">
                </form>
                <form action="{{'/admin/validateRegistrationUpdate='. $userEdit->id . '/status=' . 0}}" method="POST">
                    @csrf
                    <input type="submit" class="btn btn-danger" value="Negar Acesso">
                </form>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
