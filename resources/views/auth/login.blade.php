<!DOCTYPE html>
@extends('layout.app')

@section('title', "Entrar")

@section('body')
</br>
<form action="/signIn" method="POST">
    @csrf
    <div class="card">
        <article class="card-body">
            <form>
                <div class="form-group">
                    <label for="emailSignIn">Email:</label>
                    <input required type="text" class="form-control" value="leonardomartin98@admin.com.br-igoremarciafilmagensme@empresa.com.br-lizsophialarissaferreira@tutor.com.br-simonelizesthernunes@cliente.com.br" name="emailSignIn" id="emailSignIn" placeholder="Digite seu email">
                    <!-- <input required type="text" class="form-control" value="simonelizesthernunes@cliente.com.br" name="emailSignIn" id="emailSignIn" placeholder="Digite seu email"> -->
                </div>
                <div class="form-group">
                    <a class="float-right" href="" data-toggle="modal" data-target="#esqueci">Esqueci minha senha</a>
                    <label for="passwordSignIn">Senha:</label>
                    <input required type="password" class="form-control" value="12345" name="passwordSignIn" id="passwordSignIn" placeholder="Digite sua senha">
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label> <input type="checkbox"> Salvar Senha </label>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"> Logar </button>
                </div>
            </form>
            <a href="" class="btn btn-outline-primary btn-block" data-toggle="modal" data-target="#choose">Criar Conta</a>
        </article>
    </div>
</form>
</br>

<!-- MODAL ESQUECI MINHA SENHA-->
<div class="modal fade" id="esqueci" tabindex="-1" role="dialog" aria-labelledby="esqueci" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Esqueci minha senha</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/forgotPassword" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="email">Insira o seu e-mail:</label>
                        <input required type="email" class="form-control" name="email" id="email" placeholder="Insira o seu e-mail de acesso">
                    </div>
                    <span>Você receberá os próximos passos no seu email caso ele esteja cadastrado em nossa base.</span>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-success" value="Enviar">
            </div>
            </form>
        </div>
    </div>
</div>

<!-- MODAL ESCOLHA -->
<div class="modal fade" id="choose" tabindex="-1" role="dialog" aria-labelledby="choose" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Escolher o tipo de conta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <a href="" class="float-left btn btn-outline-primary" data-toggle="modal" data-target="#fisica">Pessoa física</a>
                <a href="" class="float-right btn btn-outline-primary" data-toggle="modal" data-target="#juridica">Pessoa juridica</a>
            </div>
        </div>
    </div>
</div>

<!-- MODAL CRIAR PESSOA FISICA-->
<div class="modal fade" id="fisica" tabindex="-1" role="dialog" aria-labelledby="fisica" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Criar Conta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/storeCPF" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nome Completo:</label>
                        <input required type="text" class="form-control" name="name" id="name" placeholder="Pessoa física nome completo">
                    </div>
                    <div class="form-group">
                        <label for="cpf">CPF:</label>
                        <input required class="form-control" name="cpf" id="cpf" type="text" placeholder="Pessoa física CPF">
                    </div>
                    <div class="form-group">
                        <label for="phone">Telefone:</label>
                        <input required type="text" class="form-control" name="phone" id="phone" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" placeholder="Telefone">
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input required type="email" class="form-control" name="email" id="email" placeholder="Digite seu email">
                    </div>
                    <div class="form-group">
                        <label for="password">Senha:</label>
                        <input required type="password" class="form-control" name="password" id="password" placeholder="Digite sua senha">
                    </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-success" value="Criar">
            </div>
            </form>
        </div>
    </div>
</div>

<!-- MODAL CRIAR PESSOA JURIDICA-->
<div class="modal fade" id="juridica" tabindex="-1" role="dialog" aria-labelledby="juridica" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Criar Conta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/storeCNPJ" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nome Fantasia:</label>
                        <input required type="text" class="form-control" name="name" id="name" placeholder="Pessoa juridica nome fantasia">
                    </div>
                    <div class="form-group">
                        <label for="cnpj">CNPJ:</label>
                        <input required class="form-control" name="cnpj" id="cnpj" type="text" placeholder="Pessoa juridica CNPJ">
                    </div>
                    <div class="form-group">
                        <label for="phone">Telefone:</label>
                        <input required type="text" class="form-control" name="phone" id="phone" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" placeholder="Telefone">
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input required type="email" class="form-control" name="email" id="email" placeholder="Digite seu email">
                    </div>
                    <div class="form-group">
                        <label for="password">Senha:</label>
                        <input required type="password" class="form-control" name="password" id="password" placeholder="Digite sua senha">
                    </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-success" value="Criar">
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
