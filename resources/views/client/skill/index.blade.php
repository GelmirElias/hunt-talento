<!DOCTYPE html>

<?php

use App\Models\ClientSkill;
use App\Models\Skill;
use Illuminate\Support\Facades\Session;

?>

@extends('layout.app')

@section('title', 'Gerenciar Habilidades')

@section('body')
    <button type="submit" class="float-right btn btn-success" data-toggle="modal" data-target="#create"><i
            class="fa-solid fa-plus"></i></button>
    <a data-toggle="modal" data-target="#information">
        <button type="button" class="btn btn-warning float-right" data-toggle="tooltip" data-placement="top"
            title="O que são habilidades?">
            <span type="button" class="fa-solid fa-info fa-xl">
        </button>
    </a>
    </br></br>
    <table id="datatable" class="table table-bordered table-hover display" border="1px">
        <thead>
            <tr>
                <td>Nome</td>
                <td>Descrição</td>
                <td>Domínio</td>
                <td>Editar</td>
                <td>Remover</td>
            </tr>
        </thead>

        <tbody>
            @foreach ($clientSkills as $clientSkill)
                <?php
                $skill = Skill::where('id', $clientSkill->skill_id)->first(); ?>
                <tr>
                    <td>{{ $skill->name }}</td>
                    <td>{{ $skill->description }}</td>
                    <td>
                        @if ($clientSkill->rate == 1)
                            <i class="fa-solid fa-star" style="color: #f37101;"></i>
                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                        @elseif($clientSkill->rate == 2)
                            <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                            <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                        @elseif($clientSkill->rate == 3)
                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                            <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                            <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                        @elseif($clientSkill->rate == 4)
                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                            <i class="fa-regular fa-star" style="color: #8fbc30;"></i>
                        @elseif($clientSkill->rate == 5)
                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                        @else
                            <i class="fa-regular fa-star"></i>
                            <i class="fa-regular fa-star"></i>
                            <i class="fa-regular fa-star"></i>
                            <i class="fa-regular fa-star"></i>
                            <i class="fa-regular fa-star"></i>
                        @endif
                    </td>
                    <td>
                        <form action="/client/skill/edit={{ $clientSkill->id }}">
                            <button class="btn btn-warning edit" type="submit"><i
                                    class="fa-solid fa-pen-to-square"></i></button>
                        </form>
                    </td>
                    <td>
                        <form action="/client/skill/destroy={{ $clientSkill->id }}">
                            <button type="submit" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <!-- MODAL EDITAR -->
    @if (session()->has('clientSkillEdit'))
        <?php $clientSkillEdit = Session::get('clientSkillEdit'); ?>

        <script>
            $(function() {
                $('#rateEdit').val('<?php echo $clientSkillEdit->rate; ?>');

                var sliderEdit = document.getElementById("rateEdit");
                var outputEdit = document.getElementById("rateEditShow");

                outputEdit.innerHTML = sliderEdit.value;
                sliderEdit.oninput = function() {
                    outputEdit.innerHTML = this.value;
                }

                $('#edit').modal('show');
            });
        </script>

        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
                        <form action="/client/skill">
                            <button class="close" type="submit" aria-hidden="true">&times;</button>
                        </form>
                    </div>
                    <form id="editSkill" action="/client/skill/update={{ $clientSkillEdit->id }}">
                        <div class="modal-body">
                            @csrf
                            <div class="form-group">
                                <label for="rateEdit">Autoavaliação:</label> <span id="rateEditShow"></span>
                                <input required type="range" class="form-range" min="0" max="5"
                                    name="rateEdit" id="rateEdit" placeholder="Autoavaliação">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button form="editSkill" type="submit" class="btn btn-success"><i
                                    class="fa-solid fa-floppy-disk"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script>
            var sliderEdit = document.getElementById("rateEdit");
            var outputEdit = document.getElementById("rateEditShow");

            outputEdit.innerHTML = sliderEdit.value;
            sliderEdit.oninput = function() {
                outputEdit.innerHTML = this.value;
            }
        </script>
    @endif

    <!-- MODAL CRIAR -->
    <div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Adicionar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="createSkill" action="/client/skill" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group">
                                <label for="skillCreate">Habilidade:</label>
                                <select required name="skillCreate" class="selectpicker" title="Selecione o habilidade"
                                    data-width="100%" data-live-search="true">
                                    @foreach ($skillsCreate as $skillCreate)
                                        <option value="{{ $skillCreate->id }}"
                                            data-subtext="{{ $skillCreate->description }}">{{ $skillCreate->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="rateCreate">Autoavaliação:</label> <span id="rateCreateShow"></span>
                            <input required type="range" class="form-range" min="0" max="5"
                                name="rateCreate" id="rateCreate" placeholder="Autoavaliação">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button form="createSkill" type="submit" class="btn btn-success"><i
                            class="fa-solid fa-floppy-disk"></i></button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var sliderCreate = document.getElementById("rateCreate");
        var outputCreate = document.getElementById("rateCreateShow");

        outputCreate.innerHTML = sliderCreate.value;
        sliderCreate.oninput = function() {
            outputCreate.innerHTML = this.value;
        }
    </script>

    <!-- MODAL INFORMATIVO -->
    <div class="modal fade" id="information" tabindex="-1" role="dialog" aria-labelledby="information"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">O que são habilidades?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Habilidades são linguagens de programação vinculadas a você de forma manual. <br><br>
                        Elas podem ser obtidas da seguinte forma:
                    <ul>
                        <li>Você adicionando uma linguagem que conhece.</li>
                        <li>Você adicionando o nível de domínio da linguagem.</li>
                    </ul>
                    São úteis para sugerir vagas e formações que você possui domínio.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" aria-label="Close">
                        Compreendi
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
