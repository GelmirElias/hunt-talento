<?php

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\User;

$controller = new Controller;

?>

<!DOCTYPE html>
@extends('layout.app')

@section('title', "Meu Histórico De Contratação")

@section('body')
<table id="datatable" class="table table-bordered table-hover display" border="1px">
    <thead>
        <tr>
            <td>Empresa</td>
            <td>Cargo Ocupado</td>
            <td>Salário</td>
            <td>Data de Contratação</td>
        </tr>
    </thead>

    <tbody>
        @foreach ($hiringHistories as $hiringHistory)
        <?php
        $company = Company::where('id', $hiringHistory->company_id)->first();
        $user = User::where('id', $company->user_id)->first();
        ?>
        <tr>
            <td>{{$user->name}}</td>
            <td>{{$hiringHistory->role}}</td>
            <td>R$ {{$hiringHistory->salary}}</td>
            <td><?php echo $controller->convertStringToDateTimeFormateDate($hiringHistory->start_date) ?></td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
