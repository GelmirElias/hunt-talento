<?php

use App\Http\Controllers\FormationController;
use App\Http\Controllers\SkillController;
use App\Http\Controllers\UtilController;
use App\Models\Candidate;
use App\Models\CandidateComment;
use App\Models\CandidateHistory;
use App\Models\Client;
use App\Models\ClientFormation;
use App\Models\ClientSkill;
use App\Models\Company;
use App\Models\Formation;
use App\Models\FormationType;
use App\Models\HiringHistory;
use App\Models\Interview;
use App\Models\Permission;
use App\Models\Skill;
use App\Models\StatusVacancy;
use App\Models\User;
use App\Models\Vacancy;
use App\Models\VacancySkill;
use Illuminate\Support\Facades\Session;

?>

<!DOCTYPE html>
@extends('layout.app')

@section('title', "$vacancy->name")

@section('body')

<div class="col">
    <div class="row">
        <div class="col">
            <a type="submit" class="float-left btn btn-primary" href="/client/vacancy"><i class="fa-solid fa-backward"></i></a>
        </div>
    </div>
    <br>
    <div class="row">

        <!-- Todos os candidatos -->
        <section class="lists-container">

            <!-- Status Candidate -->
            @foreach($statusCandidates as $statusCandidate)
            <div class="list">

                <?php
                $candidates = Candidate::where('status_candidates_id', $statusCandidate->id)->where('vacancy_id', $vacancy->id)->where('client_id', session('client')->id)->get();
                ?>

                <h4 data-toggle="tooltip" title="{{$statusCandidate->description}}" class="list-title">{{$statusCandidate->name}}</h4>

                <ul class="list-items">
                    @foreach ($candidates as $candidate)
                    <?php $client = Client::where('id', $candidate->client_id)->first(); ?>
                    <?php $user = User::where('id', $client->user_id)->first(); ?>

                    <a href="/client/vacancy/candidate/show={{$candidate->id}}">
                        <li>
                            <div class="row" type="submit">
                                <div class="col">
                                    <div data-toggle="tooltip" title="{{$user->description}}" class="text-div-center">{{$user->name}}</div>
                                    <div class="divider"></div>
                                </div>
                            </div>
                            <td>
                                @if($candidate->classification == 1)
                                <i class="fa-solid fa-star" style="color: #f37101;"></i>
                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                @elseif($candidate->classification == 2)
                                <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                @elseif($candidate->classification == 3)
                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                @elseif($candidate->classification == 4)
                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                <i class="fa-regular fa-star" style="color: #8fbc30;"></i>
                                @elseif($candidate->classification == 5)
                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                @else
                                <i class="fa-regular fa-star"></i>
                                <i class="fa-regular fa-star"></i>
                                <i class="fa-regular fa-star"></i>
                                <i class="fa-regular fa-star"></i>
                                <i class="fa-regular fa-star"></i>
                                @endif
                            </td>
                        </li>
                    </a>
                    <div class="divider"></div>
                    @endforeach
                </ul>

            </div>
            @endforeach

        </section>
        <!-- End of lists container -->
    </div>
</div>

<!-- MODAL SHOW DETALHES -->
@if(Session::has('candidateShowDetail'))
<?php

// recupera dados da sessão
$candidateShowDetail = Session::get('candidateShowDetail');
$candidateCreateInterview = Session::get('candidateCreateInterview');
$candidateEditInterview = Session::get('candidateEditInterview');
$candidateApproved = Session::get('candidateApproved');
$candidateUnapproved = Session::get('candidateUnapproved');
$historicInterview = Session::get('historicInterview');
$candidateComments = Session::get('candidateComments');
$candidateHistories = Session::get('candidateHistories');
$candidateDocuments = Session::get('candidateDocuments');

//informações do candidato
$clientShow = Client::where('id', $candidateShowDetail->client_id)->first();
$userShow = User::where('id', $clientShow->user_id)->first();

$permissionShow = Permission::where('id', $userShow->permission_id)->first();

//formações do candidato
$clientFormations = ClientFormation::where('client_id', $clientShow->id)->get();
$clientFormations = $clientFormations->count() == 0 ? false : $clientFormations;

//talentos do candidato
$manualClientSkills = ClientSkill::where('client_id', $clientShow->id)->where('client_assigned', true)->get();
$manualClientSkills = $manualClientSkills->count() == 0 ? false : $manualClientSkills;

$automaticClientSkills = ClientSkill::where('client_id', $clientShow->id)->where('client_assigned', false)->get();
$automaticClientSkills = $automaticClientSkills->count() == 0 ? false : $automaticClientSkills;
?>

<script>
    $(function() {
        $('#nameShow').val('<?php echo $userShow->name ?>');
        $('#phoneShow').val('<?php echo $userShow->phone ?>');
        $('#cpfShow').val('<?php echo $clientShow->cpf ?>');
        $('#descriptionShow').val('<?php echo $userShow->description ?>');
        $('#emailShow').val('<?php echo $userShow->email ?>');
        $('#permissionShow').val('<?php echo $permissionShow->name ?>');
        $('#activeShow').val('<?php echo $userShow->active ?>');
        $('#myModal').modal('show');
    });
</script>

<!-- myModal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#interview" role="tab" aria-controls="interview">Entrevista</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#personaInfo" role="tab" aria-controls="personaInfo">Candidato</a>
                    </li>
                </ul>

                <form action="/client/vacancy/candidates/show={{$vacancy->id}}">
                    <button class="close" type="submit" aria-hidden="true">&times;</button>
                </form>
            </div>
            <div class="modal-body">
                <div class="tab-content">
                    <!-- Entrevista -->
                    <div class="tab-pane active" id="interview" role="tabpanel">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">

                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#historicInterview" role="tab" aria-controls="historicInterview">Histórico entrevista</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#historicCandidate" role="tab" aria-controls="historicCandidate">Histórico candidato</a>
                            </li>

                        </ul>

                        <div class="tab-content">
                            <!-- Historico entrevista -->
                            <div class="tab-pane active" id="historicInterview" role="tabpanel">
                                <?php
                                $interviewEdit = Interview::where('candidate_id', $candidateShowDetail->id)->first();
                                ?>

                                @if(!empty($interviewEdit))
                                <div style="margin: 3%">
                                    <script>
                                        $(function() {
                                            $('#nameClientEdit').val('<?php echo $userShow->name ?>');
                                            $('#phoneClienteCreate').val('<?php echo $userShow->phone ?>');
                                            $('#cpfClientEdit').val('<?php echo $clientShow->cpf ?>');
                                            $('#emailClientEdit').val('<?php echo $userShow->email ?>');
                                            $('#descriptionEdit').val('<?php echo $interviewEdit->description ?>');
                                            $('#startDateEdit').val('<?php echo $interviewEdit->start_date ?>');
                                            $('#endDateEdit').val('<?php echo $interviewEdit->end_date ?>');
                                            $('.approvedEdit').selectpicker('val', '<?php echo $interviewEdit->approved == null ? "-1" : $interviewEdit->approved ?>');
                                            $('.activeEdit').selectpicker('val', '<?php echo $interviewEdit->active ?>');
                                            $('#edit').modal('show');
                                        });
                                    </script>

                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="nameClientEdit">Nome do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="nameClientEdit" id="nameClientEdit" placeholder="Nome">
                                            </div>
                                            <div class="form-group">
                                                <label for="cpfClientEdit">CPF do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="cpfClientEdit" id="cpfClientEdit" placeholder="CPF">
                                            </div>
                                            <div class="form-group">
                                                <label for="emailClientEdit">Email do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="emailClientEdit" id="emailClientEdit" placeholder="Email">
                                            </div>
                                            <div class="form-group">
                                                <label for="phoneClienteCreate">Telefone do Candidato:</label>
                                                <input disabled type="text" class="form-control" name="phoneClienteCreate" id="phoneClienteCreate" onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" placeholder="Telefone">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="descriptionEdit">Informações da entrevista:</label>
                                                <textarea disabled rows="4" cols="50" type="text" class="form-control" name="descriptionEdit" id="descriptionEdit" placeholder="Detalhes de como acontecerá a entrevista"></textarea>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col">
                                                    <label for="startDateEdit">Data e Hora Inicio:</label>
                                                    <input disabled require type="datetime-local" class="form-control" name="startDateEdit" id="startDateEdit">
                                                </div>
                                                <div class="form-group col">
                                                    <label for="endDateEdit">Data e Hora Fim:</label>
                                                    <input disabled require type="datetime-local" class="form-control" name="endDateEdit" id="endDateEdit">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col">
                                                    <label for="approvedEdit">Situação entrevista:</label>
                                                    <select disabled required name="approvedEdit" class="selectpicker approvedEdit" title="Selecione o status" data-width="100%">
                                                        <option value="-1">Agendada</option>
                                                        <option value="3">Adiada</option>
                                                        <option value="2">Finalizada</option>
                                                        <option value="1">Aprovado</option>
                                                        <option value="4">Contratado</option>
                                                        <option value="0">Reprovado</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col">
                                                    <label for="activeEdit">Status:</label>
                                                    <select disabled required name="activeEdit" class="selectpicker activeEdit" title="Selecione o status" data-width="100%">
                                                        <option value="1">Ativo</option>
                                                        <option value="0">Inativo</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div style="margin: 3%" class="text-div-center">
                                    Não possui entrevista agendada.
                                </div>
                                @endif
                            </div>

                            <!-- Historico candidato -->
                            <div class="tab-pane" id="historicCandidate" role="tabpanel">
                                <div style="margin: 3%" class="text-div-center">
                                    @if(!empty($candidateHistories[0]))
                                    <table class="table table-bordered table-hover display">
                                        <thead>
                                            <tr>
                                                <td>Ordem</td>
                                                <td>Nome</td>
                                                <td>Ação</td>
                                                <td>Realizada</td>
                                            </tr>
                                        </thead>

                                        <?php $count = 0; ?>
                                        <tbody>
                                            @foreach ($candidateHistories as $candidateHistory)
                                            <?php
                                            $candidate = Candidate::where('id', $candidateHistory->candidate_id)->first();
                                            $client = Client::where('id', $candidate->client_id)->first();
                                            $user = User::where('id', $client->user_id)->first();
                                            $permission = Permission::where('id', $user->permission_id)->first();
                                            $count++;
                                            ?>
                                            <tr>
                                                <td>{{$count}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$candidateHistory->description}}</td>
                                                <td>{{$candidateHistory->created_at->format('d/m/Y H:i')}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                    Você não possui histórico!
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- Candidato -->
                    <div class="tab-pane" id="personaInfo" role="tabpanel">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#resume" role="tab" aria-controls="resume">Currículo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#formations" role="tab" aria-controls="formations">Cursos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#clientAssignedSkills" role="tab" aria-controls="skills">Interesses</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <!-- CURRICULO -->
                            <div class="tab-pane active" id="resume" role="tabpanel">
                                <div style="margin: 3%" class="text-div-center">
                                    @if(!empty($userShow->resume))
                                    <embed src="/doc/resume/{{$user->resume}}" height="800" width="1000" type="application/pdf">
                                    @else
                                    Você não possui currículo na plataforma!
                                    @endif
                                </div>
                            </div>

                            <!-- CURSOS -->
                            <div class="tab-pane" id="formations" role="tabpanel">
                                <div style="margin: 3%" class="text-div-center">
                                    @if(!empty($clientFormations))
                                    <table class="table table-bordered table-hover display">
                                        <thead>
                                            <tr>
                                                <td>Name</td>
                                                <td>Data Inicio</td>
                                                <td>Data Fim</td>
                                                <td>Tipo</td>
                                                <td>Terminou</td>
                                                <td>Visualizar</td>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($clientFormations as $clientFormation)
                                            <?php
                                            $formation = Formation::where('id', $clientFormation->formation_id)->first();
                                            $formationType = FormationType::where('id', $formation->formation_types_id)->first();
                                            ?>
                                            @if($formation->active > -1)
                                            <tr>
                                                <td>{{$formation->name}}</td>
                                                <td>{{$formation->start_date->format('d/m/Y H:i')}}</td>
                                                <td>{{$formation->end_date->format('d/m/Y H:i')}}</td>
                                                <td>{{$formationType->name}}</td>
                                                <td>{{$clientFormation->finish == 1 ? "Finalizado" : "Cursando"}}</td>
                                                <td>
                                                    <div class="cellContainer">
                                                        <div style="margin: 5px">
                                                            <a href="/client/formation/show={{$formation->id}}" target="_blank" rel="noopener noreferrer">
                                                                <button class="btn btn-primary" type="submit"><i class="fa-solid fa-eye"></i></button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                    Você não possui cursos de especialização feitos na plataforma!
                                    @endif
                                </div>
                            </div>

                            <!-- INTERESSES (CLIENTE ADICIONOU MANUALMENTE A SKILL E SEU RATE) -->
                            <div class="tab-pane" id="clientAssignedSkills" role="tabpanel">
                                <div style="margin: 3%" class="text-div-center">
                                    @if(!empty($manualClientSkills))
                                    <?php
                                    $qtdSkills = count($manualClientSkills);
                                    $countRate = 0;
                                    foreach ($manualClientSkills as $manualClientSkill)
                                        $countRate = $countRate + $manualClientSkill->rate;
                                    $mean = ($countRate / $qtdSkills);
                                    ?>
                                    <div class="col">
                                        <div class="row">
                                            Média de interesse:
                                            <div class="col">
                                                @if($mean == 1)
                                                <i class="fa-solid fa-star" style="color: #f37101;"></i>
                                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                @elseif($mean == 2)
                                                <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                                <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                @elseif($mean == 3)
                                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                                <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                                @elseif($mean == 4)
                                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                <i class="fa-regular fa-star" style="color: #8fbc30;"></i>
                                                @elseif($mean == 5)
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                @else
                                                <i class="fa-regular fa-star"></i>
                                                <i class="fa-regular fa-star"></i>
                                                <i class="fa-regular fa-star"></i>
                                                <i class="fa-regular fa-star"></i>
                                                <i class="fa-regular fa-star"></i>
                                                @endif
                                            </div>
                                        </div>
                                        </br>
                                        <div class="row">
                                            Interesses do candidato:
                                            <table class="table table-bordered table-hover display">
                                                <thead>
                                                    <tr>
                                                        <td>Name</td>
                                                        <td>Descrição</td>
                                                        <td>Associado em</td>
                                                        <td>Associado a vaga?</td>
                                                        <td>Avaliação</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($manualClientSkills as $clientSkill)
                                                    <?php
                                                    $skill = Skill::where('id', $clientSkill->skill_id)->first();
                                                    $vacancySkill = VacancySkill::where('vacancy_id', $vacancy->id)->where('skill_id', $skill->id)->where('active', true)->first(); ?>
                                                    <tr>
                                                        <td>{{$skill->name}}</td>
                                                        <td>{{$skill->description}}</td>
                                                        <td>{{$clientSkill->created_at->format('d/m/Y H:i')}}</td>
                                                        @if(!empty($vacancySkill))
                                                        <td style="background-color: #308945; color: white;">Sim</td>
                                                        @else
                                                        <td style="background-color: #f37101; color: white;">Não</td>
                                                        @endif
                                                        <td>
                                                            @if($clientSkill->rate == 1)
                                                            <i class="fa-solid fa-star" style="color: #f37101;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                                                            @elseif($clientSkill->rate == 2)
                                                            <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                                            <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                                                            @elseif($clientSkill->rate == 3)
                                                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                                            <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                                                            @elseif($clientSkill->rate == 4)
                                                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                                                            <i class="fa-regular fa-star" style="color: #8fbc30;"></i>
                                                            @elseif($clientSkill->rate == 5)
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                                                            @else
                                                            <i class="fa-regular fa-star"></i>
                                                            <i class="fa-regular fa-star"></i>
                                                            <i class="fa-regular fa-star"></i>
                                                            <i class="fa-regular fa-star"></i>
                                                            <i class="fa-regular fa-star"></i>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    @else
                                    Você não possui interesses informados na plataforma!
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<!-- END MODAL SHOW DETALHES -->

<!-- MODAL EDITAR COMENTARIO -->
@if(Session::has('commentEdit'))

<?php
$commentEdit = Session::get('commentEdit');
?>

<script>
    $(function() {
        $('#descriptionEdit').val('<?php echo $commentEdit->description ?>');
        $('.activeEdit').selectpicker('val', '<?php echo $commentEdit->active ?>');
        $('#edit').modal('show');
    });
</script>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
                <form action="/client/vacancy/candidate/show={{$commentEdit->candidate_id}}">
                    <button class="close" type="submit" aria-hidden="true">&times;</button>
                </form>
            </div>
            <div class="modal-body">
                <div style="margin: 1%">
                    <form id="updateCandidateComment" action="/client/vacancy/comment/update={{$commentEdit->id}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="description">Descrição:</label>
                            <textarea rows="10" cols="50" type="text" class="form-control" name="descriptionEdit" id="descriptionEdit" placeholder="Descrição da vaga"></textarea>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" form="updateCandidateComment" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
            </div>
        </div>
    </div>
</div>
@endif

<!-- MODAL VISUALIZAR DOCUMENTO -->
@if(Session::has('showDocument'))

<?php
$showDocument = Session::get('showDocument');
?>

<script>
    $(function() {
        $('#edit').modal('show');
    });
</script>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Documento</h5>
                <form action="/client/vacancy/candidate/show={{$showDocument->candidate_id}}">
                    <button class="close" type="submit" aria-hidden="true">&times;</button>
                </form>
            </div>
            <div class="modal-body">
                <div style="margin: 1%">
                    <div class="row">
                        <embed src="/doc/candidate/{{$showDocument->document}}" height="800" type="application/pdf">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@endsection
