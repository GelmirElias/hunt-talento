<?php

use App\Models\Company;
use App\Models\Formation;
use App\Models\StatusVacancy;
use App\Models\User;
use App\Models\Vacancy;
use App\Models\VacancyFormation;
use App\Models\VacancySkill;
use Illuminate\Support\Facades\Session;

?>

<!DOCTYPE html>
@extends('layout.app')

@section('title', "Gerenciar Vagas")

@section('body')
<br><br>
<!-- Lists container -->
<section class="lists-container">
    @foreach ($statusVacancies as $statusVacancy)
    <div class="list">
        <h4 data-toggle="tooltip" title="{{$statusVacancy->description}}" class="list-title">{{$statusVacancy->name}}</h4>
        <?php
        $vacanciesId = [];
        foreach ($candidates as $candidate)
            $vacanciesId[] = $candidate->vacancy_id;
        $vacancies = Vacancy::where('status_vacancy_id', $statusVacancy->id)->whereIn('id', $vacanciesId)->get();
        ?>
        <ul class="list-items">
            @foreach ($vacancies as $vacancy)
            <?php
            $company = Company::where('id', $vacancy->company_id)->first();
            $user = User::where('id', $company->user_id)->first();
            ?>
            <li>
                <a href="/client/vacancy/candidates/show={{$vacancy->id}}">
                    <div class="row" type="submit">
                        <div class="col">
                            <div data-toggle="tooltip" title="{{$vacancy->description}}" class="text-div-center">{{$vacancy->name}}</div>
                        </div>
                    </div>
                    </br>
                </a>
                <div data-toggle="tooltip" title="{{$user->description}}" class="text-div-center">{{strtok($user->name, " ")}}</div>
            </li>
            @endforeach
        </ul>
    </div>
    @endforeach
</section>

<!-- End of lists container -->
@endsection
