<?php

use App\Models\ClientFormation;
use App\Models\Company;
use App\Models\CompanyTutor;
use App\Models\FormationTutor;
use App\Models\FormationType;
use App\Models\User;

use function PHPUnit\Framework\isNull;

?>

<!DOCTYPE html>
@extends('layout.app')

@section('title', "Gerenciar Minhas Formações")

@section('body')
<table id="datatable" class="table table-bordered table-hover display" border="1px">
    <thead>
        <tr>
            <td>Name</td>
            <td>Data Inicio</td>
            <td>Data Fim</td>
            <td>Tipo</td>
            <td>Status</td>
            <td>Visualizar</td>
        </tr>
    </thead>

    <tbody>
        @foreach ($formations as $formation)
        <?php
        $formationType = FormationType::where('id', $formation->formation_types_id)->first();
        $clientFormartion = ClientFormation::where('client_id', session('client')->id)->where('formation_id', $formation->id)->first();
        ?>
        @if($formation->active > -1)
        <tr>
            <td>{{$formation->name}}</td>
            <td>{{$formation->start_date->format('d/m/Y H:i')}}</td>
            <td>{{$formation->end_date->format('d/m/Y H:i')}}</td>
            <td>{{$formationType->name}}</td>
            <td>{{$clientFormartion->finish == 1 ? "Finalizada" : "Em aberto"}}</td>
            <td>
                <form action="/client/formation/show={{$formation->id}}">
                    <button class="btn btn-primary" type="submit"><i class="fa-solid fa-eye"></i></button>
                </form>
            </td>
        </tr>
        @endif
        @endforeach
    </tbody>
</table>
@endsection
