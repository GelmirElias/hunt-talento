<?php

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\CompanyTutor;
use App\Models\Formation;
use App\Models\FormationType;
use App\Models\Plan;
use App\Models\Tutor;
use App\Models\User;

$controller = new Controller;

?>

<!DOCTYPE html>
@extends('layout.app')

@section('title', "Meu Histórico De Formação")

@section('body')
<table id="datatable" class="table table-bordered table-hover display" border="1px">
    <thead>
        <tr>
            <td>Formação</td>
            <td>Tipo</td>
            <td>Visualizar</td>
        </tr>
    </thead>

    <tbody>
        @foreach ($clientFormations as $clientFormation)
        <?php
        $formation = Formation::where('id', $clientFormation->formation_id)->first();
        $formationType = FormationType::where('id', $formation->formation_types_id)->first();
        ?>
        <tr>
            <td>{{$formation->name}}</td>
            <td>{{$formationType->name}}</td>
            <td>
                <form action="/client/formation/history/show={{$clientFormation->id}}" method="POST">
                    @csrf
                    <button class="btn btn-primary" type="submit"><i class="fa-solid fa-eye"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@if(!empty($clientFormationDetail))
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{$formationDetail->name}}</h5>
                <form action="/client/formation/history">
                    <button class="close" type="submit" aria-hidden="true">&times;</button>
                </form>
            </div>
            <div class="modal-body">
                @if(!empty($formationDetail->photo))
                <div class="form-group">
                    <img id="photoShow" src="/img/formations/{{$formationDetail->photo}}" alt="photo formation" width=240 height=240>
                </div>
                @endif
                <hr>
                <div class="form-group">
                    <label for="description">Descrição - <i class="fa-solid fa-message"></i></label>
                    <!-- Markdown container -->
                    <div id="editor_container" style="display: none;">
                        <textarea id="editable"></textarea>
                    </div>
                    <div id="html_container"></div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="dates">Periodo - <i class="fa-solid fa-calendar"></i></label>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="startDateEdit">Inicio: <i class="fa-sharp fa-solid fa-hourglass-start"></i></label>
                            <label readonly type="datetime-local">{{$clientFormationDetail->created_at->format('d/m/Y H:i')}}</label>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="endDateEdit">Conclusão: <i class="fa-solid fa-hourglass-end"></i></label>
                            <label readonly type="datetime-local">{{$clientFormationDetail->finish_at->format('d/m/Y H:i')}}</label>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="types">Informações - <i class="fa-solid fa-circle-info"></i></label>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="formationTypeIdEdit">Tipo:</label>
                            <?php $formationType = FormationType::where('id', $formationDetail->formation_types_id)->first() ?>
                            <label readonly type="text">{{$formationType->name}}</label>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="planIdEdit">Plano:</label>
                            <?php $plan = Plan::where('id', $formationDetail->plan_id)->first() ?>
                            <label readonly type="text">{{$plan->name}}</label>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="tutorsIdDetail[]">Autor(es) - <i class="fa-solid fa-user"></i></label>
                        @foreach($tutorsIdDetail as $tutorIdDetail)
                        <?php $tutor = Tutor::where('id', $tutorIdDetail)->first(); ?>
                        <?php $userTutor = User::where('id', $tutor->user_id)->first(); ?>
                        <label readonly type="text"><i class="fa-solid fa-user"></i> - {{$userTutor->name}}</label>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        state = false;
        id = "editable";
        md = '<?php echo $formationDetail->description; ?>';
        var simplemde = new SimpleMDE({
            element: $("#editable" + id)[0],
            initialValue: md,
        });
        html = simplemde.options.previewRender(md);
        $('#html_container').wrapInner(html);

        $('#edit').modal('show');
    });
</script>
@endif

@endsection
