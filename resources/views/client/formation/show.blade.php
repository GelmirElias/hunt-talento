<?php

use App\Models\ClientFormation;
use App\Models\Company;
use App\Models\CompanyTutor;
use App\Models\Skill;
use App\Models\Tutor;
use App\Models\User;

?>


@extends('layout.app')

@section('title', "$formationDetail->name")

@section('body')

<div class="conteudo-centralizado">
    <br><a type="submit" href="/client/formation"><button class="btn btn-primary"><i class="fa-solid fa-backward"></i></button></a><br>

        <br>
        <h4>Descrição:</h4>
        <hr>
        <div id="editor_container_description" style="display: none;">
            <textarea id="descriptionEdit" name="descriptionEdit"></textarea>
        </div>
        <div id="html_container_description"></div>
        <hr>

        <br>
        <h4>Conteúdo:</h4>
        <hr>
        <div id="editor_container" style="display: none;">
            <textarea id="contentEdit" name="contentEdit"></textarea>
        </div>
        <div id="html_container"></div>
        <hr>

    </form>

    @if(!empty($tutorsIdDetail[0]))
    <br>
    <h4>Autor(es):</h4>
    <hr>
    @foreach($tutorsIdDetail as $tutorIdDetail)
    <?php
    $tutor = Tutor::where('id', $tutorIdDetail)->first();
    $userTutor = User::where('id', $tutor->user_id)->first();
    $companyTutor = CompanyTutor::where('tutor_id', $tutor->id)->first();
    $company = Company::where('id', $companyTutor->company_id)->first();
    $userCompany = User::where('id', $company->user_id)->first();
    ?>
    - {{$userTutor->name}} ({{$userCompany->name}})</option>
    @endforeach
    <hr>
    @endif

    @if(!empty($formationSkillsDetail[0]))
    <br>
    <h4>Habilidade(s):</h4>
    <hr>
    @foreach($formationSkillsDetail as $formationSkillDetail)
    <?php
    $skill = Skill::where('id', $formationSkillDetail->skill_id)->first();
    ?>
    - {{$skill->name}} ({{$skill->description}})</option>
    @endforeach
    <hr>
    @endif

    <br>
    <a type="submit" href="/client/formation"><button class="btn btn-primary"><i class="fa-solid fa-backward"></i></button></a>
    <?php $clientFormartion = ClientFormation::where('client_id', session('client')->id)->where('formation_id', $formationDetail->id)->first(); ?>
    @if($clientFormartion->finish == false)
    <a type="submit" href="/client/formation/finish={{$formationDetail->id}}"><button class="btn btn-success"><i class="fa-solid fa-check"></i> Concluir formação</button></a>
    @endif

    <br><br><br>
</div>

<script>
    $(document).ready(function() {

        state = false;

        //markdown descriptionEdit
        var mdDescription = '<?php echo $formationDetail->description; ?>';
        var simplemdeDescription = new SimpleMDE({
            spellChecker: false,
            element: $("#descriptionEdit")[0],
            initialValue: mdDescription,
        });
        htmlDescription = simplemdeDescription.options.previewRender(mdDescription);
        $('#html_container_description').wrapInner(htmlDescription);

        //markdown contentEdit
        var mdContent = '<?php echo $formationDetail->content; ?>';
        var simplemdeContent = new SimpleMDE({
            spellChecker: false,
            element: $("#contentEdit")[0],
            initialValue: mdContent,
        });
        htmlContent = simplemdeContent.options.previewRender(mdContent);
        $('#html_container').wrapInner(htmlContent);

        //markdown edit
        $("#edit").click(function() {
            if (state) {
                $("div#editor_container_description").css('display', 'none');
                $("div#editor_container").css('display', 'none');
                // Show markdown rendered by CodeMirror
                $('#html_container_description').wrapInner(simplemdeDescription.options.previewRender(simplemdeDescription.value()));
                $('#html_container').wrapInner(simplemdeContent.options.previewRender(simplemdeContent.value()));
            } else {
                // Show editor
                $("div#editor_container_description").css('display', 'inline');
                $("div#editor_container").css('display', 'inline');
                // Do a refresh to show the editor value
                simplemdeDescription.codemirror.refresh();
                simplemdeContent.codemirror.refresh();
                $('#html_container_description').empty();
                $('#html_container').empty();
            };
            state = !state;
        });
    });
</script>
@endsection
