<!DOCTYPE html>

<?php

use App\Models\ClientSkill;
use App\Models\Skill;

?>

@extends('layout.app')

@section('title', "Meu perfil")

@section('body')
</br>

<div class="col">
    <div class="divider"></div>
    <div class="row">
        <div class="col-4">
            @if(!empty($user->photo))
            <div class="form-group">
                <img id="photoShow" src="/img/clients/{{$user->photo}}" alt="photo user" class="img-thumbnail">
            </div>
            <button type="submit" class="btn btn-warning" data-toggle="modal" data-target="#editPhoto">Alterar foto</button>
            @else
            <div class="form-group">
                <img id="photoShow" src="/img/semimagem.png" alt="photo user" class="img-thumbnail">
            </div>
            <button type="submit" class="btn btn-success" data-toggle="modal" data-target="#editPhoto">Carregar foto</button>
            @endif
        </div>
        <div class="col-1">
        </div>
        <div class="col-7">
            <dl class="col">
                <div class="row">
                    <dt class="col-sm-3">Nome</dt>
                    <dd class="col-sm-9">{{$user->name}}</dd>
                    <dt class="col-sm-3">Descrição</dt>
                    <dd class="col-sm-9">{{$user->description}}</dd>
                    <hr>
                    <dt class="col-sm-3">CPF</dt>
                    <dd class="col-sm-9">{{$client->cpf}}</dd>
                    <dt class="col-sm-3">Telefone</dt>
                    <dd class="col-sm-9">{{$user->phone}}</dd>
                    <hr>
                    <dt class="col-sm-3">Email</dt>
                    <dd class="col-sm-9">{{$user->email}}</dd>
                </div>
                <div class="divider"></div>
                <div class="row">
                    <dd class="col">
                        <button type="submit" class="btn btn-warning" data-toggle="modal" data-target="#editPassword">Alterar senha</button>
                        <div class="divider"></div>
                        @if(!empty($user->resume))
                        <button type="submit" class="btn btn-warning" data-toggle="modal" data-target="#editResume">Alterar currículo</button>
                        @else
                        <button type="submit" class="btn btn-success" data-toggle="modal" data-target="#editResume">Carregar currículo</button>
                        @endif
                        <div class="divider"></div>
                        <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#editStatus">Desativar conta</button>
                    </dd>
                </div>
            </dl>
        </div>
    </div>
</div>

<!-- MODAL ALTERAR FOTO -->
<div class="modal fade" id="editPhoto" tabindex="-1" role="dialog" aria-labelledby="editPhoto" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Altere sua foto de perfil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formEditPhoto" action="/client/profile/update/photo" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="file" accept="image/*" class="form-control-file" name="photoEdit" id="photoEdit">
                </form>
            </div>
            <div class="modal-footer">
                <button form="formEditPhoto" type="submit" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL ALTERAR SENHA -->
<div class="modal fade" id="editPassword" tabindex="-1" role="dialog" aria-labelledby="editPassword" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Altere sua senha</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formEditPassword" action="/client/profile/update/password" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="oldPassword">Senha antiga:</label>
                        <input required type="password" class="form-control" name="oldPassword" id="oldPassword" placeholder="Digite sua senha antiga">
                    </div>
                    <div class="form-group">
                        <label for="newPassword">Nova senha:</label>
                        <input required type="password" class="form-control" name="newPassword" id="newPassword" placeholder="Digite a nova senha">
                    </div>
                    <div class="form-group">
                        <label for="newPasswordConfirm">Confirme nova senha:</label>
                        <input required type="password" class="form-control" name="newPasswordConfirm" id="newPasswordConfirm" placeholder="Digite a confirmação da senha">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button form="formEditPassword" type="submit" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL ALTERAR STATUS -->
<div class="modal fade" id="editStatus" tabindex="-1" role="dialog" aria-labelledby="editStatus" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Desativar sua conta?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formEditStatus" action="/client/profile/update/active" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="password">Tem certeza que deseja desativar sua conta?</label>
                        <input required type="password" class="form-control" name="password" id="password" placeholder="Confirme sua senha atual">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button form="formEditStatus" type="submit" class="btn btn-danger"><i class="fa-solid fa-floppy-disk"></i></button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL ALTERAR CURRICULO -->
<div class="modal fade" id="editResume" tabindex="-1" role="dialog" aria-labelledby="editResume" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Meu currículo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if(!empty($user->resume))
                <div class="row">
                    <embed src="/doc/resume/{{$user->resume}}" height="800" type="application/pdf">
                </div>
                @endif
                <div class="divider"></div>
                <div class="row">
                    <form id="formEditResume" action="/client/profile/update/resume" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="resumeEdit">Carregar novo currículo:</label>
                            <input type="file" accept="application/pdf" class="form-control-file" name="resumeEdit" id="resumeEdit">
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button form="formEditResume" type="submit" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
            </div>
        </div>
    </div>
</div>
@endsection
