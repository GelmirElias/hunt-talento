<!DOCTYPE html>

<?php

use App\Models\ClientSkill;
use App\Models\Skill;
use Illuminate\Support\Facades\Session;

?>

@extends('layout.app')

@section('title', 'Gerenciar Interesses')

@section('body')
    <a data-toggle="modal" data-target="#information">
        <button type="button" class="btn btn-warning float-right" data-toggle="tooltip" data-placement="top"
            title="O que são interesses?">
            <span type="button" class="fa-solid fa-info fa-xl">
        </button>
    </a>
    </br></br>
    <table id="datatable" class="table table-bordered table-hover display" border="1px">
        <thead>
            <tr>
                <td>Nome</td>
                <td>Descrição</td>
                <td>Domínio</td>
                <td>Remover</td>
            </tr>
        </thead>

        <tbody>
            @foreach ($clientSkills as $clientSkill)
                <?php
                $skill = Skill::where('id', $clientSkill->skill_id)->first(); ?>
                <tr>
                    <td>{{ $skill->name }}</td>
                    <td>{{ $skill->description }}</td>
                    <td>
                        @if ($clientSkill->rate == 1)
                            <i class="fa-solid fa-star" style="color: #f37101;"></i>
                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                            <i class="fa-regular fa-star" style="color: #f37101;"></i>
                        @elseif($clientSkill->rate == 2)
                            <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                            <i class="fa-solid fa-star" style="color: #f2b10f;"></i>
                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                            <i class="fa-regular fa-star" style="color: #f2b10f;"></i>
                        @elseif($clientSkill->rate == 3)
                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                            <i class="fa-solid fa-star" style="color: #f0f000;"></i>
                            <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                            <i class="fa-regular fa-star" style="color: #f0f000;"></i>
                        @elseif($clientSkill->rate == 4)
                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                            <i class="fa-solid fa-star" style="color: #8fbc30;"></i>
                            <i class="fa-regular fa-star" style="color: #8fbc30;"></i>
                        @elseif($clientSkill->rate == 5)
                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                            <i class="fa-solid fa-star" style="color: #308945;"></i>
                        @else
                            <i class="fa-regular fa-star"></i>
                            <i class="fa-regular fa-star"></i>
                            <i class="fa-regular fa-star"></i>
                            <i class="fa-regular fa-star"></i>
                            <i class="fa-regular fa-star"></i>
                        @endif
                    </td>
                    <td>
                        <form action="/client/skill/interest/destroy={{ $clientSkill->id }}">
                            <button type="submit" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <!-- MODAL INFORMATIVO -->
    <div class="modal fade" id="information" tabindex="-1" role="dialog" aria-labelledby="information" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">O que são interesses?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Interesses são linguagens de programação vinculadas a você de forma automática pelo sistema. <br><br>
                        Elas podem ser obtidas de diferentes formas, como:
                    <ul>
                        <li>Finalizando um curso de especialização.</li>
                        <li>Sendo aprovado em um processo de seleção.</li>
                        <li>Carregando seu currículo na plataforma.</li>
                    </ul>
                    São úteis para sugerir vagas e formações que você possui interesse.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" aria-label="Close">
                        Compreendi
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
