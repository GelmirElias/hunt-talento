<!DOCTYPE html>
@extends('layout.app')

@section('title', "Trocar de senha")

@section('body')
<form action="{{'/changePassword'}}" method="POST">
    @csrf
    <div class="card">
        <article class="card-body">
            <form>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email" value="{{$userEdit->email}}" readonly>
                </div>
                <div class="form-group">
                    <label for="password">Digite sua nova senha:</label>
                    <input required type="password" class="form-control" name="password" id="password" placeholder="Informe sua nova senha">
                </div>
                <div class="form-group">
                    <label for="passwordConfirm">Confirme sua nova senha:</label>
                    <input required type="password" class="form-control" name="passwordConfirm" id="passwordConfirm" placeholder="Confirme sua nova senha">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Confirmar</button>
                </div>
            </form>
        </article>
    </div>
</form>
@endsection
