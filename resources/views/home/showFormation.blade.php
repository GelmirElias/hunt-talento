<?php

use App\Models\ClientFormation;
use App\Models\CompanyFormation;
use App\Models\FormationTutor;
use App\Models\User; ?>

@extends('layout.app')

@section('title', $formation->name)

@section('body')
<div class="col">
    <a type="submit" class="float-left btn btn-primary" href="/"><i class="fa-solid fa-backward"></i></a>

    <!-- botão admin -->
    @if(session('admin'))
    <form action="/formation/edit={{$formation->id}}">
        <button class="float-left btn btn-warning edit" type="submit">Editar</button>
    </form>
    @endif

    <!-- botão client -->
    @if(session('client'))
    <?php
    $clientFormation = ClientFormation::where('client_id', session('client')->id)->where('formation_id', $formation->id)->first();
    ?>
    @if(empty($clientFormation))
    <form action="/formation/store/formation={{$formation->id}}">
        <button class="float-left btn btn-success edit" type="submit">Inscrever-se</button>
    </form>
    @elseif(!empty($clientFormation))
    <a type="submit" href="/client/formation/show={{$formation->id}}"><button class="float-left btn btn-success edit">Você está inscrito</button></a>
    @endif
    @endif

    <!-- botão company -->
    @if(session('company'))
    <?php
    $company = CompanyFormation::where('company_id', session('company')->id)->where('formation_id', $formation->id)->first();
    ?>
    @if(!empty($company))
    <form action="{{'/company/formation/edit='. $formation->id}}">
        <button class="float-left btn btn-warning edit" type="submit">Editar</button>
    </form>
    @endif
    @endif

    <!-- botão tutor -->
    @if(session('tutor'))
    <?php
    $tutor = FormationTutor::where('tutor_id', session('tutor')->id)->where('formation_id', $formation->id)->first();
    ?>
    @if(!empty($tutor))
    <form action="/tutor/formation/edit={{$formation->id}}">
        <button class="float-left btn btn-warning edit" type="submit">Editar</button>
    </form>
    @endif
    @endif
</div>

</br></br>

<script></script>
<div class="container">
    <div class="row">

        <div class="col-md-4">
            <div class="project-info-box">
                <p><b>Incluso no plano:</b> {{$plan->name}}</p>
                <p><b>Começa em:</b> {{$formation->start_date->format('d/m/Y H:i')}}</p>
                <p><b>Termina em:</b> {{$formation->end_date->format('d/m/Y H:i')}}</p>

                <!-- companies -->
                @if(!empty($companies))
                <p><b>Oferecida por:</b> <?php echo $companies ?></p>
                @else
                <p><b>Oferecida por:</b> </br>EM BREVE MAIS INFORMAÇÃO</p>
                @endif

                <!-- tutors -->
                @if(!empty($tutors))
                <p><b>Dirigida por:</b> <?php echo $tutors ?></p>
                @else
                <p><b>Dirigida por:</b> </br>EM BREVE MAIS INFORMAÇÃO</p>
                @endif

                <!-- skills -->
                @if(!empty($skills))
                <p><b>Habilidades:</b> <?php echo $skills ?></p>
                @else
                <p><b>Habilidades:</b> </br>EM BREVE MAIS INFORMAÇÃO</p>
                @endif
            </div>
        </div>

        <div class="col-md-4">
            <div class="project-info-box mt-0">

                <!-- Markdown container -->
                <div id="editor_container" style="display: none;">
                    <textarea id="editable"></textarea>
                </div>
                <div id="html_container"></div>

            </div>
        </div><!-- / column -->

        <div class="col-md-4">
            @if(!empty($formation->photo))
            <img src="/img/formations/{{$formation->photo}}" alt="project-image" class="rounded" height=300>
            @endif
        </div>

    </div>
</div>

<script>
    $(document).ready(function() {
        state = false;
        id = "editable";
        md = '<?php echo $formation->description; ?>';
        var simplemde = new SimpleMDE({
            element: $("#editable" + id)[0],
            initialValue: md,
        });
        html = simplemde.options.previewRender(md);
        $('#html_container').wrapInner(html);
    });
</script>
@endsection
