@extends('layout.app')

@section('title', 'Página Principal')

@section('body')

    <?php

    use App\Http\Controllers\CompanyController;
    use App\Models\Candidate;
    use App\Models\ClientFormation;
    use App\Models\CompanyFormation;
    use App\Models\FormationTutor;
    use App\Models\Vacancy;

    $count = 0;
    $formation = 0;
    $vacancy = 0;
    ?>

    <!-- Pills navs -->
    <ul class='nav nav-pills nav-justified mb-3' id='ex1' role='tablist'>
        <li class='nav-item' role='presentation'>
            <a class='nav-link active' id='ex3-tab-1' data-mdb-toggle='pill' href='#ex3-pills-1' role='tab'
                aria-controls='ex3-pills-1' aria-selected='true'>Vagas</a>
        </li>
        <li class='nav-item' role='presentation'>
            <a class='nav-link' id='ex3-tab-2' data-mdb-toggle='pill' href='#ex3-pills-2' role='tab'
                aria-controls='ex3-pills-2' aria-selected='false'>Formações</a>
        </li>
        <li class='nav-item' role='presentation'>
            <!-- Barra de Pesquisa Formação -->
            <form action='/search' method='POST'>
                @csrf
                <div class='input-group'>
                    <div class='form-outline border-top-0 border-start-0 border-end-0'>
                        <input type='text' name='search' id='search' class='form-control' />
                        <label class='form-label' for='form1'>Buscar</label>
                    </div>
                    <button type='submit' class='btn btn-primary'>
                        <i class='fas fa-search'></i>
                    </button>
                </div>
            </form>
        </li>
    </ul>
    <!-- Pills navs -->


    <!-- Pills content -->
    <div class='tab-content' id='ex2-content'>

        <!-- Vacancy -->
        <div class='tab-pane fade show active' id='ex3-pills-1' role='tabpanel' aria-labelledby='ex3-tab-1'>


            <!-- Vagas recomendadas com base nos seus interesses. -->
            @if ($interestClientVacancies)
                <div>
                    <div style='margin: 2%' class='text-div-center'>
                        <h4>Vagas de seu interesse</h4>
                    </div>
                    @if ($qtdinterestClientVacancies == 0)
                        <div class='col'>
                            <div class='card h-100'>
                                <div class='card-body'>
                                    <h5 class='card-title'>Nenhuma recomendação nova encontrada</h5>
                                </div>
                            </div>
                        </div>
                    @else
                        @while ($qtdinterestClientVacancies != 0)
                            <div class='row row-cols-1 row-cols-md-3 g-4'>
                                @while ($count != 3)
                                    @if ($qtdinterestClientVacancies != 0)
                                        <div class='col'>
                                            <div class='card h-100'>
                                                @if (!empty($interestClientVacancies[$vacancy]->photo))
                                                    <img src='/img/vacancies/{{ $interestClientVacancies[$vacancy]->photo }}'
                                                        class='card-img-top' alt='' width=300 height=300>
                                                @else
                                                    <img src='/img/semimagem.png' class='card-img-top' alt=''
                                                        width=300 height=300>
                                                @endif
                                                <div class='card-body'>
                                                    <h5 class='card-title'>{{ $interestClientVacancies[$vacancy]->name }}
                                                    </h5>
                                                    <p class='card-text'>Faixa salarial:
                                                        R${{ $interestClientVacancies[$vacancy]->start_salary }}
                                                        até R${{ $interestClientVacancies[$vacancy]->end_salary }}</p>
                                                </div>
                                                <div class='card-footer'>
                                                    <form
                                                        action='{{ '/show/vacancy=' . $interestClientVacancies[$vacancy]->id }}'>
                                                        @csrf
                                                        <button type='submit' href='#'
                                                            class='float-left btn btn-primary'><i class="fa-solid fa-eye"></i></button>
                                                    </form>

                                                    <!-- botão admin -->
                                                    @if (session('admin'))
                                                        <a href='/vacancy/candidates/show={{ $interestClientVacancies[$vacancy]->id }}'
                                                            class='float-right btn btn-warning'><i
                                                                class='fa-solid fa-gear'></i></a>
                                                    @endif

                                                    <!-- botão company -->
                                                    <?php
                                                    try {
                                                        $company = Vacancy::where('company_id', session('company')->id)
                                                            ->where('id', $interestClientVacancies[$vacancy]->id)
                                                            ->first();
                                                    } catch (Exception $e) {
                                                    }
                                                    ?>
                                                    @if (session('company') && !empty($company))
                                                        <a href='/company/vacancy/candidates/show={{ $interestClientVacancies[$vacancy]->id }}'
                                                            class='float-right btn btn-warning'><i
                                                                class='fa-solid fa-gear'></i></a>
                                                    @endif

                                                    <!-- botão tutor -->
                                                    <?php
                                                    try {
                                                        $companiesTutor = (new CompanyController())->getAllCompaniesOfTutorAuth();
                                                        $companyId = [];
                                                        foreach ($companiesTutor as $companyTutor) {
                                                            $companyId[] = $companyTutor->id;
                                                        }
                                                        $tutor = Vacancy::whereIn('company_id', $companyId)
                                                            ->where('id', $interestClientVacancies[$vacancy]->id)
                                                            ->first();
                                                    } catch (Exception $e) {
                                                    }
                                                    ?>
                                                    @if (session('tutor') && !empty($tutor))
                                                        <a href='/tutor/vacancy/candidates/show={{ $interestClientVacancies[$vacancy]->id }}'
                                                            class='float-right btn btn-warning'><i
                                                                class='fa-solid fa-gear'></i></a>
                                                    @endif

                                                    <!-- botão client -->
                                                    @if (session('client'))
                                                        <?php $candidate = Candidate::where('client_id', session('client')->id)
                                                            ->where('vacancy_id', $interestClientVacancies[$vacancy]->id)
                                                            ->first(); ?>
                                                        @if (!empty($candidate))
                                                            <a href='/client/vacancy/candidates/show={{ $interestClientVacancies[$vacancy]->id }}'
                                                                class='float-right btn btn-warning' type='submit'><i
                                                                    class='fa-solid fa-gear'></i></a>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $count++;
                                        $vacancy++;
                                        $qtdinterestClientVacancies--; ?>
                                    @else
                                        <?php $count++; ?>
                                        <div class='col'>
                                        </div>
                                    @endif
                                @endwhile
                                <?php $count = 0;
                                $vacancy = 0; ?>
                            </div>
                            </br>
                        @endwhile
                        <?php $vacancy = 0; ?>
                    @endif
                </div>
                <hr>
            @endif
            <!-- Vagas recomendadas com base nos seus interesses. -->

            <!-- Vagas recomendadas com base nas suas habilidades. -->
            @if ($skillClientVacancies)
                <div>
                    <div style='margin: 2%' class='text-div-center'>
                        <h4>Vagas para sua habilidade</h4>
                    </div>
                    @if ($qtdskillClientVacancies == 0)
                        <div class='col'>
                            <div class='card h-100'>
                                <div class='card-body'>
                                    <h5 class='card-title'>Nenhuma recomendação nova encontrada</h5>
                                </div>
                            </div>
                        </div>
                    @else
                        @while ($qtdskillClientVacancies != 0)
                            <div class='row row-cols-1 row-cols-md-3 g-4'>
                                @while ($count != 3)
                                    @if ($qtdskillClientVacancies != 0)
                                        <div class='col'>
                                            <div class='card h-100'>
                                                @if (!empty($skillClientVacancies[$vacancy]->photo))
                                                    <img src='/img/vacancies/{{ $skillClientVacancies[$vacancy]->photo }}'
                                                        class='card-img-top' alt='' width=300 height=300>
                                                @else
                                                    <img src='/img/semimagem.png' class='card-img-top' alt=''
                                                        width=300 height=300>
                                                @endif
                                                <div class='card-body'>
                                                    <h5 class='card-title'>{{ $skillClientVacancies[$vacancy]->name }}
                                                    </h5>
                                                    <p class='card-text'>Faixa salarial:
                                                        R${{ $skillClientVacancies[$vacancy]->start_salary }}
                                                        até R${{ $skillClientVacancies[$vacancy]->end_salary }}</p>
                                                </div>
                                                <div class='card-footer'>
                                                    <form
                                                        action='{{ '/show/vacancy=' . $skillClientVacancies[$vacancy]->id }}'>
                                                        @csrf
                                                        <button type='submit' href='#'
                                                            class='float-left btn btn-primary'><i class="fa-solid fa-eye"></i></button>
                                                    </form>

                                                    <!-- botão admin -->
                                                    @if (session('admin'))
                                                        <a href='/vacancy/candidates/show={{ $skillClientVacancies[$vacancy]->id }}'
                                                            class='float-right btn btn-warning'><i
                                                                class='fa-solid fa-gear'></i></a>
                                                    @endif

                                                    <!-- botão company -->
                                                    <?php
                                                    try {
                                                        $company = Vacancy::where('company_id', session('company')->id)
                                                            ->where('id', $skillClientVacancies[$vacancy]->id)
                                                            ->first();
                                                    } catch (Exception $e) {
                                                    }
                                                    ?>
                                                    @if (session('company') && !empty($company))
                                                        <a href='/company/vacancy/candidates/show={{ $skillClientVacancies[$vacancy]->id }}'
                                                            class='float-right btn btn-warning'><i
                                                                class='fa-solid fa-gear'></i></a>
                                                    @endif

                                                    <!-- botão tutor -->
                                                    <?php
                                                    try {
                                                        $companiesTutor = (new CompanyController())->getAllCompaniesOfTutorAuth();
                                                        $companyId = [];
                                                        foreach ($companiesTutor as $companyTutor) {
                                                            $companyId[] = $companyTutor->id;
                                                        }
                                                        $tutor = Vacancy::whereIn('company_id', $companyId)
                                                            ->where('id', $skillClientVacancies[$vacancy]->id)
                                                            ->first();
                                                    } catch (Exception $e) {
                                                    }
                                                    ?>
                                                    @if (session('tutor') && !empty($tutor))
                                                        <a href='/tutor/vacancy/candidates/show={{ $skillClientVacancies[$vacancy]->id }}'
                                                            class='float-right btn btn-warning'><i
                                                                class='fa-solid fa-gear'></i></a>
                                                    @endif

                                                    <!-- botão client -->
                                                    @if (session('client'))
                                                        <?php $candidate = Candidate::where('client_id', session('client')->id)
                                                            ->where('vacancy_id', $skillClientVacancies[$vacancy]->id)
                                                            ->first(); ?>
                                                        @if (!empty($candidate))
                                                            <a href='/client/vacancy/candidates/show={{ $skillClientVacancies[$vacancy]->id }}'
                                                                class='float-right btn btn-warning' type='submit'><i
                                                                    class='fa-solid fa-gear'></i></a>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $count++;
                                        $vacancy++;
                                        $qtdskillClientVacancies--; ?>
                                    @else
                                        <?php $count++; ?>
                                        <div class='col'>
                                        </div>
                                    @endif
                                @endwhile
                                <?php $count = 0; ?>
                            </div>
                            </br>
                        @endwhile
                        <?php $vacancy = 0; ?>
                    @endif
                </div>
                <hr>
            @endif
            <!-- Vagas recomendadas com base nas suas habilidades. -->

            <!-- Todas as vagas -->
            <div>
                <div style='margin: 2%' class='text-div-center'>
                    <h4>Todas as vagas</h4>
                </div>
                @if ($qtdVacancies == 0)
                    <div class='col'>
                        <div class='card h-100'>
                            <div class='card-body'>
                                <h5 class='card-title'>Nenhuma vaga encontrada</h5>
                            </div>
                        </div>
                    </div>
                @else
                    @while ($qtdVacancies != 0)
                        <div class='row row-cols-1 row-cols-md-3 g-4'>
                            @while ($count != 3)
                                @if ($qtdVacancies != 0)
                                    <div class='col'>
                                        <div class='card h-100'>
                                            @if (!empty($vacancies[$vacancy]->photo))
                                                <img src='/img/vacancies/{{ $vacancies[$vacancy]->photo }}'
                                                    class='card-img-top' alt='' width=300 height=300>
                                            @else
                                                <img src='/img/semimagem.png' class='card-img-top' alt=''
                                                    width=300 height=300>
                                            @endif
                                            <div class='card-body'>
                                                <h5 class='card-title'>{{ $vacancies[$vacancy]->name }}</h5>
                                                <p class='card-text'>Faixa salarial:
                                                    R${{ $vacancies[$vacancy]->start_salary }}
                                                    até R${{ $vacancies[$vacancy]->end_salary }}</p>
                                            </div>
                                            <div class='card-footer'>
                                                <form action='{{ '/show/vacancy=' . $vacancies[$vacancy]->id }}'>
                                                    @csrf
                                                    <button type='submit' href='#'
                                                        class='float-left btn btn-primary'><i class="fa-solid fa-eye"></i></button>
                                                </form>

                                                <!-- botão admin -->
                                                @if (session('admin'))
                                                    <a href='/vacancy/candidates/show={{ $vacancies[$vacancy]->id }}'
                                                        class='float-right btn btn-warning'><i
                                                            class='fa-solid fa-gear'></i></a>
                                                @endif

                                                <!-- botão company -->
                                                <?php
                                                try {
                                                    $company = Vacancy::where('company_id', session('company')->id)
                                                        ->where('id', $vacancies[$vacancy]->id)
                                                        ->first();
                                                } catch (Exception $e) {
                                                }
                                                ?>
                                                @if (session('company') && !empty($company))
                                                    <a href='/company/vacancy/candidates/show={{ $vacancies[$vacancy]->id }}'
                                                        class='float-right btn btn-warning'><i
                                                            class='fa-solid fa-gear'></i></a>
                                                @endif

                                                <!-- botão tutor -->
                                                <?php
                                                try {
                                                    $companiesTutor = (new CompanyController())->getAllCompaniesOfTutorAuth();
                                                    $companyId = [];
                                                    foreach ($companiesTutor as $companyTutor) {
                                                        $companyId[] = $companyTutor->id;
                                                    }
                                                    $tutor = Vacancy::whereIn('company_id', $companyId)
                                                        ->where('id', $vacancies[$vacancy]->id)
                                                        ->first();
                                                } catch (Exception $e) {
                                                }
                                                ?>
                                                @if (session('tutor') && !empty($tutor))
                                                    <a href='/tutor/vacancy/candidates/show={{ $vacancies[$vacancy]->id }}'
                                                        class='float-right btn btn-warning'><i
                                                            class='fa-solid fa-gear'></i></a>
                                                @endif

                                                <!-- botão client -->
                                                @if (session('client'))
                                                    <?php $candidate = Candidate::where('client_id', session('client')->id)
                                                        ->where('vacancy_id', $vacancies[$vacancy]->id)
                                                        ->first(); ?>
                                                    @if (!empty($candidate))
                                                        <a href='/client/vacancy/candidates/show={{ $vacancies[$vacancy]->id }}'
                                                            class='float-right btn btn-warning' type='submit'><i
                                                                class='fa-solid fa-gear'></i></a>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $count++;
                                    $vacancy++;
                                    $qtdVacancies--; ?>
                                @else
                                    <?php $count++; ?>
                                    <div class='col'>
                                    </div>
                                @endif
                            @endwhile
                            <?php $count = 0; ?>
                        </div>
                        </br>
                    @endwhile
                    <?php $vacancy = 0; ?>
                @endif
            </div>
            <!-- Todas as vagas -->

        </div>

        <!-- Formations -->
        <div class='tab-pane fade' id='ex3-pills-2' role='tabpanel' aria-labelledby='ex3-tab-2'>

            <!-- Formações recomendadas com base nos seus interesses. -->
            @if ($interestClientFormations)
                <div>
                    <div style='margin: 2%' class='text-div-center'>
                        <h4>Formações de seu interesse</h4>
                    </div>
                    <div>
                        @if ($qtdinterestClientFormations == 0)
                            <div class='col'>
                                <div class='card h-100'>
                                    <div class='card-body'>
                                        <h5 class='card-title'>Nenhuma recomendação nova encontrada</h5>
                                    </div>
                                </div>
                            </div>
                        @else
                            @while ($qtdinterestClientFormations != 0)
                                <div class='row row-cols-1 row-cols-md-3 g-4'>
                                    @while ($count != 3)
                                        @if ($qtdinterestClientFormations != 0)
                                            <div class='col'>
                                                <div class='card h-100'>
                                                    @if (!empty($interestClientFormations[$formation]->photo))
                                                        <img src='/img/formations/{{ $interestClientFormations[$formation]->photo }}'
                                                            class='card-img-top' alt='' width=300 height=300>
                                                    @else
                                                        <img src='/img/semimagem.png' class='card-img-top' alt=''
                                                            width=300 height=300>
                                                    @endif
                                                    <div class='card-body'>
                                                        <h5 class='card-title'>
                                                            {{ $interestClientFormations[$formation]->name }}</h5>
                                                        <p class='card-text'>Data:
                                                            {{ $interestClientFormations[$formation]->start_date->format('d/m/Y') }}
                                                            até
                                                            {{ $interestClientFormations[$formation]->end_date->format('d/m/Y') }}
                                                        </p>
                                                    </div>
                                                    <div class='card-footer'>
                                                        <form
                                                            action='{{ '/show/formation=' . $interestClientFormations[$formation]->id }}'>
                                                            @csrf
                                                            <button type='submit' href='#'
                                                                class='float-left btn btn-primary'><i class="fa-solid fa-eye"></i></button>
                                                        </form>

                                                        <!-- botão admin -->
                                                        @if (session('admin'))
                                                            <form
                                                                action='/formation/edit={{ $interestClientFormations[$formation]->id }}'>
                                                                <button class='float-right btn btn-warning edit'
                                                                    type='submit'><i class="fa-solid fa-pen-to-square"></i></button>
                                                            </form>
                                                        @endif

                                                        <!-- botão company -->
                                                        @if (session('company'))
                                                            <?php
                                                            $company = CompanyFormation::where('company_id', session('company')->id)
                                                                ->where('formation_id', $interestClientFormations[$formation]->id)
                                                                ->first();
                                                            ?>
                                                            @if (!empty($company))
                                                                <form
                                                                    action='/company/formation/edit={{ $interestClientFormations[$formation]->id }}'>
                                                                    <button class='float-right btn btn-warning edit'
                                                                        type='submit'><i class="fa-solid fa-pen-to-square"></i></button>
                                                                </form>
                                                            @endif
                                                        @endif

                                                        <!-- botão tutor -->
                                                        @if (session('tutor'))
                                                            <?php
                                                            $tutor = FormationTutor::where('tutor_id', session('tutor')->id)
                                                                ->where('formation_id', $interestClientFormations[$formation]->id)
                                                                ->first();
                                                            ?>
                                                            @if (!empty($tutor))
                                                                <form
                                                                    action='/tutor/formation/edit={{ $interestClientFormations[$formation]->id }}'>
                                                                    <button class='float-right btn btn-warning edit'
                                                                        type='submit'><i class="fa-solid fa-pen-to-square"></i></button>
                                                                </form>
                                                            @endif
                                                        @endif

                                                        <!-- botão client -->
                                                        @if (session('client'))
                                                            <?php
                                                            $clientNotFinish = ClientFormation::where('client_id', session('client')->id)
                                                                ->where('formation_id', $interestClientFormations[$formation]->id)
                                                                ->where('finish', 0)
                                                                ->first();
                                                            $clientFinish = ClientFormation::where('client_id', session('client')->id)
                                                                ->where('formation_id', $interestClientFormations[$formation]->id)
                                                                ->where('finish', 1)
                                                                ->first();
                                                            ?>
                                                            @if (!empty($clientNotFinish))
                                                                <form
                                                                    action='/client/formation/show={{ $interestClientFormations[$formation]->id }}'>
                                                                    <button class='float-right btn btn-warning edit'
                                                                        type='submit'>Inscrito</button>
                                                                </form>
                                                            @elseif(!empty($clientFinish))
                                                                <form action='/client/formation/history'>
                                                                    <button class='float-right btn btn-success edit'
                                                                        type='submit'>Concluido</button>
                                                                </form>
                                                            @endif
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            $count++;
                                            $formation++;
                                            $qtdinterestClientFormations--; ?>
                                        @else
                                            <?php
                                            $count++; ?>
                                            <div class='col'>
                                            </div>
                                        @endif
                                    @endwhile
                                    <?php $count = 0; ?>
                                </div>
                                </br>
                            @endwhile
                            <?php $formation = 0; ?>
                        @endif
                    </div>
                </div>
            @endif
            <!-- Formações recomendadas com base nos seus interesses. -->

            <!-- Formações recomendadas com base nas suas habilidades. -->
            @if ($skillClientFormations)
                <div>
                    <div style='margin: 2%' class='text-div-center'>
                        <h4>Formações para sua habilidade</h4>
                    </div>
                    <div>
                        @if ($qtdskillClientFormations == 0)
                            <div class='col'>
                                <div class='card h-100'>
                                    <div class='card-body'>
                                        <h5 class='card-title'>Nenhuma recomendação nova encontrada</h5>
                                    </div>
                                </div>
                            </div>
                        @else
                            @while ($qtdskillClientFormations != 0)
                                <div class='row row-cols-1 row-cols-md-3 g-4'>
                                    @while ($count != 3)
                                        @if ($qtdskillClientFormations != 0)
                                            <div class='col'>
                                                <div class='card h-100'>
                                                    @if (!empty($skillClientFormations[$formation]->photo))
                                                        <img src='/img/formations/{{ $skillClientFormations[$formation]->photo }}'
                                                            class='card-img-top' alt='' width=300 height=300>
                                                    @else
                                                        <img src='/img/semimagem.png' class='card-img-top' alt=''
                                                            width=300 height=300>
                                                    @endif
                                                    <div class='card-body'>
                                                        <h5 class='card-title'>
                                                            {{ $skillClientFormations[$formation]->name }}</h5>
                                                        <p class='card-text'>Data:
                                                            {{ $skillClientFormations[$formation]->start_date->format('d/m/Y') }}
                                                            até
                                                            {{ $skillClientFormations[$formation]->end_date->format('d/m/Y') }}
                                                        </p>
                                                    </div>
                                                    <div class='card-footer'>
                                                        <form
                                                            action='{{ '/show/formation=' . $skillClientFormations[$formation]->id }}'>
                                                            @csrf
                                                            <button type='submit' href='#'
                                                                class='float-left btn btn-primary'><i class="fa-solid fa-eye"></i></button>
                                                        </form>

                                                        <!-- botão admin -->
                                                        @if (session('admin'))
                                                            <form
                                                                action='/formation/edit={{ $skillClientFormations[$formation]->id }}'>
                                                                <button class='float-right btn btn-warning edit'
                                                                    type='submit'><i class="fa-solid fa-pen-to-square"></i></button>
                                                            </form>
                                                        @endif

                                                        <!-- botão company -->
                                                        @if (session('company'))
                                                            <?php
                                                            $company = CompanyFormation::where('company_id', session('company')->id)
                                                                ->where('formation_id', $skillClientFormations[$formation]->id)
                                                                ->first();
                                                            ?>
                                                            @if (!empty($company))
                                                                <form
                                                                    action='/company/formation/edit={{ $skillClientFormations[$formation]->id }}'>
                                                                    <button class='float-right btn btn-warning edit'
                                                                        type='submit'><i class="fa-solid fa-pen-to-square"></i></button>
                                                                </form>
                                                            @endif
                                                        @endif

                                                        <!-- botão tutor -->
                                                        @if (session('tutor'))
                                                            <?php
                                                            $tutor = FormationTutor::where('tutor_id', session('tutor')->id)
                                                                ->where('formation_id', $skillClientFormations[$formation]->id)
                                                                ->first();
                                                            ?>
                                                            @if (!empty($tutor))
                                                                <form
                                                                    action='/tutor/formation/edit={{ $skillClientFormations[$formation]->id }}'>
                                                                    <button class='float-right btn btn-warning edit'
                                                                        type='submit'><i class="fa-solid fa-pen-to-square"></i></button>
                                                                </form>
                                                            @endif
                                                        @endif

                                                        <!-- botão client -->
                                                        @if (session('client'))
                                                            <?php
                                                            $clientNotFinish = ClientFormation::where('client_id', session('client')->id)
                                                                ->where('formation_id', $skillClientFormations[$formation]->id)
                                                                ->where('finish', 0)
                                                                ->first();
                                                            $clientFinish = ClientFormation::where('client_id', session('client')->id)
                                                                ->where('formation_id', $skillClientFormations[$formation]->id)
                                                                ->where('finish', 1)
                                                                ->first();
                                                            ?>
                                                            @if (!empty($clientNotFinish))
                                                                <form
                                                                    action='/client/formation/show={{ $skillClientFormations[$formation]->id }}'>
                                                                    <button class='float-right btn btn-warning edit'
                                                                        type='submit'>Inscrito</button>
                                                                </form>
                                                            @elseif(!empty($clientFinish))
                                                                <form action='/client/formation/history'>
                                                                    <button class='float-right btn btn-success edit'
                                                                        type='submit'>Concluido</button>
                                                                </form>
                                                            @endif
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            $count++;
                                            $formation++;
                                            $qtdskillClientFormations--; ?>
                                        @else
                                            <?php
                                            $count++; ?>
                                            <div class='col'>
                                            </div>
                                        @endif
                                    @endwhile
                                    <?php $count = 0; ?>
                                </div>
                                </br>
                            @endwhile
                            <?php $formation = 0; ?>
                        @endif
                    </div>
                </div>
            @endif
            <!-- Formações recomendadas com base nas suas habilidades. -->

            <!-- Todas as formações -->
            <div>
                <div style='margin: 2%' class='text-div-center'>
                    <h4>Todas as formações</h4>
                </div>
                <div>
                    @if ($qtdFormations == 0)
                        <div class='col'>
                            <div class='card h-100'>
                                <div class='card-body'>
                                    <h5 class='card-title'>Nenhuma formação encontrada</h5>
                                </div>
                            </div>
                        </div>
                    @else
                        @while ($qtdFormations != 0)
                            <div class='row row-cols-1 row-cols-md-3 g-4'>
                                @while ($count != 3)
                                    @if ($qtdFormations != 0)
                                        <div class='col'>
                                            <div class='card h-100'>
                                                @if (!empty($formations[$formation]->photo))
                                                    <img src='/img/formations/{{ $formations[$formation]->photo }}'
                                                        class='card-img-top' alt='' width=300 height=300>
                                                @else
                                                    <img src='/img/semimagem.png' class='card-img-top' alt=''
                                                        width=300 height=300>
                                                @endif
                                                <div class='card-body'>
                                                    <h5 class='card-title'>{{ $formations[$formation]->name }}</h5>
                                                    <p class='card-text'>Data:
                                                        {{ $formations[$formation]->start_date->format('d/m/Y') }}
                                                        até {{ $formations[$formation]->end_date->format('d/m/Y') }}</p>
                                                </div>
                                                <div class='card-footer'>
                                                    <form action='{{ '/show/formation=' . $formations[$formation]->id }}'>
                                                        @csrf
                                                        <button type='submit' href='#'
                                                            class='float-left btn btn-primary'><i class="fa-solid fa-eye"></i></button>
                                                    </form>

                                                    <!-- botão admin -->
                                                    @if (session('admin'))
                                                        <form action='/formation/edit={{ $formations[$formation]->id }}'>
                                                            <button class='float-right btn btn-warning edit'
                                                                type='submit'><i class="fa-solid fa-pen-to-square"></i></button>
                                                        </form>
                                                    @endif

                                                    <!-- botão company -->
                                                    @if (session('company'))
                                                        <?php
                                                        $company = CompanyFormation::where('company_id', session('company')->id)
                                                            ->where('formation_id', $formations[$formation]->id)
                                                            ->first();
                                                        ?>
                                                        @if (!empty($company))
                                                            <form
                                                                action='/company/formation/edit={{ $formations[$formation]->id }}'>
                                                                <button class='float-right btn btn-warning edit'
                                                                    type='submit'><i class="fa-solid fa-pen-to-square"></i></button>
                                                            </form>
                                                        @endif
                                                    @endif

                                                    <!-- botão tutor -->
                                                    @if (session('tutor'))
                                                        <?php
                                                        $tutor = FormationTutor::where('tutor_id', session('tutor')->id)
                                                            ->where('formation_id', $formations[$formation]->id)
                                                            ->first();
                                                        ?>
                                                        @if (!empty($tutor))
                                                            <form
                                                                action='/tutor/formation/edit={{ $formations[$formation]->id }}'>
                                                                <button class='float-right btn btn-warning edit'
                                                                    type='submit'><i class="fa-solid fa-pen-to-square"></i></button>
                                                            </form>
                                                        @endif
                                                    @endif

                                                    <!-- botão client -->
                                                    @if (session('client'))
                                                        <?php
                                                        $clientNotFinish = ClientFormation::where('client_id', session('client')->id)
                                                            ->where('formation_id', $formations[$formation]->id)
                                                            ->where('finish', 0)
                                                            ->first();
                                                        $clientFinish = ClientFormation::where('client_id', session('client')->id)
                                                            ->where('formation_id', $formations[$formation]->id)
                                                            ->where('finish', 1)
                                                            ->first();
                                                        ?>
                                                        @if (!empty($clientNotFinish))
                                                            <form
                                                                action='/client/formation/show={{ $formations[$formation]->id }}'>
                                                                <button class='float-right btn btn-warning edit'
                                                                    type='submit'>Inscrito</button>
                                                            </form>
                                                        @elseif(!empty($clientFinish))
                                                            <form action='/client/formation/history'>
                                                                <button class='float-right btn btn-success edit'
                                                                    type='submit'>Concluido</button>
                                                            </form>
                                                        @endif
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $count++;
                                        $formation++;
                                        $qtdFormations--; ?>
                                    @else
                                        <?php
                                        $count++; ?>
                                        <div class='col'>
                                        </div>
                                    @endif
                                @endwhile
                                <?php $count = 0; ?>
                            </div>
                            </br>
                        @endwhile
                        <?php $formation = 0; ?>
                    @endif
                </div>
            </div>
            <!-- Todas as formações -->

        </div>
    </div>
    <!-- Pills content -->
@endsection
