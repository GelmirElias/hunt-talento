<?php

use App\Http\Controllers\CompanyController;
use App\Models\Candidate;
use App\Models\Formation;
use App\Models\Skill;
use App\Models\User;
use App\Models\Vacancy;

?>

@extends('layout.app')

@section('title', 'Detalhes da Vaga')

@section('body')
    <div class="col">
        <a type="submit" class="float-left btn btn-primary" href="/"><i class="fa-solid fa-backward"></i></a>

        <!-- botão admin -->
        @if (session('admin'))
            <a href="/vacancy/candidates/show={{ $vacancy->id }}" class="float-left btn btn-warning"><i
                    class="fa-solid fa-eye"></i></a>
        @endif

        <!-- botão cliente -->
        @if (session('client'))
            <?php $candidate = Candidate::where('client_id', session('client')->id)
                ->where('vacancy_id', $vacancy->id)
                ->first(); ?>

            @if ($statusVacancy->id == 2)
                @if (empty($candidate))
                    <form action="/candidate/store/vacancy={{ $vacancy->id }}">
                        <button class="float-left btn btn-success edit" type="submit">Inscrever-se</button>
                    </form>
                @else
                    <a class="float-left btn btn-success edit"
                        href="/client/vacancy/candidates/show={{ $vacancy->id }}">Você está inscrito</a>
                @endif
            @endif

            @if ($statusVacancy->id < 2)
                <button class="float-left btn btn-warning">A vaga ainda não está aceitando inscrições</button>
            @elseif($statusVacancy->id > 2)
                <button class="float-left btn btn-warning">A vaga não aceita novas inscrições</button>
            @endif

        @endif


        <!-- botão company -->
        <?php try {
            $company = Vacancy::where('company_id', session('company')->id)
                ->where('id', $vacancy->id)
                ->first();
        } catch (Exception $e) {
        } ?>
        @if (session('company') && !empty($company))
            <a href="/company/vacancy/candidates/show={{ $vacancy->id }}" class="float-left btn btn-warning"><i
                    class="fa-solid fa-eye"></i></a>
        @endif

        <!-- botão tutor -->
        <?php
        try {
            $companiesTutor = (new CompanyController())->getAllCompaniesOfTutorAuth();
            $companyId = [];
            foreach ($companiesTutor as $companyTutor) {
                $companyId[] = $companyTutor->id;
            }
            $tutor = Vacancy::whereIn('company_id', $companyId)
                ->where('id', $vacancies[$vacancy]->id)
                ->first();
        } catch (Exception $e) {
        }
        ?>
        @if (session('tutor') && !empty($tutor))
            <a href="/tutor/vacancy/candidates/show={{ $vacancy->id }}" class="float-right btn btn-warning"><i
                    class="fa-solid fa-eye"></i></a>
        @endif
    </div>

    </br></br>

    <div class="row">

        <div class="col-md-3">
            <div class="project-info-box mt-0">
                <h4>{{ $vacancy->name }}</h4>
                <p class="mb-0">{{ $vacancy->description }}</p>
            </div><!-- / project-info-box -->

            <div class="project-info-box">
                <p><b>Salário entre:</b> R${{ $vacancy->start_salary }} - R${{ $vacancy->end_salary }}</p>
                <p><b>Vaga(s) ofertada(s):</b> {{ $vacancy->quantity }}</p>
                <p><b>Empresa responsável:</b> {{ $user->name }}</p>
                <p><b>Vaga em:</b> {{ $statusVacancy->name }}</p>
                <p><b>Criada em:</b> {{ $vacancy->created_at->format('d/m/Y H:i') }}</p>
                <p><b>Atualiza em:</b> {{ $vacancy->updated_at->format('d/m/Y H:i') }}</p>
            </div><!-- / project-info-box -->

        </div><!-- / column -->

        <div class="col-md-3">

            <!-- vacancyFormationsRequired -->
            <div class="project-info-box mt-0">
                <h4>Formações obrigatórias:</h4>
                @if (!empty($vacancyFormationsRequired[0]))
                    @foreach ($vacancyFormationsRequired as $vacancyFormationRequired)
                        <?php $formation = Formation::where('id', $vacancyFormationRequired->formation_id)
                            ->where('active', true)
                            ->first(); ?>
                        <h7>- <a href="/show/formation={{ $formation->id }}" target="_blank"
                                rel="noopener noreferrer">{{ $formation->name }}</a> </h7></br>
                    @endforeach
                @else
                    A vaga não possui formação obrigatória.
                @endif
            </div>

            <!-- vacancyFormationsNotRequired -->
            <div class="project-info-box mt-0">
                <h4>Formações recomendadas:</h4>
                @if (!empty($vacancyFormationsNotRequired[0]))
                    @foreach ($vacancyFormationsNotRequired as $vacancyFormationNotRequired)
                        <?php $formation = Formation::where('id', $vacancyFormationNotRequired->formation_id)
                            ->where('active', true)
                            ->first(); ?>
                        <h7>- <a href="/show/formation={{ $formation->id }}" target="_blank"
                                rel="noopener noreferrer">{{ $formation->name }}</a> </h7></br>
                    @endforeach
                @else
                    A vaga não possui recomendação de formação.
                @endif
            </div>

            <!-- vacancySkills -->
            <div class="project-info-box mt-0">
                <h4>Habilidades relacionadas:</h4>
                @if (!empty($vacancySkills[0]))
                    @foreach ($vacancySkills as $vacancySkill)
                        <?php $skill = Skill::where('id', $vacancySkill->skill_id)
                            ->where('active', true)
                            ->first(); ?>
                        <h7>- {{ $skill->name }} </h7></br>
                    @endforeach
                @else
                    A vaga não possui talento informado.
                @endif
            </div>

        </div><!-- / column -->

        <div class="col-md-6">
            @if (!empty($vacancy->photo))
                <img src="/img/vacancies/{{ $vacancy->photo }}" alt="project-image" class="rounded"
                    height=600>
            @else
                <img src="/img/semimagem.png" alt="project-image" class="rounded" height=600>
            @endif
        </div><!-- / column -->

    </div>

@endsection
