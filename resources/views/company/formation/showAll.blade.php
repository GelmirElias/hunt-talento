<?php

use App\Models\Company;
use App\Models\CompanyTutor;
use App\Models\FormationType;
use App\Models\User;

?>

<!DOCTYPE html>
@extends('layout.app')

@section('title', "Gerenciar Formações")

@section('body')
<button type="submit" class="float-right btn btn-success" data-toggle="modal" data-target="#create"><i class="fa-solid fa-plus"></i></button>
</br></br>
<table id="datatable" class="table table-bordered table-hover display" border="1px">
    <thead>
        <tr>
            <td>Name</td>
            <td>Data Inicio</td>
            <td>Data Fim</td>
            <td>Tipo</td>
            <td>Visibilidade</td>
            <td>Editar</td>
            <td>Visualizar</td>
        </tr>
    </thead>

    <tbody>
        @foreach ($formations as $formation)
        <?php
        $formationType = FormationType::where('id', $formation->formation_types_id)->first();
        ?>
        @if($formation->active > -1)
        <tr>
            <td>{{$formation->name}}</td>
            <td>{{$formation->start_date->format('d/m/Y H:i')}}</td>
            <td>{{$formation->end_date->format('d/m/Y H:i')}}</td>
            <td>{{$formationType->name}}</td>
            <td>{{$formation->active == 1 ? "Ativo" : "Inativo"}}</td>

            <td>
                <div class="cellContainer">
                    <div style="margin: 5px">
                        <form action="/company/formation/edit={{$formation->id}}">
                            <button class="btn btn-warning edit" type="submit"><i class="fa-solid fa-pen-to-square"></i></button>
                        </form>
                    </div>
                </div>
            </td>
            <td>
                <div class="cellContainer">
                    <div style="margin: 5px">
                        <form action="/company/formation/show={{$formation->id}}">
                            @csrf
                            <button class="btn btn-primary" type="submit"><i class="fa-solid fa-eye"></i></button>
                        </form>
                    </div>
                </div>
            </td>
        </tr>
        @endif
        @endforeach
    </tbody>
</table>

<!-- MODAL EDITAR -->
@if(!empty($formationEdit))
<script>
    $(function() {
        $('#nameEdit').val('<?php echo $formationEdit->name; ?>');
        $('#startDateEdit').val('<?php echo $formationEdit->start_date; ?>');
        $('#endDateEdit').val('<?php echo $formationEdit->end_date; ?>');

        //preenchendo o multi select com as empresas da formação
        var tutorsIdEdit = [];
        <?php foreach ($tutorsIdEdit as $tutorIdEdit) { ?>
            tutorsIdEdit.push(<?php echo $tutorIdEdit; ?>);
        <?php } ?>
        $('.tutorIdEdit').selectpicker('val', tutorsIdEdit);

        //preenchendo o multi select com os talentos da formação
        var formationSkillsEdit = [];
        <?php foreach ($formationSkillsEdit as $vacancySkillEdit) { ?>
            formationSkillsEdit.push(<?php echo $vacancySkillEdit->skill_id; ?>);
        <?php } ?>
        $('.formationSkillsEdit').selectpicker('val', formationSkillsEdit);

        $('.planIdEdit').selectpicker('val', '<?php echo $formationEdit->plan_id; ?>');
        $('.formationTypeIdEdit').selectpicker('val', '<?php echo  $formationEdit->formation_types_id; ?>');
        $('.activeEdit').selectpicker('val', '<?php echo  $formationEdit->active; ?>');

        //markdown do descriptionEdit
        $('#descriptionEdit').each(function() {
            var simplemde = new SimpleMDE({
                element: this,
                spellChecker: false,
                initialValue: '<?php echo $formationEdit->description; ?>',
            });
            simplemde.render();
            $("#edit").focus(function() {
                simplemde.codemirror.refresh();
            });
        });

        $('#edit').modal('show');
    });
</script>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
                <form action="/company/formation">
                    <button class="close" type="submit" aria-hidden="true">&times;</button>
                </form>
            </div>
            <div class="modal-body">
                <form id="formEditFormation" action="{{'/company/formation/update='. $formationEdit->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @if(!empty($formationEdit->photo))
                    <div class="form-group">
                        <label for="photoShow">Foto:</label>
                        <img id="photoShow" src="/img/formations/{{$formationEdit->photo}}" alt="photo formation" width=240 height=240>
                    </div>
                    @endif
                    <div class="form-group">
                        <label for="name">Nome:</label>
                        <input required type="text" class="form-control" name="nameEdit" id="nameEdit" placeholder="Nome">
                    </div>
                    <div class="form-group">
                        <label for="description">Descrição:</label>
                        <textarea rows="4" cols="50" type="text" class="form-control" name="descriptionEdit" id="descriptionEdit" placeholder="Descrição"></textarea>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="startDateEdit">Data inicio:</label>
                            <input type="datetime-local" class="form-control" name="startDateEdit" id="startDateEdit">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="endDateEdit">Data fim:</label>
                            <input type="datetime-local" class="form-control" name="endDateEdit" id="endDateEdit">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="tutorIdEdit[]">Tutor:</label>
                            <select class="selectpicker tutorIdEdit" name="tutorIdEdit[]" multiple title="Selecione o(s) tutor(s)" data-width="100%" data-live-search="true">
                                @foreach($tutors as $tutor)
                                <?php $userTutor = User::where('id', $tutor->user_id)->first(); ?>
                                <?php $companyTutor = CompanyTutor::where('tutor_id', $tutor->id)->first(); ?>
                                <?php $company = Company::where('id', $companyTutor->company_id)->first(); ?>
                                <?php $userCompany = User::where('id', $company->user_id)->first(); ?>
                                <option value="{{$tutor->id}}" data-subtext="{{$userCompany->name}}">{{$userTutor->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="planIdEdit">Plano:</label>
                            <select required class="selectpicker planIdEdit" name="planIdEdit" title="Selecione o plano" data-width="100%" data-live-search="true">
                                @foreach($plans as $plan)
                                <option value="{{$plan->id}}" data-subtext="{{$plan->description}}">{{$plan->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="formationTypeIdEdit">Tipo:</label>
                            <select required class="selectpicker formationTypeIdEdit" name="formationTypeIdEdit" title="Selecione o tipo" data-width="100%" data-live-search="true">
                                @foreach($formationTypes as $formationType)
                                <option value="{{$formationType->id}}">{{$formationType->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="activeEdit">Status:</label>
                            <select required name="activeEdit" class="selectpicker activeEdit" title="Selecione o status" data-width="100%" data-live-search="true">
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="formationSkillsEdit[]">Habilidades relacionadas:</label>
                        <select class="selectpicker formationSkillsEdit" name="formationSkillsEdit[]" multiple title="Selecione a(s) habilidade(s)" data-width="100%" data-live-search="true">
                            @foreach($skills as $skill)
                            <option value="{{$skill->id}}">{{$skill->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="photo">Foto:</label>
                        <input type="file" class="form-control-file" name="photoEdit" id="photoEdit">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="formEditFormation" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
                <form id="formDestroyFormation" action="/company/formation/destroy={{$formationEdit->id}}">
                    <button form="formDestroyFormation" type="submit" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
@endif

<!-- MODAL CRIAR -->
<script>
    $(function() {
        //markdown do descriptionEdit
        $('#description').each(function() {
            var simplemde = new SimpleMDE({
                element: this,
                spellChecker: false
            });
            simplemde.render();
            $("#create").focus(function() {
                simplemde.codemirror.refresh();
            });
        });
    });
</script>

<div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Criar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/company/formation" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nome*:</label>
                        <input required type="text" class="form-control" name="name" placeholder="Nome">
                    </div>
                    <div class="form-group">
                        <label for="description">Descrição:</label>
                        <textarea rows="4" cols="50" type="text" class="form-control" id="description" name="description" placeholder="Descrição"></textarea>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="startDate">Data inicio*:</label>
                            <input required type="datetime-local" class="form-control" name="startDate" id="startDate">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="endDate">Data fim*:</label>
                            <input required type="datetime-local" class="form-control" name="endDate" id="endDate">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="tutorId[]">Tutor:</label>
                            <select class="selectpicker" name="tutorId[]" multiple title="Selecione o(s) tutor(s)" data-width="100%" data-live-search="true">
                                @foreach($tutors as $tutor)
                                <?php $userTutor = User::where('id', $tutor->user_id)->first(); ?>
                                <?php $companyTutor = CompanyTutor::where('tutor_id', $tutor->id)->first(); ?>
                                <?php $company = Company::where('id', $companyTutor->company_id)->first(); ?>
                                <?php $userCompany = User::where('id', $company->user_id)->first(); ?>
                                <option value="{{$tutor->id}}" data-subtext="{{$userCompany->name}}">{{$userTutor->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="planId">Plano*:</label>
                            <select required class="selectpicker" name="planId" title="Selecione o plano" data-width="100%" data-live-search="true">
                                @foreach($plans as $plan)
                                <option value="{{$plan->id}}" data-subtext="{{$plan->description}}">{{$plan->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="formationTypeId">Tipo*:</label>
                            <select required class="selectpicker" name="formationTypeId" title="Selecione o tipo" data-width="100%" data-live-search="true">
                                @foreach($formationTypes as $formationType)
                                <option value="{{$formationType->id}}">{{$formationType->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="active">Visibilidade*:</label>
                            <select required name="active" class="selectpicker" title="Selecione a visibilidade" data-width="100%">
                                <option selected value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="skillId">Habilidades relacionadas:</label>
                        <select class="selectpicker" name="skillId[]" multiple title="Selecione a(s) habilidade(s)" data-width="100%" data-live-search="true">
                            @foreach($skills as $skill)
                            <option value="{{$skill->id}}">{{$skill->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="photo">Foto:</label>
                        <input type="file" class="form-control-file" name="photo">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i></button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
