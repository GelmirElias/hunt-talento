----------------------------------Configuração inicial (única vez)-----------------------

-> baixar e instalar git
https://git-scm.com/downloads

-> clonar projeto no bitbucket
https://bitbucket.org/jhoseju/softins.hunting/src/Masterd/

-> baixar e instalar xampp com php nessa versão (7.4.25 / PHP 7.4.25):
https://www.apachefriends.org/xampp-files/7.4.25/xampp-windows-x64-7.4.25-0-VC15-installer.exe

-> baixar e instalar composer
https://getcomposer.org/download/

-> baixar e instalar python <= 3.10.3
https://www.python.org/downloads/

-> abrir terminar, entrar na pasta raiz do projeto clonado, e digitar "composer install"

-> na pasta raiz do projeto copiar e colar o arquivo ".env.example"

-> remover do nome o ".example" do arquivo copiado

-> abrir o arquivo .env, alterar na linha 30, pelo serviço de email usado

-> abrir o xampp control panel, e iniciar o apache e mysql

-> no xampp control panel, clicar no botão admin no mysql, e criar um banco de dados com o nome desejado

-> abrir o arquivo .env, alterar na linha 13 (DB_DATABASE), pelo nome que criou no banco

-> abrir terminar, entrar na pasta raiz do projeto clonado, e digitar "php artisan migrate:fresh --seed"

-> abrir terminar, entrar na pasta raiz do projeto clonado, e digitar "php artisan key:generate"

----------------------------------Iniciar projeto (toda vez que for usar)-----------------------
-> abrir o xampp control panel, e iniciar o mysql

-> abrir terminar, entrar na pasta raiz do projeto clonado, e digitar "php artisan serve --host 0.0.0.0 --port 80"

-> ip notebook de teste: 192.168.15.157

-> clicar no link gerado

---------------------------------------------------

php artisan make:model CompanyVacancy -f -s -m -c -r
php artisan migrate:fresh --seed

php artisan config:clear
php artisan cache:clear
composer dump-autoload
